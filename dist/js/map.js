new Morris.Line({
    element: 'bar-chart-user',
    data: [
        { month: 'Jan', a: 20 },
        { month: 'Feb', a: 10 },
        { month: 'Mar', a: 5 },
        { month: 'April', a: 5 },
        { month: 'May', a: 80 },
        { month: 'June', a: 90 },
        { month: 'July', a: 100 },
        { month: 'Aug', a: 115 },
        { month: 'Sep', a: 100 },
        { month: 'Oct', a: 115 },
        { month: 'Nov', a: 100 },
        { month: 'Dec', a: 115 }
    ],
    // The name of the data record attribute that contains x-values.
    xkey: 'month',
    parseTime: false,
    // A list of names of data record attributes that contain y-values.
    ykeys: ['a'],
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Register'],
    lineColors: ['#373651', '#E65A26']
});

new Morris.Line({
    element: 'bar-chart-revenue',
    data: [
        { month: 'Jan', a: 20 },
        { month: 'Feb', a: 10 },
        { month: 'Mar', a: 5 },
        { month: 'April', a: 5 },
        { month: 'May', a: 80 },
        { month: 'June', a: 90 },
        { month: 'July', a: 100 },
        { month: 'Aug ', a: 115 },
        { month: 'Sep', a: 100 },
        { month: 'Oct', a: 115 },
        { month: 'Nov', a: 100 },
        { month: 'Dec', a: 115 }
    ],
    // The name of the data record attribute that contains x-values.
    xkey: 'month',
    parseTime: false,
    // A list of names of data record attributes that contain y-values.
    ykeys: ['a'],
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Course Sale'],
    lineColors: ['#373651', '#E65A26']
});


Morris.Bar({
    element: 'stacked',
    data: [
        { month: 'Jan', a: 50, b: 90, c: 20, d: 30 },
        { month: 'Feb', a: 65, b: 75, c: 120, d: 90 },
        { month: 'Mar', a: 50, b: 50, c: 100, d: 30 },
        { month: 'April', a: 75, b: 60, c: 20, d: 120 },
        { month: 'May', a: 80, b: 65, c: 20, d: 75 },
        { month: 'June', a: 90, b: 70, c: 90, d: 30 },
        { month: 'July', a: 100, b: 75, c: 20, d: 120 },
        { month: 'Aug', a: 115, b: 75, c: 20, d: 30 },
        { month: 'Sep', a: 120, b: 85, c: 75, d: 30 },
        { month: 'Oct', a: 145, b: 85, c: 20, d: 75 },
        { month: 'Nov', a: 160, b: 95, c: 75, d: 30 },
        { month: 'Dec', a: 160, b: 95, c: 20, d: 30 }
    ],
    // The name of the data record attribute that contains x-values.
    xkey: 'month',
    ykeys: ['a', 'b', 'c', 'd'],
    labels: ['course1', 'course2', 'course3', 'course4', 'course5'],
    fillOpacity: 0.6,
    hideHover: 'auto',
    behaveLikeLine: true,
    resize: true,
    stacked: true,
    barColors: ['#007bff', '#6c757d', '#28a745', '#dc3545'],
    pointFillColors: ['#ffffff'],
    pointStrokeColors: ['black'],
});
// Morris.Bar({
//     element: 'stacked',
//     data: [{
//             label: 'Risk Level',
//             low: 67.8,
//             moderate: 20.7,
//             high: 11.4,
//         },
//         {
//             label: 'Risk Level',
//             low: 67.8,
//             moderate: 20.7,
//             high: 11.4,
//         }
//     ],
//     xkey: 'label',
//     ykeys: ['low', 'moderate', 'high'],
//     labels: ['Low', 'Moderate', 'High'],
//     barColors: ['#D6E9C6', '#FAEBCC', '#EBCCD1'],
//     stacked: true,
//     resize: true,
// });