<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'MainController/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['css'] = 'MainController/css';
$route['js'] = 'MainController/js';
$route['index'] = 'MainController/index';
$route['logout'] = 'MainController/logout';

$route['forgot_password'] = 'MainController/forgot_password';
$route['forgot_Password_dashboard'] = 'MainController/forgot_Password_dashboard';

$route['home'] = 'MainController/home';
$route['footer'] = 'MainController/footer';
$route['header'] = 'MainController/header';
$route['dashboard'] = 'MainController/dashboard';
$route['profile'] = 'MainController/profile';
$route['profile_update_dash'] = 'MainController/profile_update_dash';
$route['update_password'] = 'MainController/update_password';

// $route['sidemenu'] = 'MainController/sidemenu';

//Setting

// category
$route['add-category'] = 'MainController/add_category';
$route['add-category/(:any)'] = 'MainController/add_category';
$route['delete_category/(:any)'] = 'MainController/delete_category/$1';
$route['edit_category/(:any)'] = 'MainController/edit_category/$1';

// board
$route['add_board'] = 'MainController/add_board';
$route['add_board/(:any)'] = 'MainController/add_board';
$route['delete_board/(:any)'] = 'MainController/delete_board/$1';
$route['edit_board/(:any)'] = 'MainController/edit_board/$1';

//  standard
$route['add_standard'] = 'MainController/add_standard';
$route['add_standard/(:any)'] = 'MainController/add_standard';
$route['delete_standard/(:any)'] = 'MainController/delete_standard/$1';
$route['edit_standard/(:any)'] = 'MainController/edit_standard/$1';

// subject
$route['add_subject'] = 'MainController/add_subject';
$route['add_subject/(:any)'] = 'MainController/add_subject';
$route['delete_subject/(:any)'] = 'MainController/delete_subject/$1';
$route['edit_subject/(:any)'] = 'MainController/edit_subject/$1';

// subcategory
$route['add_subcategory'] = 'MainController/add_subcategory';

//  difficulty_level
$route['add_difficulty_level'] = 'MainController/add_difficulty_level';
$route['add_difficulty_level/(:any)'] = 'MainController/add_difficulty_level';
$route['delete_difficulty_level/(:any)'] = 'MainController/delete_difficulty_level/$1';
$route['edit_difficulty_level/(:any)'] = 'MainController/edit_difficulty_level/$1';

// language
$route['add_language'] = 'MainController/add_language';
$route['add_language/(:any)'] = 'MainController/add_language';
$route['delete_language/(:any)'] = 'MainController/delete_language/$1';
$route['edit_language/(:any)'] = 'MainController/edit_language/$1';

$route['aside'] = 'MainController/aside';

//Test Managment
$route['aside_test'] = 'MainController/aside_test';

// add test
$route['add_test'] = 'MainController/add_test';
$route['add_test/(:any)/(:any)'] = 'MainController/add_test/$1/$1';
$route['manage_test'] = 'MainController/manage_test';
$route['manage_test/(:any)'] = 'MainController/manage_test';
$route['delete_test/(:any)'] = 'MainController/delete_test/$1';

// manage_test , test series list
$route['manage_test_list/(:any)'] = 'MainController/manage_test_list/$1';
$route['manage_test_list/(:any)/(:any)'] = 'MainController/manage_test_list/$1/$1';
 
//edit
$route['edit_test/(:any)'] = 'MainController/edit_test/$1';
$route['update_test_info/(:any)'] = 'MainController/update_test_info/$1';
$route['aside_update_test/(:any)'] = 'MainController/aside_update_test/$1';
// result
$route['result/(:any)/(:any)'] = 'MainController/result/$1/$1';

$route['evaluation'] = 'MainController/evaluation';

// import_question
$route['import_question'] = 'MainController/import_question';
$route['import_question/(:any)/(:any)'] = 'MainController/import_question';
$route['import_question/(:any)'] = 'MainController/import_question';
$route['import_question/(:any)'] = 'MainController/import_question/$1';

//add imported questions
$route['add_imported_questions'] = 'MainController/add_imported_questions';
$route['delete_questions_as_per_section/(:any)'] = 'MainController/delete_questions_as_per_section/$1';
$route['get_question_record_as_per_section'] = 'MainController/get_question_record_as_per_section';



// section
$route['create_section/(:any)'] = 'MainController/create_section/$1';
$route['create_section/(:any)/(:any)'] = 'MainController/create_section';
$route['delete_section/(:any)'] = 'MainController/delete_section/$1';
$route['edit_section/(:any)'] = 'MainController/edit_section/$1';

// manage_testSeries
$route['manage_testSeries'] = 'MainController/manage_testSeries';
$route['manage_testSeries/(:any)'] = 'MainController/manage_testSeries';
$route['delete_testSeries/(:any)'] = 'MainController/delete_testSeries/$1';
$route['edit_testSeries/(:any)'] = 'MainController/edit_testSeries/$1';



$route['test_profile_setting'] = 'MainController/test_profile_setting';

//Question Managment
$route['aside_question'] = 'MainController/aside_question';

// manage_question
$route['manage_question'] = 'MainController/manage_question';
$route['manage_question/(:any)'] = 'MainController/manage_question';
$route['delete_manage_question/(:any)'] = 'MainController/delete_manage_question/$1';
$route['edit_manage_question'] = 'MainController/edit_manage_question';

// upload_question
$route['upload_question'] = 'MainController/upload_question';

// add_questions
$route['add_questions'] = 'MainController/add_questions';

//Payment Managment
$route['payment_mgmt'] = 'MainController/payment_mgmt';

$route['today_revenue'] = 'MainController/today_revenue';
$route['today_revenue/(:any)'] = 'MainController/today_revenue';

$route['revenue_list'] = 'MainController/revenue_list';
$route['revenue_list/(:any)'] = 'MainController/revenue_list';

$route['current_month_revenue'] = 'MainController/current_month_revenue';
$route['current_month_revenue/(:any)'] = 'MainController/current_month_revenue';

$route['user_payment_info'] = 'MainController/user_payment_info';


//User MaSnagment
$route['user_mgmt'] = 'MainController/user_mgmt';
$route['user_mgmt/(:any)'] = 'MainController/user_mgmt';
$route['delete_user_mgmt/(:any)'] = 'MainController/delete_user_mgmt/$1';

// deactive_user
$route['deactive_user'] = 'MainController/deactive_user';
$route['delete_deactive_user/(:any)'] = 'MainController/delete_deactive_user/$1';

$route['user_profile'] = 'MainController/user_profile';
$route['user_profile/(:any)'] = 'MainController/user_profile';
$route['edit_user_profile/(:any)'] = 'MainController/edit_user_profile/$1';

// user test_series
$route['user_test_series'] = 'MainController/user_test_series';
$route['user_test_series/(:any)/(:any)'] = 'MainController/user_test_series/$1/$1';
$route['invoice'] = 'MainController/invoice';
$route['invoice/(:any)/(:any)'] = 'MainController/invoice/$1/$1';


//Website Managment
$route['banner_screen'] = 'MainController/banner_screen';
$route['banner_screen/(:any)'] = 'MainController/banner_screen';
$route['delete_banner_screen/(:any)'] = 'MainController/delete_banner_screen/$1';
$route['edit_banner_screen/(:any)'] = 'MainController/edit_banner_screen/$1';

$route['aside_web_mgmt'] = 'MainController/aside_web_mgmt';
$route['about_us'] = 'MainController/about_us';
$route['contact_us'] = 'MainController/contact_us';
$route['privacy_policy'] = 'MainController/privacy_policy';
$route['terms_condition'] = 'MainController/terms_condition';

// testimonial
$route['testimonial'] = 'MainController/testimonial';
$route['testimonial/(:any)'] = 'MainController/testimonial';
$route['delete_testimonial/(:any)'] = 'MainController/delete_testimonial/$1';
$route['edit_testimonial/(:any)'] = 'MainController/edit_testimonial/$1';

// student_resourse
$route['student_resourse'] = 'MainController/student_resourse';
$route['student_resourse/(:any)'] = 'MainController/student_resourse';
$route['delete_student_resourse/(:any)'] = 'MainController/delete_student_resourse/$1';
$route['edit_student_resourse/(:any)'] = 'MainController/edit_student_resourse/$1';

// aside_stud_resourse
$route['aside_stud_resourse'] = 'MainController/aside_stud_resourse';

// sub category
$route['stud_resourse_subcat'] = 'MainController/stud_resourse_subcat';
$route['stud_resourse_subcat/(:any)'] = 'MainController/stud_resourse_subcat';
$route['delete_stud_resourse_subcat/(:any)'] = 'MainController/delete_stud_resourse_subcat/$1';
$route['edit_stud_resourse_subcat/(:any)'] = 'MainController/edit_stud_resourse_subcat/$1';




/************************************  API's  ******************************************/

// http://localhost/true_learning_dashboard/home

$route['send_otp'] = 'Api_Controller/send_otp';
$route['verify_mobile_otp'] = 'Api_Controller/verify_mobile_otp';
$route['profile_update'] = 'Api_Controller/profile_update';
$route['get_profile'] = 'Api_Controller/get_profile';

$route['get_about_us'] = 'Api_Controller/get_about_us';
$route['get_terms_conditions'] = 'Api_Controller/get_terms_conditions';
$route['get_contact_us'] = 'Api_Controller/get_contact_us';
$route['get_notification'] = 'Api_Controller/get_notification';

// test series
$route['get_banner'] = 'Api_Controller/get_banner';
$route['get_test_series'] = 'Api_Controller/get_test_series';
$route['get_category_wise_test_series'] = 'Api_Controller/get_category_wise_test_series';
$route['get_series_wise_test_list'] = 'Api_Controller/get_series_wise_test_list';

// orders
$route['get_my_orders'] = 'Api_Controller/get_my_orders';
$route['create_payment_details'] = 'Api_Controller/create_payment_details';


// Test 
$route['get_test_sections'] = 'Api_Controller/get_test_sections';
$route['get_questions_as_per_sections'] = 'Api_Controller/get_questions_as_per_sections';

// Purchased Test 
$route['get_my_purchased_test'] = 'Api_Controller/get_my_purchased_test';

// Save Test  Result
$route['add_questions_answer'] = 'Api_Controller/add_questions_answer';