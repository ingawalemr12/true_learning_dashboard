 <?php 

 	// get questions as per sections list 26-09-2022 Mahadev 
    public function get_questions_as_per_sections()
    {
    	if($this->input->post('section_id') != null)
   		{
	    	$section_id = $this->input->post('section_id');

			$wh_s = '(section_id="'.$section_id.'")'; 
	     	$check_section_id = $this->Api_Model->getData($tbl='section',$wh_s);

	     	if(!empty($check_section_id))
            {
            	$order_by = ('section_id desc');
            	$wh_s_id = '(section_id="'.$section_id.'")';
            	$questions = $this->Api_Model->getAllData_as_per_order($tbl='import_question',$wh_s_id,$order_by);

            	if(!empty($questions))
            	{
            		$options_d = array();

            		foreach ($questions as $ql)
            		{
            			// get questions record
            			$wh_q = '(question_id="'.$ql['question_id'].'")'; 
	     				$question = $this->Api_Model->getData($tbl='questions',$wh_q);


	     				// get answer_type
	     				if ($question['answer_type'] == 1) 
	     				{
	     					$answer_type = 'Single Choice';
	     				}
	     				elseif ($question['answer_type'] == 2) 
	     				{
	     					$answer_type = 'Multiple Choice';
	     				}
	     				elseif ($question['answer_type'] == 3) 
	     				{
	     					$answer_type = 'Integer';
	     				}
	     				elseif ($question['answer_type'] == 4) 
	     				{
	     					$answer_type = 'True / False';
	     				}
	     				elseif ($question['answer_type'] == 5) 
	     				{
	     					$answer_type = 'Match Matrix';
	     				}
	     				elseif ($question['answer_type'] == 6) 
	     				{
	     					$answer_type = 'Match The Following';
	     				}else
	     				{
	     					$answer_type = ' ';
	     				}
	     				
	     				// get answers record
	     				$wh_q = '(question_id="'.$question['question_id'].'")'; 
	     				$option_list_1 = $this->Api_Model->getData($tbl='answers_single_choice',$wh_q);
	     				$option_list_2 = $this->Api_Model->getData($tbl='answers_multiple_choice',$wh_q);
	     				$option_list_3 = $this->Api_Model->getData($tbl='answers_integer',$wh_q);
	     				$option_list_4 = $this->Api_Model->getData($tbl='answers_true_false',$wh_q);
	     				$option_list_5 = $this->Api_Model->getData($tbl='answers_match_matrix',$wh_q);
	     				$option_list_6 = $this->Api_Model->getData($tbl='answers_match_the_following',$wh_q);

	     				$correct_answer = ''; 
	     				$question_opts = array();

	     				if ($option_list_1) 	// 1-Single Choice
	     				{
     					 	$options_d = array();

                            $option1 = array();
                            $option2 = array();
                            $option3 = array();
                            $option4 = array();

                            $option_id1 = $option_list_1['answer_id']."_1";

                            $option_id2 = $option_list_1['answer_id']."_2";

                            $option_id3 = $option_list_1['answer_id']."_3";

                            $option_id4 = $option_list_1['answer_id']."_4";

                            $option1 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id1,
                                                'opt' => $option_list_1['options_A'],
                                                //strip_tags($option_list_1['options_A'])
                                            );
                            $option2 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id2,
                                                'opt' => $option_list_1['options_B'],
                                            );
                            $option3 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id3,
                                                'opt' => $option_list_1['options_C'],
                                            );
                            $option4 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id4,
                                                'opt' => $option_list_1['options_D'],
                                            );

                            $options_d[] =  $option1;
                            $options_d[] =  $option2;
                            $options_d[] =  $option3;
                            $options_d[] =  $option4;


                            // 25-11-2022 		//  pass correct answer  
	                    	// $answer_mc = $this->Api_Model->getData('answers_single_choice',$wh_q);
	                    	$options_A = $option_list_1['options_A'];
		                    $options_B = $option_list_1['options_B'];
		                    $options_C = $option_list_1['options_C'];
		                    $options_D = $option_list_1['options_D'];

                            // option
		                    $correct_sc_answer = $option_list_1['correct_answer'];
		                   		                   
		                    $correct_answer = 0;
		                   
		                    if($correct_sc_answer == "A")
		                    {
		                        $correct_answer = $options_A;
		                    }
		                    else
	                    	{
	                    		if($correct_sc_answer == "B") 
		                        {
		                            $correct_answer = $options_B;
		                        }
		                        else
		                        {
		                            if($correct_sc_answer == "C") 
		                            {
		                                $correct_answer = $options_C;
		                            }
		                            else
		                            {
		                                if($correct_sc_answer == "D")   
		                                {
		                                    $correct_answer = $options_D;
		                                }
		                            }
		                        }
	                    	} // else
	     				} 
	     				
	     				if ($option_list_2) 	// 2-Multiple Choice
	     				{
     					 	$options_d = array();

                            $option1 = array();
                            $option2 = array();
                            $option3 = array();
                            $option4 = array();

                            $option_id1 = $option_list_2['answer_id']."_1";

                            $option_id2 = $option_list_2['answer_id']."_2";

                            $option_id3 = $option_list_2['answer_id']."_3";

                            $option_id4 = $option_list_2['answer_id']."_4";

                            $option1 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id1,
                                                'opt' => strip_tags($option_list_2['options_A']),
                                                //$option_list_2['options_A'],
                                                
                                            );
                            $option2 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id2,
                                                'opt' => strip_tags($option_list_2['options_B']),
                                            );
                            $option3 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id3,
                                                'opt' => strip_tags($option_list_2['options_C']),
                                            );
                            $option4 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id4,
                                                'opt' => strip_tags($option_list_2['options_D']),
                                            );

                            $options_d[] =  $option1;
                            $options_d[] =  $option2;
                            $options_d[] =  $option3;
                            $options_d[] =  $option4;

                            // 24-11-2022 		//  pass correct answer  
	                    	// $answer_mc = $this->Api_Model->getData('answers_multiple_choice',$wh_q);
	                    	$options_A = $option_list_2['options_A'];
		                    $options_B = $option_list_2['options_B'];
		                    $options_C = $option_list_2['options_C'];
		                    $options_D = $option_list_2['options_D'];

                            // option
		                    $option_mc = $this->Api_Model->getData(' answers_option_multiple_choice',$wh_q);
		                    $correct_mc_opt = $option_mc['correct_answer'];
		                   		                   
		                    $correct_answer = 0;
		                   
		                    if($correct_mc_opt == "A")  // "A" -> $options_A
		                    {
		                        $correct_answer = $options_A;
		                        // echo $correct_answer;
		                    }
		                    else
	                    	{
	                    		if($correct_mc_opt == "B")  // "B" -> $options_A
		                        {
		                            $correct_answer = $options_B;
		                        }
		                        else
		                        {
		                            if($correct_mc_opt == "C")  // "V" -> $options_A
		                            {
		                                $correct_answer = $options_C;
		                            }
		                            else
		                            {
		                                if($correct_mc_opt == "D")  // "D" -> $options_A
		                                {
		                                	//  D == "D"
		                                    $correct_answer = $options_D; // facebook
		                                }
		                            }
		                        }
	                    	} // else
	     				}

	     				if ($option_list_3) 	// 3-Integer
	     				{
     					 	$options_d = array();

     					 	$answer_int = $this->Api_Model->getData('answers_integer',$wh_q);
     					 	$correct_answer = $answer_int['correct_answer'];
                            /*$option1 = array();
                            $option_id1 = $option_list_3['answer_id'];
                            $option1 = array(
                                                'question_id'=>$ql['question_id'],
                                                'answer_id' => $option_id1,
                                            );
                            $options_d[] =  $option1;*/
	     				}

	     				if ($option_list_4) 	// 4-True / False
	     				{
     					 	$options_d = array();

                            $option1 = array();
                            $option2 = array();

                            $option_id1 = $option_list_4['answer_id']."_1";

                            $option_id2 = $option_list_4['answer_id']."_2";

                            $option1 = array(
                                                'question_id'=>$ql['question_id'],
                                                //'answer_id' => $option_id1,
                                                'opt_id' => 1,
                                                'opt' => 'TRUE',
                                                
                                            );
                            $option2 = array(
                                                'question_id'=>$ql['question_id'],
                                                // 'answer_id' => $option_id2,
                                                'opt_id' => 2,
                                                'opt' => 'FALSE',
                                            );

                            $options_d[] =  $option1;
                            $options_d[] =  $option2;

                            $correct_answer  = $option_list_4['correct_answer'];

	     				}

	     				if ($option_list_5) 	// 5-Match Matrix
	     				{
	     					$question_opts = array();
     					 	$options_d = array();

                            $option_l1 = array();
                            $option_l2 = array();
                            $option_l3 = array();
                            $option_l4 = array();

                            $option_r1 = array();
                            $option_r2 = array();
                            $option_r3 = array();
                            $option_r4 = array();

                            $option_id1 = $option_list_5['answer_id']."_1";

                            $option_id2 = $option_list_5['answer_id']."_2";

                            $option_id3 = $option_list_5['answer_id']."_3";

                            $option_id4 = $option_list_5['answer_id']."_4";

                            // Left Options
                            $option_l1 = array(
                                                'question_id'=>$ql['question_id'],
                                                'left_opt_1' => strip_tags($option_list_5['left_opt_1']),
                                                //$option_list_5['options_A'],
                                                
                                            );
                            $option_l2 = array(
                                                'question_id'=>$ql['question_id'],
                                                'left_opt_2' => strip_tags($option_list_5['left_opt_2']),
                                            );
                            $option_l3 = array(
                                                'question_id'=>$ql['question_id'],
                                                'left_opt_3' => strip_tags($option_list_5['left_opt_3']),
                                            );
                            $option_l4 = array(
                                                'question_id'=>$ql['question_id'],
                                                'left_opt_4' => strip_tags($option_list_5['left_opt_4']),
                                            );

                            // Right Options
                            $option_r1 = array(
                                                'question_id'=>$ql['question_id'],
                                                'right_opt_A' => strip_tags($option_list_5['right_opt_A']),
                                                //$option_list_5['options_A'],
                                                
                                            );
                            $option_r2 = array(
                                                'question_id'=>$ql['question_id'],
                                                'right_opt_B' => strip_tags($option_list_5['right_opt_B']),
                                            );
                            $option_r3 = array(
                                                'question_id'=>$ql['question_id'],
                                                'right_opt_C' => strip_tags($option_list_5['right_opt_C']),
                                            );
                            $option_r4 = array(
                                                'question_id'=>$ql['question_id'],
                                                'right_opt_D' => strip_tags($option_list_5['right_opt_D']),
                                            ); 

                            $question_opts[] =  $option_l1;
                            $question_opts[] =  $option_l2;
                            $question_opts[] =  $option_l3; 
                            $question_opts[] =  $option_l4;

                            $question_opts[] =  $option_r1;
                            $question_opts[] =  $option_r2;
                            $question_opts[] =  $option_r3;
                            $question_opts[] =  $option_r4;
	     				}

	     				if ($option_list_6) 	// 6-Match The Following
	     				{
	     					$question_opts = array();
     					 	$options_d = array();

                            $option_l1 = array();
                            $option_l2 = array();
                            $option_l3 = array();
                            $option_l4 = array();

                            $option_r1 = array();
                            $option_r2 = array();
                            $option_r3 = array();
                            $option_r4 = array();

                            $option_id1 = $option_list_6['answer_id']."_1";

                            $option_id2 = $option_list_6['answer_id']."_2";

                            $option_id3 = $option_list_6['answer_id']."_3";

                            $option_id4 = $option_list_6['answer_id']."_4";

                            // Left Options
                            $option_l1 = array(
                                                'question_id'=>$ql['question_id'],
                                                'left_opt_1' => strip_tags($option_list_6['left_opt_1']),
                                                //$option_list_6['options_A'],
                                                
                                            );
                            $option_l2 = array(
                                                'question_id'=>$ql['question_id'],
                                                'left_opt_2' => strip_tags($option_list_6['left_opt_2']),
                                            );
                            $option_l3 = array(
                                                'question_id'=>$ql['question_id'],
                                                'left_opt_3' => strip_tags($option_list_6['left_opt_3']),
                                            );
                            $option_l4 = array(
                                                'question_id'=>$ql['question_id'],
                                                'left_opt_4' => strip_tags($option_list_6['left_opt_4']),
                                            );

                            // Right Options
                            $option_r1 = array(
                                                'question_id'=>$ql['question_id'],
                                                'right_opt_A' => strip_tags($option_list_6['right_opt_A']),
                                                //$option_list_6['options_A'],
                                                
                                            );
                            $option_r2 = array(
                                                'question_id'=>$ql['question_id'],
                                                'right_opt_B' => strip_tags($option_list_6['right_opt_B']),
                                            );
                            $option_r3 = array(
                                                'question_id'=>$ql['question_id'],
                                                'right_opt_C' => strip_tags($option_list_6['right_opt_C']),
                                            );
                            $option_r4 = array(
                                                'question_id'=>$ql['question_id'],
                                                'right_opt_D' => strip_tags($option_list_6['right_opt_D']),
                                            ); 

                            $question_opts[] =  $option_l1;
                            $question_opts[] =  $option_l2;
                            $question_opts[] =  $option_l3; 
                            $question_opts[] =  $option_l4;

                            $question_opts[] =  $option_r1;
                            $question_opts[] =  $option_r2;
                            $question_opts[] =  $option_r3;
                            $question_opts[] =  $option_r4;


                            // 25-11-2022 		//  pass correct answer 
                            // $answer_mtf = $this->Api_Model->getData('answers_match_the_following',$wh_q); 
	                    	/*$l_1 = $option_list_6['left_opt_1'];
		                    $l_2 = $option_list_6['left_opt_2'];
		                    $l_3 = $option_list_6['left_opt_3'];
		                    $l_4 = $option_list_6['left_opt_4'];

		                    $r_A = $option_list_6['right_opt_A'];
		                    $r_B = $option_list_6['right_opt_B'];
		                    $r_C = $option_list_6['right_opt_C'];
		                    $r_D = $option_list_6['right_opt_D'];

                            // option
		                    $option_mtf = $this->Api_Model->getData(' answers_option_match_the_following',$wh_q);
		                    $correct_options_l = $option_mtf['correct_options_l'];
		                    $correct_options_r = $option_mtf['correct_options_r'];
		                    
		                    $correct_answer = 0;
		                   
		                    if($correct_options_l == 1 && $correct_options_r == "A") 
		                    {
		                        $correct_answer = $r_A;
		                    } 
		                    else
	                    	{
	                    		if($correct_options_l == 2 && $correct_options_r == "B")  
	                    		{
		                            $correct_answer = $r_B;
		                        }
		                        else
		                        {
		                            if($correct_options_l== 3 && $correct_options_r == "C")
		                            {
		                                $correct_answer = $r_C;
		                            }
		                            else
		                            {
		                                if($correct_options_l == 4 && $correct_options_r == "d")
		                                {
		                                    $correct_answer = $r_D;  
		                                }
		                            }
		                        }
	                    	} // else

	                    	$options_d[] =  $l_1;
                            $options_d[] =  $l_2;
                            $options_d[] =  $l_3; 
                            $options_d[] =  $l_4;

                            $options_d[] =  $r_A;
                            $options_d[] =  $r_B;
                            $options_d[] =  $r_C;
                            $options_d[] =  $r_D;*/
	     				}

            			$send_data[] = array( 
            									'imp_id'=>$ql['imp_id'],
            									'test_id'=>$ql['test_id'],
    											'section_id'=>$ql['section_id'],
    											'question_type_id'=>$question['answer_type'],
    											'answer_type'=>$answer_type,
	                                        	'question_id'=>$ql['question_id'],
	                                        	'question_name'=>strip_tags($question['question_name']),
	                                        	'correct_answer' => strip_tags($correct_answer),
	                                        	'question_opts' => $question_opts,
	                                        	'options' => $options_d,
	                                        	
	                                    	);
            		}

	            	$response['success'] = "1";
	                $response['error_code'] = "200";
	                $response['message'] = "Success";
	                $response['data'] = $send_data;
	                echo json_encode($response);
	                exit();
				}
	            else
	            {
	            	$response['success'] = "0";
	            	$response['error_code'] = "404";
	                $response['message'] = "Section List not found.";
	                echo json_encode($response);
	                exit(); 
	            }
            }
            else
            {
            	$response['success'] = "0";
            	$response['error_code'] = "404";
                $response['message'] = "Section not found.";
                echo json_encode($response);
                exit(); 
            }
    	}
    	else
	    {
	    	$response['success'] = "0";
	    	$response['error_code'] = "406";
	        $response['message'] = "Required Parameter Missing";
	        echo json_encode($response);
	        exit();
	    }
    }
  
  	public function get_my_purchased_test()
    {
    	if($this->input->post('uid') != null && $this->input->post('test_series_id') != null)
	    {
	    	$uid = $this->input->post('uid');
	    	$test_series_id = $this->input->post('test_series_id');

	    	$where = '(uid="'.$uid.'" AND user_type = 2)';
            $check_user = $this->Api_Model->getData($tbl='user_details',$where);

            if(!empty($check_user))
            {
            	$wh_ord = '(uid="'.$uid.'" AND test_series_id="'.$test_series_id.'" )';
            	$get_order = $this->Api_Model->getAllData($tbl='orders',$wh_ord);
            	// print_r($get_order);die;
            	if(!empty($get_order))
            	{
	            	$order_by = ('test_id desc');
	            	$wh_series_id = '(test_series_id="'.$test_series_id.'")';
	            	$check_t_list = $this->Api_Model->getAllData_as_per_order($tbl='test_list',$wh_series_id,$order_by);
	            	// print_r($check_t_list);die;
					
					if(!empty($check_t_list))
	            	{
	            		// series overview
	            		$wh_cat = '(test_series_id="'.$test_series_id.'")';
	            		$ts = $this->Api_Model->getData($tbl='test_series',$wh_cat);
	            		
	            		$test_list_details = [];
	            		//$series_content = [];
		            	foreach ($check_t_list as $tl) 
		            	{
		            		$wh_cat = '(category_id="'.$tl['category_id'].'")';
		            		$check_category = $this->Api_Model->getData($tbl='category',$wh_cat);
		            		
		            		$wh_l = '(lang_id="'.$tl['lang_id'].'")';
		            		$check_lang = $this->Api_Model->getData($tbl='language',$wh_l);

		            		$wh_l = '(difficulty_id="'.$tl['difficulty_id'].'")';
		            		$check_dif = $this->Api_Model->getData($tbl='difficulty_level',$wh_l);

		            		$test_list_details = array( 
		            										'test_id'=>$tl['test_id'],
					                                        't_instructions'=>strip_tags($tl['t_instructions']),
					                                        't_duration'=>$tl['t_duration'],
					                                        'lang_id'=>$check_lang['lang_id'],
					                                        'language_name'=>$check_lang['language_name'],
					                                        'difficulty_id'=>$tl['difficulty_id'],
					                                        'difficulty'=>$check_dif['difficulty_medium'],
					                                        't_questions'=>$tl['t_questions'],
			                                        		't_marks'=>$tl['t_marks'],
					                                    );

							$series_content[] = array( 
														'test_id'=>$tl['test_id'],
														'test_name'=>$tl['test_name'],
														't_instructions'=>strip_tags($tl['t_instructions']),
			                                        	'test_series_id'=>$tl['test_series_id'],
			                                        	'test_series_type'=>$tl['test_series_type'],//1-Free 2-Paid	
			                                        	't_questions'=>$tl['t_questions'],
				                                        't_marks'=>$tl['t_marks'],
				                                        't_duration'=>$tl['t_duration'],
			                                       		'image'=>$tl['test_image'],
			                                       		'banner'=>$ts['banner_image'],
			                                       		//  test_list_details
			                                        	'test_list_details'=>$test_list_details,

				                                       	/*'category_id'=>$tl['category_id'],
				                                        'category_name'=>$check_category['category_name'],
				                                        'std_id'=>$tl['std_id'],
				                                        'subject_id'=>$tl['subject_id'],
				                                        'difficulty_id'=>$tl['difficulty_id'],
				                                        'lang_id'=>$tl['lang_id'],
				                                        'language_name'=>$check_lang['language_name'],
				                                        'topic'=>$tl['topic'],
				                                        'sub_topic'=>strip_tags($tl['sub_topic']),
				                                        'correct_marks'=>$tl['correct_marks'],
				                                        'negative_marks'=>$tl['negative_marks'],
				                                        'not_attempt_marks'=>$tl['not_attempt_marks'],*/
			                                    	);
		            	}

		            	
	            		// Language
	            		$wh_l = '(lang_id="'.$ts['lang_id'].'")';
		            	$check_langs = $this->Api_Model->getData($tbl='language',$wh_l);

	            		$series_overview[] = array( 
	            									'banner'=>$ts['banner_image'],
			                                        'test_series_id'=>$ts['test_series_id'],
			                                        'description'=>strip_tags($ts['description']),
			                                        'test_list_count'=>count($check_t_list),
			                                        'validity'=>$ts['end_date'],
			                                        'price'=>$ts['price'],
			                                        'offer'=>$ts['offer'],
			                                        'language'=>$ts['lang_id'],
			                                        'language_name'=>$check_langs['language_name'],
			                                        'learning_material'=>" ",
			                                        /* 'test_series_type'=>$ts['test_series_type'],
			                                        //1-Free 2-Paid	
			                                        */
			                                    );

	            		$send_data = array(
		            						'content'=>$series_content,
		            						'overview'=>$series_overview,
	            						);

		            	$response['success'] = "1";
		                $response['error_code'] = "200";
		                $response['message'] = "Success";
		                $response['data'] = $send_data;
		                echo json_encode($response);
		                exit();
		            }
		            else
		            {
		            	$response['success'] = "0";
		            	$response['error_code'] = "404";
		                $response['message'] = "Test list not found.";
		                echo json_encode($response);
		                exit(); 
		            }
	            }
	            else
	            {
	            	$response['success'] = "0";
	            	$response['error_code'] = "404";
	                $response['message'] = "Test Series not purchased.";
	                echo json_encode($response);
	                exit(); 
	            }
            }
            else
            {
            	$response['success'] = "0";
            	$response['error_code'] = "404";
                $response['message'] = "User not found.";
                echo json_encode($response);
                exit(); 
            }
	    }
	    else
	    {
	    	$response['success'] = "0";
	    	$response['error_code'] = "406";
	        $response['message'] = "Required Parameter Missing";
	        echo json_encode($response);
	        exit();
	    }
    }
  	
  	// 29-11-2022
    public function add_questions_answer()
    {
    	if($this->input->post('uid') != null && $this->input->post('test_series_id') != null && $this->input->post('test_id') != null && $this->input->post('questions_data') != null && $this->input->post('section_id') != null  && $this->input->post('is_test_completed') != null)
	    {
	    	$uid = $this->input->post('uid');
	    	$test_series_id = $this->input->post('test_series_id');
	    	$test_id = $this->input->post('test_id');
	    	$section_id = $this->input->post('section_id');
	    	$is_test_completed = $this->input->post('is_test_completed');

	    	$where = '(uid="'.$uid.'" AND user_type = 2)';
            $check_user = $this->Api_Model->getData($tbl='user_details',$where);

            if(!empty($check_user))
            {
            	$questions_data = json_decode($this->input->post('questions_data'),TRUE);

            	if(is_array($questions_data))
			 	{
			        foreach ($questions_data as $que_d) 
	            	{
	            		// question
	            		$wh_ques = '(question_id = "'.$que_d['question_id'].'")';
	                    $question_data = $this->Api_Model->getData('questions',$wh_ques);

	                    $correct_marks = $question_data['correct_marks'];
	                    $negative_marks = $question_data['negative_marks'];
	                    $not_attempt_marks = $question_data['not_attempt_marks']; 

	                    if ($que_d['answer_type'] == 1) 	// 1 - Singal Choice
				    	{
				    		// [{"question_id":"3","opt_id":"","answer_id":"3","correct_answer":"ij", "answer_type":"1"}]

				    		// answer
		                    $wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_sc = $this->Api_Model->getData('answers_single_choice',$wh_ans);
	                    	$correct_answer  = $answer_sc['correct_answer'];
	                    	/*print_r($correct_answer);
	                    	echo $que_d['correct_answer'];*/
	                    	$options_A = $answer_sc['options_A'];
		                    $options_B = $answer_sc['options_B'];
		                    $options_C = $answer_sc['options_C'];
		                    $options_D = $answer_sc['options_D'];

	                    	if($correct_answer == "A")
		                    {
		                        $is_correct_ans = $options_A;
		                        // echo $is_correct_ans;
		                    }
		                    else
	                    	{
	                    		if($correct_answer == "B") 
		                        {
		                            $is_correct_ans = $options_B;
		                        }
		                        else
		                        {
		                            if($correct_answer == "C") 
		                            {
		                                $is_correct_ans = $options_C;
		                            }
		                            else
		                            {
		                                if($correct_answer == "D")   
		                                {
		                                    $is_correct_ans = $options_D;
		                                }
		                            }
		                        }
	                    	} // else

	                    	// echo $is_correct_ans ;
	                    	// Get Result 
	                		if($que_d['correct_answer'] == $is_correct_ans)
		                    {
		                    		// D == ij
		                        $mark = $correct_marks;
		                        $is_correct = 1; // Yes
		                        $mark_type = 1;
		                    }
		                    elseif($que_d['correct_answer'] !== $is_correct_ans)
		                    {
		                        $mark = $negative_marks;
		                        $is_correct = 0; // Not correct
		                        $mark_type = 2;
		                    }
		                    else
		                    {
		                        $mark = $not_attempt_marks;
		                        $is_correct = 0; // Not attept
		                        $mark_type = 3;
		                    }

		                    // Save Result
		                    $add ="";
	                    	$update = "";
	                
		                    $wh_check = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'" AND question_id ="'.$que_d['question_id'].'" AND answer_id ="'.$que_d['answer_id'].'")';

		                    $answers = $this->Api_Model->getData('test_details',$wh_check);
		                    
		                    if($answers)
		                    {
		                        $arr_edit = array(
		                        					'section_id' => $section_id,
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
		                                        );

		                        $update = $this->Api_Model->editData('test_details',$wh_check,$arr_edit);
		                    } else
		                    {
		                        $arr_add = array(
			                                        'uid' => $uid,
			                                        'test_id' => $test_id,
			                                        'test_series_id' => $test_series_id,
			                                        'section_id' => $section_id,
			                                        'question_id' => $que_d['question_id'],
			                                        'opt_id' => '-',
			                                        'answer_id' => $que_d['answer_id'],
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
			                                    );

		                        $add = $this->Api_Model->insertData('test_details',$arr_add);
		                    }
				    		/*$response['success'] = "1";
			                $response['error_code'] = "200";
			                $response['message'] = "Success";
			                echo json_encode($response);
			                exit();*/
				    	}  // answer type

	                    if ($que_d['answer_type'] == 2) 	// 2 - Multiple Choice
				    	{
				    		// [{"question_id":"26","opt_id":"32","answer_id":"9","correct_answer":"D", "answer_type":"2"}]
		                   
		                    // answer
	                    	$wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_mc = $this->Api_Model->getData('answers_multiple_choice',$wh_ans);
	                    	$options_A = $answer_mc['options_A'];
		                    $options_B = $answer_mc['options_B'];
		                    $options_C = $answer_mc['options_C'];
		                    $options_D = $answer_mc['options_D'];


	                    	// option
	                    	$wh_option = '(opt_id = "'.$que_d['opt_id'].'" AND question_id = "'.$que_d['question_id'].'")';
		                    $option_mc = $this->Api_Model->getData(' answers_option_multiple_choice',$wh_option);
		                    $correct_mc_opt = $option_mc['correct_answer'];
		                    
		                   /* echo " correct_mc_opt ". $correct_mc_opt; // D
		                    echo " options_D ". $options_D;die; 	// facebook*/
		                    $is_correct_ans = 0;
		                   
		                    if($correct_mc_opt == "A")  // "A" -> $options_A
		                    {
		                        $is_correct_ans = $options_A;
		                        // echo $is_correct_ans;
		                    }
		                    else
	                    	{
	                    		if($correct_mc_opt == "B")  // "B" -> $options_A
		                        {
		                            $is_correct_ans = $options_B;
		                        }
		                        else
		                        {
		                            if($correct_mc_opt == "C")  // "V" -> $options_A
		                            {
		                                $is_correct_ans = $options_C;
		                            }
		                            else
		                            {
		                                if($correct_mc_opt == "D")  // "D" -> $options_A
		                                {
		                                	//  D == "D"
		                                    $is_correct_ans = $options_D; // facebook
		                                }
		                            }
		                        }
	                    	} // else

	                    	/*echo "correct_mc_opt ".$correct_mc_opt;echo "<br>";
		                    echo "correct_answer ".$que_d['correct_answer'] ;echo "<br>";
		                  	echo "is_correct_ans ".$is_correct_ans ;echo "<br>";*/

	                    	// Get Result 
	                		if($que_d['correct_answer'] == strip_tags($is_correct_ans) )
		                    {
		                    		// D == facebook
		                        $mark = $correct_marks;
		                        $is_correct = 1; // Yes
		                        $mark_type = 1;
		                    }
		                    elseif($que_d['correct_answer'] !== strip_tags($is_correct_ans) )
		                    {
		                        $mark = $negative_marks;
		                        $is_correct = 0; // Not correct
		                        $mark_type = 2;
		                    }
		                    else
		                    {
		                        $mark = $not_attempt_marks;
		                        $is_correct = 0; // Not attept
		                        $mark_type = 3;
		                    }

		                    // Save Result
		                    $add ="";
	                    	$update = "";
	                
		                    $wh_check = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'" AND question_id ="'.$que_d['question_id'].'" AND opt_id ="'.$que_d['opt_id'].'" AND answer_id ="'.$que_d['answer_id'].'")';

		                    $answers = $this->Api_Model->getData('test_details',$wh_check);
		                    
		                    if($answers)
		                    {
		                        $arr_edit = array(
		                        	                'section_id' => $section_id,
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
		                                        );

		                        $update = $this->Api_Model->editData('test_details',$wh_check,$arr_edit);
		                    } else
		                    {
		                        $arr_add = array(
			                                        'uid' => $uid,
			                                        'test_id' => $test_id,
			                                        'test_series_id' => $test_series_id,
			                                        'section_id' => $section_id,
			                                        'question_id' => $que_d['question_id'],
			                                        'opt_id' => $que_d['opt_id'],
			                                        'answer_id' => $que_d['answer_id'],
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
			                                    );

		                        $add = $this->Api_Model->insertData('test_details',$arr_add);
		                    }

		                    /*$response['success'] = "1";
			                $response['error_code'] = "200";
			                $response['message'] = "Success";
			                // $response['data'] = $send_data;
			                echo json_encode($response);
			                exit();*/
		                } // answer type


		                if ($que_d['answer_type'] == 3) 	// 3 - integer
				    	{
				    		// [{"question_id":"9","opt_id":"9","answer_id":"3","correct_answer":"9", "answer_type":"3"}]
				    		
				    		// answer
		                    $wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_int = $this->Api_Model->getData('answers_integer',$wh_ans);
	                    	$correct_int_answer  = $answer_int['correct_answer'];


	                    	// Get Result 
						    if($que_d['correct_answer'] == $que_d['opt_id'])
						    {
						        $mark = $correct_marks;
						        $is_correct = 1; // Yes
						        $mark_type = 1;
						    }
						    elseif($que_d['correct_answer'] !== $que_d['opt_id'])
						    {
						        $mark = $negative_marks;
						        $is_correct = 0; // Not correct
						        $mark_type = 2;
						    }
						    else
						    {
						        $mark = $not_attempt_marks;
						        $is_correct = 0; // Not attept
						        $mark_type = 3;
						    }

						    // Save Result
		                    $add ="";
	                    	$update = "";
	                
		                    $wh_check = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'" AND question_id ="'.$que_d['question_id'].'" AND answer_id ="'.$que_d['answer_id'].'")';

		                    $answers = $this->Api_Model->getData('test_details',$wh_check);
		                    
		                    if($answers)
		                    {
		                        $arr_edit = array(
		                        					'section_id' => $section_id,
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
		                                        );

		                        $update = $this->Api_Model->editData('test_details',$wh_check,$arr_edit);
		                    } else
		                    {
		                        $arr_add = array(
			                                        'uid' => $uid,
			                                        'test_id' => $test_id,
			                                        'test_series_id' => $test_series_id,
			                                        'section_id' => $section_id,
			                                        'question_id' => $que_d['question_id'],
			                                        'opt_id' => '-',
			                                        'answer_id' => $que_d['answer_id'],
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
			                                    );

		                        $add = $this->Api_Model->insertData('test_details',$arr_add);
		                    }
				    		/*$response['success'] = "1";
			                $response['error_code'] = "200";
			                $response['message'] = "Success";
			                echo json_encode($response);
			                exit();*/
	                    } // answer type

	                    if ($que_d['answer_type'] == 4) 	// 4 - T/F
				    	{
				    		// [{"question_id":"12","opt_id":"1","answer_id":"2","correct_answer":"1", "answer_type":"4"}]
				    		
				    		// answer
		                    $wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_tf = $this->Api_Model->getData('answers_true_false',$wh_ans);
	                    	$correct_tf_answer  = $answer_tf['correct_answer'];

	                    	// Get Result 
						    if($que_d['correct_answer'] == $que_d['opt_id'])
						    {
						        $mark = $correct_marks;
						        $is_correct = 1; // Yes
						        $mark_type = 1;
						    }
						    elseif($que_d['correct_answer'] !== $que_d['opt_id'])
						    {
						        $mark = $negative_marks;
						        $is_correct = 0; // Not correct
						        $mark_type = 2;
						    }
						    else
						    {
						        $mark = $not_attempt_marks;
						        $is_correct = 0; // Not attept
						        $mark_type = 3;
						    }

						    // Save Result
		                    $add ="";
	                    	$update = "";
	                
		                    $wh_check = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'" AND question_id ="'.$que_d['question_id'].'" AND answer_id ="'.$que_d['answer_id'].'")';

		                    $answers = $this->Api_Model->getData('test_details',$wh_check);
		                    
		                    if($answers)
		                    {
		                        $arr_edit = array(
			                                        'section_id' => $section_id,
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
		                                        );

		                        $update = $this->Api_Model->editData('test_details',$wh_check,$arr_edit);
		                    } else
		                    {
		                        $arr_add = array(
			                                        'uid' => $uid,
			                                        'test_id' => $test_id,
			                                        'test_series_id' => $test_series_id,
			                                        'section_id' => $section_id,
			                                        'question_id' => $que_d['question_id'],
			                                        'opt_id' => '-',
			                                        'answer_id' => $que_d['answer_id'],
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
			                                    );

		                        $add = $this->Api_Model->insertData('test_details',$arr_add);
		                    }
				    		/*$response['success'] = "1";
			                $response['error_code'] = "200";
			                $response['message'] = "Success";
			                echo json_encode($response);
			                exit();*/

				    	} // answer type


				    	if ($que_d['answer_type'] == 6) 	// 6 - Match The Following
				    	{
				    		// opt: 1-B, 2-A, 3-D, 4-C //{"1":"D","2":"B","3":"C","4":"A",}
				    		// [{"question_id":"16","opt_id":"1","answer_id":"1","answer_type":"6"}]

				    		// options
	                    	$wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$opt_mtf = $this->Api_Model->getData('answers_match_the_following',$wh_ans);
	                    	
	                    	$l_1 = $opt_mtf['left_opt_1'];
		                    $l_2 = $opt_mtf['left_opt_2'];
		                    $l_3 = $opt_mtf['left_opt_3'];
		                    $l_4 = $opt_mtf['left_opt_4'];

	                    	$r_A = $opt_mtf['right_opt_A'];
		                    $r_B = $opt_mtf['right_opt_B'];
		                    $r_C = $opt_mtf['right_opt_C'];
		                    $r_D = $opt_mtf['right_opt_D'];

	                    	// answer
		                    $wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_mtf = $this->Api_Model->getAllData('answers_option_match_the_following',$wh_ans);
	                    	
	                    	foreach ($answer_mtf as $ans_mtf) 
	            			{
	            				$correct_mtf_ans_l[]  = $ans_mtf['correct_options_l'];
	            				$correct_mtf_ans_r[]  = $ans_mtf['correct_options_r'];
	                    		$is_correct_ans = 0;	
	                    	} //die;
	                    	
	                    	// print_r($correct_mtf_ans_l);
	                    	// print_r($correct_mtf_ans_r);   die;
	                    	if($correct_mtf_ans_r[0] == $que_d['opt_id'])  
		                    {
		                        $is_correct_ans = $r_A;
		                    }
		                    else
	                    	{
	                    		if($correct_mtf_ans_r[1] == $que_d['opt_id'])
		                        {
		                            $is_correct_ans = $r_B;
		                        }
		                        else
		                        {
		                            if($correct_mtf_ans_r[2] == $que_d['opt_id'])  
		                            {
		                                $is_correct_ans = $r_C;
		                            }
		                            else
		                            {
		                                if($correct_mtf_ans_r[3] == $que_d['opt_id']) 
		                                {
		                                	//  D == "D"
		                                    $is_correct_ans = $r_D; // D
		                                }
		                            }
		                        }
	                    	} // else*/
	                    	// Get Result 
						 	
						 	// Save Result
		                    $add ="";
	                    	$update = "";

	                    	
				    	}

	            	} // die;

	            	// Result Response 
	            	/*
	            	if($add || $update)
                    {
                    	// 1 SumofCorrectMarks
					    $wh_sum = '(test_id="'.$test_id.'" AND mark_type = 1)';
					    $SumofCorrectMarks = $this->Api_Model->getTotalSumofMarks($tbl='test_details',$wh_sum);
					    if ($SumofCorrectMarks ) {
					    	$correct_marks = $SumofCorrectMarks[0]['mark']; 
					    }else{
					    	$correct_marks  = 0;
					    }
					    

					    // 2 SumofNegativeMarks
					    $wh_nsum = '(test_id="'.$test_id.'" AND mark_type = 2)';
					    $SumofNegativeMarks = $this->Api_Model->getTotalSumofMarks($tbl='test_details',$wh_nsum);
					    if ($SumofNegativeMarks) {
					    	$negative_marks = $SumofNegativeMarks[0]['mark']; 
					    }else{
					    	$negative_marks  = 0;
					    }

					    // 3 SumofUnutteptMarks
					    $wh_na_sum = '(test_id="'.$test_id.'" AND mark_type = 3)';
					    $SumofUnutteptMarks = $this->Api_Model->getTotalSumofMarks($tbl='test_details',$wh_na_sum);
					    if ($SumofUnutteptMarks) {
					    	$unattept_marks = $SumofUnutteptMarks[0]['mark']; 
					    }else{
					    	$unattept_marks  = 0;
					    }

					    // $sum = $SumofCorrectMarks[0]['mark'] -$SumofNegativeMarks[0]['mark']- $SumofUnutteptMarks[0]['mark'];
					    $final_marks = $SumofCorrectMarks[0]['mark'] - $SumofNegativeMarks[0]['mark'];

					    // test mark
					    $wh_id = '(test_id="'.$test_id.'")';
		            	$test_list=$this->Api_Model->getData($tbl='test_list',$wh_id,);

					    // Final Result save
					    $wh_final = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'")';
                        $final_result = $this->Api_Model->getData('test_final_result',$wh_final);
                        // print_r($final_result);

                        if($final_result)
	                    {
	                        $arr_final1 = array(
	                        					'uid' => $uid,
	                        					'test_series_id' => $test_series_id,
	                        					'test_id' => $test_id,
		                                        'correct_marks'=>$correct_marks,
		                                        'negative_marks' => $negative_marks,
		                                        //'unattept_marks'=>$unattept_marks,
		                                        'final_marks' => $final_marks,
		                                        'total_marks' => $test_list['t_marks'],
		                                        'is_test_completed' => $is_test_completed,
		                                        'test_date' => date('Y-m-d'),
	                                        );

	                        $this->Api_Model->editData('test_final_result',$wh_final,$arr_final1);
	                    } else
	                    {
	                        $arr_final2 = array(
		                                        'uid' => $uid,
	                        					'test_series_id' => $test_series_id,
	                        					'test_id' => $test_id,
		                                        'correct_marks'=>$correct_marks,
		                                        'negative_marks' => $negative_marks,
		                                        // 'unattept_marks'=>$unattept_marks,
		                                        'final_marks' => $final_marks,
		                                        'total_marks' => $test_list['t_marks'],
		                                        'is_test_completed' => $is_test_completed,
		                                        'test_date' => date('Y-m-d'),
		                                    );

	                        $this->Api_Model->insertData('test_final_result',$arr_final2);
	                    }
			    		$response['success'] = 1;
		                $response['error_code'] = "200";
		                $response['message'] = "Success";// "Test Submitted Successfully";
		                echo json_encode($response);
		                exit();
                    }
                    else
                    {
                        $response['error_code'] = "403";
                        $response['message'] = "Something went wrong";
                        echo json_encode($response);
                        exit();
                    }
					*/
			    } // if is_array()
			    else
			    {
			      	echo " No Data in array";
			    }
            }
            else
            {
            	$response['success'] = "0";
            	$response['error_code'] = "404";
                $response['message'] = "User not found.";
                echo json_encode($response);
                exit(); 
            }
	    }
	    else
	    {
	    	$response['success'] = "0";
	    	$response['error_code'] = "406";
	        $response['message'] = "Required Parameter Missing";
	        echo json_encode($response);
	        exit();
	    }
    }
  	
    //backup on 29-11-2022
  	/*public function add_questions_answer()
    {
    	if($this->input->post('uid') != null && $this->input->post('test_series_id') != null && $this->input->post('test_id') != null && $this->input->post('questions_data') != null )
	    {
	    	$uid = $this->input->post('uid');
	    	$test_series_id = $this->input->post('test_series_id');
	    	$test_id = $this->input->post('test_id');

	    	$where = '(uid="'.$uid.'" AND user_type = 2)';
            $check_user = $this->Api_Model->getData($tbl='user_details',$where);

            if(!empty($check_user))
            {
            	$questions_data = json_decode($this->input->post('questions_data'),TRUE);

            	if(is_array($questions_data))
			 	{
			        foreach ($questions_data as $que_d) 
	            	{
	            		// question
	            		$wh_ques = '(question_id = "'.$que_d['question_id'].'")';
	                    $question_data = $this->Api_Model->getData('questions',$wh_ques);

	                    $correct_marks = $question_data['correct_marks'];
	                    $negative_marks = $question_data['negative_marks'];
	                    $not_attempt_marks = $question_data['not_attempt_marks']; 

	                    if ($que_d['answer_type'] == 1) 	// 1 - Singal Choice
				    	{
				    		// [{"question_id":"3","opt_id":"","answer_id":"3","correct_answer":"ij", "answer_type":"1"}]

				    		// answer
		                    $wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_sc = $this->Api_Model->getData('answers_single_choice',$wh_ans);
	                    	$correct_answer  = $answer_sc['correct_answer'];
	                    	
	                    	$options_A = $answer_sc['options_A'];
		                    $options_B = $answer_sc['options_B'];
		                    $options_C = $answer_sc['options_C'];
		                    $options_D = $answer_sc['options_D'];

	                    	if($correct_answer == "A")
		                    {
		                        $is_correct_ans = $options_A;
		                        // echo $is_correct_ans;
		                    }
		                    else
	                    	{
	                    		if($correct_answer == "B") 
		                        {
		                            $is_correct_ans = $options_B;
		                        }
		                        else
		                        {
		                            if($correct_answer == "C") 
		                            {
		                                $is_correct_ans = $options_C;
		                            }
		                            else
		                            {
		                                if($correct_answer == "D")   
		                                {
		                                    $is_correct_ans = $options_D;
		                                }
		                            }
		                        }
	                    	} // else

	                    	// echo $is_correct_ans ;
	                    	// Get Result 
	                		if($que_d['correct_answer'] == $is_correct_ans)
		                    {
		                    		// D == ij
		                        $mark = $correct_marks;
		                        $is_correct = 1; // Yes
		                        $mark_type = 1;
		                    }
		                    elseif($que_d['correct_answer'] !== $is_correct_ans)
		                    {
		                        $mark = $negative_marks;
		                        $is_correct = 0; // Not correct
		                        $mark_type = 2;
		                    }
		                    else
		                    {
		                        $mark = $not_attempt_marks;
		                        $is_correct = 0; // Not attept
		                        $mark_type = 3;
		                    }

		                    // Save Result
		                    $add ="";
	                    	$update = "";
	                
		                    $wh_check = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'" AND question_id ="'.$que_d['question_id'].'" AND answer_id ="'.$que_d['answer_id'].'")';

		                    $answers = $this->Api_Model->getData('test_details',$wh_check);
		                    
		                    if($answers)
		                    {
		                        $arr_edit = array(
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
		                                        );

		                        $update = $this->Api_Model->editData('test_details',$wh_check,$arr_edit);
		                    } else
		                    {
		                        $arr_add = array(
			                                        'uid' => $uid,
			                                        'test_id' => $test_id,
			                                        'test_series_id' => $test_series_id,
			                                        'question_id' => $que_d['question_id'],
			                                        'opt_id' => '-',
			                                        'answer_id' => $que_d['answer_id'],
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
			                                    );

		                        $add = $this->Api_Model->insertData('test_details',$arr_add);
		                    }
				    		$response['success'] = "1";
			                $response['error_code'] = "200";
			                $response['message'] = "Success";
			                echo json_encode($response);
			                exit();
				    	}  // answer type

	                    if ($que_d['answer_type'] == 2) 	// 2 - Multiple Choice
				    	{
				    		// [{"question_id":"26","opt_id":"32","answer_id":"9","correct_answer":"D", "answer_type":"2"}]
		                   
		                    // answer
	                    	$wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_mc = $this->Api_Model->getData('answers_multiple_choice',$wh_ans);
	                    	$options_A = $answer_mc['options_A'];
		                    $options_B = $answer_mc['options_B'];
		                    $options_C = $answer_mc['options_C'];
		                    $options_D = $answer_mc['options_D'];


	                    	// option
	                    	$wh_option = '(opt_id = "'.$que_d['opt_id'].'" AND question_id = "'.$que_d['question_id'].'")';
		                    $option_mc = $this->Api_Model->getData(' answers_option_multiple_choice',$wh_option);
		                    $correct_mc_opt = $option_mc['correct_answer'];
		                    
		                    $is_correct_ans = 0;
		                   
		                    if($correct_mc_opt == "A")  // "A" -> $options_A
		                    {
		                        $is_correct_ans = $options_A;
		                        // echo $is_correct_ans;
		                    }
		                    else
	                    	{
	                    		if($correct_mc_opt == "B")  // "B" -> $options_A
		                        {
		                            $is_correct_ans = $options_B;
		                        }
		                        else
		                        {
		                            if($correct_mc_opt == "C")  // "V" -> $options_A
		                            {
		                                $is_correct_ans = $options_C;
		                            }
		                            else
		                            {
		                                if($correct_mc_opt == "D")  // "D" -> $options_A
		                                {
		                                	//  D == "D"
		                                    $is_correct_ans = $options_D; // facebook
		                                }
		                            }
		                        }
	                    	} // else

	                    	

	                    	// Get Result 
	                		if($que_d['correct_answer'] == strip_tags($is_correct_ans) )
		                    {
		                    		// D == facebook
		                        $mark = $correct_marks;
		                        $is_correct = 1; // Yes
		                        $mark_type = 1;
		                    }
		                    elseif($que_d['correct_answer'] !== strip_tags($is_correct_ans) )
		                    {
		                        $mark = $negative_marks;
		                        $is_correct = 0; // Not correct
		                        $mark_type = 2;
		                    }
		                    else
		                    {
		                        $mark = $not_attempt_marks;
		                        $is_correct = 0; // Not attept
		                        $mark_type = 3;
		                    }

		                    // Save Result
		                    $add ="";
	                    	$update = "";
	                
		                    $wh_check = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'" AND question_id ="'.$que_d['question_id'].'" AND opt_id ="'.$que_d['opt_id'].'" AND answer_id ="'.$que_d['answer_id'].'")';

		                    $answers = $this->Api_Model->getData('test_details',$wh_check);
		                    
		                    if($answers)
		                    {
		                        $arr_edit = array(
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
		                                        );

		                        $update = $this->Api_Model->editData('test_details',$wh_check,$arr_edit);
		                    } else
		                    {
		                        $arr_add = array(
			                                        'uid' => $uid,
			                                        'test_id' => $test_id,
			                                        'test_series_id' => $test_series_id,
			                                        'question_id' => $que_d['question_id'],
			                                        'opt_id' => $que_d['opt_id'],
			                                        'answer_id' => $que_d['answer_id'],
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
			                                    );

		                        $add = $this->Api_Model->insertData('test_details',$arr_add);
		                    }

		                    $response['success'] = "1";
			                $response['error_code'] = "200";
			                $response['message'] = "Success";
			                // $response['data'] = $send_data;
			                echo json_encode($response);
			                exit();
		                } // answer type


		                if ($que_d['answer_type'] == 3) 	// 3 - integer
				    	{
				    		// [{"question_id":"9","opt_id":"","answer_id":"3","correct_answer":"9", "answer_type":"3"}]
				    		
				    		// answer
		                    $wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_int = $this->Api_Model->getData('answers_integer',$wh_ans);
	                    	$correct_int_answer  = $answer_int['correct_answer'];


	                    	// Get Result 
						    if($que_d['correct_answer'] == $que_d['opt_id'])
						    {
						        $mark = $correct_marks;
						        $is_correct = 1; // Yes
						        $mark_type = 1;
						    }
						    elseif($que_d['correct_answer'] !== $que_d['opt_id'])
						    {
						        $mark = $negative_marks;
						        $is_correct = 0; // Not correct
						        $mark_type = 2;
						    }
						    else
						    {
						        $mark = $not_attempt_marks;
						        $is_correct = 0; // Not attept
						        $mark_type = 3;
						    }

						    // Save Result
		                    $add ="";
	                    	$update = "";
	                
		                    $wh_check = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'" AND question_id ="'.$que_d['question_id'].'" AND answer_id ="'.$que_d['answer_id'].'")';

		                    $answers = $this->Api_Model->getData('test_details',$wh_check);
		                    
		                    if($answers)
		                    {
		                        $arr_edit = array(
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
		                                        );

		                        $update = $this->Api_Model->editData('test_details',$wh_check,$arr_edit);
		                    } else
		                    {
		                        $arr_add = array(
			                                        'uid' => $uid,
			                                        'test_id' => $test_id,
			                                        'test_series_id' => $test_series_id,
			                                        'question_id' => $que_d['question_id'],
			                                        'opt_id' => '-',
			                                        'answer_id' => $que_d['answer_id'],
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
			                                    );

		                        $add = $this->Api_Model->insertData('test_details',$arr_add);
		                    }
				    		$response['success'] = "1";
			                $response['error_code'] = "200";
			                $response['message'] = "Success";
			                echo json_encode($response);
			                exit();
	                    } // answer type
						
                      	
                      	if ($que_d['answer_type'] == 4) 	// 4 - T/F
				    	{
				    		// [{"question_id":"12","opt_id":"1","answer_id":"2","correct_answer":"1", "answer_type":"4"}]
				    		
				    		// answer
		                    $wh_ans = '(question_id = "'.$que_d['question_id'].'")';
	                    	$answer_tf = $this->Api_Model->getData('answers_true_false',$wh_ans);
	                    	$correct_tf_answer  = $answer_tf['correct_answer'];

	                    	// Get Result 
						    if($que_d['correct_answer'] == $que_d['opt_id'])
						    {
						        $mark = $correct_marks;
						        $is_correct = 1; // Yes
						        $mark_type = 1;
						    }
						    elseif($que_d['correct_answer'] !== $que_d['opt_id'])
						    {
						        $mark = $negative_marks;
						        $is_correct = 0; // Not correct
						        $mark_type = 2;
						    }
						    else
						    {
						        $mark = $not_attempt_marks;
						        $is_correct = 0; // Not attept
						        $mark_type = 3;
						    }

						    // Save Result
		                    $add ="";
	                    	$update = "";
	                
		                    $wh_check = '(uid ="'.$uid.'" AND test_series_id = "'.$test_series_id.'" AND test_id ="'.$test_id.'" AND question_id ="'.$que_d['question_id'].'" AND answer_id ="'.$que_d['answer_id'].'")';

		                    $answers = $this->Api_Model->getData('test_details',$wh_check);
		                    
		                    if($answers)
		                    {
		                        $arr_edit = array(
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
		                                        );

		                        $update = $this->Api_Model->editData('test_details',$wh_check,$arr_edit);
		                    } else
		                    {
		                        $arr_add = array(
			                                        'uid' => $uid,
			                                        'test_id' => $test_id,
			                                        'test_series_id' => $test_series_id,
			                                        'question_id' => $que_d['question_id'],
			                                        'opt_id' => '-',
			                                        'answer_id' => $que_d['answer_id'],
			                                        'correct_answer'=>$que_d['correct_answer'],
			                                        'is_correct' => $is_correct,
			                                        'mark' => $mark,
			                                        'mark_type' => $mark_type,
			                                        'test_date' => date('Y-m-d'),
			                                    );

		                        $add = $this->Api_Model->insertData('test_details',$arr_add);
		                    }
				    		$response['success'] = "1";
			                $response['error_code'] = "200";
			                $response['message'] = "Success";
			                echo json_encode($response);
			                exit();

				    	} // answer type
                      
	            	} // die;

	            	
	            	// response
	            	

			    } // if is_array()
			    else
			    {
			      	echo " No Data in array";
			    }
            }
            else
            {
            	$response['success'] = "0";
            	$response['error_code'] = "404";
                $response['message'] = "User not found.";
                echo json_encode($response);
                exit(); 
            }
	    }
	    else
	    {
	    	$response['success'] = "0";
	    	$response['error_code'] = "406";
	        $response['message'] = "Required Parameter Missing";
	        echo json_encode($response);
	        exit();
	    }
    }
  */
