<?php
defined('BASEPATH') OR exit('No direct script access allowed');

ob_start(); 

// PHPMailer
require 'PHPMailer/PHPMailerAutoload.php';
require 'PHPMailer/class.smtp.php';
require 'PHPMailer/class.phpmailer.php';

class MainController extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
        $this->load->helper('date'); 
        $this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model('Main_Model');
        $this->load->library('pagination');
	}
	
	public function index()
	{
		$this->form_validation->set_rules('user_name','Username', 'required|trim');
		$this->form_validation->set_rules('password','Password', 'required|trim');
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">','</p>');

		if ($this->form_validation->run()==TRUE) 
		{
			$user_name = $this->input->post('user_name');
			$password = $this->input->post('password');
			// echo $user_name." ".$password;die;

			$where = '(user_name = "'.$user_name.'")';
			$admin = $this->Main_Model->getData($tbl='user_details', $where);
			// echo "<pre>";print_r($admin);echo "</pre>";die;

			if (!empty($admin)) 
			{
				// echo "<pre>";print_r($admin);echo "</pre>";die;				
				if ($admin['user_name'] == $user_name && $admin['status'] == 1 && $admin['user_type'] == 1 && $admin['password'] == md5($password))
				{
					// echo "login success";die;
					$data = array(
									'user_id' => $admin['uid'],
									'user_name' => $admin['user_name'],
									'email' => $admin['email'],
								);
					$this->session->set_userdata($data);
					$this->session->set_flashdata('login_success', 'You have successfully logged in!'); 
					redirect(base_url('home'));
				}	
				else
				{
					// echo "login fail";die;
					$this->session->set_flashdata('login_fail', 'This Email is not registered, please check the Email');
					redirect(base_url());
				}
			} 
			else
			{
				// echo "wrong data";die;
				$this->session->set_flashdata('login_fail', 'This Email is not registered, please check the Email');
				redirect(base_url());
			}
		}
		else
		{
			$this->load->view('index');
		}
		// $this->load->view('index');
	}

	public function logout()
	{
		if ($this->session->userdata('user_id'))
		{
			$this->session->unset_userdata('user_id');
			$this->session->set_flashdata('log_out', 'You have successfully logged out ! ');
			redirect(base_url());
		}

	    redirect(base_url());
	}

	public function forgot_password()
	{
		$this->load->view('forgot_password');
	}

	public function forgot_Password_dashboard()
	{
		// echo "forgot_Password_dashboard";
		$this->form_validation->set_rules('email','Email', 'required|trim|valid_email');
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">','</p>');

		if($this->form_validation->run()==TRUE) 
		{
			$email = $this->input->post('email');
			$where = '(email = "'.$email.'" AND user_type = 1)';
			$check_email= $this->Main_Model->getData($tbl='user_details', $where);
			
			if ($check_email) 
			{
				// print_r($check_email);
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $new_password = substr( str_shuffle( $chars ), 0, 5 );
               
                $arr = array(
                				'password'=>md5($new_password),
                				'password_text'=>$new_password,
                			);
                $this->Main_Model->editData($tbl = 'user_details', $where, $arr);

                $mail = new PHPMailer;
	        	$mail->Host = 'smtp.gmail.com'; // ssl://smtp.googlemail.com 
            	$mail->SMTPAuth = true;
            	$mail->Username = 'ingawalemr12@gmail.com'; 
            	$mail->Password = '123';
              	$mail->SMTPSecure = 'tls';
                $mail->Port = '465';
                $mail->setFrom('ingawalemr12@gmail.com', 'Admin'); // from
            	$mail->addAddress($email); // to     
                $mail->isHTML(true);
                $mail->Subject = 'True Learning Admin Dashboard Password Reset';
                $mail->Body = 'Your new password for Aartoon Admin Dashboard is '.$new_password;

                if ($mail->send())
                {
                    $this->session->set_flashdata('mail_send', 'Email has been sent on your email id, Please Check.');
                    redirect(base_url('forgot_Password_dashboard'));
                }
                else
                {
                    $this->session->set_flashdata('email_fail', 'Error in Email sending. Please try again..!'); 
                    redirect(base_url('forgot_Password_dashboard'));
                }
			}
			else
			{
				//echo "user not found"; exit();
                $this->session->set_flashdata('email_fail', "User not found");
                // redirect(base_url('forgot_Password_dashboard'));
                $this->load->view('forgot_password');
			}
		}
		else
		{
			$this->load->view('forgot_password');
		}
	}

	public function profile()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$this->load->view('profile');
	}

	public function profile_update_dash()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$where = '(uid = "'.$user_id.'")';
		$data1= $this->Main_Model->getData($tbl='user_details', $where);
		// echo "<pre>"; print_r($data1); echo "</pre>"; die;

		$path = 'assets/images/users/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$this->form_validation->set_rules('full_name','Full Name', 'required|trim');
		$this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required|numeric|regex_match[/^[0-9]{10}$/]|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('email','Email', 'required|trim|valid_email');
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">','</p>');

		if($this->form_validation->run()==TRUE) 
		{
			// echo "<pre>"; print_r($_FILES); echo "</pre>"; die;
			if ($_FILES['photo']['name'] !="") 
			{				
				if(file_exists($path . $_FILES['photo']['name']))
                {
                    unlink($path . $_FILES['photo']['name']);
                }   			
				
				if ($this->upload->do_upload('photo')) 
				{
					$data = $this->upload->data(); 
					
					$arr = array(
									'photo'=>base_url().$path.$data['file_name'],
									'full_name'=>$this->input->post('full_name'),
									'mobile_no'=>$this->input->post('mobile_no'),
									'email'=>$this->input->post('email'),
									'address'=>$this->input->post('address'),
									'city'=>$this->input->post('city'),
									'pincode'=>$this->input->post('pincode'),
									'country'=>$this->input->post('country'),
								);
					$where = '(uid = "'.$user_id.'")';
					$this->Main_Model->editData($tbl='user_details', $where, $arr);
					$this->session->set_flashdata('profile_success', 'Admin profile has been updated successfully..!');
					redirect(base_url().'profile');
				}
				else
				{
					//echo "error show";
					$data['imageError'] = $this->upload->display_errors();
					$this->load->view('profile', $data);
				}
			}
			else
			{
				# insert  without image 
				$arr = array(
								'full_name'=>$this->input->post('full_name'),
								'mobile_no'=>$this->input->post('mobile_no'),
								'email'=>$this->input->post('email'),
								'address'=>$this->input->post('address'),
								'city'=>$this->input->post('city'),
								'pincode'=>$this->input->post('pincode'),
								'country'=>$this->input->post('country'),
							);
				$where = '(uid = "'.$user_id.'")';
				$this->Main_Model->editData($tbl='user_details', $where, $arr);
				$this->session->set_flashdata('profile_success', 'Admin profile has been updated successfully..!');
				redirect(base_url().'profile');
			} # end - insert  without image 
		} # end - form_validation
		else
		{
			$this->load->view('profile');
		}

	}

	public function update_password()	// Admin Update Password
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		// form_validation
		$this->form_validation->set_rules('oldpassword','Old Password','callback_passwordCheck');
		$this->form_validation->set_rules('newPassword','New Password', 'required|trim');
		$this->form_validation->set_rules('confPassword','Re-Enter Password','required|trim|matches[newPassword]'); 
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">','</p>');

		if($this->form_validation->run()==TRUE) 
		{
			$arr = array(
							'password'=>md5($this->input->post('newPassword')),
							'password_text'=>$this->input->post('newPassword'),
						);
			$where = '(uid = "'.$user_id.'")';
			$admin= $this->Main_Model->editData($tbl='user_details', $where, $arr);
			$this->session->set_flashdata('psw_update', 'Admin Password has been updated successfully..!');
			redirect(base_url().'profile');

		} # end - form_validation
		else
		{
			$this->session->set_flashdata('psw_fail', 'Password Updation Failed..!');
			$this->load->view('profile');
		}
	}

	public function passwordCheck($oldpassword)
	{
		$user_id = $this->session->userdata('user_id');
		$where = '(uid = "'.$user_id.'")';
		$data= $this->Main_Model->getData($tbl='user_details', $where);
		// echo "<pre>"; print_r($data); echo "</pre>"; exit();

		if ($data['password'] != md5($oldpassword)) 
		{
			$this->form_validation->set_message('passwordCheck', 'The {field} does not match');
			return false;
		}
		return true;
	}

	public function home()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$this->load->view('home');
	}
	
	public function dashboard()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$this->load->view('dashboard');
	}


	//Settings

	// category
	public function add_category()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$path = 'assets/images/admin/category/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$category_name = $this->input->post('category_name');

		$wh_category = '(category_name = "'.$category_name.'")';
		$categories = $this->Main_Model->getData($tbl='category', $wh_category);

		if (isset($_POST['add_category'])) 
		{
			if(empty($categories)) 
			{
				if ($_FILES['cat_image']['name'] !="") 
				{				
					if(file_exists($path . $_FILES['cat_image']['name']))
	                {
	                    unlink($path . $_FILES['cat_image']['name']);
	                }   			
					
					if ($this->upload->do_upload('cat_image')) 
					{
						$data = $this->upload->data(); 
						
						$arr = array(
										'cat_image'=>base_url().$path.$data['file_name'],
										'category_name'=>$category_name,
									);
						$this->Main_Model->insertData($tbl='category', $arr);
						$this->session->set_flashdata('create', 'Category Name has been added successfully..!');
						redirect(base_url().'add-category');
					}
					else
					{
						//echo "error show";
						$data['imageError'] = $this->upload->display_errors();
						$this->load->view('settings/add-category', $data);
					}
				}
			}
			else
			{
				$this->session->set_flashdata('exists', 'Category Already exists!');
				redirect(base_url().'add-category');
			}
		} else
		{
			$config['base_url'] = base_url('add-category'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='category'); // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);


			$order_by = ('category_id desc');
			$data['category_list']= $this->Main_Model->getAllRecords($tbl='category', $config['per_page'], $this->uri->segment(2), $order_by);
			//$this->Main_Model->getAllOrderData($tbl='category', $order_by);
		 	// echo "<pre>"; print_r($data['category_list']); echo "<pre>";	die;

			$this->load->view('settings/add-category', $data);
		}

	}

	public function delete_category($category_id)
	{
		// echo $category_id;
		$where = '(category_id="'.$category_id.'")';
		$this->Main_Model->deleteData($tbl='category',$where);
	}

	public function edit_category($category_id)
	{
		// echo $category_id;die;
		$path = 'assets/images/admin/category/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$category_name = $this->input->post('category_name');

		if ($_FILES['cat_image']['name'] !="") 
		{				
			if(file_exists($path . $_FILES['cat_image']['name']))
            {
                unlink($path . $_FILES['cat_image']['name']);
            }   			
			
			if ($this->upload->do_upload('cat_image')) 
			{
				$data = $this->upload->data(); 
				
				$arr = array(
								'cat_image'=>base_url().$path.$data['file_name'],
								'category_name'=>$category_name,
							);
				$where = '(category_id = "'.$category_id.'")';
				$this->Main_Model->editData($tbl='category', $where, $arr);

				$this->session->set_flashdata('edit', 'Category Name has been updated successfully..!');
				redirect(base_url().'add-category');
			}
		}
		else
		{
			$arr = array(
							'category_name'=>$category_name,
						);
			$where = '(category_id = "'.$category_id.'")';
			$this->Main_Model->editData($tbl='category', $where, $arr);

			$this->session->set_flashdata('edit', 'Category Name has been updated successfully..!');
			redirect(base_url().'add-category');
		}	
	}

	public function active_category_status() 
    {
        $status = $this->input->post('status'); 

        $category_id = $this->input->post('category_id');
        // echo $category_id;die;         
        $where = '(category_id="'.$category_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='category',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }
    
    // board
	public function add_board()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        $board_name = $this->input->post('board_name');

        $wh_boards = '(board_name = "'.$board_name.'")';
        $boards = $this->Main_Model->getData($tbl='board', $wh_boards);

        if (isset($_POST['add_board'])) 
        {
        	if(empty($boards)) 
            {
            	$arr = array(
                                'board_name'=>$board_name,
                            );
                $this->Main_Model->insertData($tbl='board', $arr);
                $this->session->set_flashdata('create', 'Board Name has been added successfully..!');
                redirect(base_url().'add_board');
            }
            else
            {
                $this->session->set_flashdata('exists', 'Board Name Already exists!');
                redirect(base_url().'add_board');
            }
		} elseif (isset($_POST['search_board']))  
		{
			// echo $board_name;
			if(!empty($board_name))
		    {
		    	$data['board_list'] = $this->Main_Model->getFilterData($tbl='board', $board_name);

		    	$this->load->view('settings/add_board', $data);
		    }
		}
		else
        {
            $config['base_url'] = base_url('add_board'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='board'); 
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);


            $order_by = ('board_id desc');
            $data['board_list']= $this->Main_Model->getAllRecords($tbl='board', $config['per_page'], $this->uri->segment(2), $order_by);
            //$this->Main_Model->getAllOrderData($tbl='board', $order_by);
            // echo "<pre>"; print_r($data['category_list']); echo "<pre>"; die;

            $this->load->view('settings/add_board', $data);
        }          
		// $this->load->view('settings/add_board');
	}

	public function delete_board($board_id)
    {
        // echo $board_id;
        $where = '(board_id="'.$board_id.'")';
        $this->Main_Model->deleteData($tbl='standard',$where);
        $this->Main_Model->deleteData($tbl='board',$where);
        $this->Main_Model->deleteData($tbl='subjects',$where);
    }

    public function edit_board($board_id)
    {
        // echo $board_id;die;
        $board_name = $this->input->post('board_name');

        $arr = array(
                        'board_name'=>$board_name,
                    );
        $where = '(board_id = "'.$board_id.'")';
        $this->Main_Model->editData($tbl='board', $where, $arr);

        $this->session->set_flashdata('edit', 'Board Name has been updated successfully.!');
        redirect(base_url().'add_board');
    }

    public function active_board_status() 
    {
        $status = $this->input->post('status'); 

        $board_id = $this->input->post('board_id');
        // echo $board_id;die;         
        $where = '(board_id="'.$board_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='board',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    // standard
	public function add_standard()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        // $board_id = $this->input->post('board_id');
        $std_name = $this->input->post('std_name');

        $wh_std = '(std_name = "'.$std_name.'")';
        $standards = $this->Main_Model->getData($tbl='standard', $wh_std);

        if (isset($_POST['add_standard_info'])) 
        {
        	if(empty($standards)) 
            {
            	$arr = array(
                                // 'board_id'=>$board_id,
                                'std_name'=>$std_name,
                            );
                $this->Main_Model->insertData($tbl='standard', $arr);
                $this->session->set_flashdata('create', 'Standard Name has been added successfully..!');
                redirect(base_url().'add_standard');
            }
            else
            {
                $this->session->set_flashdata('exists', 'Standard Name Already exists!');
                redirect(base_url().'add_standard');
            }
		}
		elseif (isset($_POST['search_standard']))  
		{
			// if(!empty($board_id) && !empty($std_name) )
			if(!empty($std_name) )
		    {
		    	$data['standard_list'] = $this->Main_Model->getStandardFilterData($tbl='standard',$std_name);
		    	$data['board_list'] = $this->Main_Model->getAllData($tbl='board');
		    	$this->load->view('settings/add_standard', $data);
		    }
		}
		else
		{
			$config['base_url'] = base_url('add_standard'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='standard'); 
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

			$order_by = ('std_id desc');
            $data['standard_list']= $this->Main_Model->getAllRecords($tbl='standard', $config['per_page'], $this->uri->segment(2), $order_by);
            //$this->Main_Model->getAllOrderData($tbl='standard', $order_by);
            $data['board_list'] = $this->Main_Model->getAllData($tbl='board');
			$this->load->view('settings/add_standard', $data);
		}
	}

	public function delete_standard($std_id)
    {
        // echo $std_id;
        $where = '(std_id="'.$std_id.'")';
        $this->Main_Model->deleteData($tbl='standard',$where);
    }

    public function edit_standard($std_id)
    {
    	// echo $std_id;
    	// $board_id = $this->input->post('board_id');
        $std_name = $this->input->post('std_name');

        $arr = array(
                        // 'board_id'=>$board_id,
                        'std_name'=>$std_name,
                    );
        $where = '(std_id = "'.$std_id.'")';
        $this->Main_Model->editData($tbl='standard', $where, $arr);

        $this->session->set_flashdata('edit', 'Standard Name has been updated successfully..!');
        redirect(base_url().'add_standard');

    }

    public function active_standard_status() 
    {
        $status = $this->input->post('status'); 

        $std_id = $this->input->post('std_id');
        // echo $std_id;die;         
        $where = '(std_id="'.$std_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='standard',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    // ajax get std list as per selected board 
	public function get_standard_list()
	{
		$board_id = $this->input->post('board_id');

		$where = '(board_id = "'.$board_id.'")';
		$std_list= $this->Main_Model->getAllData_not_order($tbl='standard',$where);
		// print_r($std_list);die;
		
		$output = '<option value="" selected disabled>Select Standard </option>';
		foreach ($std_list as $list) 
        {
            $output .='<option value="'.$list['std_id'].'">'.$list['std_name'].'</option>';
        }
        echo $output;
	}

	public function get_standard_list_1()
	{
		$board_id_1 = $this->input->post('board_id_1');

		$where = '(board_id = "'.$board_id_1.'")';
		$std_list= $this->Main_Model->getAllData_not_order($tbl='standard',$where);
		// print_r($std_list);die;
		
		$output = '<option value="" selected disabled>Select Standard </option>';
		foreach ($std_list as $list) 
        {
            $output .='<option value="'.$list['std_id'].'">'.$list['std_name'].'</option>';
        }
        echo $output;
	}

	// add subject
	public function add_subject()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        $board_id = $this->input->post('board_id');
        $std_id = $this->input->post('std_id');
        $subject_name = $this->input->post('subject_name');

        $wh_std = '(std_id = "'.$std_id.'" AND subject_name = "'.$subject_name.'")';
        $subjects = $this->Main_Model->getData($tbl='subjects', $wh_std);

        if (isset($_POST['add_subject_info'])) 
        {
        	if(empty($subjects)) 
            {
            	$arr = array(
                                // 'board_id'=>$board_id,
                                'std_id'=>$std_id,
                                'subject_name'=>$subject_name,
                            );
                $this->Main_Model->insertData($tbl='subjects', $arr);
                $this->session->set_flashdata('create', 'Subject Name has been added successfully..!');
                redirect(base_url().'add_subject');
            }
            else
            {
                $this->session->set_flashdata('exists', 'Subject Name Already exists!');
                redirect(base_url().'add_subject');
            }
		}
		elseif (isset($_POST['search_subject']))  
		{
			// echo $board_id." ".$std_id;die;
			$data['subject_list'] = $this->Main_Model->getStandardFilterData($tbl='subjects',$std_id);

			$data['board_list'] = $this->Main_Model->getAllData($tbl='board');
            $data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');

			$this->load->view('settings/add_subject', $data);
		}
		else
		{
			$config['base_url'] = base_url('add_subject'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='subjects');
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            $order_by = ('subject_id desc');
            $data['subject_list']= $this->Main_Model->getAllRecords($tbl='subjects', $config['per_page'], $this->uri->segment(2), $order_by);
            //$this->Main_Model->getAllOrderData($tbl='subjects', $order_by);
            $data['board_list'] = $this->Main_Model->getAllData($tbl='board');
            $data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');

			$this->load->view('settings/add_subject', $data);
		}
		// $this->load->view('settings/add_subject');
	}
	
	public function active_subject_status() 
    {
        $status = $this->input->post('status'); 

        $subject_id = $this->input->post('subject_id');
        // echo $subject_id;die;         
        $where = '(subject_id="'.$subject_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='subjects',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

	public function delete_subject($subject_id)
    {
        // echo $subject_id;
        $where = '(subject_id="'.$subject_id.'")';
        $this->Main_Model->deleteData($tbl='subjects',$where);
    }

    // ajax update list as per selected board 
  	public function get_standard_list_for_edit() // update
	{
		$board_id = $this->input->post('board_id');

		$where = '(board_id = "'.$board_id.'")';
		$std_list= $this->Main_Model->getAllData_not_order($tbl='standard',$where);
		// print_r($data['std_list']);
		
		// $output = '<option value=""  disabled>Select Standard 1</option>';
		foreach ($std_list as $list) 
        {
           $output .= '<option if($sub[std_id] == $list[std_id]){ echo "selected" ;} value="'.$list['std_id'].'">'.$list['std_name'].'</option>';
        }
        echo $output;
	}

	public function edit_subject($subject_id)
    {
    	// echo $subject_id;
    	// $board_id = $this->input->post('board_id');
        $std_id = $this->input->post('std_id');
        $subject_name = $this->input->post('subject_name');

        $arr = array(
                        // 'board_id'=>$board_id,
                        'std_id'=>$std_id,
                        'subject_name'=>$subject_name,
                    );
        $where = '(subject_id = "'.$subject_id.'")';
        $this->Main_Model->editData($tbl='subjects', $where, $arr);

        $this->session->set_flashdata('create', 'Subjects Name has been updated successfully..!');
        redirect(base_url().'add_subject');

    }

    // difficulty_level
	public function add_difficulty_level()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        $difficulty_medium = $this->input->post('difficulty_medium');

        $wh_boards = '(difficulty_medium = "'.$difficulty_medium.'")';
        $difficulties = $this->Main_Model->getData($tbl='difficulty_level', $wh_boards);

        if (isset($_POST['add_difficulty_info'])) 
        {
            if(empty($difficulties)) 
            {
                $arr = array(
                                'difficulty_medium'=>$difficulty_medium,
                            );
                $this->Main_Model->insertData($tbl='difficulty_level', $arr);
                $this->session->set_flashdata('create', 'Difficulty Medium has been added successfully..!');
                redirect(base_url().'add_difficulty_level');
            }
            else
            {
                $this->session->set_flashdata('exists', 'Difficulty Medium Already exists!');
                redirect(base_url().'add_difficulty_level');
            }
        }
        else
        {
        	$config['base_url'] = base_url('add_difficulty_level'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='difficulty_level');
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            $order_by = ('difficulty_id desc');
            $data['difficulty_list']= $this->Main_Model->getAllRecords($tbl='difficulty_level', $config['per_page'], $this->uri->segment(2), $order_by);

        	$this->load->view('settings/add_difficulty_level',$data);
        }
	}
	
	public function delete_difficulty_level($difficulty_id)
    {
        // echo $difficulty_id;
        $where = '(difficulty_id="'.$difficulty_id.'")';
        $this->Main_Model->deleteData($tbl='difficulty_level',$where);
    }

    public function edit_difficulty_level($difficulty_id)
    {
        // echo $difficulty_id;die;
        $difficulty_medium = $this->input->post('difficulty_medium');

        $arr = array(
                        'difficulty_medium'=>$difficulty_medium,
                    );
        $where = '(difficulty_id = "'.$difficulty_id.'")';
        $this->Main_Model->editData($tbl='difficulty_level', $where, $arr);

        $this->session->set_flashdata('edit', 'Difficulty Medium has been updated successfully.!');
        redirect(base_url().'add_difficulty_level');
    }

	public function active_difficulty_status() 
    {
        $status = $this->input->post('status'); 

        $difficulty_id = $this->input->post('difficulty_id');
        // echo $difficulty_id;die;         
        $where = '(difficulty_id="'.$difficulty_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='difficulty_level',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

	// language
	public function add_language()
	{
        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        $language_name = $this->input->post('language_name');

        $wh_boards = '(language_name = "'.$language_name.'")';
        $languages = $this->Main_Model->getData($tbl='language', $wh_boards);

        if (isset($_POST['add_language_info'])) 
        {
            if(empty($languages)) 
            {
                $arr = array(
                                'language_name'=>$language_name,
                            );
                $this->Main_Model->insertData($tbl='language', $arr);
                $this->session->set_flashdata('create', 'Language Name has been added successfully..!');
                redirect(base_url().'add_language');
            }
            else
            {
                $this->session->set_flashdata('exists', 'Language Name Already exists!');
                redirect(base_url().'add_language');
            }
        }
        else
        {
            $config['base_url'] = base_url('add_language'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='language');
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);


            $order_by = ('lang_id desc');
            $data['language_list']= $this->Main_Model->getAllRecords($tbl='language', $config['per_page'], $this->uri->segment(2), $order_by);
            //$this->Main_Model->getAllOrderData($tbl='language', $order_by);
            
            $this->load->view('settings/add_language', $data);
        }          
        // $this->load->view('settings/add_language');
    }

    public function delete_language($lang_id)
    {
        // echo $lang_id;
        $where = '(lang_id="'.$lang_id.'")';
        $this->Main_Model->deleteData($tbl='language',$where);
    }

    public function edit_language($lang_id)
    {
        // echo $lang_id;die;
        $language_name = $this->input->post('language_name');

        $arr = array(
                        'language_name'=>$language_name,
                    );
        $where = '(lang_id = "'.$lang_id.'")';
        $this->Main_Model->editData($tbl='language', $where, $arr);

        $this->session->set_flashdata('edit', 'Language Name has been updated successfully.!');
        redirect(base_url().'add_language');
    }

    public function active_language_status() 
    {
        $status = $this->input->post('status'); 

        $lang_id = $this->input->post('lang_id');
        // echo $lang_id;die;         
        $where = '(lang_id="'.$lang_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='language',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    // add_subcategory
    public function add_subcategory()
	{
		$this->load->view('settings/add_subcategory');
	}

	// aside
	public function aside()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

		$this->load->view('aside');
	}

	//Website Managment
	
	// banner
	public function banner_screen()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        $path = 'assets/images/admin/banner/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$title = $this->input->post('title');
		
		$wh_banner = '(title = "'.$title.'")';
		$banners = $this->Main_Model->getData($tbl='banner', $wh_banner);

		if (isset($_POST['add_banner_info'])) 
		{
			if(empty($banners)) 
			{
				if ($_FILES['banner_image']['name'] !="") 
				{				
					if(file_exists($path . $_FILES['banner_image']['name']))
	                {
	                    unlink($path . $_FILES['banner_image']['name']);
	                }   			
					
					if ($this->upload->do_upload('banner_image')) 
					{
						$data = $this->upload->data(); 
						
						$arr = array(
										'banner_image'=>base_url().$path.$data['file_name'],
										'title'=>$title,
									);
						$this->Main_Model->insertData($tbl='banner', $arr);
						$this->session->set_flashdata('create', 'Banner details has been added successfully..!');
						redirect(base_url().'banner_screen');
					}
					else
					{
						//echo "error show";
						$data['imageError'] = $this->upload->display_errors();
						$this->load->view('website_managment/banner_screen', $data);
					}
				}
				else
				{
					echo " ";
				}
			}
			else
			{
				$this->session->set_flashdata('exists', 'Banner details Already exists!');
				redirect(base_url().'banner_screen');
			}
		} else
		{
			$config['base_url'] = base_url('banner_screen'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='banner'); 
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            $order_by = ('banner_id desc');
			$data['banner_list']= $this->Main_Model->getAllRecords($tbl='banner', $config['per_page'], $this->uri->segment(2), $order_by);

			$this->load->view('website_managment/banner_screen',$data);
		}
	}

	public function delete_banner_screen($banner_id)
    {
        // echo $banner_id;
        $where = '(banner_id="'.$banner_id.'")';
        $this->Main_Model->deleteData($tbl='banner',$where);
    }

    public function edit_banner_screen($banner_id)
    {
        // echo $banner_id;
        $path = 'assets/images/admin/banner/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$title = $this->input->post('title');

		if ($_FILES['banner_image']['name'] !="") 
		{				
			if(file_exists($path . $_FILES['banner_image']['name']))
            {
                unlink($path . $_FILES['banner_image']['name']);
            }   			
			
			if ($this->upload->do_upload('banner_image')) 
			{
				$data = $this->upload->data(); 
				
				$arr = array(
								'banner_image'=>base_url().$path.$data['file_name'],
								'title'=>$title,
							);
				$where = '(banner_id = "'.$banner_id.'")';
	            $this->Main_Model->editData($tbl='banner', $where, $arr);
				$this->session->set_flashdata('edit', 'Banner details has been updated successfully..!');
				redirect(base_url().'banner_screen');
			}
			else
			{
				//echo "error show";
				$data['imageError'] = $this->upload->display_errors();
				$this->load->view('website_managment/banner_screen', $data);
			}
		}
		else
		{
			$arr = array(
							'title'=>$title,
						);
			$where = '(banner_id = "'.$banner_id.'")';
            $this->Main_Model->editData($tbl='banner', $where, $arr);
			$this->session->set_flashdata('edit', 'Banner details has been updated successfully..!');
			redirect(base_url().'banner_screen');
		}
    }

	public function active_banner_status() 
    {
        $status = $this->input->post('status'); 

        $banner_id = $this->input->post('banner_id');
        // echo $banner_id;die;         
        $where = '(banner_id="'.$banner_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='banner',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

	// about_us
	public function about_us()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

		if (isset($_POST['update_about_us']))
		{
			$heading = $this->input->post('heading');
			$about_us = $this->input->post('about_us');

	        $arr = array(
	                        'heading'=>$heading,
	                        'about_us'=>$about_us,
	                    );
	        $where = '(id = 1)';
	        $this->Main_Model->editData($tbl='about_us', $where, $arr);

	        $this->session->set_flashdata('edit', 'About Us details has been updated successfully.!');
	        redirect(base_url().'about_us');
		}
		else
		{
			$where = '(id = 1)';
        	$data['about'] = $this->Main_Model->getData($tbl='about_us',$where);
			$this->load->view('website_managment/about_us', $data);
		}
	}

	public function contact_us()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

		if (isset($_POST['update_contact_info']))
		{
			$phone = $this->input->post('phone');
			$email = $this->input->post('email');
			$address = $this->input->post('address');

	        $arr = array(
	                        'phone'=>$phone,
	                        'email'=>$email,
	                        'address'=>$address,
	                    );
	       
	        $where = '(id = 1)';
	        $this->Main_Model->editData($tbl='contact_us', $where, $arr);

	        $this->session->set_flashdata('edit', 'Contact details has been updated successfully.!');
	        redirect(base_url().'contact_us');
		}
		else
		{
			$where = '(id = 1)';
        	$data['contact'] = $this->Main_Model->getData($tbl='contact_us',$where);
        	$this->load->view('website_managment/contact_us', $data);
		}
		
	}

	// privacy_policy
	public function privacy_policy()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

		if (isset($_POST['update_privacy_policy']))
		{
			$privacy_policy = $this->input->post('privacy_policy');
			
	        $arr = array(
	                        'privacy_policy'=>$privacy_policy,
	                    );
	       
	        $where = '(id = 1)';
	        $this->Main_Model->editData($tbl='privacy_policy', $where, $arr);

	        $this->session->set_flashdata('edit', 'Privacy Policy details has been updated successfully.!');
	        redirect(base_url().'privacy_policy');
		}
		else
		{
			$where = '(id = 1)';
        	$data['privacy_policy'] = $this->Main_Model->getData($tbl='privacy_policy',$where);
        	$this->load->view('website_managment/privacy_policy', $data);
		}

		// $this->load->view('website_managment/privacy_policy');
	}

	// terms_condition
	public function terms_condition()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

		if (isset($_POST['update_terms_conditions']))
		{
			$terms = $this->input->post('terms');
			
	        $arr = array(
	                        'terms_conditions'=>$terms,
	                    );
	       
	        $where = '(id = 1)';
	        $this->Main_Model->editData($tbl='terms_and_conditions', $where, $arr);

	        $this->session->set_flashdata('edit', 'Terms and Conditions details has been updated successfully.!');
	        redirect(base_url().'terms_condition');
		}
		else
		{
			$where = '(id = 1)';
        	$data['terms'] = $this->Main_Model->getData($tbl='terms_and_conditions',$where);
        	$this->load->view('website_managment/terms_condition', $data);
		}

		// $this->load->view('website_managment/terms_condition');
	}

	// testimonial
	public function testimonial()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        $path = 'assets/images/admin/testimonial/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$testimonial_name = $this->input->post('testimonial_name');
		$qualification = $this->input->post('qualification');
		$description = $this->input->post('description');

		$wh_testmo = '(testimonial_name = "'.$testimonial_name.'" AND qualification = "'.$qualification.'")';
		$testimonials = $this->Main_Model->getData($tbl='testimonial', $wh_testmo);

		if (isset($_POST['add_testimonial_info'])) 
		{
			if(empty($testimonials)) 
			{
				if ($_FILES['testimol_image']['name'] !="") 
				{				
					if(file_exists($path . $_FILES['testimol_image']['name']))
	                {
	                    unlink($path . $_FILES['testimol_image']['name']);
	                }   			
					
					if ($this->upload->do_upload('testimol_image')) 
					{
						$data = $this->upload->data(); 
						
						$arr = array(
										'testimol_image'=>base_url().$path.$data['file_name'],
										'testimonial_name'=>$testimonial_name,
										'qualification'=>$qualification,
										'description'=>$description,
									);
						$this->Main_Model->insertData($tbl='testimonial', $arr);
						$this->session->set_flashdata('create', 'Testimonials details has been added successfully..!');
						redirect(base_url().'testimonial');
					}
					else
					{
						//echo "error show";
						$data['imageError'] = $this->upload->display_errors();
						$this->load->view('website_managment/testimonial', $data);
					}
				}
				else
				{
					echo " ";
				}
			}
			else
			{
				$this->session->set_flashdata('exists', 'Testimonials details Already exists!');
				redirect(base_url().'testimonial');
			}
		} else
		{
			$config['base_url'] = base_url('testimonial'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='testimonial'); 
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            $order_by = ('t_id desc');
			$data['testimonial_list']= $this->Main_Model->getAllRecords($tbl='testimonial', $config['per_page'], $this->uri->segment(2), $order_by);

			$this->load->view('website_managment/testimonial', $data);
		}
		
	}

	public function delete_testimonial($t_id)
	{
		// echo $t_id;
		$where = '(t_id="'.$t_id.'")';
		$this->Main_Model->deleteData($tbl='testimonial',$where);
	}

	public function edit_testimonial($t_id)
	{
		// echo $t_id;die;
		$path = 'assets/images/admin/testimonial/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$testimonial_name = $this->input->post('testimonial_name');
		$qualification = $this->input->post('qualification');
		$description = $this->input->post('description');

		if ($_FILES['testimol_image']['name'] !="") 
		{				
			if(file_exists($path . $_FILES['testimol_image']['name']))
            {
                unlink($path . $_FILES['testimol_image']['name']);
            }   			
			
			if ($this->upload->do_upload('testimol_image')) 
			{
				$data = $this->upload->data(); 
				
				$arr = array(
								'testimol_image'=>base_url().$path.$data['file_name'],
								'testimonial_name'=>$testimonial_name,
								'qualification'=>$qualification,
								'description'=>$description,
							);
				
				$where = '(t_id = "'.$t_id.'")';
				$this->Main_Model->editData($tbl='testimonial', $where, $arr);
				$this->session->set_flashdata('edit', 'Testimonials details has been updated successfully..!');
				redirect(base_url().'testimonial');
			}
			else
			{
				//echo "error show";
				$data['imageError'] = $this->upload->display_errors();
				$this->load->view('website_managment/testimonial', $data);
			}
		}
		else
		{
			$arr = array(
							'testimonial_name'=>$testimonial_name,
							'qualification'=>$qualification,
							'description'=>$description,
						);
			$where = '(t_id = "'.$t_id.'")';
			$this->Main_Model->editData($tbl='testimonial', $where, $arr);
			$this->session->set_flashdata('edit', 'Testimonials details has been updated successfully..!');
			redirect(base_url().'testimonial');
		}
	}

	public function active_testimonial_status() 
    {
        $status = $this->input->post('status'); 

        $t_id = $this->input->post('t_id');
        // echo $t_id;die;         
        $where = '(t_id="'.$t_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='testimonial',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function aside_web_mgmt()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

		$this->load->view('aside_web_mgmt');
	}

	// stud_resourse
	public function aside_stud_resourse()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$this->load->view('aside_stud_resourse');
	}

	// student resourse
	public function student_resourse()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$category_name = $this->input->post('category_name');

		$wh_cat = '(category_name = "'.$category_name.'")';
		$student_category = $this->Main_Model->getData($tbl='student_resourse_category', $wh_cat);

		if (isset($_POST['add_student_resourse_category'])) 
		{
			if(empty($student_category)) 
			{
				$arr = array(
								'category_name'=>$category_name,
							);
				$this->Main_Model->insertData($tbl='student_resourse_category', $arr);
				$this->session->set_flashdata('create', 'Student Resourse category name has been added successfully..!');
				redirect(base_url().'student_resourse');
			}
			else
			{
				$this->session->set_flashdata('exists', 'Category Already exists!');
				redirect(base_url().'student_resourse');
			}
		}
		else
		{
			$config['base_url'] = base_url('student_resourse'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='student_resourse_category'); 
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            $order_by = ('cat_id desc');
			$data['category_list']= $this->Main_Model->getAllRecords($tbl='student_resourse_category', $config['per_page'], $this->uri->segment(2), $order_by);

            $this->load->view('student_resourse', $data);
		}
	}

	public function delete_student_resourse($cat_id)
	{
		// echo $cat_id;
		$where = '(cat_id="'.$cat_id.'")';
		$this->Main_Model->deleteData($tbl='student_resourse_category',$where);
		$this->Main_Model->deleteData($tbl='student_resourse_sub_category',$where);
	}

	public function edit_student_resourse($cat_id)
	{
		// echo $cat_id;die;
		$category_name = $this->input->post('category_name');

		$arr = array(
						'category_name'=>$category_name,
					);
		$where = '(cat_id = "'.$cat_id.'")';
		$this->Main_Model->editData($tbl='student_resourse_category', $where, $arr);

		$this->session->set_flashdata('edit', 'Student Resourse category name has been updated successfully..!');
		redirect(base_url().'student_resourse');
		
	}

	public function active_student_resourse_category_status() 
    {
        $status = $this->input->post('status'); 

        $cat_id = $this->input->post('cat_id');
        // echo $cat_id;die;         
        $where = '(cat_id="'.$cat_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='student_resourse_category',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    // Sub-Category (student resourse) Info
	public function stud_resourse_subcat()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$cat_id = $this->input->post('cat_id');
		$sub_cat_name = $this->input->post('sub_cat_name');
		$upload_details = $this->input->post('upload_details');
		
		$wh_cat = '(sub_cat_name = "'.$sub_cat_name.'")';
		$sub_category = $this->Main_Model->getData($tbl='student_resourse_sub_category', $wh_cat);

		if (isset($_POST['add_student_resourse_sub_category'])) 
		{
			if(empty($sub_category)) 
			{
				$arr = array(
								'cat_id'=>$cat_id,
								'sub_cat_name'=>$sub_cat_name,
								'upload_details'=>$upload_details,
							);
				$this->Main_Model->insertData($tbl='student_resourse_sub_category', $arr);
				$this->session->set_flashdata('create', 'Student Resourse sub category name has been added successfully..!');
				redirect(base_url().'stud_resourse_subcat');
			}
			else
			{
				$this->session->set_flashdata('exists', 'Sub Category Already exists!');
				redirect(base_url().'stud_resourse_subcat');
			}
		}elseif (isset($_POST['search_sub_category']))  
		{
			$data['sub_category_list'] = $this->Main_Model->getStudentResourseFilterData($tbl='student_resourse_sub_category',$cat_id,$sub_cat_name);

            $data['category_list'] = $this->Main_Model->getAllData($tbl='student_resourse_category');

			$this->load->view('stud_resourse_subcat', $data);
		}
		else
		{
			$config['base_url'] = base_url('stud_resourse_subcat'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='student_resourse_sub_category'); 
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            $order_by = ('sub_cat_id desc');
			$data['sub_category_list']= $this->Main_Model->getAllRecords($tbl='student_resourse_sub_category', $config['per_page'], $this->uri->segment(2), $order_by);

			$data['category_list'] = $this->Main_Model->getAllData($tbl='student_resourse_category');

			$this->load->view('stud_resourse_subcat', $data);
		}
	}

	// ajax drop down
	public function get_sub_category_list()
	{
		$cat_id = $this->input->post('cat_id');

		$where = '(cat_id = "'.$cat_id.'")';
		$sub_cat_list= $this->Main_Model->getAllData_not_order($tbl='student_resourse_sub_category',$where);
		// print_r($sub_cat_list);die;
		
		// $output = '<option value="" selected disabled>Select Standard </option>';
		foreach ($sub_cat_list as $list) 
        {
            $output .='<option value="'.$list['sub_cat_id'].'">'.$list['sub_cat_name'].'</option>';
        }
        echo $output;
	}

	public function delete_stud_resourse_subcat($sub_cat_id)
	{
		// echo $sub_cat_id;
		$where = '(sub_cat_id="'.$sub_cat_id.'")';
		$this->Main_Model->deleteData($tbl='student_resourse_sub_category',$where);
	}

	public function edit_stud_resourse_subcat($sub_cat_id)
	{
		// echo $sub_cat_id;die;
		$cat_id = $this->input->post('cat_id');
		$sub_cat_name = $this->input->post('sub_cat_name');
		$upload_details = $this->input->post('upload_details');

		$arr = array(
						'cat_id'=>$cat_id,
						'sub_cat_name'=>$sub_cat_name,
						'upload_details'=>$upload_details,
					);
		$where = '(sub_cat_id = "'.$sub_cat_id.'")';
		$this->Main_Model->editData($tbl='student_resourse_sub_category', $where, $arr);

		$this->session->set_flashdata('edit', 'Student Resourse sub category has been updated successfully..!');
		redirect(base_url().'stud_resourse_subcat');
		
	}

	public function active_stud_sub_category_status() 
    {
        $status = $this->input->post('status'); 

        $sub_cat_id = $this->input->post('sub_cat_id');
        // echo $sub_cat_id;die;         
        $where = '(sub_cat_id="'.$sub_cat_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='student_resourse_sub_category',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

	// User Managment
	public function user_mgmt()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$search_by = $this->input->post('search_by'); 
		$uid = $this->input->post('uid'); 
		$registration_date = $this->input->post('registration_date'); 
		// echo $search_by; echo $uid; echo $registration_date; echo "<br>";

		if (isset($_POST['search_user_info'])) 
		{
			if ($search_by == 1) 
			{
				$data['user_list'] = $this->Main_Model->getUserFilterDataByUid($tbl='user_details',$uid);
				$this->load->view('user_managment/user_mgmt', $data);
			}
			elseif ($search_by == 2) 
			{
				$data['user_list'] = $this->Main_Model->getUserFilterDataByDate($tbl='user_details',$registration_date);
				$this->load->view('user_managment/user_mgmt', $data);
			}
		}
		else
		{
			$where = '(user_type = 2 AND is_registered = 1 AND status = 1)';

			$config['base_url'] = base_url('user_mgmt'); 
	        $config['total_rows'] = $this->Main_Model->getAllArrayData_row_wh($tbl='user_details',$where); 
	        // echo $config['total_rows'] ;die;
	        $config['per_page'] = 10;

	        $config['first_link'] = 'First';
	        $config['last_link'] = 'Last';
	        $config['next_link'] = '>';
	        $config['prev_link'] = '<';
	        $config['full_tag_open'] = "<ul class='pagination'>";
	        $config['full_tag_close'] ="</ul>";
	        $config['num_tag_open']= '<li class="page-item">';
	        $config['num_tag_close']='</li>';
	        $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
	                                    <a href='#' class='page-link'>";
	        $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] ="<li class='page-item'>";
	        $config['next_tag1_close'] ="</li>";
	        $config['prev_tag_open'] ="<li>";
	        $config['prev_tag1_close'] ="<li class='page-item'>";
	        $config['first_tag_open'] ="<li>";
	        $config['first_tag1_close'] ="<li class='page-item'>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag1_close'] = "<li class='page-item'>";
	        $config['attributes'] = array('class' => 'page-link wid'); 

	        $this->pagination->initialize($config);

			$order_by = ('uid desc');
	        $data['user_list']= $this->Main_Model->getAllRecords_wh($tbl='user_details', $where , $config['per_page'], $this->uri->segment(2), $order_by);
	      	 // $this->Main_Model->getAllData_as_per_order($tbl='user_details', $where, $order_by);  
			$this->load->view('user_managment/user_mgmt', $data);
		}
		
	}

	public function active_user_status() 
    {
        $status = $this->input->post('status'); 

        $uid = $this->input->post('uid');
        // echo $uid;die;         
        $where = '(uid="'.$uid.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='user_details',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function delete_user_mgmt($uid)
	{
		// echo $uid;die;
		$where = '(uid="'.$uid.'")';
		$this->Main_Model->deleteData($tbl='user_details',$where);
	}

	// user_profile
	public function user_profile()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$uid = $this->uri->segment(2);
		// echo $uid;die;

		// All Test Series
		$order_by_o = ('uid desc');
        $wh_o = '(uid="'.$uid.'")';
        $data['user_test_series_list'] = $this->Main_Model->getAllData_as_per_order($tbl='orders',$wh_o,$order_by_o);

        // Completed Test Series
        /*$wh_co = '(uid="'.$uid.'" AND completed_series = 1 )'; // 1-yes
        $data['completed_test_series_list'] = $this->Main_Model->getAllData_as_per_order($tbl='orders',$wh_co,$order_by_o);*/
		$this->load->view('user_managment/user_profile', $data);
	}

	public function edit_user_profile($uid)
	{
		// echo $uid;die;
		$path = 'assets/images/users/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$full_name = $this->input->post('full_name');
		$email = $this->input->post('email');
		$mobile_no = $this->input->post('mobile_no');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $pincode = $this->input->post('pincode');

        if ($_FILES['photo']['name'] !="") 
        {               
            if(file_exists($path . $_FILES['photo']['name']))
            {
                unlink($path . $_FILES['photo']['name']);
            }               
            
            if ($this->upload->do_upload('photo')) 
            {
                $data = $this->upload->data(); 

                $arr = array(
                                'photo'=>base_url().$path.$data['file_name'],
                                'full_name'=>$full_name,
								'email'=>$email,
								'mobile_no'=>$mobile_no,
								'address'=>$address,
								'city'=>$city,
								'pincode'=>$pincode,
                            );

                $where = '(uid = "'.$uid.'")';
				$this->Main_Model->editData($tbl='user_details', $where, $arr);
				$this->session->set_flashdata('edit', 'User details has been updated successfully..!');
				redirect(base_url().'user_profile/'.$uid);
            }
            else
            {
                //echo "error show";
                $data['imageError'] = $this->upload->display_errors();
                $this->load->view('question_managment/user_profile', $data);
            }
        }
        else // without image
        {
			$arr = array(
							'full_name'=>$full_name,
							'email'=>$email,
							'mobile_no'=>$mobile_no,
							'address'=>$address,
							'city'=>$city,
							'pincode'=>$pincode,
						);
			$where = '(uid = "'.$uid.'")';
			$this->Main_Model->editData($tbl='user_details', $where, $arr);
			$this->session->set_flashdata('edit', 'User details has been updated successfully..!');
			redirect(base_url().'user_profile/'.$uid);
		}

		// $where = '(uid="'.$uid.'")';
	}

	public function deactive_user() // deactive users list
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$search_by = $this->input->post('search_by'); 
		$uid = $this->input->post('uid'); 
		$registration_date = $this->input->post('registration_date'); 
		// echo $search_by; echo $uid; echo $registration_date; echo "<br>";
		
		if (isset($_POST['search_deactive_user_info'])) 
		{
			if ($search_by == 1) 
			{
				$data['user_list'] = $this->Main_Model->getUserFilterDataByUid($tbl='user_details',$uid);
				$this->load->view('user_managment/user_mgmt', $data);
			}
			elseif ($search_by == 2) 
			{
				$data['user_list'] = $this->Main_Model->getUserFilterDataByDate($tbl='user_details',$registration_date);
				$this->load->view('user_managment/user_mgmt', $data);
			}
		}
		else
		{
			$where1 = '(user_type = 2 AND status = 0)';

			$config['base_url'] = base_url('deactive_user'); 
	        $config['total_rows'] = $this->Main_Model->getAllArrayData_row_wh($tbl='user_details',$where1); 
	        // echo $config['total_rows'] ;die;
	        $config['per_page'] = 10;

	        $config['first_link'] = 'First';
	        $config['last_link'] = 'Last';
	        $config['next_link'] = '>';
	        $config['prev_link'] = '<';
	        $config['full_tag_open'] = "<ul class='pagination'>";
	        $config['full_tag_close'] ="</ul>";
	        $config['num_tag_open']= '<li class="page-item">';
	        $config['num_tag_close']='</li>';
	        $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
	                                    <a href='#' class='page-link'>";
	        $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] ="<li class='page-item'>";
	        $config['next_tag1_close'] ="</li>";
	        $config['prev_tag_open'] ="<li>";
	        $config['prev_tag1_close'] ="<li class='page-item'>";
	        $config['first_tag_open'] ="<li>";
	        $config['first_tag1_close'] ="<li class='page-item'>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag1_close'] = "<li class='page-item'>";
	        $config['attributes'] = array('class' => 'page-link wid'); 

	        $this->pagination->initialize($config);

			$order_by = ('uid desc');
	        $data['deactive_users'] = $this->Main_Model->getAllRecords_wh($tbl='user_details', $where1 , $config['per_page'], $this->uri->segment(2), $order_by);
	        //$this->Main_Model->getAllData_as_per_order($tbl='user_details', $where1, $order_by );
	       	// print_r($data['deactive_users']);die;

			$this->load->view('user_managment/deactive_user',$data);
		}
	}

	public function delete_deactive_user($uid)
	{
		// echo $uid;die;
		$where = '(uid="'.$uid.'")';
		$this->Main_Model->deleteData($tbl='user_details',$where);
	}

	// 16-11-2022
	public function user_test_series()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$u_id = $this->uri->segment(2);
		$test_series_id = $this->uri->segment(3);

		$wh_series_id = '(test_series_id="'.$test_series_id.'")';

		$config['base_url'] = base_url().'user_test_series/'.$u_id.'/'.$test_series_id; 
        $config['total_rows'] = $this->Main_Model->getAllArrayData_row_wh($tbl='test_list',$wh_series_id); 
        // echo $config['total_rows'] ;die;
        $config['per_page'] = 10;
        
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open']= '<li class="page-item">';
        $config['num_tag_close']='</li>';
        $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                    <a href='#' class='page-link'>";
        $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] ="<li class='page-item'>";
        $config['next_tag1_close'] ="</li>";
        $config['prev_tag_open'] ="<li>";
        $config['prev_tag1_close'] ="<li class='page-item'>";
        $config['first_tag_open'] ="<li>";
        $config['first_tag1_close'] ="<li class='page-item'>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag1_close'] = "<li class='page-item'>";
        $config['attributes'] = array('class' => 'page-link wid'); 

        $this->pagination->initialize($config);


        // get record
		$order_by = ('test_id desc');
    	$data['check_t_list'] = $this->Main_Model->getAllData_as_per_order($tbl='test_list',$wh_series_id,$order_by);

    	// $data['check_t_list']=$this->Main_Model->getAllRecords_wh($tbl='test_list',$config['per_page'], $this->uri->segment(2),$wh_series_id,$order_by);
    	// print_r($data['check_t_list']);die;

		$this->load->view('user_managment/user_test_series', $data);
	}

	// 17-11-2022
	public function invoice()
	{
		$this->load->view('user_managment/invoice');
	}
	


	// Question Managment
	public function add_questions()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        $path = 'assets/images/admin/questions/';
        $file_path = './' . $path;
        
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

        // question - 1-Single Choice
		$std_id = $this->input->post('std_id');
		$subject_id = $this->input->post('subject_id');
		$topic = $this->input->post('topic');
		$sub_topic = $this->input->post('sub_topic');
		$difficulty_id = $this->input->post('difficulty_id');
		$lang_id = $this->input->post('lang_id');
		$correct_marks = $this->input->post('correct_marks');
		$negative_marks = $this->input->post('negative_marks');
		$not_attempt_marks = $this->input->post('not_attempt_marks');
		$question_name = $this->input->post('question_name');
		
		// $question_type = $this->input->post('question_type');
		$answer_type = $this->input->post('answer_type');

		// Answer parameters
		$options_A = $this->input->post('options_A');
		$options_B = $this->input->post('options_B');
		$options_C = $this->input->post('options_C');
		$options_D = $this->input->post('options_D');
		
		// common
		$solution = $this->input->post('solution');
		$video_solution = $this->input->post('video_solution');

		// ans - single choice
    	$sc_correct_option = $this->input->post('sc_correct_option');

		// integer
		$correct_answer = $this->input->post('correct_answer');

		// true_false
		$true_false = $this->input->post('true_false');

		// match_matrix
		$left_opt_1 = $this->input->post('left_opt_1');
		$left_opt_2 = $this->input->post('left_opt_2');
		$left_opt_3 = $this->input->post('left_opt_3');
		$left_opt_4 = $this->input->post('left_opt_4');

		$right_opt_A = $this->input->post('right_opt_A');
		$right_opt_B = $this->input->post('right_opt_B');
		$right_opt_C = $this->input->post('right_opt_C');
		$right_opt_D = $this->input->post('right_opt_D');

		// match_matrix - correct_options_r - old
		$mm_correct_option1 = $this->input->post('mm_correct_option1');
		$mm_correct_option2 = $this->input->post('mm_correct_option2');
		$mm_correct_option3 = $this->input->post('mm_correct_option3');
		$mm_correct_option4 = $this->input->post('mm_correct_option4');

		// match_matrix - correct_options_r- new
      	$mm_correct_option_1 =  $this->input->post('mm_correct_option_1');
        $mm_correct_option_2 =  $this->input->post('mm_correct_option_2');
        $mm_correct_option_3 =  $this->input->post('mm_correct_option_3');
        $mm_correct_option_4 =  $this->input->post('mm_correct_option_4');
      	
      	//new
		$mm_correct_option = $this->input->post('mm_correct_option');

		// match_the_following - correct_options_r
		$correct_option1 = $this->input->post('correct_option1');
		$correct_option2 = $this->input->post('correct_option2');
		$correct_option3 = $this->input->post('correct_option3');
		$correct_option4 = $this->input->post('correct_option4');


		// $wh_que = '(std_id = "'.$std_id.'" AND subject_id = "'.$subject_id.'")';
		/*$wh_que = '(question_name = "'.$question_name.'")';
		$question = $this->Main_Model->getData($tbl='questions', $wh_que);*/

		if (isset($_POST['add_question_answer'])) 
		{ 
			// echo $mm_correct_option1."".$mm_correct_option2."".$mm_correct_option3."".$mm_correct_option4;die;
			/*if(empty($question)) 
			{*/
				if ($_FILES['images']['name'] !="") 
                {               
                    if(file_exists($path . $_FILES['images']['name']))
                    {
                        unlink($path . $_FILES['images']['name']);
                    }               
                    
                    if ($this->upload->do_upload('images')) 
                    {
                        $data = $this->upload->data(); 
                        
                        // ADD questions
                        $arr = array(
                        				// question parameters
                                        'images'=>base_url().$path.$data['file_name'],
                                        'std_id'=>$std_id,
										'subject_id'=>$subject_id,
										'topic'=>$topic,
										'sub_topic'=>$sub_topic,
										'difficulty_id'=>$difficulty_id,
										'lang_id'=>$lang_id,
										'correct_marks'=>$correct_marks,
										'negative_marks'=>$negative_marks,
										'not_attempt_marks'=>$not_attempt_marks,
										'question_name'=>$question_name,
										'answer_type'=>$answer_type,
                                    );

                        $last_id = $this->Main_Model->insertData($tbl='questions', $arr);
                        if ($last_id) 
                        {
                        	 // echo $answer_type;die;
	                    	if ($answer_type == 1) 
	                    	{
	                    		// 1-Single Choice
		                    	$arr_single = array(
                                                      // answer
                                                      'question_id'=>$last_id,
                                                      'answer_type'=>$answer_type,
                                                      'options_A'=>$options_A,
                                                      'options_B'=>$options_B,
                                                      'options_C'=>$options_C,
                                                      'options_D'=>$options_D,
                                                      'correct_answer'=>$sc_correct_option,
                                                      'solution'=>$solution,
                                                      'video_solution'=>$video_solution,
                                                  );
		                    	 $this->Main_Model->insertData($tbl='answers_single_choice', $arr_single);
		                    }
		                    elseif ($answer_type == 2) 
	                    	{
	                    		// 2-Multiple Choice
	                    		$arr_multi_choice = array(
                                                            // answer
                                                            'question_id'=>$last_id,
                                                            'answer_type'=>$answer_type,
                                                            'options_A'=>$options_A,
                                                            'options_B'=>$options_B,
                                                            'options_C'=>$options_C,
                                                            'options_D'=>$options_D,
                                                            // 'correct_answer'=> $sc_correct_option,
                                                            'solution'=>$solution,
                                                            'video_solution'=>$video_solution,
                                                        );
		                    	$id = $this->Main_Model->insertData($tbl='answers_multiple_choice', $arr_multi_choice);
		                    	if ($id) 
		                    	{
		                    		foreach($sc_correct_option as $value)
		                    		{
								        $arr_multi_opt = array(
																// options
																'question_id'=>$last_id,
																'answer_type'=>$answer_type,
																'answer_id'=>$id,
																'correct_answer'=> $value,
						                                    );
								        $this->Main_Model->insertData($tbl='answers_option_multiple_choice', $arr_multi_opt);
								    }
		                    	}
	                    	}
	                    	elseif ($answer_type == 3) 
	                    	{
	                    		// 3-Integer
	                    		$arr_integer = array(
														// answer
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'correct_answer'=>$correct_answer,
														'solution'=>$solution,
														'video_solution'=>$video_solution,
				                                    );
		                    	 $this->Main_Model->insertData($tbl='answers_integer', $arr_integer);
	                    	}
	                    	elseif ($answer_type == 4) 
	                    	{
	                    		// 4-True/ False
	                    		$arr_true_false = array(
															// answer
                                                          'question_id'=>$last_id,
                                                          'answer_type'=>$answer_type,
                                                          'correct_answer'=>$correct_answer,
                                                          'solution'=>$solution,
                                                          'video_solution'=>$video_solution,
				                                    );
		                    	 $this->Main_Model->insertData($tbl='answers_true_false', $arr_true_false);
	                    	}elseif ($answer_type == 5) // 5-Match Matrix
                            {
                              $arr_mm = array(
                                				// answer
                                              'question_id'=>$last_id,
                                              'answer_type'=>$answer_type,

                                              'left_opt_1'=>$left_opt_1,
                                              'left_opt_2'=>$left_opt_2,
                                              'left_opt_3'=>$left_opt_3,
                                              'left_opt_4'=>$left_opt_4,

                                              'right_opt_A'=>$right_opt_A,
                                              'right_opt_B'=>$right_opt_B,
                                              'right_opt_C'=>$right_opt_C,
                                              'right_opt_D'=>$right_opt_D,

                                              'solution'=>$solution,
                                              'video_solution'=>$video_solution,
                                            );
                              $id_mm = $this->Main_Model->insertData($tbl='answers_match_matrix', $arr_mm);
                             
                              
                              if ($id_mm) 
                              {
                                foreach($mm_correct_option_1 as $value)
                                {
                                  $arr_mm_opt1 = array(
                                                        // options
                                                        'question_id'=>$last_id,
                                                        'answer_type'=>$answer_type,
                                                        'answer_id'=>$id_mm,
                                                        'correct_options_l'=>1,
                                                        'correct_options_r'=> $value,
                                                      );
                                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt1);
                                }                    			

                                foreach($mm_correct_option_2 as $value)
                                {
                                  $arr_mm_opt2 = array(
                                                          // options
                                                          'question_id'=>$last_id,
                                                          'answer_type'=>$answer_type,
                                                          'answer_id'=>$id_mm,
                                                          'correct_options_l'=>2,
                                                          'correct_options_r'=> $value,
                                                        );
                                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt2);
                                }     

                                foreach($mm_correct_option_3 as $value)
                                {
                                  $arr_mm_opt3 = array(
                                                          // options
                                                          'question_id'=>$last_id,
                                                          'answer_type'=>$answer_type,
                                                          'answer_id'=>$id_mm,
                                                          'correct_options_l'=>3,
                                                          'correct_options_r'=> $value,
                                                        );
                                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt3);
                                }     

                                foreach($mm_correct_option_4 as $value)
                                {
                                  $arr_mm_opt4 = array(
                                                        // options
                                                        'question_id'=>$last_id,
                                                        'answer_type'=>$answer_type,
                                                        'answer_id'=>$id_mm,
                                                        'correct_options_l'=>4,
                                                        'correct_options_r'=> $value,
                                                      );
                                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt4);
                                }     
                              }
                            }
	                    	elseif ($answer_type == 6) 
	                    	{
	                    		// 6-Match The Following
	                    		$arr_mf = array(
													// answer
													'question_id'=>$last_id,
													'answer_type'=>$answer_type,
													
													'left_opt_1'=>$left_opt_1,
													'left_opt_2'=>$left_opt_2,
													'left_opt_3'=>$left_opt_3,
													'left_opt_4'=>$left_opt_4,

													'right_opt_A'=>$right_opt_A,
													'right_opt_B'=>$right_opt_B,
													'right_opt_C'=>$right_opt_C,
													'right_opt_D'=>$right_opt_D,

													'solution'=>$solution,
													'video_solution'=>$video_solution,
			                                    );
		                    	$id_mf = $this->Main_Model->insertData($tbl='answers_match_the_following', $arr_mf);
		                    	// echo $id_mf ;die;
		                    	if ($id_mf) 
	                    		{
	                    			$arr_mf_opt1= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mf,
															'correct_options_l'=>1,
															'correct_options_r'=>$correct_option1,
				                                    	);
	                    			
	                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt1);
	                    			
	                    			$arr_mf_opt2= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mf,
															'correct_options_l'=>2,
															'correct_options_r'=>$correct_option2,
				                                    	);
	                    			
	                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt2);
	                    			
	                    			$arr_mf_opt3= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mf,
															'correct_options_l'=>3,
															'correct_options_r'=>$correct_option3,
				                                    	);
	                    			
	                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt3);
	                    			
	                    			$arr_mf_opt4= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mf,
															'correct_options_l'=>4,
															'correct_options_r'=>$correct_option4,
				                                    	);
	                    			
	                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt4);
	                    		}
	                    	}
                    	}
						$this->session->set_flashdata('create', 'Questions has been added successfully..!');
						redirect(base_url().'manage_question');
                    }
                    else
                    {
                        //echo "error show";
                        $data['imageError'] = $this->upload->display_errors();
                        $this->load->view('question_managment/add_questions', $data);
                    }
                }
                else // without image
                {
                	// ADD questions - without image
					$arr = array(
									'std_id'=>$std_id,
									'subject_id'=>$subject_id,
									'topic'=>$topic,
									'sub_topic'=>$sub_topic,
									'difficulty_id'=>$difficulty_id,
									'lang_id'=>$lang_id,
									'correct_marks'=>$correct_marks,
									'negative_marks'=>$negative_marks,
									'not_attempt_marks'=>$not_attempt_marks,
									'question_name'=>$question_name,
									'answer_type'=>$answer_type,
								);
					$last_id = $this->Main_Model->insertData($tbl='questions', $arr);

					if ($last_id) 
                    {
                    	 // echo $answer_type;die;
                    	if ($answer_type == 1) 
                    	{
                    		// 1-Single Choice
	                    	$arr_single = array(
                                                  // answer
                                                  'question_id'=>$last_id,
                                                  'answer_type'=>$answer_type,
                                                  'options_A'=>$options_A,
                                                  'options_B'=>$options_B,
                                                  'options_C'=>$options_C,
                                                  'options_D'=>$options_D,
                                                  'correct_answer'=>$sc_correct_option,
                                                  'solution'=>$solution,
                                                  'video_solution'=>$video_solution,
                                              );
	                    	 $this->Main_Model->insertData($tbl='answers_single_choice', $arr_single);
	                    }
	                    elseif ($answer_type == 2) // 2-Multiple Choice
                    	{
                    		$arr_multi_choice = array(
														// answer
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'options_A'=>$options_A,
														'options_B'=>$options_B,
														'options_C'=>$options_C,
														'options_D'=>$options_D,
														// 'correct_answer'=> $sc_correct_option,
														'solution'=>$solution,
														'video_solution'=>$video_solution,
				                                    );
		                    	
	                    	$id = $this->Main_Model->insertData($tbl='answers_multiple_choice', $arr_multi_choice);
                          
	                    	if ($id) 
	                    	{
                              	foreach($sc_correct_option as $value)
	                    		{
                                  
							        $arr_multi_opt = array(
															// options
                                      						'opt'=>$last_id,
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id,
															'correct_answer'=> $value,
					                                    );
							        $this->Main_Model->insertData($tbl='answers_option_multiple_choice', $arr_multi_opt);
							    }
	                    	}
                    	}
                    	elseif ($answer_type == 3) 
                    	{
                    		// 3-Integer

                    		$arr_integer = array(
                                                  // answer
                                                  'question_id'=>$last_id,
                                                  'answer_type'=>$answer_type,
                                                  'correct_answer'=>$correct_answer,
                                                  'solution'=>$solution,
                                                  'video_solution'=>$video_solution,
                                              );
	                    	 $this->Main_Model->insertData($tbl='answers_integer', $arr_integer);
                    	}
                    	elseif ($answer_type == 4) 
                    	{
                    		// 4-True/ False
                    		$arr_true_false = array(
														// answer
                                                      'question_id'=>$last_id,
                                                      'answer_type'=>$answer_type,
                                                      'correct_answer'=>$correct_answer,
                                                      'solution'=>$solution,
                                                      'video_solution'=>$video_solution,
                                                  );
	                    	 $this->Main_Model->insertData($tbl='answers_true_false', $arr_true_false);
                    	}elseif ($answer_type == 5) // 5-Match Matrix
                    	{
                          $arr_mm = array(
                            				// answer
                                          'question_id'=>$last_id,
                                          'answer_type'=>$answer_type,

                                          'left_opt_1'=>$left_opt_1,
                                          'left_opt_2'=>$left_opt_2,
                                          'left_opt_3'=>$left_opt_3,
                                          'left_opt_4'=>$left_opt_4,

                                          'right_opt_A'=>$right_opt_A,
                                          'right_opt_B'=>$right_opt_B,
                                          'right_opt_C'=>$right_opt_C,
                                          'right_opt_D'=>$right_opt_D,

                                          'solution'=>$solution,
                                          'video_solution'=>$video_solution,
                                        );
                          $id_mm = $this->Main_Model->insertData($tbl='answers_match_matrix', $arr_mm);
                         
                          
                          if ($id_mm) 
                          {
                            foreach($mm_correct_option_1 as $value)
                            {
                              $arr_mm_opt1 = array(
                                                    // options
                                                    'question_id'=>$last_id,
                                                    'answer_type'=>$answer_type,
                                                    'answer_id'=>$id_mm,
                                                    'correct_options_l'=>1,
                                                    'correct_options_r'=> $value,
                                                  );
                              $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt1);
                            }                    			

                            foreach($mm_correct_option_2 as $value)
                            {
                              $arr_mm_opt2 = array(
                                                      // options
                                                      'question_id'=>$last_id,
                                                      'answer_type'=>$answer_type,
                                                      'answer_id'=>$id_mm,
                                                      'correct_options_l'=>2,
                                                      'correct_options_r'=> $value,
                                                    );
                              $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt2);
                            }     

                            foreach($mm_correct_option_3 as $value)
                            {
                              $arr_mm_opt3 = array(
                                                      // options
                                                      'question_id'=>$last_id,
                                                      'answer_type'=>$answer_type,
                                                      'answer_id'=>$id_mm,
                                                      'correct_options_l'=>3,
                                                      'correct_options_r'=> $value,
                                                    );
                              $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt3);
                            }     

                            foreach($mm_correct_option_4 as $value)
                            {
                              $arr_mm_opt4 = array(
                                                    // options
                                                    'question_id'=>$last_id,
                                                    'answer_type'=>$answer_type,
                                                    'answer_id'=>$id_mm,
                                                    'correct_options_l'=>4,
                                                    'correct_options_r'=> $value,
                                                  );
                              $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt4);
                            }     
                          }
                        }
                    	elseif ($answer_type == 6) 
                    	{
                    		// 6-Match The Following
                    		$arr_mf = array(
												// answer
												'question_id'=>$last_id,
												'answer_type'=>$answer_type,
												
												'left_opt_1'=>$left_opt_1,
												'left_opt_2'=>$left_opt_2,
												'left_opt_3'=>$left_opt_3,
												'left_opt_4'=>$left_opt_4,

												'right_opt_A'=>$right_opt_A,
												'right_opt_B'=>$right_opt_B,
												'right_opt_C'=>$right_opt_C,
												'right_opt_D'=>$right_opt_D,

												'solution'=>$solution,
												'video_solution'=>$video_solution,
		                                    );
	                    	$id_mf = $this->Main_Model->insertData($tbl='answers_match_the_following', $arr_mf);
	                    	// echo $id_mf ;die;
	                    	if ($id_mf) 
                    		{
                    			$arr_mf_opt1= array(
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'answer_id'=>$id_mf,
														'correct_options_l'=>1,
														'correct_options_r'=>$correct_option1,
			                                    	);
                    			
                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt1);
                    			
                    			$arr_mf_opt2= array(
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'answer_id'=>$id_mf,
														'correct_options_l'=>2,
														'correct_options_r'=>$correct_option2,
			                                    	);
                    			
                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt2);
                    			
                    			$arr_mf_opt3= array(
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'answer_id'=>$id_mf,
														'correct_options_l'=>3,
														'correct_options_r'=>$correct_option3,
			                                    	);
                    			
                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt3);
                    			
                    			$arr_mf_opt4= array(
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'answer_id'=>$id_mf,
														'correct_options_l'=>4,
														'correct_options_r'=>$correct_option4,
			                                    	);
                    			
                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt4);                   			
                    		}
                    	}
                    }

					$this->session->set_flashdata('create', 'Questions has been added successfully..!');
					redirect(base_url().'manage_question');
				}
			/*}
			else
			{
				$this->session->set_flashdata('exists', 'Questions Already exists!');
				redirect(base_url().'add_questions');
			}*/
		}
		else
		{
			$data['board_list'] = $this->Main_Model->getAllData($tbl='board');
	        $data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
	        $data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
	        $data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
	        $data['language_list'] = $this->Main_Model->getAllData($tbl='language');

			$this->load->view('question_managment/add_questions', $data);				
		}
		
	}
  
  
	// ajax get Subject list as per selected std_id  
	public function get_subject_list()
	{
		$std_id = $this->input->post('std_id');

		$where = '(std_id = "'.$std_id.'")';
		$sub_list= $this->Main_Model->getAllData_not_order($tbl='subjects',$where);
		// print_r($sub_list);die;
		
		// $output = '<option value="" selected disabled>Select Subject </option>';
		foreach ($sub_list as $list) 
        {
            $output .='<option value="'.$list['subject_id'].'">'.$list['subject_name'].'</option>';
        }
        echo $output;
	}


	public function manage_question()
	{
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

        $std_id = $this->input->post('std_id');
		$subject_id = $this->input->post('subject_id');
		$difficulty_id = $this->input->post('difficulty_id');

        if (isset($_POST['search_questions']))  
		{
			// echo $board_id." ".$std_id;die;
			$data['questions_list'] = $this->Main_Model->getQuestionsFilterData($tbl='questions',$std_id, $subject_id, $difficulty_id);

			$data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
			$data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
	        $data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
           
			$this->load->view('question_managment/manage_question', $data);		
		}	
		else
		{
			// pagination
	        $config['base_url'] = base_url('manage_question'); 
	        $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='questions'); 
	        // echo $config['total_rows'] ;die;
	        $config['per_page'] = 10;

	        $config['first_link'] = 'First';
	        $config['last_link'] = 'Last';
	        $config['next_link'] = '>';
	        $config['prev_link'] = '<';
	        $config['full_tag_open'] = "<ul class='pagination'>";
	        $config['full_tag_close'] ="</ul>";
	        $config['num_tag_open']= '<li class="page-item">';
	        $config['num_tag_close']='</li>';
	        $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
	                                    <a href='#' class='page-link'>";
	        $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
	        $config['next_tag_open'] ="<li class='page-item'>";
	        $config['next_tag1_close'] ="</li>";
	        $config['prev_tag_open'] ="<li>";
	        $config['prev_tag1_close'] ="<li class='page-item'>";
	        $config['first_tag_open'] ="<li>";
	        $config['first_tag1_close'] ="<li class='page-item'>";
	        $config['last_tag_open'] = "<li>";
	        $config['last_tag1_close'] = "<li class='page-item'>";
	        $config['attributes'] = array('class' => 'page-link wid'); 

	        $this->pagination->initialize($config);

	        // fet records
	        $data['board_list'] = $this->Main_Model->getAllData($tbl='board');
	        $data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
	        $data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
	        $data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
	        $data['language_list'] = $this->Main_Model->getAllData($tbl='language');

		    // question records
	        $order_by = ('question_id desc');
			$data['questions_list']= $this->Main_Model->getAllRecords($tbl='questions', $config['per_page'], $this->uri->segment(2), $order_by);
			//$this->Main_Model->getAllOrderData($tbl='questions', $order_by);

			$this->load->view('question_managment/manage_question', $data);		
		}    
	}
	
	
	public function get_manage_question_record()
	{
		$question_id = $this->input->post('question_id');

		$where = '(question_id="'.$question_id.'")';
		$c_que=$this->Main_Model->getData($tbl='questions',$where);

		if(!empty($c_que))
        {
        	// 1-Single Choice
        	if ($c_que['answer_type'] == 1 ) 
        	{ 
        		// 1-Single Choice
        		$ans= $this->Main_Model->getData($tbl='answers_single_choice',$where);

        		$final_quetions = array(
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,

                                        	'answer_type'=>$ans['answer_type'] ,
                                    		'options_A'=>$ans['options_A'] ,
                                    		'options_B'=>$ans['options_B'] ,
                                    		'options_C'=>$ans['options_C'] ,
                                    		'options_D'=>$ans['options_D'] ,
                                    		'correct_answer'=>$ans['correct_answer'] ,
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}
        	
        	// 2-Multiple Choice
        	if ($c_que['answer_type'] == 2 ) 
        	{
        		// 2-Multiple Choice
        		$ans= $this->Main_Model->getData($tbl='answers_multiple_choice',$where);

        		$check_options = $this->Main_Model->getAllData_not_order($tbl='answers_option_multiple_choice',$where);

        		$final_opt=array();
        		foreach ($check_options as $c_opt) 
        		{
        			$final_opt[] = array(
        									//'question_id'=>$c_que['question_id'] ,
        									'opt_id'=>$c_opt['opt_id'],
        									'answer_type'=>$c_opt['answer_type'],
	        								'answer_id'=>$c_opt['answer_id'],
			                                'correct_answer'=>$c_opt['correct_answer'],
                            		);
            	}

        		$final_quetions = array(
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                        	'answer_type'=>$ans['answer_type'] ,
                                    		'options_A'=>$ans['options_A'] ,
                                    		'options_B'=>$ans['options_B'] ,
                                    		'options_C'=>$ans['options_C'] ,
                                    		'options_D'=>$ans['options_D'] ,
                                    		// 'correct_answer'=>$ans['correct_answer'] ,
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,

                                        	// options
                                        	'options'=>$final_opt,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}
          
        	// 3-Integer
        	if ($c_que['answer_type'] == 3 ) 
        	{
        		// 3-Integer
        		$ans= $this->Main_Model->getData($tbl='answers_integer',$where);

        		$final_quetions = array(
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                    		'correct_answer'=>$ans['correct_answer'] ,
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}

        	// 4-True/ False 
        	if ($c_que['answer_type'] == 4 ) 
        	{
        		// 4-True/ False
        		$ans= $this->Main_Model->getData($tbl='answers_true_false',$where);

        		$final_quetions = array(
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                    		'correct_answer'=>$ans['correct_answer'] ,
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}

        	// 5-Match Matrix 
        	if ($c_que['answer_type'] == 5 ) 
        	{
        		//  5-Match Matrix
        		$ans= $this->Main_Model->getData($tbl='answers_match_matrix',$where);
        		$check_options = $this->Main_Model->getAllData_not_order($tbl='answers_option_match_matrix',$where);

        		$final_opt=array();
        		foreach ($check_options as $c_opt) 
        		{
        			$final_opt[] = array(
        									//'question_id'=>$c_que['question_id'] ,
        									'opt_id'=>$c_opt['opt_id'],
        									'answer_type'=>$c_opt['answer_type'],
	        								'answer_id'=>$c_opt['answer_id'],
			                                'correct_options_l'=>$c_opt['correct_options_l'],
			                                'correct_options_r'=>$c_opt['correct_options_r'],
                            		);
            	}
                                        
        		$final_quetions = array(
        									// quetions
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                        	// answers
                                        	'answer_type'=>$ans['answer_type'] ,
                                    		'left_opt_1'=>$ans['left_opt_1'] ,
                                    		'left_opt_2'=>$ans['left_opt_2'] ,
                                    		'left_opt_3'=>$ans['left_opt_3'] ,
                                    		'left_opt_4'=>$ans['left_opt_4'] ,

                                    		'right_opt_A'=>$ans['right_opt_A'] ,
                                    		'right_opt_B'=>$ans['right_opt_B'] ,
                                    		'right_opt_C'=>$ans['right_opt_C'] ,
                                    		'right_opt_D'=>$ans['right_opt_D'] ,
                                    		
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,

                                        	// options
                                        	'options'=>$final_opt,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}

        	// 6-Match The Following 
        	if ($c_que['answer_type'] == 6 ) 
        	{
        		// 6-Match The Following
        		$ans= $this->Main_Model->getData($tbl='answers_match_the_following',$where);
        		$check_options = $this->Main_Model->getAllData_not_order($tbl='answers_option_match_the_following',$where);

        		$final_opt=array();
        		foreach ($check_options as $c_opt) 
        		{
        			$final_opt[] = array(
        									'opt_id'=>$c_opt['opt_id'],
	        								'answer_type'=>$c_opt['answer_type'],
	        								'answer_id'=>$c_opt['answer_id'],
			                                'correct_options_l'=>$c_opt['correct_options_l'],
			                                'correct_options_r'=>$c_opt['correct_options_r'],
	                            		);
            	}
                                        
        		$final_quetions = array(
        									// quetions
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                        	// answers
                                        	'answer_type'=>$ans['answer_type'] ,
                                    		'left_opt_1'=>$ans['left_opt_1'] ,
                                    		'left_opt_2'=>$ans['left_opt_2'] ,
                                    		'left_opt_3'=>$ans['left_opt_3'] ,
                                    		'left_opt_4'=>$ans['left_opt_4'] ,

                                    		'right_opt_A'=>$ans['right_opt_A'] ,
                                    		'right_opt_B'=>$ans['right_opt_B'] ,
                                    		'right_opt_C'=>$ans['right_opt_C'] ,
                                    		'right_opt_D'=>$ans['right_opt_D'] ,
                                    		
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,

                                        	// options
                                        	'options'=>$final_opt,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}
        }
        else
        {
        	echo " ";
        }
	}

	public function edit_manage_question()
	{
		// echo $question_id;die;

		$path = 'assets/images/admin/questions/';
        $file_path = './' . $path;
        
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

        // question data
		$question_id = $this->input->post('question_id');
		$std_id = $this->input->post('std_id');
		$subject_id = $this->input->post('subject_id');
		$topic = $this->input->post('topic');
		$sub_topic = $this->input->post('sub_topic');
		$difficulty_id = $this->input->post('difficulty_id');
		$lang_id = $this->input->post('lang_id');
		$correct_marks = $this->input->post('correct_marks');
		$negative_marks = $this->input->post('negative_marks');
		$not_attempt_marks = $this->input->post('not_attempt_marks');
		$question_name = $this->input->post('question_name');
		// $editId = $this->input->post('editId');
		$answer_type = $this->input->post('answer_type');
      
      
     	//get multiple checkbox value
      	$opt_check_data = $this->input->post('sc_correct_option[]');
      
       	/*foreach($opt_check_data as $value)
        {
            echo "check value =: ".$value.'<br/>';
        }*/
      
      	// Answer parameters
		$options_A = $this->input->post('options_A');
		$options_B = $this->input->post('options_B');
		$options_C = $this->input->post('options_C');
		$options_D = $this->input->post('options_D');
		
		// common
		$solution = $this->input->post('solution');
		$video_solution = $this->input->post('video_solution');

		// ans - single choice
    	$sc_correct_option = $this->input->post('sc_correct_option');

		// integer
		$correct_answer = $this->input->post('correct_answer');

		// true_false
		$true_false = $this->input->post('true_false');

		// match_matrix
		$left_opt_1 = $this->input->post('left_opt_1');
		$left_opt_2 = $this->input->post('left_opt_2');
		$left_opt_3 = $this->input->post('left_opt_3');
		$left_opt_4 = $this->input->post('left_opt_4');

		$right_opt_A = $this->input->post('right_opt_A');
		$right_opt_B = $this->input->post('right_opt_B');
		$right_opt_C = $this->input->post('right_opt_C');
		$right_opt_D = $this->input->post('right_opt_D');

		// match_matrix - correct_options_r
		$mm_correct_option1 = $this->input->post('mm_correct_option1');
		$mm_correct_option2 = $this->input->post('mm_correct_option2');
		$mm_correct_option3 = $this->input->post('mm_correct_option3');
		$mm_correct_option4 = $this->input->post('mm_correct_option4');

		// match_matrix - correct_options_r- new
      	$mm_correct_option_1 =  $this->input->post('mm_correct_option_1');
        $mm_correct_option_2 =  $this->input->post('mm_correct_option_2');
        $mm_correct_option_3 =  $this->input->post('mm_correct_option_3');
        $mm_correct_option_4 =  $this->input->post('mm_correct_option_4');

		// match_the_following - correct_options_r
		$correct_option1 = $this->input->post('correct_option1');
		$correct_option2 = $this->input->post('correct_option2');
		$correct_option3 = $this->input->post('correct_option3');
		$correct_option4 = $this->input->post('correct_option4');

      	// echo "question_id ".$question_id." ". "answer_type ".$answer_type; die;

      	if ($answer_type == 1) // 1-Single Choice
    	{
    		if ($_FILES['images']['name'] !="")  // with image
    		{
    			if(file_exists($path . $_FILES['images']['name']))
                {
                    unlink($path . $_FILES['images']['name']);
                }               
                
                if ($this->upload->do_upload('images')) 
                {
                    $data = $this->upload->data(); 

                    // edit questions
					$arr_q = array(
									'images'=>base_url().$path.$data['file_name'],
									'std_id'=>$std_id,
									'subject_id'=>$subject_id,
									'topic'=>$topic,
									'sub_topic'=>$sub_topic,
									'difficulty_id'=>$difficulty_id,
									'lang_id'=>$lang_id,
									'correct_marks'=>$correct_marks,
									'negative_marks'=>$negative_marks,
									'not_attempt_marks'=>$not_attempt_marks,
									'question_name'=>$question_name,
									'answer_type'=>$answer_type,
								);

					$wh_q = '(question_id = "'.$question_id.'")';
					$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
					
					// edit answer
		        	$arr_single = array(
											// answer
											'answer_type'=>$answer_type,
											'options_A'=>$options_A,
											'options_B'=>$options_B,
											'options_C'=>$options_C,
											'options_D'=>$options_D,
											'correct_answer'=>$sc_correct_option,
											'solution'=>$solution,
											'video_solution'=>$video_solution,
				                        );
		 
		        	// $where = '(question_id = "'.$question_id.'")';
					$this->Main_Model->editData($tbl='answers_single_choice', $wh_q, $arr_single);
					$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
					redirect(base_url().'manage_question');
					// redirect(base_url().'manage_question/'.$this->uri->segment(2));
                }
                else
                {
                    //echo "error show";
                    $data['imageError'] = $this->upload->display_errors();
                    $this->load->view('question_managment/manage_question', $data);
                }
    		}
    		else // without image
            {
            	// edit questions
				$arr_q = array(
								'std_id'=>$std_id,
								'subject_id'=>$subject_id,
								'topic'=>$topic,
								'sub_topic'=>$sub_topic,
								'difficulty_id'=>$difficulty_id,
								'lang_id'=>$lang_id,
								'correct_marks'=>$correct_marks,
								'negative_marks'=>$negative_marks,
								'not_attempt_marks'=>$not_attempt_marks,
								'question_name'=>$question_name,
								'answer_type'=>$answer_type,
							);

				$wh_q = '(question_id = "'.$question_id.'")';
				$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
				
				// edit answer
	        	$arr_single = array(
										// answer
										'answer_type'=>$answer_type,
										'options_A'=>$options_A,
										'options_B'=>$options_B,
										'options_C'=>$options_C,
										'options_D'=>$options_D,
										'correct_answer'=>$sc_correct_option,
										'solution'=>$solution,
										'video_solution'=>$video_solution,
			                        );
	 
	        	// $where = '(question_id = "'.$question_id.'")';
				$this->Main_Model->editData($tbl='answers_single_choice', $wh_q, $arr_single);
				$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
				redirect(base_url().'manage_question');
				// redirect(base_url().'manage_question/'.$this->uri->segment(2));
            }
    		
        }
        elseif ($answer_type == 2) // 2-Multiple Choice
    	{
    		if ($_FILES['images']['name'] !="")  // with image
    		{
    			if(file_exists($path . $_FILES['images']['name']))
                {
                    unlink($path . $_FILES['images']['name']);
                }               
                
                if ($this->upload->do_upload('images')) 
                {
                    $data = $this->upload->data(); 

                    $arr_q = array(
                    					'images'=>base_url().$path.$data['file_name'],
										'std_id'=>$std_id,
										'subject_id'=>$subject_id,
										'topic'=>$topic,
										'sub_topic'=>$sub_topic,
										'difficulty_id'=>$difficulty_id,
										'lang_id'=>$lang_id,
										'correct_marks'=>$correct_marks,
										'negative_marks'=>$negative_marks,
										'not_attempt_marks'=>$not_attempt_marks,
										'question_name'=>$question_name,
										'answer_type'=>$answer_type,
									);

					$wh_q = '(question_id = "'.$question_id.'")';
					$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
					
					// edit answer
		    		$arr_multi_choice = array(
												'answer_type'=>$answer_type,
												'options_A'=>$options_A,
												'options_B'=>$options_B,
												'options_C'=>$options_C,
												'options_D'=>$options_D,
												//'correct_answer'=> $sc_correct_option,
												'solution'=>$solution,
												'video_solution'=>$video_solution,
					                        );

					$update_id = $this->Main_Model->editData($tbl='answers_multiple_choice', $wh_q, $arr_multi_choice);
					
					if ($update_id)
					{
		              	$all_correct_opt = $this->Main_Model->getAllData_not_order($tbl='answers_option_multiple_choice', $wh_q);
		              	
		              	// echo "<pre>"; print_r($all_correct_opt); echo "</pre>";	die;
		              
		              	if ($all_correct_opt) // delete
						{
		                  	$this->Main_Model->deleteData($tbl='answers_option_multiple_choice',$wh_q);
		                }
		              	
						$ans_id = $this->Main_Model->getData($tbl='answers_multiple_choice', $wh_q);
						if ($ans_id) 
						{
							$id = $ans_id['answer_id'];
						}
						else
						{
							$id = 0;
						}

		              	foreach($sc_correct_option as $value)
		              	{
		                    $opt_mc = array(
		                                      'question_id'=>$question_id,
		                                      'answer_id'=>$id,
		                                      'answer_type'=>$answer_type,
		                                      'correct_answer'=>$value,
		                                    );
		                    $this->Main_Model->insertData($tbl='answers_option_multiple_choice', $opt_mc);
		                }  
		                
		            }
		          
					$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
					redirect(base_url().'manage_question');
                }
                else
                {
                    //echo "error show";
                    $data['imageError'] = $this->upload->display_errors();
                    $this->load->view('question_managment/manage_question', $data);
                }
    		}
    		else // without image
            {
            	// edit questions
				$arr_q = array(
								'std_id'=>$std_id,
								'subject_id'=>$subject_id,
								'topic'=>$topic,
								'sub_topic'=>$sub_topic,
								'difficulty_id'=>$difficulty_id,
								'lang_id'=>$lang_id,
								'correct_marks'=>$correct_marks,
								'negative_marks'=>$negative_marks,
								'not_attempt_marks'=>$not_attempt_marks,
								'question_name'=>$question_name,
								'answer_type'=>$answer_type,
							);

				$wh_q = '(question_id = "'.$question_id.'")';
				$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
				
				// edit answer
	    		$arr_multi_choice = array(
											'answer_type'=>$answer_type,
											'options_A'=>$options_A,
											'options_B'=>$options_B,
											'options_C'=>$options_C,
											'options_D'=>$options_D,
											//'correct_answer'=> $sc_correct_option,
											'solution'=>$solution,
											'video_solution'=>$video_solution,
				                        );

				$update_id = $this->Main_Model->editData($tbl='answers_multiple_choice', $wh_q, $arr_multi_choice);
				
				if ($update_id)
				{
	              	$all_correct_opt = $this->Main_Model->getAllData_not_order($tbl='answers_option_multiple_choice', $wh_q);
	              	
	              	// echo "<pre>"; print_r($all_correct_opt); echo "</pre>";	die;
	              
	              	if ($all_correct_opt) // delete
					{
	                  	$this->Main_Model->deleteData($tbl='answers_option_multiple_choice',$wh_q);
	                }
	              	
					$ans_id = $this->Main_Model->getData($tbl='answers_multiple_choice', $wh_q);
					if ($ans_id) 
					{
						$id = $ans_id['answer_id'];
					}
					else
					{
						$id = 0;
					}

	              	foreach($sc_correct_option as $value)
	              	{
	                    $opt_mc = array(
	                                      'question_id'=>$question_id,
	                                      'answer_id'=>$id,
	                                      'answer_type'=>$answer_type,
	                                      'correct_answer'=>$value,
	                                    );
	                    $this->Main_Model->insertData($tbl='answers_option_multiple_choice', $opt_mc);
	                }  
	                
	            }
	          
				$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
				redirect(base_url().'manage_question');
            }
    	}
    	elseif ($answer_type == 3) // 3-Integer
    	{
    		if ($_FILES['images']['name'] !="")  // with image
    		{
    			if(file_exists($path . $_FILES['images']['name']))
                {
                    unlink($path . $_FILES['images']['name']);
                }               
                
                if ($this->upload->do_upload('images')) 
                {
                    $data = $this->upload->data();

                    // edit questions
					$arr_q = array(
										'images'=>base_url().$path.$data['file_name'],
										'std_id'=>$std_id,
										'subject_id'=>$subject_id,
										'topic'=>$topic,
										'sub_topic'=>$sub_topic,
										'difficulty_id'=>$difficulty_id,
										'lang_id'=>$lang_id,
										'correct_marks'=>$correct_marks,
										'negative_marks'=>$negative_marks,
										'not_attempt_marks'=>$not_attempt_marks,
										'question_name'=>$question_name,
										'answer_type'=>$answer_type,
									);

					$wh_q = '(question_id = "'.$question_id.'")';
					$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
					
					// edit answer 
		    		$arr_integer = array(
											'answer_type'=>$answer_type,
											'correct_answer'=>$correct_answer,
											'solution'=>$solution,
											'video_solution'=>$video_solution,
		                                );
		        	
					$this->Main_Model->editData($tbl='answers_integer', $wh_q, $arr_integer);
					$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
					redirect(base_url().'manage_question');
                    // 
                }
                else
                {
                    //echo "error show";
                    $data['imageError'] = $this->upload->display_errors();
                    $this->load->view('question_managment/manage_question', $data);
                }
    		}
    		else // without image
            {
            	// edit questions
				$arr_q = array(
								'std_id'=>$std_id,
								'subject_id'=>$subject_id,
								'topic'=>$topic,
								'sub_topic'=>$sub_topic,
								'difficulty_id'=>$difficulty_id,
								'lang_id'=>$lang_id,
								'correct_marks'=>$correct_marks,
								'negative_marks'=>$negative_marks,
								'not_attempt_marks'=>$not_attempt_marks,
								'question_name'=>$question_name,
								'answer_type'=>$answer_type,
							);

				$wh_q = '(question_id = "'.$question_id.'")';
				$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
				
				// edit answer 
	    		$arr_integer = array(
										'answer_type'=>$answer_type,
										'correct_answer'=>$correct_answer,
										'solution'=>$solution,
										'video_solution'=>$video_solution,
	                                );
	        	
				$this->Main_Model->editData($tbl='answers_integer', $wh_q, $arr_integer);
				$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
				redirect(base_url().'manage_question');
            }
    	}
    	elseif ($answer_type == 4) // 4-True/ False
    	{
    		if ($_FILES['images']['name'] !="")  // with image
    		{
    			if(file_exists($path . $_FILES['images']['name']))
                {
                    unlink($path . $_FILES['images']['name']);
                }               
                
                if ($this->upload->do_upload('images')) 
                {
                    $data = $this->upload->data(); 

                    $arr_q = array(
	                    				'images'=>base_url().$path.$data['file_name'],
										'std_id'=>$std_id,
										'subject_id'=>$subject_id,
										'topic'=>$topic,
										'sub_topic'=>$sub_topic,
										'difficulty_id'=>$difficulty_id,
										'lang_id'=>$lang_id,
										'correct_marks'=>$correct_marks,
										'negative_marks'=>$negative_marks,
										'not_attempt_marks'=>$not_attempt_marks,
										'question_name'=>$question_name,
										'answer_type'=>$answer_type,
									);

					$wh_q = '(question_id = "'.$question_id.'")';
					$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
					
					// edit answer 
		    		$arr_true_false = array(
												'answer_type'=>$answer_type,
												'correct_answer'=>$correct_answer,
												'solution'=>$solution,
												'video_solution'=>$video_solution,
			                                );
		    		
					$this->Main_Model->editData($tbl='answers_true_false', $wh_q, $arr_true_false);
					$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
					redirect(base_url().'manage_question');
                    //
                }
                else
                {
                    //echo "error show";
                    $data['imageError'] = $this->upload->display_errors();
                    $this->load->view('question_managment/manage_question', $data);
                }
    		}
    		else // without image
            {
            	// edit questions
				$arr_q = array(
								'std_id'=>$std_id,
								'subject_id'=>$subject_id,
								'topic'=>$topic,
								'sub_topic'=>$sub_topic,
								'difficulty_id'=>$difficulty_id,
								'lang_id'=>$lang_id,
								'correct_marks'=>$correct_marks,
								'negative_marks'=>$negative_marks,
								'not_attempt_marks'=>$not_attempt_marks,
								'question_name'=>$question_name,
								'answer_type'=>$answer_type,
							);

				$wh_q = '(question_id = "'.$question_id.'")';
				$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
				
				// edit answer 
	    		$arr_true_false = array(
											'answer_type'=>$answer_type,
											'correct_answer'=>$correct_answer,
											'solution'=>$solution,
											'video_solution'=>$video_solution,
		                                );
	    		
				$this->Main_Model->editData($tbl='answers_true_false', $wh_q, $arr_true_false);
				$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
				redirect(base_url().'manage_question');
            }
    	}
    	elseif ($answer_type == 5) // 5-Match Matrix
    	{
    		if ($_FILES['images']['name'] !="")  // with image
    		{
    			if(file_exists($path . $_FILES['images']['name']))
                {
                    unlink($path . $_FILES['images']['name']);
                }               
                
                if ($this->upload->do_upload('images')) 
                {
                    $data = $this->upload->data(); 

                    $arr_q = array(
                    					'images'=>base_url().$path.$data['file_name'],
										'std_id'=>$std_id,
										'subject_id'=>$subject_id,
										'topic'=>$topic,
										'sub_topic'=>$sub_topic,
										'difficulty_id'=>$difficulty_id,
										'lang_id'=>$lang_id,
										'correct_marks'=>$correct_marks,
										'negative_marks'=>$negative_marks,
										'not_attempt_marks'=>$not_attempt_marks,
										'question_name'=>$question_name,
										'answer_type'=>$answer_type,
									);

					$wh_q = '(question_id = "'.$question_id.'")';
					$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
					
					// edit answer 
		    		$arr_mm = array(
										'answer_type'=>$answer_type,
										
										'left_opt_1'=>$left_opt_1,
										'left_opt_2'=>$left_opt_2,
										'left_opt_3'=>$left_opt_3,
										'left_opt_4'=>$left_opt_4,

										'right_opt_A'=>$right_opt_A,
										'right_opt_B'=>$right_opt_B,
										'right_opt_C'=>$right_opt_C,
										'right_opt_D'=>$right_opt_D,

										'solution'=>$solution,
										'video_solution'=>$video_solution,
		                            );
					$update_id = $this->Main_Model->editData($tbl='answers_match_matrix', $wh_q, $arr_mm);
					if ($update_id)
					{
		              	$all_correct_opt = $this->Main_Model->getAllData_not_order($tbl='answers_option_match_matrix', $wh_q);
		              	
		              	// echo "<pre>"; print_r($all_correct_opt); echo "</pre>";	die;
		              
		              	if ($all_correct_opt) // delete
						{
		                  	$this->Main_Model->deleteData($tbl='answers_option_match_matrix',$wh_q);
		                }
		              	
						$ans_id = $this->Main_Model->getData($tbl='answers_match_matrix', $wh_q);
						if ($ans_id) 
						{
							$id = $ans_id['answer_id'];
						}
						else
						{
							$id = 0;
						}

						foreach($mm_correct_option_1 as $value)
		                {
		                  $arr_mm_opt1 = array(
		                                        	// options
												'question_id'=>$question_id,
												'answer_type'=>$answer_type,
												'answer_id'=>$id,	
												'correct_options_l'=>1,
												'correct_options_r'=>$value,
		                                      );
		                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt1);
		                }  
		                foreach($mm_correct_option_2 as $value)
		                {
		                  $arr_mm_opt2 = array(
		                                          // options
		                                          'question_id'=>$question_id,
		                                          'answer_type'=>$answer_type,
		                                          'answer_id'=>$id,
		                                          'correct_options_l'=>2,
		                                          'correct_options_r'=> $value,
		                                        );
		                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt2);
		                }     

		                foreach($mm_correct_option_3 as $value)
		                {
		                  $arr_mm_opt3 = array(
		                                          // options
		                                          'question_id'=>$question_id,
		                                          'answer_type'=>$answer_type,
		                                          'answer_id'=>$id,
		                                          'correct_options_l'=>3,
		                                          'correct_options_r'=> $value,
		                                        );
		                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt3);
		                }     

		                foreach($mm_correct_option_4 as $value)
		                {
		                  $arr_mm_opt4 = array(
		                                        // options
		                                        'question_id'=>$question_id,
		                                        'answer_type'=>$answer_type,
		                                        'answer_id'=>$id,
		                                        'correct_options_l'=>4,
		                                        'correct_options_r'=> $value,
		                                      );
		                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt4);
		                }     
		            }
		        	
		    		$this->session->set_flashdata('edit', 'Options details has been updated successfully..!'); 
					redirect(base_url().'manage_question');
                }
                else
                {
                    //echo "error show";
                    $data['imageError'] = $this->upload->display_errors();
                    $this->load->view('question_managment/manage_question', $data);
                }
    		}
    		else // without image
            {
            	// edit questions
				$arr_q = array(
								'std_id'=>$std_id,
								'subject_id'=>$subject_id,
								'topic'=>$topic,
								'sub_topic'=>$sub_topic,
								'difficulty_id'=>$difficulty_id,
								'lang_id'=>$lang_id,
								'correct_marks'=>$correct_marks,
								'negative_marks'=>$negative_marks,
								'not_attempt_marks'=>$not_attempt_marks,
								'question_name'=>$question_name,
								'answer_type'=>$answer_type,
							);

				$wh_q = '(question_id = "'.$question_id.'")';
				$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
				
				// edit answer 
	    		$arr_mm = array(
									'answer_type'=>$answer_type,
									
									'left_opt_1'=>$left_opt_1,
									'left_opt_2'=>$left_opt_2,
									'left_opt_3'=>$left_opt_3,
									'left_opt_4'=>$left_opt_4,

									'right_opt_A'=>$right_opt_A,
									'right_opt_B'=>$right_opt_B,
									'right_opt_C'=>$right_opt_C,
									'right_opt_D'=>$right_opt_D,

									'solution'=>$solution,
									'video_solution'=>$video_solution,
	                            );
				$update_id = $this->Main_Model->editData($tbl='answers_match_matrix', $wh_q, $arr_mm);
				if ($update_id)
				{
	              	$all_correct_opt = $this->Main_Model->getAllData_not_order($tbl='answers_option_match_matrix', $wh_q);
	              	
	              	// echo "<pre>"; print_r($all_correct_opt); echo "</pre>";	die;
	              
	              	if ($all_correct_opt) // delete
					{
	                  	$this->Main_Model->deleteData($tbl='answers_option_match_matrix',$wh_q);
	                }
	              	
					$ans_id = $this->Main_Model->getData($tbl='answers_match_matrix', $wh_q);
					if ($ans_id) 
					{
						$id = $ans_id['answer_id'];
					}
					else
					{
						$id = 0;
					}

					foreach($mm_correct_option_1 as $value)
	                {
	                  $arr_mm_opt1 = array(
	                                        	// options
											'question_id'=>$question_id,
											'answer_type'=>$answer_type,
											'answer_id'=>$id,	
											'correct_options_l'=>1,
											'correct_options_r'=>$value,
	                                      );
	                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt1);
	                }  
	                foreach($mm_correct_option_2 as $value)
	                {
	                  $arr_mm_opt2 = array(
	                                          // options
	                                          'question_id'=>$question_id,
	                                          'answer_type'=>$answer_type,
	                                          'answer_id'=>$id,
	                                          'correct_options_l'=>2,
	                                          'correct_options_r'=> $value,
	                                        );
	                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt2);
	                }     

	                foreach($mm_correct_option_3 as $value)
	                {
	                  $arr_mm_opt3 = array(
	                                          // options
	                                          'question_id'=>$question_id,
	                                          'answer_type'=>$answer_type,
	                                          'answer_id'=>$id,
	                                          'correct_options_l'=>3,
	                                          'correct_options_r'=> $value,
	                                        );
	                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt3);
	                }     

	                foreach($mm_correct_option_4 as $value)
	                {
	                  $arr_mm_opt4 = array(
	                                        // options
	                                        'question_id'=>$question_id,
	                                        'answer_type'=>$answer_type,
	                                        'answer_id'=>$id,
	                                        'correct_options_l'=>4,
	                                        'correct_options_r'=> $value,
	                                      );
	                  $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt4);
	                }     
	            }
	        	
	    		$this->session->set_flashdata('edit', 'Options details has been updated successfully..!'); 
				redirect(base_url().'manage_question');
            }
    	}
  		elseif ($answer_type == 6) // 6-Match The Following
    	{
    		if ($_FILES['images']['name'] !="")  // with image
    		{
    			if(file_exists($path . $_FILES['images']['name']))
                {
                    unlink($path . $_FILES['images']['name']);
                }               
                
                if ($this->upload->do_upload('images')) 
                {
                    $data = $this->upload->data(); 

                    $arr_q = array(
	                    				'images'=>base_url().$path.$data['file_name'],
										'std_id'=>$std_id,
										'subject_id'=>$subject_id,
										'topic'=>$topic,
										'sub_topic'=>$sub_topic,
										'difficulty_id'=>$difficulty_id,
										'lang_id'=>$lang_id,
										'correct_marks'=>$correct_marks,
										'negative_marks'=>$negative_marks,
										'not_attempt_marks'=>$not_attempt_marks,
										'question_name'=>$question_name,
										'answer_type'=>$answer_type,
									);

					$wh_q = '(question_id = "'.$question_id.'")';
					$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
					
					// edit answer 
		    		$arr_mf = array(
										'answer_type'=>$answer_type,
										
										'left_opt_1'=>$left_opt_1,
										'left_opt_2'=>$left_opt_2,
										'left_opt_3'=>$left_opt_3,
										'left_opt_4'=>$left_opt_4,

										'right_opt_A'=>$right_opt_A,
										'right_opt_B'=>$right_opt_B,
										'right_opt_C'=>$right_opt_C,
										'right_opt_D'=>$right_opt_D,

										'solution'=>$solution,
										'video_solution'=>$video_solution,
		                            );
		        	
					$update_id = $this->Main_Model->editData($tbl='answers_match_the_following', $wh_q, $arr_mf);

		        	if ($update_id) 
		    		{
		    			$opt_id_1 = $this->input->post('opt_id_1');
		    			$wh_q_1 = '(question_id = "'.$question_id.'" AND opt_id = "'.$opt_id_1.'")';

		    			$arr_mf_opt1 = array(
												'answer_type'=>$answer_type,
												// 'answer_id'=>$id_mf,
												'correct_options_l'=>1,
												'correct_options_r'=>$correct_option1,
		                                	);
		    			$this->Main_Model->editData($tbl='answers_option_match_the_following',$wh_q_1, $arr_mf_opt1);
		    			//

		    			$opt_id_2 = $this->input->post('opt_id_2');
		    			$wh_q_2 = '(question_id = "'.$question_id.'" AND opt_id = "'.$opt_id_2.'")';
		    			
		    			$arr_mf_opt2 = array(
												'answer_type'=>$answer_type,
												// 'answer_id'=>$id_mf,
												'correct_options_l'=>2,
												'correct_options_r'=>$correct_option2,
		                                	);
		    			
		    			$this->Main_Model->editData($tbl='answers_option_match_the_following',$wh_q_2, $arr_mf_opt2);
		    			//

		    			$opt_id_3 = $this->input->post('opt_id_3');
		    			$wh_q_3 = '(question_id = "'.$question_id.'" AND opt_id = "'.$opt_id_3.'")';
		    			
		    			$arr_mf_opt3 = array(
												'answer_type'=>$answer_type,
												// 'answer_id'=>$id_mf,
												'correct_options_l'=>3,
												'correct_options_r'=>$correct_option3,
		                                	);
		    			
		    			$this->Main_Model->editData($tbl='answers_option_match_the_following',$wh_q_3, $arr_mf_opt3);
		    			//

		    			$opt_id_4 = $this->input->post('opt_id_4');
		    			$wh_q_4 = '(question_id = "'.$question_id.'" AND opt_id = "'.$opt_id_4.'")';
		    			
		    			$arr_mf_opt4 = array(
												'answer_type'=>$answer_type,
												// 'answer_id'=>$id_mf,
												'correct_options_l'=>4,
												'correct_options_r'=>$correct_option4,
		                                	);
		    			
		    			$this->Main_Model->editData($tbl='answers_option_match_the_following',$wh_q_4, $arr_mf_opt4);
		    		}
		    		$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
					redirect(base_url().'manage_question');
                }
                else
                {
                    //echo "error show";
                    $data['imageError'] = $this->upload->display_errors();
                    $this->load->view('question_managment/manage_question', $data);
                }
    		}
    		else // without image
            {
            	// edit questions
				$arr_q = array(
								'std_id'=>$std_id,
								'subject_id'=>$subject_id,
								'topic'=>$topic,
								'sub_topic'=>$sub_topic,
								'difficulty_id'=>$difficulty_id,
								'lang_id'=>$lang_id,
								'correct_marks'=>$correct_marks,
								'negative_marks'=>$negative_marks,
								'not_attempt_marks'=>$not_attempt_marks,
								'question_name'=>$question_name,
								'answer_type'=>$answer_type,
							);

				$wh_q = '(question_id = "'.$question_id.'")';
				$this->Main_Model->editData($tbl='questions', $wh_q, $arr_q);
				
				// edit answer 
	    		$arr_mf = array(
									'answer_type'=>$answer_type,
									
									'left_opt_1'=>$left_opt_1,
									'left_opt_2'=>$left_opt_2,
									'left_opt_3'=>$left_opt_3,
									'left_opt_4'=>$left_opt_4,

									'right_opt_A'=>$right_opt_A,
									'right_opt_B'=>$right_opt_B,
									'right_opt_C'=>$right_opt_C,
									'right_opt_D'=>$right_opt_D,

									'solution'=>$solution,
									'video_solution'=>$video_solution,
	                            );
	        	
				$update_id = $this->Main_Model->editData($tbl='answers_match_the_following', $wh_q, $arr_mf);

	        	if ($update_id) 
	    		{
	    			$opt_id_1 = $this->input->post('opt_id_1');
	    			$wh_q_1 = '(question_id = "'.$question_id.'" AND opt_id = "'.$opt_id_1.'")';

	    			$arr_mf_opt1 = array(
											'answer_type'=>$answer_type,
											// 'answer_id'=>$id_mf,
											'correct_options_l'=>1,
											'correct_options_r'=>$correct_option1,
	                                	);
	    			$this->Main_Model->editData($tbl='answers_option_match_the_following',$wh_q_1, $arr_mf_opt1);
	    			//

	    			$opt_id_2 = $this->input->post('opt_id_2');
	    			$wh_q_2 = '(question_id = "'.$question_id.'" AND opt_id = "'.$opt_id_2.'")';
	    			
	    			$arr_mf_opt2 = array(
											'answer_type'=>$answer_type,
											// 'answer_id'=>$id_mf,
											'correct_options_l'=>2,
											'correct_options_r'=>$correct_option2,
	                                	);
	    			
	    			$this->Main_Model->editData($tbl='answers_option_match_the_following',$wh_q_2, $arr_mf_opt2);
	    			//

	    			$opt_id_3 = $this->input->post('opt_id_3');
	    			$wh_q_3 = '(question_id = "'.$question_id.'" AND opt_id = "'.$opt_id_3.'")';
	    			
	    			$arr_mf_opt3 = array(
											'answer_type'=>$answer_type,
											// 'answer_id'=>$id_mf,
											'correct_options_l'=>3,
											'correct_options_r'=>$correct_option3,
	                                	);
	    			
	    			$this->Main_Model->editData($tbl='answers_option_match_the_following',$wh_q_3, $arr_mf_opt3);
	    			//

	    			$opt_id_4 = $this->input->post('opt_id_4');
	    			$wh_q_4 = '(question_id = "'.$question_id.'" AND opt_id = "'.$opt_id_4.'")';
	    			
	    			$arr_mf_opt4 = array(
											'answer_type'=>$answer_type,
											// 'answer_id'=>$id_mf,
											'correct_options_l'=>4,
											'correct_options_r'=>$correct_option4,
	                                	);
	    			
	    			$this->Main_Model->editData($tbl='answers_option_match_the_following',$wh_q_4, $arr_mf_opt4);
	    		}
	    		$this->session->set_flashdata('edit', 'Questions and Answer details has been updated successfully..!'); 
				redirect(base_url().'manage_question');
            }
    		
    	}
  	}

	public function delete_manage_question($question_id)
	{
		// echo $question_id;
		$where = '(question_id="'.$question_id.'")';
		$this->Main_Model->deleteData($tbl='questions',$where);
		$this->Main_Model->deleteData($tbl='answers_single_choice',$where);
		$this->Main_Model->deleteData($tbl='answers_multiple_choice',$where);
		$this->Main_Model->deleteData($tbl='answers_option_multiple_choice',$where);
		
		$this->Main_Model->deleteData($tbl='answers_integer',$where);
		$this->Main_Model->deleteData($tbl='answers_true_false',$where);
		$this->Main_Model->deleteData($tbl='answers_match_matrix',$where);
		$this->Main_Model->deleteData($tbl='answers_option_match_matrix',$where);

		$this->Main_Model->deleteData($tbl='answers_match_the_following',$where);
		$this->Main_Model->deleteData($tbl='answers_option_match_the_following',$where);
	}


	public function active_question_status() 
    {
        $status = $this->input->post('status'); 

        $question_id = $this->input->post('question_id');
        // echo $question_id;die;         
        $where = '(question_id="'.$question_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='questions',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function upload_question()
	{
		$this->load->library('excel');
		$user_id = $this->session->userdata('user_id');
	    if (empty($user_id)) 
	    {
	        $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
	        redirect(base_url());
	    }

		if(isset($_FILES["uploadFile"]["name"]))
	    {
	      	$path = $_FILES["uploadFile"]["tmp_name"];
	        $object = PHPExcel_IOFactory::load($path);
	        
	        $data = array();

	        foreach($object->getWorksheetIterator() as $worksheet)
	        {
	            $highestRow = $worksheet->getHighestRow();
	            $highestColumn = $worksheet->getHighestColumn();

	            for($row=2; $row<=$highestRow; $row++)
	            {
	              	$std_id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
	                $subject_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
	                $topic = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
	                $sub_topic = $worksheet->getCellByColumnAndRow(3, $row)->getValue();  
	                $difficulty_id = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
	                $lang_id = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
	                $correct_marks = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
	                $negative_marks = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
	                $not_attempt_marks = $worksheet->getCellByColumnAndRow(8, $row)->getValue();  
	                $question_name = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
	                $answer_type = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
	                
	                $correct_answer = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
	                $solution = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
	                $video_solution = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
	                
	                //   answers_single_choice, answers_multiple_choice
	                $options_A = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
	                $options_B = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
	                $options_C = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
	                $options_D = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
	                // $correct_answer_sc = $worksheet->getCellByColumnAndRow(18, $row)->getValue();

	                // match_matrix
	                $left_opt_1 = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
	                $left_opt_2 = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
	                $left_opt_3 = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
	                $left_opt_4 = $worksheet->getCellByColumnAndRow(21, $row)->getValue();

	                $right_opt_A = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
	                $right_opt_B = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
	                $right_opt_C = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
	                $right_opt_D = $worksheet->getCellByColumnAndRow(25, $row)->getValue();

	                $correct_options_l = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
	                $correct_options_r = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
	                
		
	                $data_1 = array(
	                                    'std_id' => $std_id,
	                                    'subject_id' => $subject_id,
	                                    'topic' => $topic,
	                                    'sub_topic' => $sub_topic,
	                                    'difficulty_id' => $difficulty_id,
	                                    'lang_id' => $lang_id,
	                                    'correct_marks'=>$correct_marks,
						                'negative_marks' => $negative_marks,

						                'not_attempt_marks' => $not_attempt_marks,
	                                    'question_name' => $question_name,
	                                    'answer_type' => $answer_type,
	                                    'status' => 1, // 0-Deactive, 1-Active  
	                                );
	                $wh_que = '(question_name = "'.$question_name.'" AND answer_type = "'.$answer_type.'")';
		            // $wh_que = '(question_name = "'.$question_name.'")';
		    		$que= $this->Main_Model->getData($tbl='questions', $wh_que);
		    		if (sizeof($que) == 0) 
    				{
		                $last_id = $this->Main_Model->insertData($tbl='questions', $data_1);
		                if($last_id)
			            {
			            	if ($answer_type == 1) 
		                    { 
				              	$arr_single = array(
														// answer
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'solution'=>$solution,
														'video_solution'=>$video_solution,

														'options_A'=>$options_A,
														'options_B'=>$options_B,
														'options_C'=>$options_C,
														'options_D'=>$options_D,
														'correct_answer'=>$correct_answer,
					                                  );

			              		$insert=$this->Main_Model->insertData($tbl='answers_single_choice', $arr_single);
			              	}

			              	if ($answer_type == 2) 
		                    { 
				              	$arr_multi_choice = array(
														// answer
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'solution'=>$solution,
														'video_solution'=>$video_solution,

														'options_A'=>$options_A,
														'options_B'=>$options_B,
														'options_C'=>$options_C,
														'options_D'=>$options_D,
														'correct_answer'=>$correct_answer,
					                                  );

			              		$insert=$this->Main_Model->insertData($tbl='answers_multiple_choice', $arr_multi_choice);
			              	}

			            	if ($answer_type == 3) 
		                    {
				              	$arr_integer = array(
										                  // answer
										                  'question_id'=>$last_id,
										                  'answer_type'=>$answer_type,
										                  'correct_answer'=>$correct_answer,
										                  'solution'=>$solution,
										                  'video_solution'=>$video_solution,
					                                  );

			              		$insert=$this->Main_Model->insertData($tbl='answers_integer', $arr_integer);
			              	}
			              	
			              	if ($answer_type == 4) 
		                	{
		                		// 4-True/ False
		                		$arr_true_false = array(
															// answer
														'question_id'=>$last_id,
														'answer_type'=>$answer_type,
														'correct_answer'=>$correct_answer,
														'solution'=>$solution,
														'video_solution'=>$video_solution,
				                                    );
		                    	$insert = $this->Main_Model->insertData($tbl='answers_true_false', $arr_true_false);
		                	}

		                	if ($answer_type == 5) 
		                	{
		                		// 5-Match Matrix
		                		$arr_mm = array(
													// answer
													'question_id'=>$last_id,
													'answer_type'=>$answer_type,
													
													'left_opt_1'=>$left_opt_1,
													'left_opt_2'=>$left_opt_2,
													'left_opt_3'=>$left_opt_3,
													'left_opt_4'=>$left_opt_4,

													'right_opt_A'=>$right_opt_A,
													'right_opt_B'=>$right_opt_B,
													'right_opt_C'=>$right_opt_C,
													'right_opt_D'=>$right_opt_D,

													'solution'=>$solution,
													'video_solution'=>$video_solution,
			                                    );
		                    	$id_mm=$this->Main_Model->insertData($tbl='answers_match_matrix', $arr_mm);

		                    	if ($id_mm) 
	                    		{
	                    			$l =(explode(",",$correct_options_l));
									$correct_opt_l1 = $l[0];
									$correct_opt_l2 = $l[1];
									$correct_opt_l3 = $l[2];
									$correct_opt_l4 = $l[3];

	                    			$r =(explode(",",$correct_options_r));
									$correct_opt_r1 = $r[0];
									$correct_opt_r2 = $r[1];
									$correct_opt_r3 = $r[2];
									$correct_opt_r4 = $r[3];
									
	                    			$arr_mm_opt1= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mm,
															'correct_options_l'=>$correct_opt_l1,
															'correct_options_r'=>$correct_opt_r1,
				                                    	);
	                    			
	                    			$this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt1);

                        			$arr_mm_opt2= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mm,
															'correct_options_l'=>$correct_opt_l2,
															'correct_options_r'=>$correct_opt_r2,
				                                    	);
                        			
                        			$this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt2);
                        			
                        			$arr_mm_opt3= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mm,
															'correct_options_l'=>$correct_opt_l3,
															'correct_options_r'=>$correct_opt_r3,
				                                    	);
                        			
                        			$this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt3);
                        			
                        			$arr_mm_opt4= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mm,
															'correct_options_l'=>$correct_opt_l4,
															'correct_options_r'=>$correct_opt_r4,
				                                    );
                        			
                        			$insert = $this->Main_Model->insertData($tbl='answers_option_match_matrix', $arr_mm_opt4);
	                    		}
		                	}

		                	if ($answer_type == 6) 
		                	{
		                		// 6-Match The Following
		                		$arr_mf = array(
													// answer
													'question_id'=>$last_id,
													'answer_type'=>$answer_type,
													
													'left_opt_1'=>$left_opt_1,
													'left_opt_2'=>$left_opt_2,
													'left_opt_3'=>$left_opt_3,
													'left_opt_4'=>$left_opt_4,

													'right_opt_A'=>$right_opt_A,
													'right_opt_B'=>$right_opt_B,
													'right_opt_C'=>$right_opt_C,
													'right_opt_D'=>$right_opt_D,

													'solution'=>$solution,
													'video_solution'=>$video_solution,
			                                    );
		                    	$id_mf=$this->Main_Model->insertData($tbl='answers_match_the_following', $arr_mf);

		                    	if ($id_mf) 
	                    		{
	                    			$l =(explode(",",$correct_options_l));
									$correct_opt_l1 = $l[0];
									$correct_opt_l2 = $l[1];
									$correct_opt_l3 = $l[2];
									$correct_opt_l4 = $l[3];

	                    			$r =(explode(",",$correct_options_r));
									$correct_opt_r1 = $r[0];
									$correct_opt_r2 = $r[1];
									$correct_opt_r3 = $r[2];
									$correct_opt_r4 = $r[3];
	                    			$arr_mf_opt1= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mf,
															'correct_options_l'=>$correct_opt_l1,
															'correct_options_r'=>$correct_opt_r1,
				                                    	);
	                    			
	                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt1);

	                    			$arr_mf_opt2= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mf,
															'correct_options_l'=>$correct_opt_l2,
															'correct_options_r'=>$correct_opt_r2,
				                                    	);
	                    			
	                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt2);
	                    			
	                    			$arr_mf_opt3= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mf,
															'correct_options_l'=>$correct_opt_l3,
															'correct_options_r'=>$correct_opt_r3,
				                                    	);
	                    			
	                    			$this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt3);
	                    			
	                    			$arr_mf_opt4= array(
															'question_id'=>$last_id,
															'answer_type'=>$answer_type,
															'answer_id'=>$id_mf,
															'correct_options_l'=>$correct_opt_l4,
															'correct_options_r'=>$correct_opt_r4,
				                                    	);
	                    			
	                    			$insert = $this->Main_Model->insertData($tbl='answers_option_match_the_following', $arr_mf_opt4);
	                    		}
		                	}
			            } // end if($last_id)
			        } else
		            {
		                $this->session->set_flashdata('exists','Upload failed! This file already exist!');
		            	redirect(base_url('manage_question'));
		            }
	            } // for loop end
	        } // foreach loop end    
            
			if($insert)
	        {
	            $this->session->set_flashdata('edit','Data Imported successfully');
	         	redirect(base_url('manage_question'));
	        }
	        else
	        {
	            $this->session->set_flashdata('exists','Something went wrong');
	          	redirect(base_url('manage_question'));
	        }
          
	    }
	    else
	    {
	      $this->load->view('question_managment/upload_question');
	    }
	}

	/* only question added here
	public function upload_question()
	{
		$this->load->library('excel');
		$user_id = $this->session->userdata('user_id');
        if (empty($user_id)) 
        {
            $this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
            redirect(base_url());
        }

		if(isset($_FILES["uploadFile"]["name"]))
        {
        	$path = $_FILES["uploadFile"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            
            $data = array();

            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();

                for($row=2; $row<=$highestRow; $row++)
                {
              		$std_id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $subject_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $topic = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                   	$sub_topic = $worksheet->getCellByColumnAndRow(3, $row)->getValue();	
                    $difficulty_id = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $lang_id = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $correct_marks = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                   	$negative_marks = $worksheet->getCellByColumnAndRow(7, $row)->getValue();

                   	$not_attempt_marks = $worksheet->getCellByColumnAndRow(8, $row)->getValue();	
                    $question_name = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $answer_type = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    
                    $data[] = array(
                                        'std_id' => $std_id,
                                        'subject_id' => $subject_id,
                                        'topic' => $topic,
                                        'sub_topic' => $sub_topic,
                                        'difficulty_id' => $difficulty_id,
                                        'lang_id' => $lang_id,
                                        'correct_marks'=>$correct_marks,
										'negative_marks' => $negative_marks,

										'not_attempt_marks' => $not_attempt_marks,
                                        'question_name' => $question_name,
                                        'answer_type' => $answer_type,
                                        //'correct_answer'=>$correct_answer,
										//'solution' => $solution,
										//'video_solution' => $video_solution,
                                        'status' => 1, // 0-Deactive, 1-Active	
                                    );
                } // for loop end
            } // foreach loop end

            $insert = $this->Main_Model->insertExcelData($data);

            if($insert)
            {
                $this->session->set_flashdata('edit','Data Imported successfully');
            	redirect(base_url('manage_question'));
            }
            else
            {
                $this->session->set_flashdata('edit','Something went wrong');
            	redirect(base_url('upload_question'));
            }
        }
        else
        {
        	 $this->load->library('excel');
        	$this->load->view('question_managment/upload_question');
        }
		
	}
	*/

	public function aside_question()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}


		$this->load->view('aside_question');
	}
	
	//Test Managment
	public function aside_test()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$this->load->view('aside_test');
	}

	// add test
	public function add_test()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$path = 'assets/images/admin/test_list/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);


		$category_id = $this->input->post('category_id');
		$test_series_id = $this->input->post('test_series_id');
		$test_name = $this->input->post('test_name');
		$std_id = $this->input->post('std_id');
        $subject_id = $this->input->post('subject_id');
        $topic = $this->input->post('topic');
        $sub_topic = $this->input->post('sub_topic');
        $difficulty_id = $this->input->post('difficulty_id');
        $lang_id = $this->input->post('lang_id');
        $test_series_type = $this->input->post('test_series_type');
        $t_questions = $this->input->post('t_questions');
        $t_marks = $this->input->post('t_marks');
        $t_duration = $this->input->post('t_duration');
        $correct_marks = $this->input->post('correct_marks');
        $negative_marks = $this->input->post('negative_marks');
        $not_attempt_marks = $this->input->post('not_attempt_marks');
        $t_instructions = $this->input->post('t_instructions');

		if (isset($_POST['add_test_info'])) 
		{
			// ADD questions
			if ($_FILES['test_image']['name'] !="") 
			{				
				if(file_exists($path . $_FILES['test_image']['name']))
                {
                    unlink($path . $_FILES['test_image']['name']);
                }   			
				
				if ($this->upload->do_upload('test_image')) 
				{
					$data = $this->upload->data(); 
		           
		            $arr = array(
		                            'category_id'=>$category_id,
		                            'test_series_id'=>$test_series_id,
		                            'test_name'=>$test_name,
		                            'std_id'=>$std_id,
		                            'subject_id'=>$subject_id,
		                            'topic'=>$topic,
		                            'sub_topic'=>$sub_topic,
		                            'difficulty_id'=>$difficulty_id,
		                            'lang_id'=>$lang_id,
		                            'test_series_type'=>$test_series_type,
		                            't_questions'=>$t_questions,
		                            't_marks'=>$t_marks,
		                            't_duration'=>$t_duration,
		                            'correct_marks'=>$correct_marks,
		                            'negative_marks'=>$negative_marks,
		                            'not_attempt_marks'=>$not_attempt_marks,
		                            't_instructions'=>$t_instructions,
		                            'test_image'=>base_url().$path.$data['file_name'],
		                        );

		            $this->Main_Model->insertData($tbl='test_list', $arr);
		            $this->session->set_flashdata('create', 'Test details has been added successfully..!');
		            redirect(base_url().'manage_test');
		        }
				else
				{
					//echo "error show";
					$data['imageError'] = $this->upload->display_errors();
					$this->load->view('settings/add-category', $data);
				}
			} else {
				// 
			}
		}
		else
		{
			$data['category_list'] = $this->Main_Model->getAllData($tbl='category');
			$data['board_list'] = $this->Main_Model->getAllData($tbl='board');
			$data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
			$data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
			$data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
			$data['language_list'] = $this->Main_Model->getAllData($tbl='language');
	            
			$this->load->view('test_managment/add_test', $data);
		}
	}

	public function manage_test()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$test_series_id = $this->input->post('test_series_id');

		if (isset($_POST['search_test']))  
		{
			// echo $category_id;
			if(!empty($test_series_id))
		    {
		    	$data['category_list'] = $this->Main_Model->getAllData($tbl='category');
		    	$data['category_list'] = $this->Main_Model->getAllData($tbl='category');
				$data['board_list'] = $this->Main_Model->getAllData($tbl='board');
				$data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
				$data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
				$data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
				$data['language_list'] = $this->Main_Model->getAllData($tbl='language');
				$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

				// filter
				$data['test_lists'] = $this->Main_Model->getTestFilterData($tbl='test_list', $test_series_id);
		    	$this->load->view('test_managment/manage_test', $data);
		    }
		}
		else
		{
			$config['base_url'] = base_url('manage_test'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='test_list'); 
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            // other table records
			$data['category_list'] = $this->Main_Model->getAllData($tbl='category');
			$data['board_list'] = $this->Main_Model->getAllData($tbl='board');
			$data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
			$data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
			$data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
			$data['language_list'] = $this->Main_Model->getAllData($tbl='language');
			$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

			// get test list
			$order_by = ('test_id desc');
	        $data['test_lists']= $this->Main_Model->getAllRecords($tbl='test_list', $config['per_page'], $this->uri->segment(2), $order_by);
	        // $data['test_lists'] = $this->Main_Model->getAllOrderData($tbl='test_list', $order_by);

			$this->load->view('test_managment/manage_test', $data);
		}	
	}

	public function manage_test_list()
	{
		$test_series_id = $this->uri->segment(3);

		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$where = '(test_series_id="'.$test_series_id.'")';

		$config['base_url'] = base_url().'manage_test_list/'.$test_series_id; 
        $config['total_rows'] = $this->Main_Model->getAllArrayData_row_wh($tbl='test_list',$where); 
        // echo $config['total_rows'] ;die;
        $config['per_page'] = 10;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open']= '<li class="page-item">';
        $config['num_tag_close']='</li>';
        $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                    <a href='#' class='page-link'>";
        $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] ="<li class='page-item'>";
        $config['next_tag1_close'] ="</li>";
        $config['prev_tag_open'] ="<li>";
        $config['prev_tag1_close'] ="<li class='page-item'>";
        $config['first_tag_open'] ="<li>";
        $config['first_tag1_close'] ="<li class='page-item'>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag1_close'] = "<li class='page-item'>";
        $config['attributes'] = array('class' => 'page-link wid'); 

        $this->pagination->initialize($config);

		// other table records
		$data['category_list'] = $this->Main_Model->getAllData($tbl='category');
		$data['board_list'] = $this->Main_Model->getAllData($tbl='board');
		$data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
		$data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
		$data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
		$data['language_list'] = $this->Main_Model->getAllData($tbl='language');
		$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

		// get test list
		$order_by = ('test_id desc');
		//$where = '(test_series_id="'.$test_series_id.'")';
		$data['test_lists'] = $this->Main_Model->getAllData_as_per_order($tbl='test_list',$where,$order_by);
        //$data['test_lists']=$this->Main_Model->getAllRecords_wh($tbl='test_list',$config['per_page'], $this->uri->segment(2),$where,$order_by);
		$this->load->view('test_managment/manage_testSeriesList', $data);
	}

	// ajax  on change of Category Name , it select Test Series 
	public function get_test_series_list()
	{
		$category_id = $this->input->post('category_id');

		$where = '(category_id = "'.$category_id.'")';
		$cat_list= $this->Main_Model->getAllData_not_order($tbl='test_series',$where);
		// print_r($cat_list);die;
		
		$output = '<option value="" selected disabled>Select Test Series  </option>';
		foreach ($cat_list as $list) 
        {
            $output .='<option value="'.$list['test_series_id'].'">'.$list['test_series_name'].'</option>';
        }
        echo $output;
	}

	public function get_test_series_list_for_edit() // update
	{
		$category_id = $this->input->post('category_id');

		$where = '(category_id = "'.$category_id.'")';
		$series_list= $this->Main_Model->getAllData_not_order($tbl='test_series',$where);
		// print_r($data['series_list']);
		
		// $output = '<option value=""  disabled>Select Standard 1</option>';
		foreach ($series_list as $list) 
        {
           $output .= '<option if($test_info[test_series_id] == $list[test_series_id]){ echo "selected" ;} value="'.$list['test_series_id'].'">'.$list['test_series_name'].'</option>';
        }
        echo $output;
	}

	// ajax update list as per selected std 
  	public function get_subject_list_for_edit() // update
	{
		$std_id = $this->input->post('std_id');

		$where = '(std_id = "'.$std_id.'")';
		$sub_list= $this->Main_Model->getAllData_not_order($tbl='subjects',$where);
		// print_r($data['sub_list']);
		
		// $output = '<option value=""  disabled>Select Standard 1</option>';
		foreach ($sub_list as $list) 
        {
           $output .= '<option if($test_info[subject_id] == $list[subject_id]){ echo "selected" ;} value="'.$list['subject_id'].'">'.$list['subject_name'].'</option>';
        }
        echo $output;
	}


	public function active_test_status() 
    {
        $status = $this->input->post('status'); 

        $test_id = $this->input->post('test_id');
        // echo $test_id;die;         
        $where = '(test_id="'.$test_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='test_list',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    // delete test
    public function delete_test($test_id)
    {
        // echo $test_id;
        $where = '(test_id="'.$test_id.'")';
        $this->Main_Model->deleteData($tbl='test_list',$where);
       	// delete other table belongs to this series
       	//section

    }

    public function aside_update_test()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$this->load->view('aside_update_test');
	}
	
	// edit_test
	public function edit_test($test_id)
	{
		// echo $test_id;die;
		$data['category_list'] = $this->Main_Model->getAllData($tbl='category');
		$data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
		$data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
		$data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
		$data['language_list'] = $this->Main_Model->getAllData($tbl='language');
		$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

		$this->load->view('test_managment/edit_test', $data);
	}

	// update_test_info  
	public function update_test_info($test_id)
	{
		// echo $test_id;die;
		$path = 'assets/images/admin/test_list/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$category_id = $this->input->post('category_id');
		$test_series_id = $this->input->post('test_series_id');
		$test_name = $this->input->post('test_name');
		$std_id = $this->input->post('std_id');
        $subject_id = $this->input->post('subject_id');
        $topic = $this->input->post('topic');
        $sub_topic = $this->input->post('sub_topic');
        $difficulty_id = $this->input->post('difficulty_id');
        $lang_id = $this->input->post('lang_id');
        $test_series_type = $this->input->post('test_series_type');
        $t_questions = $this->input->post('t_questions');
        $t_marks = $this->input->post('t_marks');
        $t_duration = $this->input->post('t_duration');
        $correct_marks = $this->input->post('correct_marks');
        $negative_marks = $this->input->post('negative_marks');
        $not_attempt_marks = $this->input->post('not_attempt_marks');
        $t_instructions = $this->input->post('t_instructions');

        if ($_FILES['test_image']['name'] !="") 
		{				
			if(file_exists($path . $_FILES['test_image']['name']))
            {
                unlink($path . $_FILES['test_image']['name']);
            }   			
			
			if ($this->upload->do_upload('test_image')) 
			{
				$data = $this->upload->data(); 
				$arr = array(
		                        'category_id'=>$category_id,
		                        'test_series_id'=>$test_series_id,
		                        'test_name'=>$test_name,
		                        'std_id'=>$std_id,
		                        'subject_id'=>$subject_id,
		                        'topic'=>$topic,
		                        'sub_topic'=>$sub_topic,
		                        'difficulty_id'=>$difficulty_id,
		                        'lang_id'=>$lang_id,
		                        'test_series_type'=>$test_series_type,
		                        't_questions'=>$t_questions,
		                        't_marks'=>$t_marks,
		                        't_duration'=>$t_duration,
		                        'correct_marks'=>$correct_marks,
		                        'negative_marks'=>$negative_marks,
		                        'not_attempt_marks'=>$not_attempt_marks,
		                        't_instructions'=>$t_instructions,
		                        'test_image'=>base_url().$path.$data['file_name'],
		                    );
		        $where = '(test_id="'.$test_id.'")';
		    	$this->Main_Model->editData($tbl='test_list', $where, $arr);
		        $this->session->set_flashdata('create', 'Test details has been updated successfully..!');
		        redirect(base_url().'create_section/'.$test_id);
			}
		}
		else
		{
	        $arr = array(
	                        'category_id'=>$category_id,
	                        'test_series_id'=>$test_series_id,
	                        'test_name'=>$test_name,
	                        'std_id'=>$std_id,
	                        'subject_id'=>$subject_id,
	                        'topic'=>$topic,
	                        'sub_topic'=>$sub_topic,
	                        'difficulty_id'=>$difficulty_id,
	                        'lang_id'=>$lang_id,
	                        'test_series_type'=>$test_series_type,
	                        't_questions'=>$t_questions,
	                        't_marks'=>$t_marks,
	                        't_duration'=>$t_duration,
	                        'correct_marks'=>$correct_marks,
	                        'negative_marks'=>$negative_marks,
	                        'not_attempt_marks'=>$not_attempt_marks,
	                        't_instructions'=>$t_instructions,
	                    );
	        $where = '(test_id="'.$test_id.'")';
	    	$this->Main_Model->editData($tbl='test_list', $where, $arr);
	        $this->session->set_flashdata('create', 'Test details has been updated successfully..!');
	        redirect(base_url().'create_section/'.$test_id);
	    }
	}

	// create_section
	public function create_section($test_id)
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		// echo $test_id ;die;
		// $test_id = $this->input->post('test_id');
		// $test_series_id = $this->input->post('test_series_id');
		$section_name = $this->input->post('section_name');
		$instruction = $this->input->post('instruction');

		if (isset($_POST['add_section_info']))  
		{ 
			// ADD questions
            $arr = array(
                            'section_name'=>$section_name,
                            'instruction'=>$instruction,
                            'test_id'=>$test_id,
                        );

            $this->Main_Model->insertData($tbl='section', $arr);
            $this->session->set_flashdata('create', 'Section details has been added successfully..!');
            redirect(base_url().'create_section/'.$test_id);
		}
		else
		{
			// echo $test_id ;die;
			// pagination - create_section
			$where = '(test_id="'.$test_id.'")';
			// $config['base_url'] = base_url('create_section/'.$test_id); 
			$config['base_url'] = base_url('create_section');
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row_wh($tbl='section',$where);  
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            // fet records
	        $data['board_list'] = $this->Main_Model->getAllData($tbl='board');
	        $data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
	        $data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
	        $data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
	        $data['language_list'] = $this->Main_Model->getAllData($tbl='language');

			$order_by = ('section_id desc');
            $data['section_list'] = $this->Main_Model->getAllData_as_per_order($tbl='section',$where,$order_by);
            //$this->Main_Model->getAllRecords_wh($tbl='section',$where,$config['per_page'],$this->uri->segment(2),$order_by);
            
            $data['section_list_count'] = count($data['section_list']);
            // echo "<pre>"; print_r($data['section_list']); echo "<pre>";die;
            
			$this->load->view('test_managment/create_section', $data);
		}
		
	}

	public function edit_section($section_id)
    {
    	// echo $section_id;die;
    	$test_id = $this->input->post('test_id');
    	$section_name = $this->input->post('section_name');
        $instruction = $this->input->post('instruction');

        $arr = array(
                        'section_name'=>$section_name,
                        'instruction'=>$instruction,
                    );
        $where = '(section_id = "'.$section_id.'")';
        $this->Main_Model->editData($tbl='section', $where, $arr);

        $this->session->set_flashdata('edit', 'Section Name has been updated successfully..!');
       redirect(base_url().'create_section/'.$test_id);

    }

    public function delete_section($section_id)
    {
        // echo $section_id;
        $where = '(section_id="'.$section_id.'")';
        $this->Main_Model->deleteData($tbl='section',$where);
        $this->Main_Model->deleteData($tbl='import_question',$where);
    }

	// import_question
	/*public function import_question()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		} 

		// $test_id = $this->uri->segment(2);
		// $test_id = $this->input->post('test_id');
		// $section_id = $this->input->post('section_id');
		 
		$section_id = $this->uri->segment(2);
		$wh_i = '(section_id="'.$section_id.'")';
        $section_id_info = $this->Main_Model->getData($tbl='section',$wh_i);

		// echo $section_id ;die;
		if (empty($section_id_info)) 
		{
			$this->session->set_flashdata('section_id', 'Section is Mandatory');
			redirect(base_url().'manage_testSeries');
		} 

		// pagination
        $config['base_url'] = base_url('import_question'); 
        $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='questions'); 
        // echo $config['total_rows'] ;die;
        $config['per_page'] = 15;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open']= '<li class="page-item">';
        $config['num_tag_close']='</li>';
        $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                    <a href='#' class='page-link'>";
        $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] ="<li class='page-item'>";
        $config['next_tag1_close'] ="</li>";
        $config['prev_tag_open'] ="<li>";
        $config['prev_tag1_close'] ="<li class='page-item'>";
        $config['first_tag_open'] ="<li>";
        $config['first_tag1_close'] ="<li class='page-item'>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag1_close'] = "<li class='page-item'>";
        $config['attributes'] = array('class' => 'page-link wid'); 

        $this->pagination->initialize($config);

		// fet records
        $data['board_list'] = $this->Main_Model->getAllData($tbl='board');
        $data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
        $data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
        $data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
        $data['language_list'] = $this->Main_Model->getAllData($tbl='language');

	    // question records
        $order_by = ('question_id desc');
		$data['questions_list']= $this->Main_Model->getAllOrderData($tbl='questions', $order_by);
		//$this->Main_Model->getAllRecords($tbl='questions', $config['per_page'], $this->uri->segment(2), $order_by);

		$this->load->view('test_managment/import_question', $data);
	}*/
	public function import_question()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		} 

		$test_id = $this->input->post('test_id');
		$section_id = $this->input->post('section_id');
		 
		// pagination
        $config['base_url'] = base_url('import_question'); 
        $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='questions'); 
        // echo $config['total_rows'] ;die;
        $config['per_page'] = 10;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open']= '<li class="page-item">';
        $config['num_tag_close']='</li>';
        $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                    <a href='#' class='page-link'>";
        $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] ="<li class='page-item'>";
        $config['next_tag1_close'] ="</li>";
        $config['prev_tag_open'] ="<li>";
        $config['prev_tag1_close'] ="<li class='page-item'>";
        $config['first_tag_open'] ="<li>";
        $config['first_tag1_close'] ="<li class='page-item'>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag1_close'] = "<li class='page-item'>";
        $config['attributes'] = array('class' => 'page-link wid'); 

        $this->pagination->initialize($config);

		// fet records
        $data['board_list'] = $this->Main_Model->getAllData($tbl='board');
        $data['standard_list'] = $this->Main_Model->getAllData($tbl='standard');
        $data['subject_list'] = $this->Main_Model->getAllData($tbl='subjects');
        $data['difficulty_list'] = $this->Main_Model->getAllData($tbl='difficulty_level');
        $data['language_list'] = $this->Main_Model->getAllData($tbl='language');

	    // question records
        $order_by = ('question_id desc');
		$data['questions_list']= $this->Main_Model->getAllRecords($tbl='questions', $config['per_page'], $this->uri->segment(2), $order_by);
      	//$this->Main_Model->getAllOrderData($tbl='questions', $order_by);
		$data['test_id']=$test_id;
		$data['section_id']=$section_id;

		$this->load->view('test_managment/import_question', $data);
	}
	

	public function add_imported_questions()
	{
		$question_id = $this->input->post('question_id');
		$section_id = $this->input->post('section_id');
      	$test_id = $this->input->post('test_id');

      	foreach($question_id as $value)
        {
          $arr_q = array(
          					'test_id'=>$test_id,
							'section_id'=>$section_id,
							'question_id'=> $value,
							'status'=> 1,
                        );
          $this->Main_Model->insertData($tbl='import_question', $arr_q);
        }
      	// redirect(base_url().'create_section/'.$test_id);
	}
	
	public function delete_questions_as_per_section($imp_id)
    {
        // echo $imp_id;
        $where = '(imp_id="'.$imp_id.'")';
        $this->Main_Model->deleteData($tbl='import_question',$where);
    }

    public function get_question_record_as_per_section()
	{
		$question_id = $this->input->post('question_id');

		$where = '(question_id="'.$question_id.'")';
		$c_que=$this->Main_Model->getData($tbl='questions',$where);
		// echo "<pre>";print_r($c_que); echo "</pre>";

		if(!empty($c_que))
        {
        	if ($c_que['answer_type'] == 1 ) // 1-Single Choice
        	{ 
        		// 1-Single Choice
        		$ans= $this->Main_Model->getData($tbl='answers_single_choice',$where);

        		$final_quetions = array(
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,

                                        	'answer_type'=>$ans['answer_type'] ,
                                    		'options_A'=>$ans['options_A'] ,
                                    		'options_B'=>$ans['options_B'] ,
                                    		'options_C'=>$ans['options_C'] ,
                                    		'options_D'=>$ans['options_D'] ,
                                    		'correct_answer'=>$ans['correct_answer'] ,
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}
        	
        	if ($c_que['answer_type'] == 2 ) // 2-Multiple Choice
        	{
        		// 2-Multiple Choice
        		$ans= $this->Main_Model->getData($tbl='answers_multiple_choice',$where);

        		$check_options = $this->Main_Model->getAllData_not_order($tbl='answers_option_multiple_choice',$where);

        		$final_opt=array();
        		foreach ($check_options as $c_opt) 
        		{
        			$final_opt[] = array(
        									//'question_id'=>$c_que['question_id'] ,
        									'opt_id'=>$c_opt['opt_id'],
        									'answer_type'=>$c_opt['answer_type'],
	        								'answer_id'=>$c_opt['answer_id'],
			                                'correct_answer'=>$c_opt['correct_answer'],
                            		);
            	}

        		$final_quetions = array(
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                        	'answer_type'=>$ans['answer_type'] ,
                                    		'options_A'=>$ans['options_A'] ,
                                    		'options_B'=>$ans['options_B'] ,
                                    		'options_C'=>$ans['options_C'] ,
                                    		'options_D'=>$ans['options_D'] ,
                                    		// 'correct_answer'=>$ans['correct_answer'] ,
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,

                                        	// options
                                        	'options'=>$final_opt,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}
          
        	if ($c_que['answer_type'] == 3 ) // 3-Integer
        	{
        		// 3-Integer
        		$ans= $this->Main_Model->getData($tbl='answers_integer',$where);

        		$final_quetions = array(
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                    		'correct_answer'=>$ans['correct_answer'] ,
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}

        	if ($c_que['answer_type'] == 4 )  // 4-True/ False 
        	{
        		// 4-True/ False
        		$ans= $this->Main_Model->getData($tbl='answers_true_false',$where);

        		$final_quetions = array(
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                    		'correct_answer'=>$ans['correct_answer'] ,
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}

        	if ($c_que['answer_type'] == 5 ) // 5-Match Matrix 
        	{
        		//  5-Match Matrix
        		$ans= $this->Main_Model->getData($tbl='answers_match_matrix',$where);
        		$check_options = $this->Main_Model->getAllData_not_order($tbl='answers_option_match_matrix',$where);

        		$final_opt=array();
        		foreach ($check_options as $c_opt) 
        		{
        			$final_opt[] = array(
        									//'question_id'=>$c_que['question_id'] ,
        									'opt_id'=>$c_opt['opt_id'],
        									'answer_type'=>$c_opt['answer_type'],
	        								'answer_id'=>$c_opt['answer_id'],
			                                'correct_options_l'=>$c_opt['correct_options_l'],
			                                'correct_options_r'=>$c_opt['correct_options_r'],
                            		);
            	}
                                        
        		$final_quetions = array(
        									// quetions
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                        	// answers
                                        	'answer_type'=>$ans['answer_type'] ,
                                    		'left_opt_1'=>$ans['left_opt_1'] ,
                                    		'left_opt_2'=>$ans['left_opt_2'] ,
                                    		'left_opt_3'=>$ans['left_opt_3'] ,
                                    		'left_opt_4'=>$ans['left_opt_4'] ,

                                    		'right_opt_A'=>$ans['right_opt_A'] ,
                                    		'right_opt_B'=>$ans['right_opt_B'] ,
                                    		'right_opt_C'=>$ans['right_opt_C'] ,
                                    		'right_opt_D'=>$ans['right_opt_D'] ,
                                    		
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,

                                        	// options
                                        	'options'=>$final_opt,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}
        	
        	if ($c_que['answer_type'] == 6 ) // 6-Match The Following 
        	{
        		// 6-Match The Following
        		$ans= $this->Main_Model->getData($tbl='answers_match_the_following',$where);
        		$check_options = $this->Main_Model->getAllData_not_order($tbl='answers_option_match_the_following',$where);

        		$final_opt=array();
        		foreach ($check_options as $c_opt) 
        		{
        			$final_opt[] = array(
        									'opt_id'=>$c_opt['opt_id'],
	        								'answer_type'=>$c_opt['answer_type'],
	        								'answer_id'=>$c_opt['answer_id'],
			                                'correct_options_l'=>$c_opt['correct_options_l'],
			                                'correct_options_r'=>$c_opt['correct_options_r'],
	                            		);
            	}
                                        
        		$final_quetions = array(
        									// quetions
        									'question_id'=>$c_que['question_id'] ,
        									'answer_type'=>$c_que['answer_type'] ,
											'std_id'=>$c_que['std_id'] ,
			                            	'subject_id'=>$c_que['subject_id'] ,
			                            	'topic'=>$c_que['topic'] ,
											'sub_topic'=>$c_que['sub_topic'] ,
			                            	'difficulty_id'=>$c_que['difficulty_id'] ,
			                            	'lang_id'=>$c_que['lang_id'] ,
											'correct_marks'=>$c_que['correct_marks'] ,
			                            	'negative_marks'=>$c_que['negative_marks'] ,
			                            	'not_attempt_marks'=>$c_que['not_attempt_marks'] ,
											'question_name'=>$c_que['question_name'] ,
			                            	'images'=>$c_que['images'] ,
			                            	'status'=>$c_que['status'] ,
                                        	// 'quetions'=>$quetions,
                                        	
                                        	// answers
                                        	'answer_type'=>$ans['answer_type'] ,
                                    		'left_opt_1'=>$ans['left_opt_1'] ,
                                    		'left_opt_2'=>$ans['left_opt_2'] ,
                                    		'left_opt_3'=>$ans['left_opt_3'] ,
                                    		'left_opt_4'=>$ans['left_opt_4'] ,

                                    		'right_opt_A'=>$ans['right_opt_A'] ,
                                    		'right_opt_B'=>$ans['right_opt_B'] ,
                                    		'right_opt_C'=>$ans['right_opt_C'] ,
                                    		'right_opt_D'=>$ans['right_opt_D'] ,
                                    		
                                        	'solution'=>$ans['solution'] ,
                                        	'video_solution'=>$ans['video_solution'] ,

                                        	// options
                                        	'options'=>$final_opt,
                                        );
        		if($final_quetions)
		        {
		            echo json_encode($final_quetions);
		        }
		        else
		        {
		            echo 0;
		        }
        	}
        }
        else
        {
        	echo " ";
        }
	}

	public function test_profile_setting()
	{
		$this->load->view('test_profile_setting');
	}

	public function evaluation()
	{
		$this->load->view('test_managment/evaluation');
	}
	
	public function result()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$test_id = $this->uri->segment(3);
		$order_by = ('test_detail_id desc'); 
		$wh_t = '(test_id = "'.$test_id.'")';
        $data['test_result'] = $this->Main_Model->getAllData_as_per_order($tbl='test_details', $wh_t,$order_by);
        // echo "<per>";print_r($test_result);echo "</per>";die;

		$this->load->view('test_managment/result', $data);
	}
	
	
	// manage_testSeries 02-09-2022
	public function manage_testSeries()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$path = 'assets/images/admin/test_management/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

		$category_id = $this->input->post('category_id');
		$test_series_name = $this->input->post('test_series_name');
		/*$validity = $this->input->post('validity');
		$validity_time = $this->input->post('validity_time');*/
		$test_series_type = $this->input->post('test_series_type');
		$price = $this->input->post('price');
		$description = $this->input->post('description');
		$lang_id = $this->input->post('lang_id');

		// new
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$offer = $this->input->post('offer');

        $is_trending_series = $this->input->post('is_trending_series');
      	if (empty($is_trending_series)) 
        {
            $is_trending_series1 = 0;
        }
        else
        {
            $is_trending_series1 = $is_trending_series;
        }

		$wh_t = '(category_id = "'.$category_id.'" AND test_series_name = "'.$test_series_name.'")';
        $test = $this->Main_Model->getData($tbl='test_series', $wh_t);

        if (isset($_POST['add_test_series'])) 
        {
        	if(empty($test)) 
            {
            	if ($_FILES['banner_image']['name'] !="") 
				{				
					if(file_exists($path . $_FILES['banner_image']['name']))
		            {
		                unlink($path . $_FILES['banner_image']['name']);
		            }   			
					
					if ($this->upload->do_upload('banner_image')) 
					{
						$data_b = $this->upload->data(); 
					}
				}

				if ($_FILES['icon_image']['name'] !="") 
				{				
					if(file_exists($path . $_FILES['icon_image']['name']))
		            {
		                unlink($path . $_FILES['icon_image']['name']);
		            }   			
					
					if ($this->upload->do_upload('icon_image')) 
					{
						$data_icon = $this->upload->data(); 
					}
				}

            	if ($test_series_type == 2) // Paid Test
            	{
            		$arr = array(
	                                'category_id'=>$category_id,
	                                'test_series_name'=>$test_series_name,
	                                /*'validity'=>$validity,
	                                'validity_time'=>$validity_time,*/
	                                'test_series_type'=>$test_series_type,
	                                'start_date'=>$start_date,
	                                'end_date'=>$end_date,
	                                'offer'=>$offer,
	                                'price'=>$price,
	                                'description'=>$description,
	                                'lang_id'=>$lang_id,
	                                'is_trending_series'=>$is_trending_series1,
	                                'banner_image'=>base_url().$path.$data_b['file_name'],
	                                'icon_image'=>base_url().$path.$data_icon['file_name'],
	                            );

	                $this->Main_Model->insertData($tbl='test_series', $arr);
	                $this->session->set_flashdata('create', 'Test series name has been added successfully..!');
	                redirect(base_url().'manage_testSeries');
            	}
            	else // Free Test
            	{
            		$arr = array(
	                                'category_id'=>$category_id,
	                                'test_series_name'=>$test_series_name,
	                                /*'validity'=>$validity,
	                                'validity_time'=>$validity_time,*/
	                                'start_date'=>$start_date,
	                                'end_date'=>$end_date,
	                                'test_series_type'=>$test_series_type,
	                                'description'=>$description,
	                                'lang_id'=>$lang_id,
	                                'is_trending_series'=>$is_trending_series1,
	                                'banner_image'=>base_url().$path.$data_b['file_name'],
	                                'icon_image'=>base_url().$path.$data_icon['file_name'],
	                            );

	                $this->Main_Model->insertData($tbl='test_series', $arr);
	                $this->session->set_flashdata('create', 'Test series name has been added successfully..!');
	                redirect(base_url().'manage_testSeries');
            	}
            }
            else
            {
                $this->session->set_flashdata('exists', 'Test series name Already exists!');
                redirect(base_url().'manage_testSeries');
            }
		}elseif (isset($_POST['search_test_series']))  
		{
			// echo $category_id;
			if(!empty($category_id))
		    {
		    	$data['test_series_list'] = $this->Main_Model->getTestSeriesFilterData($tbl='test_series', $category_id);
		    	$data['category_list'] = $this->Main_Model->getAllData($tbl='category');

		    	$data['language_list'] = $this->Main_Model->getAllData($tbl='language');
		    	$this->load->view('test_managment/manage_testSeries', $data);
		    }
		}
		else
        {
        	$config['base_url'] = base_url('manage_testSeries'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row($tbl='test_series'); 
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            $data['language_list'] = $this->Main_Model->getAllData($tbl='language');

        	$data['category_list'] = $this->Main_Model->getAllData($tbl='category');

        	$order_by = ('test_series_id desc');
        	$data['test_series_list']= $this->Main_Model->getAllRecords($tbl='test_series', $config['per_page'], $this->uri->segment(2), $order_by);
        	//$data['test_series_list'] = $this->Main_Model->getAllOrderData($tbl='test_series', $order_by);
            // echo "<pre>"; print_r($data['test_series_list']); echo "<pre>"; die;
			$this->load->view('test_managment/manage_testSeries', $data);
		}
	}

	public function delete_testSeries($test_series_id)
    {
        // echo $test_series_id;
        $where = '(test_series_id="'.$test_series_id.'")';
        $this->Main_Model->deleteData($tbl='test_series',$where);
        $this->Main_Model->deleteData($tbl='test_list',$where);
       	// delete other table belongs to this series
    }

    public function edit_testSeries($test_series_id)
    {
        // echo $test_series_id;
        $path = 'assets/images/admin/test_management/';
        $file_path = './' . $path;
		
        $config['upload_path'] = $file_path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

        $category_id = $this->input->post('category_id');
		$test_series_name = $this->input->post('test_series_name');
		/*$validity = $this->input->post('validity');
		$validity_time = $this->input->post('validity_time');*/
		$test_series_type = $this->input->post('test_series_type');
		$price = $this->input->post('price');
		$description = $this->input->post('description');
		$lang_id = $this->input->post('lang_id');

		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$offer = $this->input->post('offer');

		// echo $_FILES['icon_image'];echo $_FILES['banner_image'];die;

		$is_trending_series = $this->input->post('is_trending_series');
      	if (empty($is_trending_series)) 
        {
            $is_trending_series1 = 0;
        }
        else
        {
            $is_trending_series1 = $is_trending_series;
        }
      
		if ($test_series_type == 2) // Paid Test
    	{
			if ($_FILES['banner_image']['name'] !="" && $_FILES['icon_image']['name'] !="") 
			{				
				if(file_exists($path . $_FILES['banner_image']['name']))
	            {
	                unlink($path . $_FILES['banner_image']['name']);
	            }   			
				
				if ($this->upload->do_upload('banner_image')) 
				{
					$data_b = $this->upload->data(); 
				}

				if(file_exists($path . $_FILES['icon_image']['name']))
	            {
	                unlink($path . $_FILES['icon_image']['name']);
	            }   			
				
				if ($this->upload->do_upload('icon_image')) 
				{
					$data_icon = $this->upload->data(); 
				}
			
				$arr = array(
	    						'icon_image'=>base_url().$path.$data_icon['file_name'],
	    						'banner_image'=>base_url().$path.$data_b['file_name'],
	                            'category_id'=>$category_id,
	                            'test_series_name'=>$test_series_name,
	                            /*'validity'=>$validity,
	                            'validity_time'=>$validity_time,*/
	                            'start_date'=>$start_date,
                                'end_date'=>$end_date,
                                'offer'=>$offer,
	                            'test_series_type'=>$test_series_type,
	                            'price'=>$price,
	                            'description'=>$description,
	                            'lang_id'=>$lang_id,
	                            'is_trending_series'=>$is_trending_series1,
	                        );

			 	$where = '(test_series_id="'.$test_series_id.'")';
	        	$this->Main_Model->editData($tbl='test_series', $where, $arr);
	            $this->session->set_flashdata('edit', 'Test series name has been updated successfully..!');
	            redirect(base_url().'manage_testSeries');
			}
    		elseif ($_FILES['icon_image']['name'] !="") 
			{				
				if(file_exists($path . $_FILES['icon_image']['name']))
	            {
	                unlink($path . $_FILES['icon_image']['name']);
	            }   			
				
				if ($this->upload->do_upload('icon_image')) 
				{
					$data_icon = $this->upload->data(); 
					$arr = array(
		    						'icon_image'=>base_url().$path.$data_icon['file_name'],
		                            'category_id'=>$category_id,
		                            'test_series_name'=>$test_series_name,
		                            /*'validity'=>$validity,
		                            'validity_time'=>$validity_time,*/
		                            'start_date'=>$start_date,
	                                'end_date'=>$end_date,
	                                'offer'=>$offer,
		                            'test_series_type'=>$test_series_type,
		                            'price'=>$price,
		                            'description'=>$description,
		                            'lang_id'=>$lang_id,
		                            'is_trending_series'=>$is_trending_series1,
		                        );

				 	$where = '(test_series_id="'.$test_series_id.'")';
		        	$this->Main_Model->editData($tbl='test_series', $where, $arr);
		            $this->session->set_flashdata('edit', 'Test series name has been updated successfully..!');
		            redirect(base_url().'manage_testSeries');
				}
			}
			elseif ($_FILES['banner_image']['name'] !="") 
			{				
				if(file_exists($path . $_FILES['banner_image']['name']))
	            {
	                unlink($path . $_FILES['banner_image']['name']);
	            }   			
				
				if ($this->upload->do_upload('banner_image')) 
				{
					$data_b = $this->upload->data(); 

					$arr = array(
		    						'banner_image'=>base_url().$path.$data_b['file_name'],
		                            'category_id'=>$category_id,
		                            'test_series_name'=>$test_series_name,
		                            /*'validity'=>$validity,
		                            'validity_time'=>$validity_time,*/
		                            'start_date'=>$start_date,
	                                'end_date'=>$end_date,
	                                'offer'=>$offer,
		                            'test_series_type'=>$test_series_type,
		                            'price'=>$price,
		                            'description'=>$description,
		                            'lang_id'=>$lang_id,
		                            'is_trending_series'=>$is_trending_series1,
		                        );

				 	$where = '(test_series_id="'.$test_series_id.'")';
		        	$this->Main_Model->editData($tbl='test_series', $where, $arr);
		            $this->session->set_flashdata('edit', 'Test series name has been updated successfully..!');
		            redirect(base_url().'manage_testSeries');
				}
			}
			else
			{
				$arr = array(
	    						'category_id'=>$category_id,
	                            'test_series_name'=>$test_series_name,
	                            /*'validity'=>$validity,
	                            'validity_time'=>$validity_time,*/
	                            'start_date'=>$start_date,
                                'end_date'=>$end_date,
                                'offer'=>$offer,
	                            'test_series_type'=>$test_series_type,
	                            'price'=>$price,
	                            'description'=>$description,
	                            'lang_id'=>$lang_id,
	                            'is_trending_series'=>$is_trending_series1,
	                        );

			 	$where = '(test_series_id="'.$test_series_id.'")';
	        	$this->Main_Model->editData($tbl='test_series', $where, $arr);
	            $this->session->set_flashdata('edit', 'Test series name has been updated successfully..!');
	            redirect(base_url().'manage_testSeries');
			}
    	} // Paid Test
    	elseif ($test_series_type == 1) // Free Test
    	{
    		if ($_FILES['banner_image']['name'] !="" && $_FILES['icon_image']['name'] !="") 
			{				
				if(file_exists($path . $_FILES['banner_image']['name']))
	            {
	                unlink($path . $_FILES['banner_image']['name']);
	            }   			
				
				if ($this->upload->do_upload('banner_image')) 
				{
					$data_b = $this->upload->data(); 
				}

				if(file_exists($path . $_FILES['icon_image']['name']))
	            {
	                unlink($path . $_FILES['icon_image']['name']);
	            }   			
				
				if ($this->upload->do_upload('icon_image')) 
				{
					$data_icon = $this->upload->data(); 
				}
			
				$arr = array(
	                            'icon_image'=>base_url().$path.$data_icon['file_name'],
	                            'banner_image'=>base_url().$path.$data_b['file_name'],
	                            'category_id'=>$category_id,
	                            'test_series_name'=>$test_series_name,
	                            /*'validity'=>$validity,
	                            'validity_time'=>$validity_time,*/
	                            'start_date'=>$start_date,
                                'end_date'=>$end_date,
	                            'test_series_type'=>$test_series_type,
	                            'description'=>$description,
	                            'lang_id'=>$lang_id,
	                            'is_trending_series'=>$is_trending_series1,
	                        );

	            $where = '(test_series_id="'.$test_series_id.'")';
	        	$this->Main_Model->editData($tbl='test_series', $where, $arr);
	            $this->session->set_flashdata('edit', 'Test series name has been updated successfully..!');
	            redirect(base_url().'manage_testSeries');
			}
    		elseif ($_FILES['icon_image']['name'] !="") 
			{				
				if(file_exists($path . $_FILES['icon_image']['name']))
	            {
	                unlink($path . $_FILES['icon_image']['name']);
	            }   			
				
				if ($this->upload->do_upload('icon_image')) 
				{
					$data_icon = $this->upload->data(); 
					$arr = array(
		                            'icon_image'=>base_url().$path.$data_icon['file_name'],
		                            'category_id'=>$category_id,
		                            'test_series_name'=>$test_series_name,
		                            /*'validity'=>$validity,
		                            'validity_time'=>$validity_time,*/
		                            'start_date'=>$start_date,
                                	'end_date'=>$end_date,
		                            'test_series_type'=>$test_series_type,
		                            'description'=>$description,
		                            'lang_id'=>$lang_id,
		                            'is_trending_series'=>$is_trending_series1,
		                        );

		            $where = '(test_series_id="'.$test_series_id.'")';
		        	$this->Main_Model->editData($tbl='test_series', $where, $arr);
		            $this->session->set_flashdata('edit', 'Test series name has been updated successfully..!');
		            redirect(base_url().'manage_testSeries');
				}
			}
			elseif ($_FILES['banner_image']['name'] !="") 
			{				
				if(file_exists($path . $_FILES['banner_image']['name']))
	            {
	                unlink($path . $_FILES['banner_image']['name']);
	            }   			
				
				if ($this->upload->do_upload('banner_image')) 
				{
					$data_b = $this->upload->data(); 

					$arr = array(
		                            'banner_image'=>base_url().$path.$data_b['file_name'],
		                            'category_id'=>$category_id,
		                            'test_series_name'=>$test_series_name,
		                            /*'validity'=>$validity,
		                            'validity_time'=>$validity_time,*/
		                            'start_date'=>$start_date,
                                	'end_date'=>$end_date,
		                            'test_series_type'=>$test_series_type,
		                            'description'=>$description,
		                            'lang_id'=>$lang_id,
		                            'is_trending_series'=>$is_trending_series1,
		                        );

		            $where = '(test_series_id="'.$test_series_id.'")';
		        	$this->Main_Model->editData($tbl='test_series', $where, $arr);
		            $this->session->set_flashdata('edit', 'Test series name has been updated successfully..!');
		            redirect(base_url().'manage_testSeries');
				}
			}
			else
			{
				$arr = array(

	                            'category_id'=>$category_id,
	                            'test_series_name'=>$test_series_name,
	                            /*'validity'=>$validity,
	                            'validity_time'=>$validity_time,*/
	                            'start_date'=>$start_date,
                                'end_date'=>$end_date,
	                            'test_series_type'=>$test_series_type,
	                            'description'=>$description,
	                            'lang_id'=>$lang_id,
	                            'is_trending_series'=>$is_trending_series1,
	                        );

	            $where = '(test_series_id="'.$test_series_id.'")';
	        	$this->Main_Model->editData($tbl='test_series', $where, $arr);
	            $this->session->set_flashdata('edit', 'Test series name has been updated successfully..!');
	            redirect(base_url().'manage_testSeries');
			}
    	
    	}
    }


	public function active_test_series_status() 
    {
        $status = $this->input->post('status'); 

        $test_series_id = $this->input->post('test_series_id');
        // echo $test_series_id;die;         
        $where = '(test_series_id="'.$test_series_id.'")';
        $arr = array(
                        'status' => $status
                    );
        $update_id = $this->Main_Model->editData($tbl='test_series',$where,$arr);
         
        if($update_id)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

	
	// Payment Managment
	public function payment_mgmt()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$order_by = ('o_id  desc');
		$data['latest_revenue_list'] = $this->Main_Model->getAllArrayLimitData($tbl='orders', $order_by);
		// echo "<pre>"; print_r($data['latest_revenue_list']);echo "</pre>";die;

		$this->load->view('payment_managment/payment_mgmt', $data);
	}
	
	// Today's Revenue
	public function today_revenue()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$test_series_id = $this->input->post('test_series_id');

		if (isset($_POST['search_todays_revenue']))  
		{
			if(!empty($test_series_id))
		    {
		    	$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

				// filter
				$where = '(payment_date = "'.date("Y-m-d").'" AND payment_status = 1)';
				$data['todays_revenue_list'] = $this->Main_Model->getPaymentFilterData($tbl='orders',$where,$test_series_id);
		    	$this->load->view('payment_managment/today_revenue',$data);
		    }
		}
		else
		{
			// pagination
			$where = '(payment_date = "'.date("Y-m-d").'" AND payment_status = 1)';

			$config['base_url'] = base_url('today_revenue'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row_wh($tbl='orders',$where);  
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            // get test_series_list for filter
			$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');
			
			$order_by = ('o_id desc');
			$data['todays_revenue_list'] = $this->Main_Model->getAllRecords_wh($tbl='orders', $where , $config['per_page'], $this->uri->segment(2), $order_by);
			// $this->Main_Model->getAllData_as_per_order($tbl='orders', $where, $order_by);

			$this->load->view('payment_managment/today_revenue',$data);
		}
	}

	// revenue current month
  	public function current_month_revenue()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$test_series_id = $this->input->post('test_series_id');

		if (isset($_POST['search_todays_revenue']))  
		{
			if(!empty($test_series_id))
		    {
		    	$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

				// filter
				$where = '(MONTH(payment_date) = MONTH(CURDATE()) AND payment_status = 1)';
				$data['todays_revenue_list'] = $this->Main_Model->getPaymentFilterData($tbl='orders',$where,$test_series_id);
		    	$this->load->view('payment_managment/today_revenue',$data);
		    }
		}
		else
		{
			// pagination
			$where = '(MONTH(payment_date) = MONTH(CURDATE()) AND payment_status = 1)';

			$config['base_url'] = base_url('current_month_revenue'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row_wh($tbl='orders',$where);  
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            // get test_series_list for filter
			$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

			// table record
			$order_by = ('o_id  desc');
			$data['monthly_revenue_list'] =  $this->Main_Model->getAllRecords_wh($tbl='orders', $where , $config['per_page'], $this->uri->segment(2), $order_by);
			//$this->Main_Model->getAllData_as_per_order($tbl='orders', $where, $order_by);

			$this->load->view('payment_managment/current_month_revenue',$data);
		}
	}
	
	// revenue Till Now
	public function revenue_list()
	{
		$user_id = $this->session->userdata('user_id');
		if (empty($user_id)) 
		{
			$this->session->set_flashdata('login_fail', 'Enter Valid Username and Password');
			redirect(base_url());
		}

		$test_series_id = $this->input->post('test_series_id');

		if (isset($_POST['search_todays_revenue']))  
		{
			if(!empty($test_series_id))
		    {
		    	$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

				// filter 
				$where = '(payment_status = 1)';
				// $where = '(YEAR(payment_date) = YEAR(CURDATE()) AND payment_status = 1)';
				$data['todays_revenue_list'] = $this->Main_Model->getPaymentFilterData($tbl='orders',$where,$test_series_id);
		    	$this->load->view('payment_managment/today_revenue',$data);
		    }
		}
		else
		{
			// pagination
			$where = '(payment_status = 1)';
			// $where = '(YEAR(payment_date) = YEAR(CURDATE()) AND payment_status = 1)';

			$config['base_url'] = base_url('revenue_list'); 
            $config['total_rows'] = $this->Main_Model->getAllArrayData_row_wh($tbl='orders',$where);  
            // echo $config['total_rows'] ;die;
            $config['per_page'] = 10;

            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = '>';
            $config['prev_link'] = '<';
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open']= '<li class="page-item">';
            $config['num_tag_close']='</li>';
            $config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
                                        <a href='#' class='page-link'>";
            $config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] ="<li class='page-item'>";
            $config['next_tag1_close'] ="</li>";
            $config['prev_tag_open'] ="<li>";
            $config['prev_tag1_close'] ="<li class='page-item'>";
            $config['first_tag_open'] ="<li>";
            $config['first_tag1_close'] ="<li class='page-item'>";
            $config['last_tag_open'] = "<li>";
            $config['last_tag1_close'] = "<li class='page-item'>";
            $config['attributes'] = array('class' => 'page-link wid'); 

            $this->pagination->initialize($config);

            // get test_series_list for filter
			$data['test_series_list'] = $this->Main_Model->getAllData($tbl='test_series');

			// table record
			$order_by = ('o_id  desc');
			$data['total_revenue_list'] = $this->Main_Model->getAllRecords_wh($tbl='orders', $where , $config['per_page'], $this->uri->segment(2), $order_by);
			// $this->Main_Model->getAllData_as_per_order($tbl='orders', $where, $order_by);
			// echo "<pre>"; print_r($data['total_revenue_list']);echo "</pre>";die;

			$this->load->view('payment_managment/revenue_list',$data);
		}
	}


	public function user_payment_info()
	{
		$this->load->view('payment_managment/user_payment_info');
	}
	

	// include js, css, header,footer
	public function css()
	{
		$this->load->view('css');
	}
	public function header()
	{
		$this->load->view('header');
	}
	public function footer()
	{
		$this->load->view('footer');
	}
	public function js()
	{
		$this->load->view('js');
	}
	
	/*public function sidemenu()
	{
		$this->load->view('sidemenu');
	}*/

}
?>