<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Model extends CI_Model 
{
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    // To get data in row
    public function getData($tbl,$where)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($where);
        $query =$this->db->get();
        return $query->row_array();
    }
    
    // To get All Data with id 
    public function getAllData($tbl,$where)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($where);
        $query =$this->db->get();
        return $query->result_array();
    }

    public function getAllArrayData($tbl)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $query = $this->db->get();
        return $query->result_array();
    } 
	
  	// Limit data
  	public function getAllArrayLimitData($tbl,$order_by)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->limit(5);
        $this->db->order_by($order_by);
        $query = $this->db->get();
        return $query->result_array();
    } 

  
     // To get All Data
    public function getAllOrderData($tbl,$order_by)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->order_by($order_by);
        $query = $this->db->get();
        return $query->result_array();
    } 
	
  	//group_by
    public function getAllData_as_group($tbl,$where,$group_by)
    {
        // $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($where);
        $this->db->group_by($group_by);
        $query = $this->db->get();
        return $query->result_array();
    } 
  	
  	public function getAllData_as_per_order($tbl,$where,$order_by)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($where);
        $this->db->order_by($order_by);
        $query = $this->db->get();
        return $query->result_array();
    }
  
    // To Insert Data 
    public function insertData($tbl,$arr)
    {
        $this->db->insert($tbl,$arr);
        return $this->db->insert_id();
    }
    
     // To Edit Data 
    public function editData($tbl,$where,$arr)
    {
        $this->db->where($where);
        if($this->db->update($tbl,$arr))
        {
            return TRUE;
        } 
        else 
        {
            return FALSE;
        }
    }

    // To  delete  
    public function deleteData($tbl,$where)
    {
        $this->db->where($where);
        if($this->db->delete($tbl))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
  	/*// SELECT sum(video_length) FROM `course_video_management`
    public function getVideoLength($tbl,$where)
    {
        $this->db->select_sum('video_length');
        $this->db->from($tbl);
        $this->db->where($where);
        $query =$this->db->get();
        return $query->result_array();
    }
  	
  	// Total Revenue
   	public function getTotalRevenue($tbl,$where)
    {
        $this->db->select_sum('revenue');
        $this->db->from($tbl);
        $this->db->where($where);
        $query =$this->db->get();
        return $query->result_array();
    }
	
  	public function getTotalPlay_VdoLimitData($tbl,$where,$order_by)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->limit(1);
        $this->db->where($where);
        $this->db->order_by($order_by);
        $query = $this->db->get();
        return $query->result_array();
    } 
  	*/

     // select_sum       // 29-11-2022
    public function getTotalSumofMarks($tbl,$where)
    {
        $this->db->select_sum('mark');
        $this->db->from($tbl);
        $this->db->where($where);
        $query =$this->db->get();
        return $query->result_array();
    }
}

?>