<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Profile</title>

    <?php $this->load->view('css'); ?>
</head>

<?php 
$user_id = $this->session->userdata('user_id');

$where = '(uid = "'.$user_id.'" AND user_type=1 )';
$admin_profile= $this->Main_Model->getData($tbl='user_details', $where);
// echo "<pre>";print_r($admin_profile);echo "</pre>";
?>     

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">

        <?php $this->load->view('header'); ?>
        <div class="content-wrapper">
            <section class="content">
                <div class="app-content main-content">
                    <div class="side-app">
                        <!--Page header-->
                        <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Edit Profile</h4>
                            </span>
                            
                        </div>
                            <div class="page-rightheader ml-auto d-lg-flex d-none">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item ">
                                        <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center "> 
                                            <i class="breadcrumb-item-icon fa fa-home"></i>
                                            <span class="breadcrumb-icon"> Home</span>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Profile</li>
                                </ol>
                            </div>
                        </div>
                        <!--End Page header-->
                        <!-- Row -->
                        <div class="row">
                            <div class="col-xl-4 col-lg-5">
                                <?php 
                                if (!empty($this->session->flashdata('psw_update')) )
                                { ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-success" id="alert_msg">
                                            <?php echo $this->session->flashdata('psw_update');?>
                                        </div>
                                    </div>
                                    <?php
                                }

                                if (!empty($this->session->flashdata('psw_fail')) )
                                { ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-danger" id="alert_msg">
                                            <?php echo $this->session->flashdata('psw_fail');?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>  
                                <div class="card profile">
                                    <div class="card-header">
                                        <div class="card-title">Edit Password</div>
                                    </div>
                                    
                                    <!-- Change Password  -->
                                    <form action="<?php echo base_url('update_password') ?>" method="post" class="form-horizontal">    
                                        <div class="card-body">
                                            <!-- <div class="text-center mb-5 justify-content-center">
                                                <input type="file" class=" dropify" data-default-file="<?php echo base_url(''); ?>assets/images/users/16.jpg" data-height="150" data-width="100"
                                                    required />
                                            </div> -->
                                            <div class="form-group">
                                                <label class="form-label">Current Password</label>
                                                <input type="password" value="<?php echo set_value('oldpassword'); ?>" id="oldpassword" name="oldpassword" class="form-control <?php echo(form_error('oldpassword') !="") ? 'is-invalid' : ''; ?>" placeholder="Enter Old Password" required>

                                                <?php echo form_error('oldpassword'); ?>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">New Password</label>
                                                <input type="password" class="form-control <?php echo(form_error('newPassword') !="") ? 'is-invalid' : ''; ?>" name="newPassword" id="newPassword" placeholder="Enter New Password" value="<?php echo set_value('newPassword'); ?>" required>

                                                <?php echo form_error('newPassword'); ?>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Confirm Password</label>
                                                <input type="password" class="form-control <?php echo(form_error('confPassword') !="") ? 'is-invalid' : ''; ?>" name="confPassword" id="confPassword" placeholder="Confirm Password" value="<?php echo set_value('confPassword'); ?>" required>

                                                <?php echo form_error('confPassword'); ?>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button type="submit" class="btn btn-primary">Update Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-xl-8 col-lg-7">
                                <?php 
                                if (!empty($this->session->flashdata('profile_success')) )
                                { ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-success" id="alert_msg">
                                            <?php echo $this->session->flashdata('profile_success');?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>  
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title">Edit Profile</div>
                                    </div>

                                    <form action="<?php echo base_url('profile_update_dash') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="card-body">
                                            <!-- <div class="card-title font-weight-bold">Basci info:</div> -->
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Full Name</label>
                                                        <input type="text" name="full_name" id="full_name" value="<?php echo $admin_profile['full_name']; ?>"  class="form-control <?php echo (form_error('full_name') !="") ? 'is-invalid' : ''; ?>"  placeholder="Full Name" >
                                                       
                                                        <?php echo form_error('full_name'); ?>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Email</label>
                                                        <input type="email"  name="email" id="email" value="<?php echo $admin_profile['email']; ?>" class="form-control <?php echo (form_error('email') !="") ? 'is-invalid' : ''; ?>" placeholder="Enter Email" required > 

                                                        <?php echo form_error('email'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Mobile Number</label>
                                                        <input type="text" minlength="10" maxlength="10" name="mobile_no" id="mobile_no" class="form-control <?php echo (form_error('mobile_no') !="") ? 'is-invalid' : ''; ?>" value="<?php echo $admin_profile['mobile_no']; ?>" placeholder="Mobile Number" onkeypress="return onlyNumberKey(event)" required>

                                                        <?php echo form_error('mobile_no'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Address</label>
                                                        <input type="text" name="address" id="address" value="<?php echo $admin_profile['address']; ?>" class="form-control" placeholder="Address">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label">City</label>
                                                        <input type="text"  name="city" id="city" value="<?php echo $admin_profile['city']; ?>" class="form-control" placeholder="City">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label">Postal Code</label>
                                                        <input type="text" minlength="6" maxlength="6" name="pincode" id="pincode" value="<?php echo $admin_profile['pincode']; ?>" class="form-control" placeholder="Postal Code" onkeypress="return onlyNumberKey(event)"  >
                                                    </div>
                                                </div>

                                               
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="form-label">Country</label>
                                                        <input type="text"  name="country" id="country" value="<?php echo $admin_profile['country']; ?>" class="form-control" placeholder="Country">

                                                        <!-- <select class="form-control nice-select select2">
                                                            <optgroup label="Categories">
                                                                <option data-select2-id="5">--Select--</option>
                                                                <option value="1">Germany</option>
                                                                <option value="2">Real Estate</option>
                                                                <option value="3">Canada</option>
                                                                <option value="4">Usa</option>
                                                                <option value="5">Afghanistan</option>
                                                                <option value="6">Albania</option>
                                                                <option value="7">China</option>
                                                                <option value="8">Denmark</option>
                                                                <option value="9">Finland</option>
                                                                <option value="10">India</option>
                                                                <option value="11">Kiribati</option>
                                                                <option value="12">Kuwait</option>
                                                                <option value="13">Mexico</option>
                                                                <option value="14">Pakistan</option>
                                                            </optgroup>
                                                        </select> -->
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Profile Picture</label>

                                                        <?php
                                                if ($admin_profile['photo'] !="") 
                                                { 
                                                    ?>
                                                    <div class="col-ting">
                                                        <div class="control-group file-upload" id="file-upload1">
                                                            <div class="image-box text-center">
                                                                <img src="<?php echo $admin_profile['photo'];?>" alt="img">
                                                            </div>
                                                            <div class="controls" style="display: none;">
                                                                <input type="file" name="photo" accept="image/*"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php 
                                                } 
                                                else
                                                { 
                                                    ?>
                                                    <div class="col-ting">
                                                        <div class="control-group file-upload" id="file-upload1">
                                                            <div class="image-box text-center">
                                                                <img src="<?php echo base_url().'assets/images/no_image.png' ?>" alt="img">
                                                            </div> 
                                                            <div class="controls" style="display: none;">
                                                                <input type="file" name="photo" accept="image/*"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                    <?php
                                                }
                                                ?>
                                                
                                                       
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button type="submit" name="update_profile" class="btn btn-primary">Update Profile</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- End Row-->

                    </div>
                </div>
                <!-- end app-content-->
            </section>
        </div>
        <aside class="control-sidebar control-sidebar-dark">
        </aside>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
    function onlyNumberKey(evt) 
    {
        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }
    </script>   
    <script type="text/javascript">
    const myTimeout = setTimeout(close, 2000);

    function close() 
    {
        document.getElementById("alert_msg").style.display = "none";
    }
  </script>
</body>

</html>