<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Testimonial </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">

            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Testimonial</h4>
                            </span>

                        </div>
                        <!-- <div class="page-leftheader">
                            <h4 class="page-title">Contact Us</h4>
                        </div> -->
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Testimonial</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <!-- <div class="card">
                            <div class="card-body">
                                <div class="row "> -->
                        <div class="col-md-2">
                            <?php $this->load->view('aside_web_mgmt'); ?>
                        </div>
                        <div class="col-md-10">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                    <?php 
                                                
                                    if (!empty($this->session->flashdata('create')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-success" id="alert_msg">
                                              <?php echo $this->session->flashdata('create');?>
                                          </div>
                                      </div>
                                    <?php
                                    }

                                    if (!empty($this->session->flashdata('edit')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-success" id="alert_msg">
                                              <?php echo $this->session->flashdata('edit');?>
                                          </div>
                                      </div>
                                    <?php
                                    }

                                    if (!empty($this->session->flashdata('exists')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-warning" id="alert_msg">
                                              <?php echo $this->session->flashdata('exists');?>
                                          </div>
                                      </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="card ">
                                        <div class="card-header">
                                            <div class="card-title">Testimonial
                                                Info</div>
                                            <div class="card-options">
                                                <a type="button" class="btn btn-icon btn-primary"
                                                    data-target="#add_testimonial" data-toggle="modal"><i class="fa fa-plus"></i>&nbsp;Add Testimonial</a>
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="table-responsive ">
                                                <!-- <div class="table-responsive" id="lightgallery"> -->
<table class="table table-hover border table-vcenter text-nowrap">
    <thead class="text-center">
        <tr>
            <th class="wd-15p border-bottom-0">
                Sr. No.</th>
            <th class="wd-15p border-bottom-0">
                Name
            </th>
            <th class="wd-15p border-bottom-0">
                Qualification
            </th>
            <th class="wd-15p border-bottom-0">
                Description
            </th>
            <th class="wd-15p border-bottom-0">
                Image
            </th>
            <th class="wd-20p border-bottom-0">
                Active/Deactive
            </th>
            <th class="wd-20p border-bottom-0">
                Action</th>
        </tr>
    </thead>
    <tbody class="text-center">
        <?php 
        if (!empty($testimonial_list)) 
        {
            $i= 1+$this->uri->segment(2);
            foreach ($testimonial_list as $tmo) 
            {
                ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $tmo['testimonial_name']; ?></td>
                    <td><?php echo $tmo['qualification']; ?></td>
                    <td>
                        <a type="button" class="btn btn-sm btn-icon btn-info" data-target="#view_massage_<?php echo $tmo['t_id']; ?>" data-toggle="modal"><i class="fa fa-eye"></i></a>
                    </td>
                    <td>
                        <div class="magnific-img">
                            <a class="image-popup-vertical-fit" href="<?php echo $tmo['testimol_image']; ?>" title="">
                                <img src="<?php echo $tmo['testimol_image']; ?>" alt="Sub-Category Image" style="width: 100px; height: 50px;" />
                            </a>
                        </div>
                    </td>
                    <td>
                        <?php 
                        if ($tmo['status'] == 1) 
                        {
                            ?>
                            <input type="checkbox" id="<?php echo $tmo['t_id']; ?>" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="0"> 
                            <?php
                        } 
                        else
                        {
                            ?>
                            <input type="checkbox" id="<?php echo $tmo['t_id']; ?>" data-size="sm" data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="1"> 
                            <?php         
                        } 
                        ?>
                    </td>
                    <td>
                        <!-- edit -->
                        <a type="button" class="btn btn-sm btn-icon btn-primary" data-target="#edit_testimonial_<?php echo $tmo['t_id']; ?>" data-toggle="modal"><i class="fa fa-edit"></i></a> 

                        <!-- delete -->
                        <a type="button" class="btn btn-sm btn-icon btn-secondary" id='delete_<?php echo $tmo['t_id']; ?>'><i class="fa fa-trash"></i></a>
                    </td>
                </tr>

                <!-- view description -->
                <div class="modal" id="view_massage_<?php echo $tmo['t_id']; ?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content modal-content-demo">
                            <div class="modal-header">
                                <h6 class="modal-title">Description</h6>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="">
                                    <div class="form-group">
                                        <textarea id="summernote" class="form-control" name="description" rows="4" readonly><?php echo strip_tags($tmo['description']); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn save-btn" type="button">Save changes</button> <button class="btn cancel-btn"
                                    data-dismiss="modal" type="button">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- delete -->
                <script>
                document.getElementById('delete_<?php echo $tmo['t_id']; ?>').onclick = function() 
                {
                    var id = $("#<?php echo $tmo['t_id']; ?>").val(); 
               
                    swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this file!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel",
                            closeOnConfirm: false,
                            closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) 
                        {
                            $.ajax({
                                       url: '<?php echo base_url().'delete_testimonial/'.$tmo['t_id']; ?>',
                                       type: "POST",
                                       data: {id:id},
                                       dataType:"HTML",
                                       success: function () {
                                        swal(
                                                "Deleted!",
                                                "Your file has been deleted!",
                                                "success"
                                            ),
                                            $('.confirm').click(function()
                                            {
                                                location.reload();
                                            });
                                        },
                                });
                            
                        } else {
                            swal(
                                "Cancelled",
                                "Your  file is safe !",
                                "error"
                            );
                        }
                    });
                };
                </script>

                <!-- edit -->
                <div class="modal" id="edit_testimonial_<?php echo $tmo['t_id']; ?>">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content modal-content-demo">
                            <div class="modal-header">
                                <h6 class="modal-title">Edit Testimonial</h6>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">&times;</span></button>
                            </div>
                            <form method="post"  action="<?php echo base_url().'edit_testimonial/'.$tmo['t_id'] ; ?>" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="bannername" class="form-label">Name <span class="text-red">*</span></label>
                                            
                                            <input type="text" class="form-control" name="testimonial_name" id="testimonial_name" placeholder="Enter Name" value="<?php echo $tmo['testimonial_name']; ?>" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="bannername" class="form-label">Qualification <span class="text-red">*</span></label>

                                            <input type="text" class="form-control" name="qualification" id="qualification" placeholder="Enter Qualification Name"  value="<?php echo $tmo['qualification']; ?>" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="bannername" class="form-label">Description <span
                                                    class="text-red">*</span></label>

                                            <textarea id="summernote" class="summernote" name="description" required><?php echo $tmo['description']; ?></textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="bannername" class="form-label">Upload Image
                                                <span class="text-red">*</span></label>
                                            <?php
                                            if ($tmo['testimol_image'] !="") 
                                            { 
                                              ?>
                                              <div class="col-ting">
                                                  <div class="control-group file-upload" id="file-upload1">
                                                      <div class="image-box text-center">
                                                          <img src="<?php echo $tmo['testimol_image'];?>" alt="img">
                                                      </div>
                                                      <div class="controls" style="display: none;">
                                                          <input type="file" name="testimol_image" accept="image/*" />
                                                      </div>
                                                  </div>
                                              </div>
                                              <?php 
                                            } 
                                            else
                                            { 
                                              ?>
                                               <div class="col-ting">
                                                  <div class="control-group file-upload" id="file-upload1">
                                                      <div class="image-box text-center">
                                                          <img src="<?php echo base_url().'assets/images/no_image.png' ?>" alt="img">
                                                      </div> 
                                                      <div class="controls" style="display: none;">
                                                          <input type="file" name="testimol_image" accept="image/*"/>
                                                      </div>
                                                  </div>
                                              </div>
                                              <?php
                                            }
                                            ?> 
                                            <!-- <input type="file" class="dropify" data-default-file="" data-height="150" required /> -->
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn save-btn" >Update Info</button> 
                                    <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                 <?php
            }
        }
        ?>
    </tbody>
</table>
                                            </div>
                                            <div class="table-footer align-items-center">
                                                <!-- <p class="">Showing 1 to 3 of 3 entries</p> -->
                                                <nav class="" aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">
                                                        <?php echo $this->pagination->create_links(); ?>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- </div>
                        </div>
                    </div> -->
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>

    <!-- add testimonial -->
    <div class="modal" id="add_testimonial">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Add Testimonial</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="bannername" class="form-label">Name <span class="text-red">*</span></label>
                               
                                <input type="text" class="form-control" name="testimonial_name" id="testimonial_name" placeholder="Enter Name" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="bannername" class="form-label">Qualification <span class="text-red">*</span></label>
                              
                                <input type="text" class="form-control" name="qualification" id="qualification" placeholder="Enter Qualification Name" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="bannername" class="form-label">Description <span
                                        class="text-red">*</span></label>
                                
                                <textarea id="summernote" class="summernote" name="description" required></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="bannername" class="form-label">Upload Image<span
                                        class="text-red">*</span></label>

                                <input type="file" name="testimol_image" id="testimol_image" class="dropify" data-default-file="" data-height="150" required accept="image/*" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_testimonial_info" class="btn save-btn">Save changes</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({
            // height: 100
        });
    });
    </script>

    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <!-- status active-deactive -->
    <script type="text/javascript"> 
        function change_status(idd)
        {
            var status_val=idd.value;
            var t_id= idd.id;
            
            var base_url = '<?php echo base_url() ?>';
            var t_id = t_id;

            if(status_val != '')
            {
                $.ajax({
                          url:base_url + "MainController/active_testimonial_status",
                          method:"POST",
                          data:{status:status_val,t_id:t_id},
                          
                          success: function(data)
                          {
                               // alert(data);
                               if (data == 1) 
                               {
                                   alert('Status Changed Sucessfully !..');
                                   location.reload();
                               } 
                               else 
                               {
                                   alert('Something Went Wrong !...')
                               }
                          }
                    });
            }
        }
    </script>

</body>

</html>