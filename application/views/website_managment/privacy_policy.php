<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Privacy Policy </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">

            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                    <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Privacy Policy</h4>
                            </span>
                            
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <!-- <div class="card">
                            <div class="card-body">
                                <div class="row "> -->
                        <div class="col-md-2">
                            <?php $this->load->view('aside_web_mgmt'); ?>
                        </div>
                        <div class="col-md-10">
                            <div class="row justify-content-center">
                                <?php 
                                if (!empty($this->session->flashdata('edit')) )
                                { ?>
                                  <div class="col-sm-12">
                                      <div class="alert alert-success" id="alert_msg">
                                          <?php echo $this->session->flashdata('edit');?>
                                      </div>
                                  </div>
                                    <?php
                                } 
                                ?>
                                <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                    <div class="card ">
                                        <div class="card-header">
                                            <div class="card-title">Privacy Policy
                                                Info
                                            </div>
                                        </div>
                                        <form method="post" enctype="multipart/form-data">
                                            <div class="card-body ">
                                                <div class="">
                                                    <div class="form-group">
                                                        <textarea id="summernote" class="summernote" name="privacy_policy" id="privacy_policy" ><?php echo $privacy_policy['privacy_policy']; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer text-center">
                                                <button type="submit" name="update_privacy_policy" class="btn save-btn" type="submit">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- </div>
                        </div>
                    </div> -->
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 200
        });
    });
    </script>

    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>
</body>

</html>