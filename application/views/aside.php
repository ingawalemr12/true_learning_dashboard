<aside class="">
    <div class="setting-tab ">
        <div class="panel panel-primary ">
            <div class="tab-menu-heading border-0 setting-menu">
                <div class="tabs-menu ">
                    <!-- Tabs -->
                    <ul class="nav panel-tabs nav-pills-custom  ">
                        <li class="">
                        <a class="nav-link   " id="v-pills-addCat-tab" 
                                href="<?php echo base_url('add-category'); ?>" >
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Category
                                </span></a>
                            <!-- <a class="nav-link  p-3 shadow active" id="v-pills-addCat-tab" data-toggle="pill"
                                href="#v-pills-addCat" role="tab" aria-controls="v-pills-addCat" aria-selected="true">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Category
                                </span></a> -->

                        </li>
                        <!-- <li class="">
                            <a class="nav-link   " id="v-pills-addSubCat-tab" d
                                href="<?php echo base_url('add_subcategory'); ?>" 
                                >
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Sub-Category
                                </span></a>

                        </li> -->
                        <li>
                            <a class="nav-link   " id="v-pills-sub-tab"  href="<?php echo base_url('add_board'); ?>"
                                >
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Board
                                </span></a>

                        </li>
                        <li>
                            <a class="nav-link   " id="v-pills-sub-tab"  href="<?php echo base_url('add_standard'); ?>"
                                >
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Standard
                                </span></a>

                        </li>
                        <li>
                            <a class="nav-link   " id="v-pills-sub-tab"  href="<?php echo base_url('add_subject'); ?>"
                                >
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Subject
                                </span></a>

                        </li>
                        <li>
                            <a class="nav-link   " id="v-pills-difficulty-tab" 
                                href="<?php echo base_url('add_difficulty_level'); ?>" 
                                >
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Difficulty Level
                                </span></a>

                        </li>
                        <li>
                            <a class="nav-link       " id="v-pills-board-tab" 
                                href="<?php echo base_url('add_language'); ?>" >
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Language
                                </span></a>

                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>

</aside>