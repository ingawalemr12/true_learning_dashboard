<aside class="">
    <div class="setting-tab ">
        <div class="panel panel-primary ">
            <div class="tab-menu-heading border-0 setting-menu">
                <div class="tabs-menu ">
                    <!-- Tabs -->
                    <?php 
                    $test_id = $this->uri->segment(2);
                    ?>
                    <ul class="nav panel-tabs nav-pills-custom  ">
                        <li class="">
                            <a class="nav-link   " id="v-pills-addCat-tab" href="<?php echo base_url().'edit_test/'.$test_id; ?>"><i class="fa fa-edit mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Edit Test Info </span></a>
                            <!-- <a class="nav-link  p-3 shadow active" id="v-pills-addCat-tab" data-toggle="pill"
                                href="#v-pills-addCat" role="tab" aria-controls="v-pills-addCat" aria-selected="true">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add
                                    Category
                                </span></a> -->

                        </li>
                        <li class="">
                            <a class="nav-link   " id="v-pills-addSubCat-tab" d
                                href="<?php echo base_url().'create_section/'.$test_id; ; ?>">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">
                                    Create Section
                                </span>
                            </a>
                            <i class="slide-toggle fa-2x fa fa-angle-down"></i>
                            <div class="card headerbottombar hide">
                                <div class="card-body ">
                                    <?php 
                                    if (!empty($section_list)) 
                                    {
                                        $i= 1;
                                        foreach ($section_list as $sec) 
                                        {
                                            $where = '(section_id="'.$sec['section_id'].'")';
                                            $all_que = $this->Main_Model->getAllData_not_order($tbl='import_question',$where);
                                            $all_questions = count($all_que);
                                            ?>
                                            <!-- get section record -->
                                            <div class="mb-2">
                                                <h4 class="panel-title align-items-center">
                                                    <span>Section <?php echo $i++; ?> </span>
                                                    
                                                    <span class="float-right d-flex">
                                                        <!-- edit -->
                                                        <a role="button" data-target="#update_section_<?php echo $sec['section_id']; ?>" data-toggle="modal" class=" mr-1"> <i class="fa fa-pencil" aria-hidden="true"></i></a>

                                                        <!-- delete old -->
                                                        <!-- <a id="delete1_section_<?php echo $sec['section_id'] ; ?>" role="button" class=" "><i class="fa fa-trash"></i></a> -->
                                                      
                                                        <a id="delete1_section_1_<?php echo $sec['section_id'] ; ?>" role="button" class=" "><i class="fa fa-trash"></i></a>
                                                    </span>
                                                </h4>
                                                <h3 class="section-name"> <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#section-1" aria-expanded="false" aria-controls="section-1" class="collapsed">
                                                    <?php echo $sec['section_name'] ; ?>  : 
                                                    <?php echo $all_questions ; ?> Questions</a></h3>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>  
                                    <!-- Add Section -->
                                    <div class="text-center">
                                        <a href="#" type="button" data-target="#add_section" data-toggle="modal" class=" btn btn-outline-info btn-sm"><i class="fa fa-plus-circle mr-2"></i>
                                        Add Section</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a class="nav-link" id="v-pills-sub-tab" data-target="#select_section" data-toggle="modal"><i class="fa fa-download mr-2"></i><span class="font-weight-bold small text-uppercase">Import Question</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</aside>