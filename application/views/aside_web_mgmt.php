<aside class="">
    <div class="setting-tab ">
        <div class="panel panel-primary ">
            <div class="tab-menu-heading border-0 setting-menu">
                <div class="tabs-menu ">
                    <!-- Tabs -->
                    <ul class="nav panel-tabs nav-pills-custom  ">
                        <li class="">
                            <a class="nav-link   " id="v-pills-addCat-tab" 
                                href="<?php echo base_url('banner_screen'); ?>" >
                                <i class="fa fa-picture-o mr-2" ></i>
                                <span class="font-weight-bold small text-uppercase">
                                    Banner Screen
                                </span></a>
                        </li>
                        <li class="">
                            <a class="nav-link   " id="v-pills-addCat-tab" 
                                href="<?php echo base_url('about_us'); ?>" >
                                <i class="fa fa-gg mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">
                                About Us
                                </span></a>
                        </li>
                        <li class="">
                            <a class="nav-link   " id="v-pills-addCat-tab" 
                                href="<?php echo base_url('contact_us'); ?>" >
                                <i class="fa fa-phone mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">
                                Contact Us
                                </span></a>
                        </li>
                        <li class="">
                            <a class="nav-link   " id="v-pills-addCat-tab" 
                                href="<?php echo base_url('testimonial'); ?>" >
                                <i class="fa fa-comment mr-2" ></i>
                                <span class="font-weight-bold small text-uppercase">
                                Testimonial
                                </span></a>
                        </li>
                        <li class="">
                            <a class="nav-link   " id="v-pills-addCat-tab" 
                                href="<?php echo base_url('privacy_policy'); ?>" >
                                <i class="fa fa-gavel mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">
                                Privacy Policy
                                </span></a>
                        </li>
                        <li class="">
                            <a class="nav-link   " id="v-pills-addCat-tab" 
                                href="<?php echo base_url('terms_condition'); ?>" >
                                <i class="fa fa-gavel mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">
                                Terms & Conditions
                                </span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</aside>