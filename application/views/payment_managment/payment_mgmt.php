<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Payment Management</title>

    <?php $this->load->view('css'); ?>
</head>

<!-- <body class="app sidebar-mini light-mode default-sidebar"> -->

<body class="app sidebar-mini light-mode light-sidebar">

    <div class="wrapper">

        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Payment Management</h4>
                            </span>

                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center ">

                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Payment Management</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->

                    <!--Row-->
                    <div class="row justify-content-around payment_box">
                        
                        <div class="col-sm-12 col-md-6 col-xl-3">
                            <div class="card ">
                                <div class="card-body">
                                    <a href="<?php echo base_url('today_revenue'); ?>">
                                        <div class="d-flex no-block align-items-center">

                                            <div>
                                                <h6 class="text-white">Today's Revenue</h6>
                                                <h2 class="text-white m-0 font-weight-bold">
                                                <?php 
                                                $where1 = '(payment_date = "'.date("Y-m-d").'" AND payment_status = 1 )';
                                                $todays_revenue = $this->Main_Model->getRevenueSum($tbl='orders',$where1);
                                                
                                                foreach ($todays_revenue as $t_rev) 
                                                {
                                                    echo  $t_rev['amount']; 
                                                }
                                                ?>
                                                </h2>
                                            </div>
                                            <div class="ml-auto">
                                                <span class="text-white display-6">
                                                    <i class="fa fa-inr fa-2x" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-xl-3">
                            <div class="card ">
                                <div class="card-body">
                                    <a href="<?php echo base_url('current_month_revenue'); ?>">
                                        <div class="d-flex no-block align-items-center">
                                            <div>
                                                <h6 class="text-white">Current Month revenue </h6>
                                                <h2 class="text-white m-0 font-weight-bold">
                                                <?php 
                                                $where2 = '(MONTH(payment_date) = MONTH(CURDATE()) AND YEAR(payment_date) = YEAR(CURDATE())  AND payment_status = 1 )';
                                                $todays_revenue = $this->Main_Model->getRevenueSum($tbl='orders',$where2);
                                                
                                                foreach ($todays_revenue as $t_rev) 
                                                {
                                                    echo  $t_rev['amount']; 
                                                }
                                                ?>
                                                </h2>
                                            </div>
                                            <div class="ml-auto">
                                                <span class="text-white display-6"><i class="fa fa-inr fa-2x" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                      <div class="col-sm-12 col-md-6 col-xl-3">
                            <div class="card ">
                                <div class="card-body">
                                    <a href="<?php echo base_url('revenue_list'); ?>">
                                        <div class="d-flex no-block align-items-center">
                                            <div>

                                                <h6 class="text-white">Till Date Revenue</h6>
                                                <h2 class="text-white m-0 font-weight-bold"><?php 
                                                // $where3 = '(YEAR(payment_date) = YEAR(CURDATE()) AND payment_status = 1 )';
                                                $where3 = '(payment_status = 1)';
                                                $total_revenue = $this->Main_Model->getRevenueSum($tbl='orders',$where3);
                                                
                                                foreach ($total_revenue as $t_rev) 
                                                {
                                                    echo  $t_rev['amount']; 
                                                }
                                                ?>
                                                </h2>
                                            </div>
                                            <div class="ml-auto">
                                                <span class="text-white display-6">
                                                    <i class="fa fa-inr fa-2x" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- <div class="card-footer bg-white border-top">
                                    Email: <span class="text-primary">victoriacott@Dashtic.com</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-around ">
                        <div class="col-lg-12 col-xl-11 col-md-11 col-sm-12">
                            <div class="card mt-5">
                                <div class="card-header">
                                    <div class="card-title">Letest Revenue
                                    </div>
                                    <!-- <div class="card-options">
                                        <a type="button" onclick="generateExcel('Report')" id="excel" class="btn btn-icon btn-primary" 
                                            >Excel</a>
                                    </div> -->
                                </div>

                                <div class="card-body">
                                    
                                    <div class="table-responsive ">
                                        <table border="1"
                                            class="table table-hover border table-vcenter text-nowrap category-table"
                                            id="myTable">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="wd-15p border-bottom-0">Sr. No.</th>
                                                    <th class="wd-15p border-bottom-0">User Name</th>
                                                    <!-- <th class="wd-15p border-bottom-0">Course Name</th> -->
                                                    <th class="wd-15p border-bottom-0">Test Series Name </th>
                                                    <th class="wd-15p border-bottom-0">Amount </th>
                                                    <th class="wd-20p border-bottom-0">Payment Status</th>
                                                    <th class="wd-20p border-bottom-0">Payment Type</th>
                                                    <th class="wd-20p border-bottom-0">Payment Date</th>
                                                    <!-- <th class="wd-20p border-bottom-0">Transaction Id</th>
                                                    <th class="wd-20p border-bottom-0">Order Id</th>
                                                    <th class="wd-20p border-bottom-0">Payment Mode</th> -->
                                                    <th class="wd-20p border-bottom-0 action">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                               <?php 
                                                if (!empty($latest_revenue_list)) 
                                                {
                                                    $i= 1+$this->uri->segment(2);
                                                    foreach ($latest_revenue_list as $lrl) 
                                                    {
                                                        $where = '(test_series_id = "'.$lrl['test_series_id'].'")';
                                                        $test_series = $this->Main_Model->getData($tbl='test_series', $where);
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i++; ?></td>
                                                            <td><?php echo $lrl['full_name']; ?></td>
                                                            <!-- <td>NEET</td> -->
                                                            <td><?php 
                                                            echo $test_series['test_series_name'];
                                                            //echo $test_series['test_series_id']; ?></td>
                                                            <td><i class="fa fa-inr" aria-hidden="true"></i> 
                                                                <?php echo $lrl['amount']; ?></td>
                                                            <td>
                                                                <?php 
                                                                // echo $lrl['payment_status'];
                                                                if ($lrl['payment_status'] == 1) 
                                                                {
                                                                    ?>
                                                                    <span class="badge badge-success">Success</span>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <span class="badge badge-danger">Fail</span>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php 
                                                                if ($lrl['payment_type'] == 2) 
                                                                {
                                                                    echo "Online";
                                                                }elseif ($lrl['payment_type'] == 1) 
                                                                {
                                                                    echo "Wallet";
                                                                    ?>
                                                                    <!-- <span class="badge">Wallet</span> -->
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td><?php echo $lrl['payment_date']; ?></td>
                                                            <td class="action">
                                                                <a type="button" class="btn btn-sm btn-icon btn-primary" data-target="#payment_details_<?php echo $lrl['o_id']; ?>" data-toggle="modal"> <i class="fa fa-eye"></i></a>

                                                                <!-- <a type="button" class="btn btn-sm btn-icon btn-secondary" id='delete_<?php echo $lrl['o_id']; ?>'><i class="fa fa-trash"></i></a> -->
                                                            </td>
                                                        </tr>
    
                                                        <!-- view model -->
                                                        <div class="modal" id="payment_details_<?php echo $lrl['o_id']; ?>">
                                                           <div class="modal-dialog" role="document">
                                                              <div class="modal-content modal-content-demo">
                                                                 <div class="modal-header">
                                                                    <h6 class="modal-title">Payment Details</h6>
                                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                 </div>
                                                                 <div class="modal-body">
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Order ID :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $lrl['order_id']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Test Series Name :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $lrl['test_series_id']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Payment Date :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $lrl['payment_date']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Payment Type :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p>
                                                                             <?php 
                                                                                //echo $lrl['payment_type'];
                                                                                if ($lrl['payment_type'] == 2) 
                                                                                {
                                                                                echo "Online";
                                                                                }elseif ($lrl['payment_type'] == 1) 
                                                                                {
                                                                                echo "Wallet";
                                                                                ?>
                                                                             <!-- <span class="badge">Wallet</span> -->
                                                                             <?php
                                                                                }
                                                                                ?>
                                                                          </p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Payment Rs :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $lrl['amount']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Transaction ID :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $lrl['transaction_id']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Order ID :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $lrl['order_id']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Payment Mode :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $lrl['payment_mode']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                 </div>
                                                                 <div class="modal-footer">
                                                                    <button class="btn cancel-btn"
                                                                       data-dismiss="modal" type="button">Close</button>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>

</body>

</html>