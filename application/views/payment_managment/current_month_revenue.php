<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Current Month Revenue </title>
    <?php $this->load->view('css'); ?>
</head>

<body class="app sidebar-mini light-mode light-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">  Current Month Revenue</h4>
                            </span>
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center ">

                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center ">
                                        <span class="breadcrumb-icon"> Payment Management</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">  Current Month Revenue</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!--Row-->
                    <div class="row justify-content-around">
                        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                            <div class="card ">
                                <div class="card-header">
                                    <div class="card-title">Payment Details
                                    </div>
                                    <!-- <div class="card-options">
                                        <a type="button" onclick="generateExcel('Report')" id="excel" class="btn btn-icon btn-primary" 
                                            >Excel</a>
                                    </div> -->
                                </div>
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <div class="col-md-5 col-12">
                                            <form method="post" action="">
                                                <div class="input-group mt-2">
                                                    <select name="test_series_id" id="test_series_id"  class="form-control search-input table-search-box" tabindex="-1" aria-hidden="true" required>
                                                        <option value="" selected disabled>Select Test Series Name </option>
                                                        <?php 
                                                        if (!empty($test_series_list)) 
                                                        {
                                                            foreach ($test_series_list as $t) 
                                                            { 
                                                                ?>
                                                                <option value="<?php echo $t['test_series_id'] ?>">
                                                                    <?php echo $t['test_series_name'] ?>
                                                                </option>
                                                                <?php   
                                                            }  
                                                        } 
                                                        else
                                                        {
                                                            echo " ";
                                                        }
                                                        ?>
                                                    </select>

                                                    <button class="btn btn-primary" type="submit" name="search_todays_revenue">Go!</button>  
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="float-right mb-3">
                                        <input type="text" class="form-control" name="" id="myInput" placeholder="Search">
                                    </div>
                                    <div class="table-responsive ">
                                        <table border="1"
                                            class="table table-hover border table-vcenter text-nowrap category-table"
                                            id="myTable">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="wd-15p border-bottom-0">Sr. No.</th>
                                                    <th class="wd-15p border-bottom-0">User Name</th>
                                                    <!-- <th class="wd-15p border-bottom-0">Course Name</th> -->
                                                    <th class="wd-15p border-bottom-0">Test Series Name </th>
                                                    <th class="wd-15p border-bottom-0">Amount </th>
                                                    <th class="wd-20p border-bottom-0">Payment Status</th>
                                                    <th class="wd-20p border-bottom-0">Payment Type</th>
                                                    <th class="wd-20p border-bottom-0">Payment Date</th>
                                                    <!-- <th class="wd-20p border-bottom-0">Transaction Id</th>
                                                    <th class="wd-20p border-bottom-0">Order Id</th>
                                                    <th class="wd-20p border-bottom-0">Payment Mode</th> -->
                                                    <th class="wd-20p border-bottom-0 action">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                               <?php 
                                                if (!empty($monthly_revenue_list)) 
                                                {
                                                    $i= 1+$this->uri->segment(2);
                                                    foreach ($monthly_revenue_list as $mon_r) 
                                                    {
                                                        $where = '(test_series_id = "'.$mon_r['test_series_id'].'")';
                                                        $test_series = $this->Main_Model->getData($tbl='test_series', $where);
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i++; ?></td>
                                                            <td><?php echo $mon_r['full_name']; ?></td>
                                                            <!-- <td>NEET</td> -->
                                                            <td><?php  
                                                            echo $test_series['test_series_name'];
                                                            //echo $mon_r['test_series_id']; ?></td>
                                                            <td><i class="fa fa-inr" aria-hidden="true"></i> 
                                                                <?php echo $mon_r['amount']; ?></td>
                                                            <td>
                                                                <?php 
                                                                // echo $mon_r['payment_status'];
                                                                if ($mon_r['payment_status'] == 1) 
                                                                {
                                                                    ?>
                                                                    <span class="badge badge-success">Success</span>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <span class="badge badge-danger">Fail</span>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php 
                                                                if ($mon_r['payment_type'] == 2) 
                                                                {
                                                                    echo "Online";
                                                                }elseif ($mon_r['payment_type'] == 1) 
                                                                {
                                                                    echo "Wallet";
                                                                    ?>
                                                                    <!-- <span class="badge">Wallet</span> -->
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td><?php echo $mon_r['payment_date']; ?></td>
                                                            <td class="action">
                                                                <a type="button" class="btn btn-sm btn-icon btn-primary" data-target="#payment_details_<?php echo $mon_r['o_id']; ?>" data-toggle="modal"> <i class="fa fa-eye"></i></a>

                                                                <!-- <a type="button" class="btn btn-sm btn-icon btn-secondary" id='delete_<?php echo $mon_r['o_id']; ?>'><i class="fa fa-trash"></i></a> -->
                                                            </td>
                                                        </tr>

                                                        <!-- view model -->
                                                        <div class="modal" id="payment_details_<?php echo $mon_r['o_id']; ?>">
                                                           <div class="modal-dialog" role="document">
                                                              <div class="modal-content modal-content-demo">
                                                                 <div class="modal-header">
                                                                    <h6 class="modal-title">Payment Details</h6>
                                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                 </div>
                                                                 <div class="modal-body">
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Order ID :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $mon_r['order_id']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Test Series Name :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $mon_r['test_series_id']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Payment Date :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $mon_r['payment_date']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Payment Type :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p>
                                                                             <?php 
                                                                                //echo $mon_r['payment_type'];
                                                                                if ($mon_r['payment_type'] == 2) 
                                                                                {
                                                                                    echo "Online";
                                                                                }
                                                                                elseif ($mon_r['payment_type'] == 1) 
                                                                                {
                                                                                    echo "Wallet";
                                                                                }
                                                                                ?>
                                                                          </p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Payment Rs :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $mon_r['amount']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Transaction ID :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $mon_r['transaction_id']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Order ID :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $mon_r['order_id']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                    <div class="row ml-5 mb-4">
                                                                       <div class="col-md-6">
                                                                          <p><b>Payment Mode :</b></p>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                          <p><?php echo $mon_r['payment_mode']; ?></p>
                                                                       </div>
                                                                    </div>
                                                                 </div>
                                                                 <div class="modal-footer">
                                                                    <button class="btn cancel-btn"
                                                                       data-dismiss="modal" type="button">Close</button>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        </div>

                                                        <?php
                                                    }
                                                }
                                                else
                                                {
                                                   echo "No matching records found";
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-footer align-items-center">
                                        <!-- Pagination -->
                                        <nav class="" aria-label="Page navigation example">
                                            <ul class="pagination justify-content-end">
                                            <?php echo $this->pagination->create_links(); ?>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
   
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script type="text/javascript">
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    </script>

    <script>
    function generateExcel(name) 
    {
        //getting data from our table
        $('#myTable').find('th.action, td.action').remove();
        var data_type = "data:application/vnd.ms-excel";
        var table_div = document.getElementById("myTable");
        var table_html = table_div.outerHTML.replace(/ /g, "%20");

        var a = document.createElement("a");
        a.href = data_type + ", " + table_html;
        a.download = name + ".xls";
        a.click();
        window.location.reload();
    }
    </script>
   
    <script>
    function search_table() {
        // Declare variables 
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td");
            for (j = 0; j < td.length; j++) {
                let tdata = td[j];
                if (tdata) {
                    if (tdata.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        break;
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    }
    </script>
</body>

</html>