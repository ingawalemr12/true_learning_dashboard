 <!--aside open-->
 <div class="app-sidebar app-sidebar2 ">
     <div class="app-sidebar__logo">
         <a class="header-brand" href="<?php echo base_url(''); ?>order-mgmt">
             <img src="<?php echo base_url(''); ?>/assets/images/brand/logo.png" class="header-brand-img desktop-lgo"
                 alt="Dashtic logo">
             <img src="<?php echo base_url(''); ?>/assets/images/brand/favicon.png" class="header-brand-img mobile-logo"
                 alt="Dashtic logo">
         </a>
     </div>
 </div>
 <aside class="app-sidebar app-sidebar3">
     <div class="app-sidebar__user">
         <div class="dropdown user-pro-body text-center">
             <div class="user-pic">
                 <a href="<?php echo base_url(''); ?>edit-profile">
                     <img src="<?php echo base_url(''); ?>/assets/images/users/16.jpg" alt="user-img"
                         class="avatar-xl rounded-circle mb-1">
                 </a>
             </div>
             <div class="user-info">
                 <a href="<?php echo base_url(''); ?>edit-profile">
                     <h3 class=" mb-1 font-weight-bold">John Thomson</h3>
                     <!-- <span class="text-muted app-sidebar__user-name text-mg">Shop Name</span> -->
                 </a>
             </div>
         </div>
     </div>
     <div class="app-sidebar-help">
         <div class="dropdown text-center">
             <div class="help d-flex">
                 <a href="#" class="nav-link p-0 help-dropdown" data-toggle="dropdown">
                     <span class="font-weight-bold">Help Info</span> <i class="fa fa-angle-down ml-2"></i>
                 </a>
                 <div class="ml-auto">
                     <a class="nav-link icon p-0" href="#">
                         <svg class="header-icon" x="1008" y="1248" viewBox="0 0 24 24" height="100%" width="100%"
                             preserveAspectRatio="xMidYMid meet" focusable="false">
                             <path opacity=".3" d="M12 6.5c-2.49 0-4 2.02-4 4.5v6h8v-6c0-2.48-1.51-4.5-4-4.5z"></path>
                             <path
                                 d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.9 2 2 2zm6-11c0-3.07-1.63-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.64 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2v-5zm-2 6H8v-6c0-2.48 1.51-4.5 4-4.5s4 2.02 4 4.5v6zM7.58 4.08L6.15 2.65C3.75 4.48 2.17 7.3 2.03 10.5h2a8.445 8.445 0 013.55-6.42zm12.39 6.42h2c-.15-3.2-1.73-6.02-4.12-7.85l-1.42 1.43a8.495 8.495 0 013.54 6.42z">
                             </path>
                         </svg>
                         <span class="pulse "></span>
                     </a>
                 </div>
             </div>
         </div>
     </div>
     <ul class="side-menu">
         <li class="slide">
             <a class="side-menu__item" href="<?php echo base_url(''); ?>dashboard">
                 <i class="side-menu__icon fa fa-home"></i>
                 <span class="side-menu__label">Dashboard</span>
             </a>
         </li>
         <li class="slide">
             <a class="side-menu__item" href="<?php echo base_url(''); ?>user-mgmt">
                 <i class="side-menu__icon fa fa-users"></i>
                 <span class="side-menu__label">User Managment</span>
             </a>
         </li>
         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>order-mgmt">
                <i class="side-menu__icon fa fa-book" aria-hidden="true"></i>
                <span class="side-menu__label">Cource Management</span>
            </a>
        </li> -->
         <li class="slide">
             <a class="side-menu__item" data-toggle="slide" href="#">
                 <i class="side-menu__icon fa fa-book"></i>
                 <span class="side-menu__label">Cource Management</span><i class="angle fa fa-angle-right"></i>
             </a>
             <ul class="slide-menu">
                 <li><a class="slide-item" href="<?php echo base_url(''); ?>"><span>Add Course</span></a></li>
                 <li><a class="slide-item" href="<?php echo base_url(''); ?>"><span>Store settings</span></a></li>
                 <li><a class="slide-item" href="<?php echo base_url(''); ?>"><span>Product management</span></a></li>
             </ul>
         </li>
         <li class="slide">
             <a class="side-menu__item" href="<?php echo base_url(''); ?>vendors-mgmt">
                 <i class="side-menu__icon fa fa-rupee-sign"></i>
                 <span class="side-menu__label">Payment Management</span>
             </a>
         </li>
         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>user-mgmt">
            <i class="side-menu__icon fa fa-question" aria-hidden="true"></i>    
                <span class="side-menu__label">Question Bank Management</span>
            </a>
        </li> -->
         <!-- <li class="slide">
             <a class="side-menu__item" data-toggle="slide" href="#">
                 <i class="side-menu__icon fa fa-question"></i>
                 <span class="side-menu__label">Question Bank Management</span><i class="angle fa fa-angle-right"></i>
             </a>
             <ul class="slide-menu">
                 <li><a class="slide-item" href="<?php echo base_url('add-category'); ?>"><span>Questions</span></a>
                 </li>
                 <li><a class="slide-item" href="<?php echo base_url(''); ?>"><span>Test</span></a></li>
             </ul>
         </li> -->
         <li class="slide ">
             <a class="side-menu__item" data-toggle="slide" href="#">
             <i class="side-menu__icon fa fa-question"></i>
                 <span class="side-menu__label">Question Bank Management</span><i class="angle fe fe-chevron-down"></i></a>
             <ul class="slide-menu">
                 <!-- <li class="side-menu-label1">
                     <a href="#"></a>Question Bank Management
                 </li> -->
                 <li class="sub-slide">
                     <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span
                             class="sub-side-menu__label">Questions</span><i class="sub-angle fe fe-chevron-down"></i></a>
                     <ul class="sub-slide-menu">
                         <li><a class="sub-slide-item" href="<?php echo base_url('add_questions'); ?>">Add Questions</a></li>
                         <li><a class="sub-slide-item" href="#">Manage</a></li>
                         <li><a class="sub-slide-item" href="#">Upload</a></li>
                     </ul>
                 </li>
                 <li class="sub-slide">
                     <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span
                             class="sub-side-menu__label">Test</span><i class="sub-angle fe fe-chevron-down"></i></a>
                     <ul class="sub-slide-menu">
                         <li><a class="sub-slide-item" href="#">New Test</a></li>
                         <li><a class="sub-slide-item" href="#">Manage Test</a></li>
                         <li><a class="sub-slide-item" href="#">Test Profile Setting</a></li>
                     </ul>
                 </li>
             </ul>
         </li>
         <li class="slide">
             <a class="side-menu__item" data-toggle="slide" href="#">
                 <i class="side-menu__icon fa fa-cog"></i>
                 <span class="side-menu__label">Setting</span><i class="angle fa fa-angle-right"></i>
             </a>
             <ul class="slide-menu">
                 <li><a class="slide-item" href="<?php echo base_url('add-category'); ?>"><span>Add Category</span></a>
                 </li>
                 <li><a class="slide-item" href="<?php echo base_url(''); ?>"><span>Add Branch</span></a></li>
                 <li><a class="slide-item" href="<?php echo base_url('add_board'); ?>"><span>Add Board</span></a></li>
                 <li><a class="slide-item" href="<?php echo base_url('add_standard'); ?>"><span>Add standard</span></a>
                 </li>
                 <li><a class="slide-item" href="<?php echo base_url(''); ?>"><span>Add Exam </span></a></li>
             </ul>
         </li>
         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>banner-mgmt">
                <i class="side-menu__icon fa fa-file"></i>
                <span class="side-menu__label">Banner Managment</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>enq-form">
                <i class="side-menu__icon fa fa-edit"></i>
                <span class="side-menu__label"> Enquiry Forms</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>partner-enqForm">
                <i class="side-menu__icon fa fa-handshake"></i>
                <span class="side-menu__label">Partners Enquiry Forms</span>
            </a>
        </li> -->
         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>partner-mgmt">
                <i class="side-menu__icon fa fa-handshake"></i>
                <span class="side-menu__label">Partners Managment</span>
            </a>
        </li> -->
         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>website-mgmt">
                <i class="side-menu__icon fa fa-chrome"></i>
                <span class="side-menu__label">Vendor Website </span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>add-buckets">
                <i class="side-menu__icon fa fa-shopping-basket"></i>
                <span class="side-menu__label">Manage Bucketst</span>
            </a>
        </li>
         -->
         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>prod-mgmt">
                <i class="side-menu__icon fa fa-anchor"></i>
                <span class="side-menu__label">Product Managment</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>invoice">
                <i class="side-menu__icon fa fa-file"></i>
                <span class="side-menu__label"> Invoice</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>printers">
                <i class="side-menu__icon fa fa-print"></i>
                <span class="side-menu__label"> Printers</span>
            </a>
        </li> -->
         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>staff-mgmt">
                <i class="side-menu__icon fa fa-user-plus"></i>
                <span class="side-menu__label"> Staff management</span>
            </a>
        </li>  -->
         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>admin">
                <i class="side-menu__icon fa fa-file"></i>
                <span class="side-menu__label">Admin Setting</span>
            </a>
        </li>  -->


         <!-- <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>subscription-mgmt">
                <i class="side-menu__icon fa fa-rupee-sign"></i>
                <span class="side-menu__label">Subscription Managment</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#">
                <i class="side-menu__icon fa fa-chrome"></i>
                <span class="side-menu__label">Website Managment</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li class="side-menu-label1"><a href="#"></a>Website Managment</li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>home"><span>Home page </span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>banner-mgmt"><span>Banner Managment</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>about-us"><span>About us </span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>faq"><span>FAQ </span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>privacy-policy"><span>Privacy policy </span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>term-condition"><span>Term & Condition </span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>contact-us"><span>Contact </span></a></li>

            </ul>
        </li>
        
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#">
                 <i class="side-menu__icon fa fa-cogs"></i>
                <span class="side-menu__label">Configuration</span>
               
                <i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
            <li class="side-menu-label1"><a href="#"></a>Configuration</li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>sms-confi"><span>SMS Configuration</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>email-confi"><span>Email Configuration</span></a></li>
            </ul>
        </li> -->
         <li class="slide">
             <a class="side-menu__item" href="<?php echo base_url(''); ?>revenue-mgmt">
                 <i class="side-menu__icon fa fa-chart-area"></i>
                 <span class="side-menu__label">Revenue Managment</span>
             </a>
         </li>
         <!-- <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#">
                <i class="side-menu__icon fa fa-cog"></i>
                <span class="side-menu__label">Setting</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-category"><span>Add Category</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-garments"><span>Add Garments</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-defect"><span>Add Defect</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-stains"><span>Add Stains</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-color"><span>Add Color</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-basket"><span>Add Basket</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-rank"><span>Add Rank</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-price-time"><span>Add Price & Time</span></a></li>


            </ul>
        </li> -->
     </ul>

 </aside>
 <!--aside closed-->


 <!-- 
<div class="app-sidebar app-sidebar2">
    <div class="app-sidebar__logo">
        <a class="header-brand" href="<?php echo base_url(''); ?>dashboard">
            <img src="<?php echo base_url(''); ?>/assets/images/brand/logo.png" class="header-brand-img desktop-lgo" alt="Dashtic logo">
            <img src="<?php echo base_url(''); ?>/assets/images/brand/favicon.png" class="header-brand-img mobile-logo" alt="Dashtic logo">
        </a>
    </div>
</div>
<aside class="app-sidebar app-sidebar3">
    <div class="app-sidebar__user">
        <div class="dropdown user-pro-body text-center">
            <div class="user-pic">
                <a href="<?php echo base_url(''); ?>profile">
                    <img src="<?php echo base_url(''); ?>/assets/images/users/16.jpg" alt="user-img" class="avatar-xl rounded-circle mb-1">
                </a>
            </div>
            <div class="user-info">
                <a href="<?php echo base_url(''); ?>profile">
                    <h3 class=" mb-1 font-weight-bold">John Thomson</h3>
                    <span class="text-muted app-sidebar__user-name text-mg">Shop Name</span>
                </a>
            </div>
        </div>
    </div>
    <ul class="side-menu">
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>dashboard">
            <i class="side-menu__icon fa fa-home"></i>
            <span class="side-menu__label">Dashboard</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>user-mgmt">
                <i class="side-menu__icon fa fa-users"></i>
                <span class="side-menu__label">User Managment</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>user-mgmt">
                <i class="side-menu__icon fa fa-users"></i>
                <span class="side-menu__label">Order Managment</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#">
                <i class="side-menu__icon fa fa-cog"></i>
                <span class="side-menu__label">Order Managment</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a class="slide-item" href="<?php echo base_url(''); ?>new-order"><span>New Order</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>cleaning"><span>Cleaning</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>ready"><span>Ready</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>pickups"><span>Pickups</span></a></li>


            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>dashboard">
                <i class="side-menu__icon fa fa-rupee-sign"></i>
                <span class="side-menu__label">Subscription Managment</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" href="<?php echo base_url(''); ?>dashboard">
                <i class="side-menu__icon fa fa-chart-area"></i>
                <span class="side-menu__label">Revenue Managment</span>
            </a>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#">
                <i class="side-menu__icon fa fa-cog"></i>
                <span class="side-menu__label">Setting</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-category"><span>Add Category</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-garments"><span>Add Garments</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-defect"><span>Add Defect</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-stains"><span>Add Stains</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-color"><span>Add Color</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-basket"><span>Add Basket</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-rank"><span>Add Rank</span></a></li>
                <li><a class="slide-item" href="<?php echo base_url(''); ?>add-price-time"><span>Add Price & Time</span></a></li>


            </ul>
        </li>
    </ul>
    <div class="app-sidebar-help">
        <div class="dropdown text-center">
            <div class="help d-flex">
                <a href="#" class="nav-link p-0 help-dropdown" data-toggle="dropdown">
                    <span class="font-weight-bold">Help Info</span> <i class="fa fa-angle-down ml-2"></i>
                </a>
                <div class="ml-auto">
                    <a class="nav-link icon p-0" href="#">
                        <svg class="header-icon" x="1008" y="1248" viewBox="0 0 24 24" height="100%" width="100%" preserveAspectRatio="xMidYMid meet" focusable="false"><path opacity=".3" d="M12 6.5c-2.49 0-4 2.02-4 4.5v6h8v-6c0-2.48-1.51-4.5-4-4.5z"></path><path d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.9 2 2 2zm6-11c0-3.07-1.63-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.64 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2v-5zm-2 6H8v-6c0-2.48 1.51-4.5 4-4.5s4 2.02 4 4.5v6zM7.58 4.08L6.15 2.65C3.75 4.48 2.17 7.3 2.03 10.5h2a8.445 8.445 0 013.55-6.42zm12.39 6.42h2c-.15-3.2-1.73-6.02-4.12-7.85l-1.42 1.43a8.495 8.495 0 013.54 6.42z"></path></svg>
                        <span class="pulse "></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</aside> -->