<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Home</title>
    <?php $this->load->view('css'); ?>
</head>

<!-- <body class="app sidebar-mini light-mode default-sidebar"> -->

<body class="app sidebar-mini light-mode light-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">
                    <?php 
                    if (!empty($this->session->flashdata('login_success')) )
                    { ?>
                      <div class="alert alert-success" id="alert_msg">
                          <?php echo $this->session->flashdata('login_success');?>
                      </div>
                      <?php
                    }
                    ?> 

                    <!--Row-->
                    <div class="row  main-menu ">
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="<?php echo base_url('dashboard'); ?>">
                                    <div class="card-body text-center list-icons">
                                        <i class="svg-icon fa fa-database icon-dropshadow-primary"></i>
                                        <p class="card-text mt-3 mb-0">Dashboard
                                        </p>
                                        <!-- <p class="h2 text-center font-weight-bold">
                                        <a href="http://">Course Management</a>
                                        </p> -->
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="<?php echo base_url('user_mgmt'); ?>">
                                    <div class="card-body text-center list-icons">

                                        <i class="svg-icon fa fa-user icon-dropshadow-primary" aria-hidden="true"></i>

                                        <p class="card-text mt-3 mb-0">User
                                            management</p>
                                        <!-- <p class="h2 text-center font-weight-bold">Question Bank management</p> -->
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                            <a href="<?php echo base_url('payment_mgmt'); ?>">
                                <div class="card-body text-center list-icons">
                                    
                                        <i class="svg-icon fa fa-inr icon-dropshadow-primary" aria-hidden="true"></i>
                                    
                                    <p class="card-text mt-3 mb-0">Payment management</p>
                                    <!-- <p class="h2 text-center font-weight-bold">App Management</p> -->
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                            <a href="<?php echo base_url('banner_screen'); ?>">
                                <div class="card-body text-center list-icons">
                                    <i
                                            class="svg-icon fa fa-globe icon-dropshadow-primary"></i>
                                    <p class="card-text mt-3 mb-0">Website / App
                                            Management</p>
                                    <!-- <p class="h2 text-center font-weight-bold">Website Management</p> -->
                                </div>
                                </a>
                            </div>
                        </div>
                       <!-- <div class="col-6 col-lg-4">
                            <div class="card">
                            <a href="<?php echo base_url(''); ?>">
                                <div class="card-body text-center list-icons">
                                    

                                        <i class="svg-icon fa fa-mobile icon-dropshadow-primary"
                                            aria-hidden="true"></i>
                                    <p class="card-text mt-3 mb-0">App Management</p>
                                </div>
                                </a>
                            </div>
                        </div>-->
                        <div class="col-6 col-lg-4">
                            <div class="card">
                            <a href="<?php echo base_url('student_resourse'); ?>">
                                <div class="card-body text-center list-icons">
                                    <i
                                            class="svg-icon fa fa-book icon-dropshadow-primary"></i>

                                    <p class="card-text mt-3 mb-0">Student Resourse
                                        </p>
                                    <!-- <p class="h2 text-center font-weight-bold">Question Bank management</p> -->
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                            <a href="<?php echo base_url('add-category'); ?>">
                                <div class="card-body text-center list-icons">
                                    <i
                                            class="svg-icon fa fa-cog icon-dropshadow-primary"></i>
                                    <p class="card-text mt-3 mb-0">Settings</p>
                                    <!-- <p class="h2 text-center font-weight-bold">Website Management</p> -->
                                </div>
                                </a>
                            </div>
                        </div>
                        
                        <div class="col-6 col-lg-4">
                            <div class="card">
                            <a href="<?php echo base_url('add_questions'); ?>">
                                <div class="card-body text-center list-icons">
                                    <i
                                            class="svg-icon fa fa-question icon-dropshadow-primary"></i>

                                    <p class="card-text mt-3 mb-0">Question Bank
                                            management</p>
                                    <!-- <p class="h2 text-center font-weight-bold">Question Bank management</p> -->
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                            <a href="<?php echo base_url('manage_testSeries'); ?>">
                                <div class="card-body text-center list-icons">
                                    <i
                                            class="svg-icon fa fa-pencil-square-o icon-dropshadow-primary"></i>

                                    <p class="card-text mt-3 mb-0">Test
                                            management</p>
                                    <!-- <p class="h2 text-center font-weight-bold">Question Bank management</p> -->
                                </div>
                                </a>
                            </div>
                        </div>
                        <!-- <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mb-lg-4 mb-xl-0">
                                                <div class="mb-2 fs-18 text-muted">
                                                    Total Application
                                                </div>
                                                <h1 class="font-weight-bold mb-1">45,675</h1>
                                                <span class="text-primary"><i class="fa fa-arrow-up mr-1"></i> +1.4%</span>
                                            </div>
                                            <div class="col col-auto">
                                                <div class="mx-auto chart-circle chart-circle-md chart-circle-primary mt-sm-0 mb-0" data-value="0.85" data-thickness="12" data-color="#4454c3">
                                                    <div class="mx-auto chart-circle-value text-center fs-20">85%</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mb-lg-4 mb-xl-0">
                                                <div class="mb-2 fs-18 text-muted">
                                                    Shortlisted
                                                </div>
                                                <h1 class="font-weight-bold mb-1">30,175</h1>
                                                <span class="text-success"><i class="fa fa-arrow-up mr-1"></i> +1.8%</span>
                                            </div>
                                            <div class="col col-auto">
                                                <div class="mx-auto chart-circle chart-circle-md chart-circle-success mt-sm-0 mb-0" data-value="0.60" data-thickness="12" data-color="#2dce89">
                                                    <div class="mx-auto chart-circle-value text-center fs-20">60%</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mb-lg-4 mb-xl-0">
                                                <div class="mb-2 fs-18 text-muted">
                                                    Rejected
                                                </div>
                                                <h1 class="font-weight-bold mb-1">7,745</h1>
                                                <span class="text-danger"><i class="fa fa-arrow-down mr-1"></i> -2.4%</span>
                                            </div>
                                            <div class="col col-auto">
                                                <div class="mx-auto chart-circle chart-circle-md chart-circle-secondary mt-sm-0 mb-0" data-value="0.45" data-thickness="12" data-color="#f7346b">
                                                    <div class="mx-auto chart-circle-value text-center fs-20">25%</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                    </div>
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script type="text/javascript">
    const myTimeout = setTimeout(close, 2000);

    function close() 
    {
        document.getElementById("alert_msg").style.display = "none";
    }
    </script>
</body>

</html>