<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Forgot Password</title>

    <?php $this->load->view('css'); ?>
</head>

<body class="h-100vh page-style1 light-mode default-sidebar">
    <div class="page">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col mx-auto">
                        <div class="row justify-content-center">
                            <div class="col-md-5">
                                <div class="card card-group mb-0">
                                    <div class="card p-4">
                                        <div class="card-body">
                                            <div class="text-center title-style mb-6">
                                                <h1 class="mb-2">Forgot Password</h1>
                                                
                                                <hr>
                                                <?php 
                                                if (!empty($this->session->flashdata('email_fail')) )
                                                { ?>
                                                    <div style="color: red !important" id="alert_msg">
                                                        <?php echo $this->session->flashdata('email_fail');?>
                                                    </div>
                                                    <?php
                                                }

                                                if (!empty($this->session->flashdata('mail_send')) )
                                                { ?>
                                                  <div style="color: green !important" id="alert_msg">
                                                      <?php echo $this->session->flashdata('mail_send');?>
                                                  </div>
                                                  <?php
                                                }
                                                ?>  
                                            </div>

                                            <form method="post" action="<?php echo base_url('forgot_Password_dashboard')?>">
                                                <div class="input-group mb-4">
                                                    <span class="input-group-addon"> <i class="svg-icon fa fa-envelope"></i></span>
                                                    <input type="email" name="email" id="email" placeholder="Enter Your Email" class="form-control <?php echo (form_error('email') !="") ? 'is-invalid' : ''; ?>" value="<?php echo set_value('email'); ?>" required >

                                                    <?php echo form_error('email'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"/>
                                                        <span class="custom-control-label">Agree the <a >terms and policy</a>
                                                        </span>
                                                    </label>
                                                </div>
                                                  
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn  btn-lg btn-primary btn-block px-4"><i class="fa fa-arrow-right"></i> Send</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center pt-4">
                                    <div class="font-weight-normal fs-16">Forget it <a
                                            class="btn-link font-weight-normal"
                                            href="<?php echo base_url(); ?>">Send me back</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jquery js-->
    <?php $this->load->view('js'); ?>

    <script type="text/javascript">
        const myTimeout = setTimeout(close, 3000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

</body>

</html>