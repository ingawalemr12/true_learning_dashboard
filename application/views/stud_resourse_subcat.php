<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Student Resourse </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">
                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Student Resourse</h4>
                            </span>
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Student Resourse</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('aside_stud_resourse'); ?>
                        </div>
                        <div class="col-md-12">
                            <?php 
                            if (!empty($this->session->flashdata('create')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('create');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('edit')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('edit');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('exists')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-warning" id="alert_msg">
                                      <?php echo $this->session->flashdata('exists');?>
                                  </div>
                              </div>
                            <?php
                            }
                            ?>
                            <div class="card ">
                                <div class="card-header">
                                    <div class="card-title">Sub-Category
                                        Info</div>
                                    <div class="card-options">
                                        <a type="button" class="btn btn-icon btn-primary" data-target="#add_subCategory"
                                            data-toggle="modal"><i class="fa fa-plus"></i>&nbsp;Add Sub-Category</a>

                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <div class="col-md-6">
    <!-- filter -->
    <div class="row justify-content-center">
        <div class="col-md-8 col-12">
            <form method="post" action="">
                <div class="input-group">
                    <select  class="form-control text-center table-search-box" name="cat_id" id="cat_id" required>
                        <option value="" selected disable>Select Category </option>
                        <?php 
                        if (!empty($category_list)) 
                        {
                            foreach ($category_list as $cat_list) 
                            {
                                ?>
                                <option value="<?php echo $cat_list['cat_id']; ?>"><?php echo $cat_list['category_name']; ?></option>
                                <?php  
                            }
                        }
                        ?>
                    </select> &nbsp;&nbsp;
                   
                    <select name="sub_cat_name" id="sub_cat_id"class="form-control text-center table-search-box" required>
                        <option value="" selected disabled>Select Sub-Categories</option>

                    </select>

                    <button type="submit" class="btn btn-primary" name="search_sub_category" type="button">Go!</button>
                </div>
            </form>
        </div>
    </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive ">
                                        <!-- <div class="table-responsive" id="lightgallery"> -->
    <table class="table table-hover border table-vcenter text-nowrap">
        <thead class="text-center">
            <tr>
                <th class="wd-15p border-bottom-0">
                    Sr. No.</th>
                <th class="wd-15p border-bottom-0">
                    Category Name
                </th>
                <th class="wd-15p border-bottom-0">
                    Sub-Category Name
                </th>
                <th class="wd-15p border-bottom-0">
                    Details
                </th>
                <th class="wd-20p border-bottom-0">
                    Active/Deactive
                </th>
                <th class="wd-20p border-bottom-0">
                    Action</th>
            </tr>
        </thead>
        <tbody class="text-center">
            <?php 
            if (!empty($sub_category_list)) 
            {
                $i= 1+$this->uri->segment(2);
                foreach ($sub_category_list as $sub_cat_list) 
                {
                    ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td>
                            <?php //echo $sub_cat_list['cat_id']; 
                            if (!empty($category_list)) 
                            {
                                foreach ($category_list as $cat_list) 
                                {
                                    if ($cat_list['cat_id'] == $sub_cat_list['cat_id'] ) 
                                    {
                                       echo $cat_list['category_name'];
                                    } 
                                }
                            }
                            else
                            {
                                echo " ";
                            }
                            ?>
                        </td>
                        <td><?php echo $sub_cat_list['sub_cat_name']; ?></td>
                        <td>
                            <a type="button" class="btn btn-sm btn-icon btn-info" data-target="#veiw_details_<?php echo $sub_cat_list['sub_cat_id']; ?>" data-toggle="modal"><i class="fa fa-eye"></i></a>
                        </td>
                        <td>
                            <?php 
                            if ($sub_cat_list['status'] == 1) 
                            {
                                ?>
                                <input type="checkbox" id="<?php echo $sub_cat_list['sub_cat_id']; ?>" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="0"> 
                                <?php
                            } 
                            else
                            {
                                ?>
                                <input type="checkbox" id="<?php echo $sub_cat_list['sub_cat_id']; ?>" data-size="sm" data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="1"> 
                                <?php         
                            } 
                            ?>
                        </td>
                        <td>
                            <!-- edit -->
                            <a type="button" class="btn btn-sm btn-icon btn-primary" data-target="#edit_subCategory_<?php echo $sub_cat_list['sub_cat_id']; ?>" data-toggle="modal"><i class="fa fa-edit"></i></a> 

                            <!-- delete -->
                            <a type="button" class="btn btn-sm btn-icon btn-secondary" id='delete_<?php echo $sub_cat_list['sub_cat_id']; ?>'><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>

                    <!-- description -->
                    <div class="modal" id="veiw_details_<?php echo $sub_cat_list['sub_cat_id']; ?>">
                        <div class="modal-dialog model-fullwidth" role="document">
                            <div class="modal-content modal-content-demo">
                                <div class="modal-header">
                                    <h6 class="modal-title">View Details</h6>
                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span> </button>
                                </div>
                                <div class="modal-body">
                                    <div class="">
                                       <textarea id="summernote" class="summernote1" name="example"><?php echo $sub_cat_list['upload_details']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn save-btn" type="button">Save changes</button> <button class="btn cancel-btn"
                                    data-dismiss="modal" type="button">Close</button>
                            </div>
                        </div>
                    </div>

                    <!-- delete -->
                    <script>
                    document.getElementById('delete_<?php echo $sub_cat_list['sub_cat_id']; ?>').onclick = function() 
                    {
                        var id = $("#<?php echo $sub_cat_list['sub_cat_id']; ?>").val(); 

                        swal({
                                  title: "Are you sure?",
                                  text: "You will not be able to recover this file!",
                                  type: "warning",
                                  showCancelButton: true,
                                  confirmButtonColor: '#DD6B55',
                                  confirmButtonText: 'Yes, delete it!',
                                  cancelButtonText: "No, cancel",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                          },
                          function(isConfirm) {
                              if (isConfirm) 
                              {
                                  $.ajax({
                                             url: '<?php echo base_url().'delete_stud_resourse_subcat/'.$sub_cat_list['sub_cat_id']; ?>',
                                             type: "POST",
                                             data: {id:id},
                                             dataType:"HTML",
                                             success: function () {
                                              swal(
                                                      "Deleted!",
                                                      "Your file has been deleted!",
                                                      "success"
                                                  ),
                                                  $('.confirm').click(function()
                                                  {
                                                      location.reload();
                                                  });
                                              },
                                      });

                              } else {
                                  swal(
                                      "Cancelled",
                                      "Your  file is safe !",
                                      "error"
                                  );
                              }
                          });
                    };
                    </script>

                    <!-- edit -->
                    <div class="modal" id="edit_subCategory_<?php echo $sub_cat_list['sub_cat_id']; ?>">
                        <div class="modal-dialog model-fullwidth" role="document">
                            <div class="modal-content modal-content-demo">
                                <div class="modal-header">
                                    <h6 class="modal-title">Edit Sub-Category</h6>
                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">&times;</span> </button>
                                </div>
                                <form method="post"  action="<?php echo base_url().'edit_stud_resourse_subcat/'.$sub_cat_list['sub_cat_id']; ?>" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Select Category 
                                                    <span class="text-red">*</span></label>
                                                <select class="form-control" name="cat_id" id="cat_id" required>
                                                    <option value="" selected disable>Select Category </option>
                                                    <?php 
                                                    if (!empty($category_list)) 
                                                    {
                                                        foreach ($category_list as $cat_list) 
                                                        {
                                                            if ($cat_list['cat_id'] == $sub_cat_list['cat_id'] ) 
                                                            {
                                                                ?>
                                                                <option selected="selected" value="<?php echo $cat_list['cat_id']; ?>"><?php echo $cat_list['category_name']; ?></option>
                                                                <?php  
                                                            } 
                                                            else 
                                                            {
                                                                ?>
                                                                <option value="<?php echo $cat_list['cat_id']; ?>"><?php echo $cat_list['category_name']; ?></option>
                                                                <?php  
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        echo " ";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Sub-Category Name <span class="text-red">*</span></label>
                                                
                                                <input type="text" class="form-control" placeholder="Enter Sub-Category Name" name="sub_cat_name" id="sub_cat_name" required value="<?php echo $sub_cat_list['sub_cat_name']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Upload Details<span class="text-red">*</span></label>
                                                <textarea id="summernote" name="upload_details" class="summernote" required><?php echo $sub_cat_list['upload_details']; ?></textarea>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn save-btn" >Update Info</button> 
                                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <?php  
                }
            }
            ?>
        </tbody>
    </table>
                                    </div>
                                    <div class="table-footer align-items-center">
                                        <!-- <p class="">Showing 1 to 3 of 3 entries</p> -->
                                        <nav class="" aria-label="Page navigation example">
                                            <ul class="pagination justify-content-end">
                                                <?php echo $this->pagination->create_links(); ?>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- add_subCategory -->
    <div class="modal" id="add_subCategory">
        <div class="modal-dialog model-fullwidth" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Add Sub-Category</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="<?php //echo base_url('add-category'); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Select Category
                                    <span class="text-red">*</span></label>
                                <select class="form-control" name="cat_id" id="cat_id" required>
                                    <option value="" selected disable>Select Category </option>
                                    <?php 
                                    if (!empty($category_list)) 
                                    {
                                        foreach ($category_list as $cat_list) 
                                        {
                                            ?>
                                            <option value="<?php echo $cat_list['cat_id']; ?>"><?php echo $cat_list['category_name']; ?></option>
                                            <?php  
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="bannername" class="form-label">Sub-Category Name
                                    <span class="text-red">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Sub-Category Name" name="sub_cat_name" id="sub_cat_name" required>
                            </div>
                            <div class="form-group">
                                <label for="bannername" class="form-label">Upload Details
                                    <span class="text-red">*</span></label>
                                <textarea id="summernote" name="upload_details" class="summernote" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_student_resourse_sub_category" class="btn save-btn" >Save changes</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 300,
            disableResizeEditor: true,
        });
        $('.summernote1').summernote({
            height: 500,
            disableResizeEditor: true,
        });
    });
    </script>
    
    <script type="text/javascript">
    const myTimeout = setTimeout(close, 2000);

    function close() 
    {
        document.getElementById("alert_msg").style.display = "none";
    }
    </script>

    <!-- status active-deactive -->
    <script type="text/javascript"> 
    function change_status(idd)
    {
     
        var status_val=idd.value;
        var cat_id= idd.id;
        console.log(status_val);
      
        //var status_val= document.getElementById('status_'.idd).value;
        //console.log(status_val);
        var base_url = '<?php echo base_url() ?>';
       // var status = ;

        var cat_id = cat_id;
        // alert(cat_id);

        if(status_val != '')
        {
            $.ajax({
                      url:base_url + "MainController/active_student_resourse_category_status",
                      method:"POST",
                      data:{status:status_val,cat_id:cat_id},
                      
                      success: function(data)
                      {
                          //alert(data);
                            if (data == 1) 
                            {
                               alert('Status Changed Sucessfully !..');
                               location.reload();
                            } 
                            else 
                            {
                               alert('Something Went Wrong !...')
                            }
                      }
                });
        }
    }
    </script>

    <!-- status active-deactive -->
   <script type="text/javascript"> 
   function change_status(idd)
   {
     
        var status_val=idd.value;
        var sub_cat_id= idd.id;
        
        var base_url = '<?php echo base_url() ?>';
        // var status = ;

        var sub_cat_id = sub_cat_id;
        // alert(sub_cat_id);

        if(status_val != '')
        {
            $.ajax({
                      url:base_url + "MainController/active_stud_sub_category_status",
                      method:"POST",
                      data:{status:status_val,sub_cat_id:sub_cat_id},
                      
                      success: function(data)
                      {
                          //alert(data);
                            if (data == 1) 
                            {
                               alert('Status Changed Sucessfully !..');
                               location.reload();
                            } 
                            else 
                            {
                               alert('Something Went Wrong !...')
                            }
                      }
                });
        }
    }
   </script>

    <!-- on change of CATEGORY NAME , it select SUB-CATEGORY  -->
    <script type="text/javascript">
    $("document").ready(function () 
    {
        $("#cat_id").change(function () 
        {
            var cat_id = $("#cat_id").val();  //$(this).val();
            // alert(cat_id);

            if (cat_id !="") 
            {
                $.ajax({
                    url:'<?php echo base_url().'MainController/get_sub_category_list' ?>',
                    method:'POST',
                    data: {cat_id:cat_id},
                    success:function (data) {
                        $("#sub_cat_id").html(data);
                    }
                }); //end ajax
            }

        }); // end change(function)

    });  //  end js(function)
    </script> 

</body> 

</html>