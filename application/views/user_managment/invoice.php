<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | User Managment</title>

    <?php $this->load->view('css'); ?>
</head>

<body class="app sidebar-mini light-mode light-sidebar">

    <div class="wrapper">

        <?php $this->load->view('header'); ?>
        <?php 
        $uid = $this->uri->segment(2);
         // echo $uid;die;
        $where = '(uid="'.$uid.'")';
        $user_profile = $this->Main_Model->getData($tbl='user_details',$where);
        // print_r($user_profile);
         ?>
        <section class="content ">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Invoice</h4>
                            </span>

                        </div>

                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center ">
                                    <i class="breadcrumb-item-icon fa fa-home"></i>
                                    <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    <a href="<?php echo base_url('user_mgmt'); ?>" class="d-flex align-items-center "><span class="breadcrumb-icon"> User Managment</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->

                    <?php 
                    // invoice order details
                    $test_series_id = $this->uri->segment(3);
                    $wh_o = '(test_series_id="'.$test_series_id.'")';
                    $invoice = $this->Main_Model->getData($tbl='orders',$wh_o);


                    // test_series details
                    $wh_s = '(test_series_id="'.$test_series_id.'")';
                    $t_series = $this->Main_Model->getData($tbl='test_series',$wh_s);

                    // category details
                    $wh_s = '(category_id="'.$invoice['category_id'].'")';
                    $category = $this->Main_Model->getData($tbl='category',$wh_s);
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card overflow-hidden">
                                <div class="card-status bg-primary"></div>
                                <div class="card-body">
                                    <h2 class="text-muted font-weight-bold text-center">
                                        <img src="<?php echo base_url(''); ?>/assets/images/brand/logo12X512.png" class="invoice-brand-img " alt="logo">
                                    </h2>
                                    <div class="">
                                        <h5 class="mb-1">Hi <strong> <?php echo $user_profile['full_name']; ?></strong>,</h5>

                                            This is the receipt for a payment of 
                                            <strong><i class=" fa fa-inr "></i>
                                                <?php echo $invoice['amount'];?> 
                                            </strong>
                                    </div>

                                    <div class="card-body pl-0 pr-0">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <span>Order Id : </span><br>
                                                <strong>#<?php echo $invoice['order_id'];?>  </strong>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <span>Payment Date</span><br>
                                                <strong>
                                                <?php echo date("M d, Y", strtotime($invoice['payment_date']));?> 
                                                </strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider"></div>
                                    <div class="row pt-4">
                                        <div class="col-lg-6 ">
                                            <p class="h5 font-weight-bold">Bill From</p>
                                            <?php 
                                            $wh_ct = '(id = 1 )';
                                            $admin_contct = $this->Main_Model->getData($tbl='contact_us',$wh_ct);
                                            // print_r($admin_contct);
                                            ?>
                                            <address>
                                                True Learning<br>
                                                <?php echo $admin_contct['address'];?> 
                                                <?php echo $admin_contct['phone'];?><br>
                                                <?php echo $admin_contct['email']; ?>
                                            </address>
                                        </div>
                                        <div class="col-lg-6 text-right">
                                            <p class="h5 font-weight-bold">Bill To</p>
                                            <address>
                                                <?php echo $user_profile['full_name'];?><br>
                                                <?php echo $user_profile['address'];?><br>
                                                <?php echo $user_profile['city']; ?><br>
                                                <?php echo $user_profile['pincode'];?><br>
                                                <?php echo $user_profile['mobile_no'];?><br>
                                                <?php echo $user_profile['email']; ?>
                                            </address>
                                        </div>
                                    </div><br>
                                    <div class="table-responsive push invoice">
                                        <table class="table table-bordered table-hover text-nowrap">
                                            <tr class=" ">
                                                <th class="text-center " style="width: 1%"></th>
                                                <th>Product</th>
                                                <th class="text-center" style="width: 1%">Qnty</th>
                                                <th class="text-right" style="width: 1%">
                                                Price ( <i class=" fa fa-inr "></i> ) 
                                                </th>
                                                <th class="text-right" style="width: 1%">Payment Mode </th>
                                                <th class="text-center" style="width: 1%">Transaction ID</th>
                                            </tr>
                                            <tr>
                                                <td class="text-center">1</td>
                                                <td>
                                                    <p class="font-weight-semibold mb-1">
                                                    <?php echo $t_series['test_series_name'];?> 
                                                    </p>
                                                    <div class="text-muted">
                                                        <?php echo "Category - ".$category['category_name'];?> 
                                                    </div>
                                                </td>
                                                <td class="text-center"> 1 </td>
                                                <td class="text-right">
                                                    <?php echo $t_series['price'];?> 
                                                </td>
                                                <td class="text-right">
                                                    <?php echo $invoice['payment_mode'];?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo $invoice['transaction_id'];?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="5" class="font-weight-semibold text-right">Subtotal</td>
                                                <td class="text-right">
                                                    <?php echo $t_series['price'];?> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" class="font-weight-semibold text-right">Offer ( % ) 
                                                <td class="text-right">
                                                    <?php echo $invoice['offer'];?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="5"
                                                    class="font-weight-bold text-uppercase text-right h4 mb-0">Total Amount
                                                </td>
                                                <td class="font-weight-bold text-right h4 mb-0"><?php echo $invoice['amount'];?> </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" class="text-right">

                                                    <button type="button" class="btn btn-info"
                                                        onClick="javascript:window.print();"><i
                                                            class="si si-printer"></i> Print Invoice</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <p class="text-muted text-center">Thank you very much for doing business with us. We
                                        look forward to working with you again!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    </div>
    <?php $this->load->view('js'); ?>


</body>

</html>