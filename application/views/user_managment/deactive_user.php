<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Deactive User's </title>

    <?php $this->load->view('css'); ?>
</head>

<!-- <body class="app sidebar-mini light-mode default-sidebar"> -->

<body class="app sidebar-mini light-mode light-sidebar">

    <div class="wrapper">

        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Deactive User's</h4>
                            </span>

                        </div>

                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center ">

                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Delete User's</li>

                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->

                    <!--Row-->
                    <div class="row justify-content-around">
                        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                            <div class="card user-list">
                                <!-- <div class="card-header">
                                    <div class="card-title">User List
                                        </div>
                                    <div class="card-options">
                                        <a type="button" onclick="generateExcel('Report')" id="excel" class="btn btn-icon btn-primary" 
                                            >Excel</a>
                                    </div> 
                                </div>-->
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <div class="col-md-6 col-12">
                                            <div class="row justify-content-center">
                                                <div class="col-md-8 col-12">
<!-- filter -->
<?php 
$order_by = ('uid desc');
$where1 = '(user_type = 2 AND status = 0)';
$users = $this->Main_Model->getAllData_as_per_order($tbl='user_details', $where1, $order_by); 
?>
<form method="post" action="">
    <div class="input-group">
        <select class="form-control text-center" required name="search_by" id="search_by">
            <option value="" disabled selected>Search Record By</option>
            <option value="1">User Name</option>
            <option value="2">Registration Date</option>
        </select>&nbsp; &nbsp;
      
        <select name="uid" id="select_user" class="form-control" style="display:none" required>
            <option value="" selected disabled>Select User Name</option>
             <?php 
             if (!empty($users)) 
             {
                foreach ($users as $c) 
                { 
                    ?>
                   <option value="<?php echo $c['uid'] ?>"><?php echo $c['user_name'] ?></option>
                   <?php   
                }  
             } 
             else
             {
                echo "";
             }
             ?>
        </select>&nbsp;

        <input type="date" class="form-control" name="registration_date" id="select_date" placeholder="Keywords" style="display:none"> &nbsp;&nbsp;
        
        <button class="btn btn-primary" type="submit" name="search_deactive_user_info">Go!</button>
    </div>
    <!-- submit -->
</form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="float-right mb-3">
                                        <input type="text" class="form-control" name="" id="myInput"
                                            onkeyup="search_table()" placeholder="Search">
                                    </div>
                                    <div class="table-responsive mb-3">
                                      <table class="table table-hover border table-vcenter text-nowrap mb-0 border category-table"
                                          id="myTable">
                                          <thead class="text-center">
                                              <tr>
                                                  <th class="wd-15p border-bottom-0">Sr. No.</th>
                                                  <!-- <th class="wd-15p border-bottom-0">User Name</th>
                                                  <th class="wd-15p border-bottom-0">Course Name</th>
                                                  <th class="wd-15p border-bottom-0">Test Series Name</th> -->
                                                  <th class="wd-15p border-bottom-0">User Name</th>
                                                  <th class="wd-15p border-bottom-0">Full Name</th>
                                                  <th class="wd-15p border-bottom-0">Mobile No</th>
                                                  <th class="wd-20p border-bottom-0">Registration Date</th>
                                                  <th class="wd-20p border-bottom-0">Active/Deactive</th>
                                                  <th class="wd-20p border-bottom-0 action">Action</th>
                                              </tr>
                                          </thead>
                                          <tbody class="text-center">
                                              <?php 
                                              if (!empty($deactive_users)) 
                                              {
                                                  $i= 1+$this->uri->segment(2);
                                                  foreach ($deactive_users as $li) 
                                                  {
                                                      ?>
                                                      <tr>
                                                          <td><?php echo $i++; ?></td>
                                                          <td><?php echo $li['user_name']; ?></td>
                                                          <td><?php echo $li['full_name']; ?></td>
                                                          <td><?php echo $li['mobile_no']; ?></td>
                                                          <!-- <td>1</td>
                                                          <td>Sneha</td>
                                                          <td>NEET</td>
                                                          <td>Series 1</td> -->
                                                          <td>
                                                              <?php 
                                                              echo $li['registration_date'];
                                                              //date("Y-m-d",strtotime($li['registration_date'])); 
                                                              ?>
                                                          </td>
                                                          <td>
                                                              <?php 
                                                              if ($li['status'] == 1) 
                                                              {
                                                                  ?>
                                                                  <input type="checkbox" id="<?php echo $li['uid']; ?>" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="0"> 
                                                                  <?php
                                                              } 
                                                              else
                                                              {
                                                                  ?>
                                                                  <input type="checkbox" id="<?php echo $li['uid']; ?>" data-size="sm" data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="1">
                                                                  <?php         
                                                              } 
                                                              ?>
                                                          </td>
                                                          <td class="action">
                                                              <!-- profile -->
                                                              <a href="<?php echo base_url().'user_profile/'.$li['uid']; ?>" type="button" class="btn btn-sm btn-icon btn-primary"> <i class="fa fa-eye"></i></a>

                                                              <!-- <a href="<?php echo base_url('user_profile'); ?>" type="button" class="btn btn-sm btn-icon btn-primary"> <i class="fa fa-eye"></i></a> -->

                                                              <!-- delete -->
                                                              <a type="button" class="btn btn-sm btn-icon btn-secondary" id='delete_<?php echo $li['uid']; ?>'><i class="fa fa-trash"></i></a> 
                                                          </td>
                                                      </tr>

                                                      <!-- delete -->
                                                      <script>
                                                      document.getElementById('delete_<?php echo $li['uid']; ?>').onclick = function() 
                                                      {
                                                          var id = $("#<?php echo $li['uid']; ?>").val(); 

                                                          swal({
                                                                  title: "Are you sure?",
                                                                  text: "You will not be able to recover this file!",
                                                                  type: "warning",
                                                                  showCancelButton: true,
                                                                  confirmButtonColor: '#DD6B55',
                                                                  confirmButtonText: 'Yes, delete it!',
                                                                  cancelButtonText: "No, cancel",
                                                                  closeOnConfirm: false,
                                                                  closeOnCancel: false
                                                          },
                                                          function(isConfirm) {
                                                              if (isConfirm) 
                                                              {
                                                                  $.ajax({
                                                                             url: '<?php echo base_url().'delete_deactive_user/'.$li['uid']; ?>',
                                                                             type: "POST",
                                                                             data: {id:id},
                                                                             dataType:"HTML",
                                                                             success: function () {
                                                                              swal(
                                                                                      "Deleted!",
                                                                                      "Your file has been deleted!",
                                                                                      "success"
                                                                                  ),
                                                                                  $('.confirm').click(function()
                                                                                  {
                                                                                      location.reload();
                                                                                  });
                                                                              },
                                                                      });

                                                              } else {
                                                                  swal(
                                                                      "Cancelled",
                                                                      "Your  file is safe !",
                                                                      "error"
                                                                  );
                                                              }
                                                          });
                                                      };
                                                      </script>

                                                       <?php
                                                  }
                                              }
                                              else
                                              {
                                                  ?>
                                                  <p id="founder" class="text-danger">No matching records found</p>
                                                  <?php
                                              }
                                              ?>
                                          </tbody>
                                      </table>
                                        
                                    </div>
                                    <div class="table-footer align-items-center">
                                        <nav class="" aria-label="Page navigation example">
                                            <ul class="pagination justify-content-end">
                                                <?php echo $this->pagination->create_links(); ?>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
<script>
    $('#search_by').on('change', function() {
        
        if (this.value == 0) {
            $('#select_user').hide();
            $('#select_date').hide();
        }
        if (this.value == 1) {
         
            $('#select_user').attr("required", true).addClass("table-search-box").show();
            $('#select_date').attr("required", false).hide();
            $(".table-search-box").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        });
        }
        if (this.value == 2) {
            $('#select_user').attr("required", false).hide();
            $('#select_user').next(".select2-container").hide();
            $('#select_date').attr("required", true).show();
        }
    });
    </script>
    <script>
    function generateExcel(name) {
        //getting data from our table
        $('#table-data').find('th.action, td.action').remove();
        var data_type = "data:application/vnd.ms-excel";
        var table_div = document.getElementById("table-data");
        var table_html = table_div.outerHTML.replace(/ /g, "%20");

        var a = document.createElement("a");
        a.href = data_type + ", " + table_html;
        a.download = name + ".xls";
        a.click();
        window.location.reload();
    }
    </script>
  
    <script>

    function search_table() {
        // Declare variables 
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td");
            for (j = 0; j < td.length; j++) {
                let tdata = td[j];
                if (tdata) {
                    if (tdata.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        break;
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    }
    </script>

    <!-- status active-deactive -->
    <script type="text/javascript"> 
        function change_status(idd)
        {
            var status_val=idd.value;
            var uid= idd.id;
            
            var base_url = '<?php echo base_url() ?>';
            var uid = uid;

            if(status_val != '')
            {
                $.ajax({
                          url:base_url + "MainController/active_user_status",
                          method:"POST",
                          data:{status:status_val,uid:uid},
                          
                          success: function(data)
                          {
                               // alert(data);
                               if (data == 1) 
                               {
                                   alert('Status Changed Sucessfully !..');
                                   location.reload();
                               } 
                               else 
                               {
                                   alert('Something Went Wrong !...')
                               }
                          }
                    });
            }
        }
    </script>

</body>

</html>