<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Test Series Info</title>

    <?php $this->load->view('css'); ?>
</head>


<body class="app sidebar-mini light-mode light-sidebar">

    <div class="wrapper">

        <?php 
        $this->load->view('header'); 

        $uid = $this->uri->segment(2);
        $wh_u = '(uid="'.$uid.'")';
        $user_profile = $this->Main_Model->getData($tbl='user_details',$wh_u);
        ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Test Series Info</h4>
                            </span>
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span>
                                    </a>
                                </li>
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url().'user_profile/'.$uid; ?>"
                                        class="d-flex align-items-center ">
                                        <span class="breadcrumb-icon"> User Profile</span>
                                    </a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Test Series Info</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->

                    <div class="row">
                       <!-- Profile  -->
                        <div class="col-xl-3 col-lg-4 col-md-12">

                            <!-- image -->
                            <div class="card box-widget widget-user">
                                <div class="widget-user-image mx-auto mt-5 text-center">
                                    <?php
                                    if ($user_profile['photo'] !="") 
                                    { 
                                        ?>
                                        <img width="190" height="185" alt="User" class="rounded-circle" src="<?php echo $user_profile['photo'];?>" >
                                        <?php 
                                    } 
                                    else
                                    { 
                                        ?>
                                        <img alt="User" class="rounded-circle" src="<?php echo base_url().'assets/images/no_image.png';?>" >
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="card-body text-center">
                                    <div class="pro-user">
                                        <h4 class="pro-user-username text-dark mb-1 font-weight-bold">
                                            
                                            <?php echo $user_profile['user_name']; ?>
                                            
                                            <a class="badge badge-warning edit_profile" data-target="#edit_profile_<?php echo $user_profile['uid'];?>"  data-toggle="modal"> <i class="fa fa-pencil text-dark " aria-hidden="true"></i></a>
                                        </h4>
                                    </div>
                                </div>
                            </div>

                            <!-- Personal Details -->
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Personal Details</h4>
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <tbody>
                                                <tr>
                                                    <td class="py-2 px-0">
                                                        <div class="media-icon bg-primary-transparent text-primary mr-3 mt-1">
                                                            <i class="fa fa-envelope"></i>
                                                        </div>
                                                    </td>
                                                    <td class="py-2 px-0">
                                                        <?php echo $user_profile['email']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="py-2 px-0">
                                                        <div
                                                            class="media-icon bg-warning-transparent text-warning mr-3 mt-1">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                    </td>
                                                    <td class="py-2 px-0"><?php echo $user_profile['mobile_no']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="py-2 px-0">
                                                        <div
                                                            class="media-icon bg-success-transparent text-success mr-3 mt-1">
                                                            <i class="fa fa-home"></i>
                                                        </div>
                                                    </td>
                                                    <td class="py-2 px-0">
                                                        <?php echo $user_profile['address']; ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Test List under series -->
                        <div class="col-xl-9 col-lg-8 col-md-12">
                            <div class="main-content-body main-content-body-profile card mg-b-20">
                                <div class="main-profile-body">
                                    <div class="tab-content">
                                        <div class="tab-pane show active" id="about">
                                            <?php 
                                            // print_r($check_t_list);
                                            if (!empty($check_t_list)) 
                                            {
                                                ?>
                                                <div class="card-body">
                                                    <div class="mb-5">
                                                        <div class="row">
<?php
foreach ($check_t_list as $li) 
{
    // echo "<pre>";  print_r($li); echo "</pre>"; 
    $wh_s = '(test_series_id="'.$li['test_series_id'].'")';
    $t_series = $this->Main_Model->getData($tbl='test_series',$wh_s);

    // 
    $wh_test = '(test_id="'.$li['test_id'].'")';
    $total_import_question_count = $this->Main_Model->getAllData_not_order($tbl='import_question',$wh_test);
    $question_solve = $this->Main_Model->getAllData_not_order($tbl='test_details',$wh_test);


    // correct_ans
    $wh_co_ans = '(test_id="'.$li['test_id'].'" AND is_correct = 1)';
    $correct_ans= $this->Main_Model->getAllData_not_order($tbl='test_details',$wh_co_ans);

    // incorrect_ans
    $wh_inco_ans = '(test_id="'.$li['test_id'].'" AND is_correct = 0)';
    $incorrect_ans = $this->Main_Model->getAllData_not_order($tbl='test_details',$wh_inco_ans);


    // 1 SumofCorrectMarks
    $wh_sum = '(test_id="'.$li['test_id'].'" AND mark_type = 1)';
    $SumofCorrectMarks = $this->Main_Model->getTotalSumofMarks($tbl='test_details',$wh_sum);

    // 2 SumofNegativeMarks
    $wh_nsum = '(test_id="'.$li['test_id'].'" AND mark_type = 2)';
    $SumofNegativeMarks = $this->Main_Model->getTotalSumofMarks($tbl='test_details',$wh_nsum);
    // echo $SumofNegativeMarks[0]['mark']; echo "<br>";

    // 3 SumofUnutteptMarks
    $wh_na_sum = '(test_id="'.$li['test_id'].'" AND mark_type = 3)';
    $SumofUnutteptMarks = $this->Main_Model->getTotalSumofMarks($tbl='test_details',$wh_na_sum);
    // echo $SumofUnutteptMarks[0]['mark']; echo "<br>";
    // $sum = $SumofCorrectMarks[0]['mark'] -$SumofNegativeMarks[0]['mark']- $SumofUnutteptMarks[0]['mark'];
    $sum = $SumofCorrectMarks[0]['mark'] - $SumofNegativeMarks[0]['mark'];                      
    // print_r($question_solve)
    // foreach ($question_solve as $q) {
        ?>
        <div class="col-xl-12 col-md-12 col-sm-12">
            <div class="card test-box">
                <div class="card-body p-3 pl-4">
                    <div class="row align-items-center">
                        <div class="col-md-4">
                            <div class="font-weight-bold mb-1 fs-18 text-muted">
                                <?php echo $li['test_name']; ?>
                            </div>
                            <p class="text-primary">
                                <?php echo $t_series['test_series_name']; ?>
                            </p>
                        </div>
                        <div class="col-md-8 card-bg">
                            <div class="row align-items-center">
                                <div class="col-md-3 col-6">
                                    <p><?php
                                    // print_r($question_solve);
                                    if ($question_solve) 
                                    {
                                        echo date("Y-m-d",strtotime($question_solve[0]['test_date']));
                                    } else
                                    {
                                        echo " - ";
                                    }
                                    ?>
                                    </p>
                                </div>
                                <div class="col-md-3 col-6">
                                    <p> <?php echo $sum." / ".$li['t_marks']; ?></p>
                                </div>
                                <div class="col-md-4 col-6">
                                    <?php 
                                    if ($question_solve) 
                                    {
                                        ?>
                                        <p class="badge badge-success">Attempted</p>
                                        <?php
                                    } else
                                    {
                                        ?>
                                        <p class="badge badge-primary">Not Attempted</p>
                                        <?php
                                    }
                                    ?>
                                </div>

                                <!-- view report -->
                                <div class="col-md-2 col-6 test-btn-section">
                                    <a type="button" class="btn btn-warning d-flex  mr-2 align-items-center" data-target="#view_report_<?php echo $li['test_id']; ?>" data-toggle="modal">
                                    <i class="fa fa-file-text"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- view report -->
        <div class="modal" id="view_report_<?php echo $li['test_id']; ?>">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title ">View Report</h6>
                        <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-md-6 text-center">
                                <?php 
                                // echo "sum ".$sum;echo "<br>";
                                // echo "t_marks ".$li['t_marks'];echo "<br>";
                                ?>
                                <div class="mx-auto chart-circle chart-circle-sm chart-circle-secondary mt-sm-0 mb-2 icon-dropshadow-secondary" data-value="<?php echo $sum / $li['t_marks']; ?>" data-thickness="8" data-color="#f72d66">
                                    <div class="mx-auto chart-circle-value text-center">
                                        <!-- Result Status -->
                                        <?php echo ($sum / $li['t_marks'])*100; ?> %
                                    </div>
                                </div>
                                <p class=" mb-1 ">SCORE</p>
                                <h2 class="mb-1 font-weight-bold text-primary">
                                    <?php echo $sum." / ".$li['t_marks']; ?>
                                </h2>
                            </div>
                        </div>
                        <hr class="mt-2">
                        
                        <div class="result-details text-left">
                            <div class="row align-items-center justify-content-center mb-4">
                                <div class="col-md-2 col-2 text-center">
                                    <p class="icon icon-shape bg-info rounded-circle text-white mb-0 mr-3">
                                        <i class="fa fa-question fa-lg text-white" aria-hidden="true"></i>
                                    </p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <p>Total Questions</p>
                                </div>
                                <div class="col-md-2 col-2">
                                    <p>
                                        <?php echo count($total_import_question_count); ?>
                                    </p>
                                </div>
                            </div>

                             <div class="row align-items-center justify-content-center mb-4">
                                <div class="col-md-2 col-2 text-center">
                                    <p class="icon icon-shape bg-info rounded-circle text-white mb-0 mr-3">
                                        <i class="fa fa-minus fa-lg text-white" aria-hidden="true"></i>
                                    </p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <p>Total Solved Questions</p>
                                </div>
                                <div class="col-md-2 col-2">
                                    <p><?php echo count($question_solve); ?></p>
                                </div>
                            </div>

                            <div class="row align-items-center justify-content-center mb-4">
                                <div class="col-md-2 col-2 text-center">
                                    <p class="icon icon-shape bg-success rounded-circle text-white mb-0 mr-3">
                                        <i class="fa fa-check fa-lg text-white" aria-hidden="true"></i>
                                    </p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <p>Correct Answers</p>
                                </div>
                                <div class="col-md-2 col-2">
                                    <p><?php echo count( $correct_ans ); ?></p>
                                </div>
                            </div>
                            <div class="row align-items-center justify-content-center mb-4">
                                <div class="col-md-2 col-2 text-center">
                                    <p class="icon icon-shape bg-secondary rounded-circle text-white mb-0 mr-3">
                                        <i class="fa fa-times fa-lg text-white" aria-hidden="true"></i>
                                    </p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <p>Incorrect Answers</p>
                                </div>
                                <div class="col-md-2 col-2">
                                    <p><?php echo count( $incorrect_ans ); ?></p>
                                </div>
                            </div>
                            
                            <div class="row align-items-center justify-content-center mb-4">
                                <div class="col-md-2 col-2 text-center">
                                    <!-- <i class="fa fa-exclamation fa-lg" aria-hidden="true"></i> -->
                                    <p class="icon icon-shape bg-warning rounded-circle text-white mb-0 mr-3">
                                        <i class="fa fa-exclamation fa-lg text-white" aria-hidden="true"></i>
                                    </p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <p>Unattempted Questions</p>
                                </div>
                                <div class="col-md-2 col-2">
                                    <p>
                                    <?php 
                                    $wh_neg_ans = '(test_id="'.$li['test_id'].'" AND mark_type = 3)';
                                    $unattempted_ans = $this->Main_Model->getAllData_not_order($tbl='test_details',$wh_neg_ans);
                                    echo count($total_import_question_count) - count( $question_solve );
                                    ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
    // }
}
?>
                                                        </div>
                                                    </div>
                                                    <nav aria-label="Page navigation example">
                                                        <ul class="pagination justify-content-end">
                                                             <?php echo $this->pagination->create_links(); ?>
                                                        </ul>
                                                    </nav>
                                                </div>
                                                <?php 
                                            } 
                                            else
                                            {
                                                echo "Test list is not available";
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end app-content-->
        </section>
    </div>
   
    
    <!-- edit user profile -->
    <div class="modal" id="edit_profile_<?php echo $user_profile['uid'];?>">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Edit Profile </h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>

                <form method="post"  action="<?php echo base_url().'edit_user_profile/'.$user_profile['uid']; ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row ">
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Profile Pic</label>
                                    <?php
                                    if ($user_profile['photo']  !="") 
                                    { 
                                      ?>
                                      <div class="col-ting">
                                          <div class="control-group file-upload" id="file-upload1">
                                              <div class="image-box text-center">
                                                  <img src="<?php echo $user_profile['photo'] ;?>" alt="img">
                                              </div>
                                              <div class="controls" style="display: none;">
                                                  <input type="file" name="photo" accept="image/*" />
                                              </div>
                                          </div>
                                      </div>
                                      <?php 
                                    } 
                                    else
                                    { 
                                      ?>
                                       <div class="col-ting">
                                          <div class="control-group file-upload" id="file-upload1">
                                              <div class="image-box text-center">
                                                  <img src="<?php echo base_url().'assets/images/no_image.png' ?>" alt="img">
                                              </div> 
                                              <div class="controls" style="display: none;">
                                                  <input type="file" name="photo" accept="image/*"/>
                                              </div>
                                          </div>
                                      </div>
                                      <?php
                                    }
                                    ?> 
                                    <!-- <input type="file" class="dropify" data-default-file="<?php echo base_url(''); ?>/assets/images/test-series.png" data-height="100" required /> -->
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Full Name</label>
                                    <input type="text" name="full_name" class="form-control" placeholder="Enter Name" required value="<?php echo $user_profile['full_name']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Email </label>
                                    <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $user_profile['email']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Phone Number</label>
                                    <input type="text" minlength="10" maxlength="10" name="mobile_no" class="form-control" placeholder="Phone Number" value="<?php echo $user_profile['mobile_no']; ?>" required  onkeypress="return onlyNumberKey(event)">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Address</label>
                                    <input type="text" name="address" class="form-control" placeholder="Address" value="<?php echo $user_profile['address']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">City</label>
                                    <input type="text" name="city" class="form-control" placeholder="City"  value="<?php echo $user_profile['city']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Postal Code</label>
                                    <input type="text" name="pincode" class="form-control" placeholder="ZIP Code" value="<?php echo $user_profile['pincode']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn save-btn">Update Info</button> 
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- edit user profile end -->
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>

    <!-- <script src="<?php echo base_url(''); ?>assets/js/chart.js"></script> -->
    <!-- <script src="<?php echo base_url(''); ?>assets/js/apexchart-custom.js"></script> -->
    <script>
    var options8 = {
        series: [70],
        chart: {
            height: 200,
            type: 'radialBar',
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    size: '70%',
                }
            },
        },
        labels: ['Sales'],
        colors: ['#4454c3'],
        responsive: [{
            options: {
                legend: {
                    show: false,
                }
            }
        }]
    };
    var chart8 = new ApexCharts(document.querySelector("#chart8"), options8);
    chart8.render();
    </script>
</body>

</html>