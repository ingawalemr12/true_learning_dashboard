<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | User Profile</title>

    <?php $this->load->view('css'); ?>
</head>

<!-- <body class="app sidebar-mini light-mode default-sidebar"> -->
<?php 
$uid = $this->uri->segment(2);
 // echo $uid;die;
$where = '(uid="'.$uid.'")';
$user_profile = $this->Main_Model->getData($tbl='user_details',$where);
// print_r($user_profile);
 ?>
<body class="app sidebar-mini light-mode light-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">
                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">User Profile</h4>
                            </span>
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center ">

                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">User Profile</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <div class="row">
                        <?php 
                        if (!empty($this->session->flashdata('edit')) )
                        { ?>
                          <div class="col-sm-12">
                              <div class="alert alert-success" id="alert_msg">
                                  <?php echo $this->session->flashdata('edit');?>
                              </div>
                          </div>
                        <?php
                        }
                        ?>
                        <!-- User Profile info -->
                        <div class="col-xl-3 col-lg-4 col-md-12">
                            <div class="card box-widget widget-user">
                                <div class="widget-user-image mx-auto mt-5 text-center">
                                    <?php
                                    if ($user_profile['photo'] !="") 
                                    { 
                                        ?>
                                        <img width="190" height="185" alt="User" class="rounded-circle" src="<?php echo $user_profile['photo'];?>" >
                                        <?php 
                                    } 
                                    else
                                    { 
                                        ?>
                                        <img alt="User" class="rounded-circle" src="<?php echo base_url().'assets/images/no_image.png';?>" >
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="card-body text-center">
                                    <div class="pro-user">
                                        <h4 class="pro-user-username text-dark mb-1 font-weight-bold">
                                            <?php echo $user_profile['user_name']; ?>

                                            <a class="badge badge-warning edit_profile" data-target="#edit_profile_<?php echo $user_profile['uid'];?>" data-toggle="modal">  <i class="fa fa-pencil text-dark " aria-hidden="true"></i> 
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Personal Details</h4>
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <tbody>
                                                <tr>
                                                    <td class="py-2 px-0">
                                                        <div
                                                            class="media-icon bg-primary-transparent text-primary mr-3 mt-1">
                                                            <i class="fa fa-envelope"></i>
                                                        </div>

                                                    </td>
                                                    <td class="py-2 px-0">
                                                    <?php echo $user_profile['email']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="py-2 px-0">
                                                        <div
                                                            class="media-icon bg-warning-transparent text-warning mr-3 mt-1">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                    </td>
                                                    <td class="py-2 px-0">
                                                    <?php echo $user_profile['mobile_no']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="py-2 px-0">
                                                        <div
                                                            class="media-icon bg-success-transparent text-success mr-3 mt-1">
                                                            <i class="fa fa-home"></i>
                                                        </div>
                                                    </td>
                                                    <td class="py-2 px-0">
                                                    <?php echo $user_profile['address']; ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- User Profile end -->

                        <!-- Test Series List -->
                        <div class="col-xl-9 col-lg-8 col-md-12">

                            <!-- All Test Series -->
                            <div class="main-content-body main-content-body-profile card mg-b-20">
                                <div class="card-header">
                                    <div class="card-title">All Test Series</div>
                                </div>
                                <div class="card-body">
                                    <div class="mb-5">
                                        <div class="row">
                                            <?php 
                                            /*$order_by_o = ('uid desc');
                                            $wh_o = '(uid="'.$uid.'")';
                                            $user_test_series_list = $this->Main_Model->getAllData_as_per_order($tbl='orders',$wh_o,$order_by_o);
                                            // print_r($user_test_series_list);*/
                                            if (!empty($user_test_series_list)) 
                                            {
                                                $i = 1;
                                                foreach ($user_test_series_list as $li) 
                                                {
                                                    $wh_s = '(test_series_id="'.$li['test_series_id'].'")';
                                                    $t_series = $this->Main_Model->getData($tbl='test_series',$wh_s);
                                                    ?>
                                                    <div class="col-xl-4 col-md-4 col-sm-6">
                                                        <div
                                                            class="d-flex align-items-center border border-warning p-3 mb-3 br-7">
                                                            <div class="wrapper ml-3">
                                                                <p class="mb-0 mt-1 text-dark font-weight-semibold">
                                                                    Test Series :  
                                                                    <?php echo $i++; //echo $t_series['test_series_id']; ?>
                                                                </p>
                                                                <small><?php echo $t_series['test_series_name']; ?></small>
                                                            </div>
                                                            <!-- <div class="float-right ml-auto">
                                                                <a href="<?php echo base_url().'user_test_series/'.$uid.'/'.$li['test_series_id']; ?>" class="btn btn-primary btn-sm"><i class="si si-eye mr-1"></i></a>
                                                            </div> -->

                                                            <div class="float-right ml-auto">
                                                                <a href="<?php echo base_url().'invoice/'.$uid.'/'.$li['test_series_id']; ?>" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php 
                                                }
                                            } else
                                            {
                                                echo "Test Series list is not available";
                                            }
                                            ?>
                                          
                                        </div>
                                    </div>
                                </div>
                                <!-- <nav class="" aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center">
                                        // paination
                                    </ul>
                                </nav> -->
                            </div>
                            <!-- All Test Series end -->

                            <!-- Completed Test Series -->
                            <div class="main-content-body main-content-body-profile card mg-b-20">
                                <div class="card-header">
                                    <div class="card-title">Completed Test Series</div>
                                </div>
                                <div class="card-body">
                                    <div class="mb-5">
                                        <div class="row">
                                            <?php 
                                            $group_by = ('test_series_id');
                                            $wh_o = '(uid="'.$uid.'")';
                                            $completed_test_series_list = $this->Main_Model->getAllData_as_group($tbl='test_details',$wh_o,$group_by);
                                            // print_r($completed_test_series_list);

                                            if (!empty($completed_test_series_list)) 
                                            {
                                                $i = 1;
                                                foreach ($completed_test_series_list as $li) 
                                                {
                                                    $wh_s = '(test_series_id="'.$li['test_series_id'].'")';
                                                    $t_series = $this->Main_Model->getData($tbl='test_series',$wh_s);
                                                    ?>
                                                    <div class="col-xl-4 col-md-4 col-sm-6">
                                                        <div
                                                            class="d-flex align-items-center border border-warning p-3 mb-3 br-7">
                                                            <div class="wrapper ml-3">
                                                                <p class="mb-0 mt-1 text-dark font-weight-semibold">
                                                                 Test Series :  
                                                                    <?php echo $i++; //echo $t_series['test_series_id']; ?>
                                                                </p>
                                                                <small><?php echo $t_series['test_series_name']; ?></small>
                                                            </div>
                                                            <div class="float-right ml-auto">
                                                                <a href="<?php echo base_url().'user_test_series/'.$uid.'/'.$li['test_series_id']; ?>" class="btn btn-primary btn-sm"><i class="si si-eye mr-1"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php 
                                                }
                                            } else
                                            {
                                                echo "Test Series list is not available";
                                            }
                                            ?>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Completed Test Series end -->
                        </div>
                        <!-- Test Series end -->

                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- edit user profile -->
    <div class="modal" id="edit_profile_<?php echo $user_profile['uid'];?>">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Edit Profile </h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>

                <form method="post"  action="<?php echo base_url().'edit_user_profile/'.$user_profile['uid']; ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row ">
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Profile Pic</label>
                                    <?php
                                    if ($user_profile['photo']  !="") 
                                    { 
                                      ?>
                                      <div class="col-ting">
                                          <div class="control-group file-upload" id="file-upload1">
                                              <div class="image-box text-center">
                                                  <img src="<?php echo $user_profile['photo'] ;?>" alt="img">
                                              </div>
                                              <div class="controls" style="display: none;">
                                                  <input type="file" name="photo" accept="image/*" />
                                              </div>
                                          </div>
                                      </div>
                                      <?php 
                                    } 
                                    else
                                    { 
                                      ?>
                                       <div class="col-ting">
                                          <div class="control-group file-upload" id="file-upload1">
                                              <div class="image-box text-center">
                                                  <img src="<?php echo base_url().'assets/images/no_image.png' ?>" alt="img">
                                              </div> 
                                              <div class="controls" style="display: none;">
                                                  <input type="file" name="photo" accept="image/*"/>
                                              </div>
                                          </div>
                                      </div>
                                      <?php
                                    }
                                    ?> 
                                    <!-- <input type="file" class="dropify" data-default-file="<?php echo base_url(''); ?>/assets/images/test-series.png" data-height="100" required /> -->
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Full Name</label>
                                    <input type="text" name="full_name" class="form-control" placeholder="Enter Name" required value="<?php echo $user_profile['full_name']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Email </label>
                                    <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $user_profile['email']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Phone Number</label>
                                    <input type="text" minlength="10" maxlength="10" name="mobile_no" class="form-control" placeholder="Phone Number" value="<?php echo $user_profile['mobile_no']; ?>" required  onkeypress="return onlyNumberKey(event)">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Address</label>
                                    <input type="text" name="address" class="form-control" placeholder="Address" value="<?php echo $user_profile['address']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">City</label>
                                    <input type="text" name="city" class="form-control" placeholder="City"  value="<?php echo $user_profile['city']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Postal Code</label>
                                    <input type="text" name="pincode" class="form-control" placeholder="ZIP Code" value="<?php echo $user_profile['pincode']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn save-btn">Update Info</button> 
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- edit user profile end -->

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>

    <script>
    function generateExcel(name) {
        //getting data from our table
        $('#table-data').find('th.action, td.action').remove();
        var data_type = "data:application/vnd.ms-excel";
        var table_div = document.getElementById("table-data");
        var table_html = table_div.outerHTML.replace(/ /g, "%20");

        var a = document.createElement("a");
        a.href = data_type + ", " + table_html;
        a.download = name + ".xls";
        a.click();
        window.location.reload();
    }
    </script>
    
    <script>
        function onlyNumberKey(evt) 
        {
            // Only ASCII character in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
    </script>   

    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

</body>

</html>