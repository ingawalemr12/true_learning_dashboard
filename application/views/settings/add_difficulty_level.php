<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Add Difficulty Level </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <!-- <div classs="container p-5 ">
                <div class="row no-gutters ">
                    <div class="col-lg-4 col-md-12 ml-auto">
                        <div class="alert bg-info shadow alert-msg hide" role="alert" id="alert-msg">
                            <div class="row justify-content-center">
                                <div class="col-10  my-auto">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="True" style="color:#fff">&times;</span>
                                    </button>
                                    <h4 class="alert-heading-gradient mb-0">Save Info Successfully</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Add Difficulty Level</h4>
                            </span>
                            
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add Difficulty Level</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <!-- <div class="card">
                            <div class="card-body">
                                <div class="row "> -->
                        <div class="col-md-2">
                            <?php $this->load->view('aside'); ?>
                        </div>
                        <div class="col-md-10">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                    <?php 
                                    if (!empty($this->session->flashdata('create')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-success" id="alert_msg">
                                              <?php echo $this->session->flashdata('create');?>
                                          </div>
                                      </div>
                                    <?php
                                    }

                                    if (!empty($this->session->flashdata('edit')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-success" id="alert_msg">
                                              <?php echo $this->session->flashdata('edit');?>
                                          </div>
                                      </div>
                                    <?php
                                    }

                                    if (!empty($this->session->flashdata('exists')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-warning" id="alert_msg">
                                              <?php echo $this->session->flashdata('exists');?>
                                          </div>
                                      </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="card ">
                                        <div class="card-header">
                                            <div class="card-title">Difficulty Level
                                                Info</div>
                                            <div class="card-options">
                                                <a type="button" class="btn btn-icon btn-primary"
                                                    data-target="#add_difficulty" data-toggle="modal"><i class="fa fa-plus"></i>&nbsp; Add Difficulty Level</a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive ">
                                                <!-- <div class="table-responsive" id="lightgallery"> -->
    <table class="table border table-vcenter table-hover text-nowrap"
        >
        <thead class="text-center">
            <tr>
                <th class="wd-15p border-bottom-0">
                    Sr. No.</th>
                <th class="wd-15p border-bottom-0">
                    Difficulty Id
                </th>
                <th class="wd-15p border-bottom-0">
                    Difficulty Level
                </th>
                <th class="wd-20p border-bottom-0">
                    Active/Deactive
                </th>
                <th class="wd-20p border-bottom-0">
                    Action</th>
            </tr>
        </thead>
        <tbody class="text-center">
            <?php 
            if (!empty($difficulty_list)) 
            {
                $i= 1+$this->uri->segment(2);
                foreach ($difficulty_list as $dif) 
                {
                    ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $dif['difficulty_id']; ?></td>
                        <td><?php echo $dif['difficulty_medium']; ?></td>
                        <td>
                            <?php 
                            if ($dif['status'] == 1) 
                            {
                                ?>
                                <input type="checkbox" id="<?php echo $dif['difficulty_id']; ?>" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="0"> 
                                <?php
                            } 
                            else
                            {
                                ?>
                                <input type="checkbox" id="<?php echo $dif['difficulty_id']; ?>" data-size="sm" data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="1">
                                <?php         
                            } 
                            ?> 
                        </td>
                        <td>
                            <!-- edit -->
                            <a type="button" class="btn btn-sm btn-icon btn-primary" data-target="#update_difficulty_level<?php echo $dif['difficulty_id']; ?>" data-toggle="modal"><i class="fa fa-edit"></i></a> 

                            <!-- delete -->
                            <a type="button" class="btn btn-sm btn-icon btn-secondary" id='delete_<?php echo $dif['difficulty_id']; ?>'><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>

                    <!-- delete -->
                    <script>
                    document.getElementById('delete_<?php echo $dif['difficulty_id']; ?>').onclick = function() 
                    {
                        var id = $("#<?php echo $dif['difficulty_id']; ?>").val(); 
                   
                        swal({
                                title: "Are you sure?",
                                text: "You will not be able to recover this file!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, delete it!',
                                cancelButtonText: "No, cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                        },
                        function(isConfirm) {
                            if (isConfirm) 
                            {
                                $.ajax({
                                           url: '<?php echo base_url().'delete_difficulty_level/'.$dif['difficulty_id']; ?>',
                                           type: "POST",
                                           data: {id:id},
                                           dataType:"HTML",
                                           success: function () {
                                            swal(
                                                    "Deleted!",
                                                    "Your file has been deleted!",
                                                    "success"
                                                ),
                                                $('.confirm').click(function()
                                                {
                                                    location.reload();
                                                });
                                            },
                                    });
                                
                            } else {
                                swal(
                                    "Cancelled",
                                    "Your  file is safe !",
                                    "error"
                                );
                            }
                        });
                    };
                    </script>

                    <!-- edit -->
                    <div class="modal" id="update_difficulty_level<?php echo $dif['difficulty_id']; ?>">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content modal-content-demo">
                                <div class="modal-header">
                                    <h6 class="modal-title">Update Difficulty Level</h6>
                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post"  action="<?php echo base_url().'edit_difficulty_level/'.$dif['difficulty_id'];; ?>" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label"> Difficulty Level <span class="text-red">*</span></label>
                                               
                                                <input type="text" name="difficulty_medium" id="difficulty_medium" class="form-control" placeholder="Enter Difficulty Level Name" value="<?php echo $dif['difficulty_medium']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn save-btn" type="button">Update Info</button> 
                                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                    <?php
                }
            }
            ?>
        </tbody>
    </table>
                                            </div>
                                            <div class="table-footer align-items-center">
                                                <!-- <p class="">Showing 1 to 3 of 3 entries</p> -->
                                                <nav class="" aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">
                                                        <?php echo $this->pagination->create_links(); ?>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- </div>
                        </div>
                    </div> -->
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    
    <!-- add_difficulty -->
    <div class="modal" id="add_difficulty">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">Add Difficulty Level</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Difficulty Level 
                                    <span class="text-red">*</span></label>
                                <input type="text" name="difficulty_medium" id="difficulty_medium" class="form-control" placeholder="Enter Difficulty Level Name" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_difficulty_info" class="btn save-btn">Save changes</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    
    <script>
        $(function() {
            setTimeout(function() { 
                $("#alert-msg").fadeOut(1500); 
            }, 2000)
        })
    </script>

    <!-- status active-deactive -->
    <script type="text/javascript"> 
        function change_status(idd)
        {
            var status_val=idd.value;
            var difficulty_id= idd.id;
            
            var base_url = '<?php echo base_url() ?>';
            var difficulty_id = difficulty_id;

            if(status_val != '')
            {
                $.ajax({
                          url:base_url + "MainController/active_difficulty_status",
                          method:"POST",
                          data:{status:status_val,difficulty_id:difficulty_id},
                          
                          success: function(data)
                          {
                               // alert(data);
                               if (data == 1) 
                               {
                                   alert('Status Changed Sucessfully !..');
                                   location.reload();
                               } 
                               else 
                               {
                                   alert('Something Went Wrong !...')
                               }
                          }
                    });
            }
        }
    </script>

    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>
</body>

</html>