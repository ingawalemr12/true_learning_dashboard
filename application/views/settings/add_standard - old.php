<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Add Standard </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <!-- <div classs="container p-5 ">
                <div class="row no-gutters ">
                    <div class="col-lg-4 col-md-12 ml-auto">
                        <div class="alert bg-info shadow alert-msg hide" role="alert" id="alert-msg">
                            <div class="row justify-content-center">
                                <div class="col-10  my-auto">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="True" style="color:#fff">&times;</span>
                                    </button>
                                    <h4 class="alert-heading-gradient mb-0">Save Info Successfully</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Add Standard</h4>
                            </span>
                            
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add Standard</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <!-- <div class="card">
                            <div class="card-body">
                                <div class="row "> -->
                        <div class="col-md-2">
                            <?php $this->load->view('aside'); ?>
                        </div>
                        <div class="col-md-10">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                    <?php 
                                    if (!empty($this->session->flashdata('create')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-success" id="alert_msg">
                                              <?php echo $this->session->flashdata('create');?>
                                          </div>
                                      </div>
                                    <?php
                                    }

                                    if (!empty($this->session->flashdata('edit')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-success" id="alert_msg">
                                              <?php echo $this->session->flashdata('edit');?>
                                          </div>
                                      </div>
                                    <?php
                                    }

                                    if (!empty($this->session->flashdata('exists')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-warning" id="alert_msg">
                                              <?php echo $this->session->flashdata('exists');?>
                                          </div>
                                      </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="card ">
                                        <div class="card-header">
                                            <div class="card-title">Standard Info</div>
                                            <div class="card-options">
                                                <a type="button" class="btn btn-icon btn-primary"
                                                    data-target="#add_standard" data-toggle="modal"><i class="fa fa-plus"></i>&nbsp;Add Standard</a>

                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row justify-content-center">
                                                <div class="col-md-8 col-12">
                                                    <div class="row justify-content-center">
                                                        <div class="col-md-8 col-12">
    <!-- filter -->
    <?php 
    $order_by = ('board_id desc');
    $board_list_1= $this->Main_Model->getAllOrderData($tbl='board', $order_by); 
    ?>
    <form method="post" action="">
        <div class="input-group">
            <select required name="board_id" id="board_id" class="table-search-box form-control" >
                <option value="" selected disabled>Select Board </option>
                <?php 
                if (!empty($board_list_1)) 
                {
                    foreach ($board_list_1 as $brd_list) 
                    { 
                        ?>
                        <option value="<?php echo $brd_list['board_id'] ?>">
                            <?php echo $brd_list['board_name'] ?>
                        </option>
                        <?php   
                    }  
                } 
                else
                {
                    echo "";
                }
                ?>
            </select>
            <select required  name="std_name" id="std_name" class="table-search-box form-control">
                <option value="" selected disabled>Select Standard</option>

            </select>

            <button type="submit" class="btn btn-primary" name="search_standard" type="button">Go!</button>
        
        </div>
    </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive ">
                                                <!-- <div class="table-responsive" id="lightgallery"> -->
    <table class="table border table-vcenter text-nowrap table-hover">
        <thead class="text-center">
            <tr>
                <th class="wd-15p border-bottom-0">Sr. No.</th>
                <th class="wd-15p border-bottom-0">Board Name
                </th>
                <th class="wd-15p border-bottom-0">Standard Name
                </th>
                <!-- <th class="wd-15p border-bottom-0">Add Subject
                </th> -->
                <th class="wd-20p border-bottom-0">
                    Active/Deactive</th>
                <th class="wd-20p border-bottom-0">Action</th>
            </tr>
        </thead>
        <tbody class="text-center">
            <?php 
            if (!empty($standard_list)) 
            {
                $i= 1+$this->uri->segment(2);
                foreach ($standard_list as $std) 
                {
                    ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td>
                            <?php
                            //echo $std['board_id']; 
                            if (!empty($board_list)) 
                            {
                                foreach ($board_list as $brd_list) 
                                {
                                    if ($brd_list['board_id'] == $std['board_id']) 
                                    {
                                        echo $brd_list['board_name'];
                                    }
                                }
                            }else
                            {
                                echo " ";
                            }
                            ?>
                        </td>
                        <td><?php echo $std['std_name']; ?></td>
                        <!-- <td>
                            <a type="button" href="<?php echo base_url('add_subject'); ?>" class="btn btn-sm btn-icon btn-info"><i class="fa fa-plus"></i> </a> 
                        </td> -->
                        <td>
                            <?php 
                            if ($std['status'] == 1) 
                            {
                                ?>
                                <input type="checkbox" id="<?php echo $std['std_id']; ?>" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="0"> 
                                <?php
                            } 
                            else
                            {
                                ?>
                                <input type="checkbox" id="<?php echo $std['std_id']; ?>" data-size="sm" data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="1">                            
                                <?php         
                            } 
                            ?> 
                            <!-- <input type="checkbox" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger"> -->

                        </td>
                        <td>
                            <!-- edit -->
                            <a type="button" class="btn btn-sm btn-icon btn-primary" data-target="#update_standard_<?php echo $std['std_id']; ?>" data-toggle="modal"><i class="fa fa-edit"></i></a> 

                            <!-- delete -->
                            <a type="button" class="btn btn-sm btn-icon btn-secondary" id='delete_<?php echo $std['std_id']; ?>'><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>

                    <!-- delete -->
                    <script>
                        document.getElementById('delete_<?php echo $std['std_id']; ?>').onclick = function() 
                        {
                            var id = $("#<?php echo $std['std_id']; ?>").val(); 
                       
                            swal({
                                    title: "Are you sure?",
                                    text: "You will not be able to recover this file!",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: '#DD6B55',
                                    confirmButtonText: 'Yes, delete it!',
                                    cancelButtonText: "No, cancel",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) 
                                {
                                    $.ajax({
                                               url: '<?php echo base_url().'delete_standard/'.$std['std_id']; ?>',
                                               type: "POST",
                                               data: {id:id},
                                               dataType:"HTML",
                                               success: function () {
                                                swal(
                                                        "Deleted!",
                                                        "Your file has been deleted!",
                                                        "success"
                                                    ),
                                                    $('.confirm').click(function()
                                                    {
                                                        location.reload();
                                                    });
                                                },
                                        });
                                    
                                } else {
                                    swal(
                                        "Cancelled",
                                        "Your  file is safe !",
                                        "error"
                                    );
                                }
                            });
                        };
                    </script>

                    <!-- edit -->
                    <div class="modal" id="update_standard_<?php echo $std['std_id']; ?>">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content modal-content-demo">
                                <div class="modal-header">
                                    <h6 class="modal-title">Edit Standard</h6>
                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post"  action="<?php echo base_url().'edit_standard/'.$std['std_id']; ?>" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Select Board 
                                                    <span class="text-red">*</span></label>

                                                    <select required name="board_id" id="board_id" class="form-control" required="">
                                                    <option value="" disabled="" selected>Select Board </option>
                                                    <?php 
                                                    if (!empty($board_list_1)) 
                                                    {
                                                        foreach ($board_list_1 as $br_li) 
                                                        { 
                                                            if ($br_li['board_id']==$std['board_id']) 
                                                            {  
                                                                ?>
                                                                <option selected="selected" value="<?php echo $br_li['board_id']; ?>"> 
                                                                     <?php echo $br_li['board_name'] ?>
                                                                </option>
                                                                <?php 
                                                            } else
                                                            {
                                                                ?>
                                                                <option value="<?php echo $br_li['board_id'] ; ?>">
                                                                    <?php echo $br_li['board_name'] ?>
                                                                </option>
                                                                <?php  
                                                            }
                                                        }  
                                                    } 
                                                    else
                                                    {
                                                        echo "";
                                                    }
                                                    ?>
                                                </select>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Standard Name <span class="text-red">*</span></label>
                                                
                                                <input type="text" class="form-control" name="std_name" id="std_name" value="<?php echo $std['std_name'];?>" minlength="1" required placeholder="Enter Standard Name">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn save-btn" type="button">Update Info</button> 
                                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php
                }
            }
            ?>
        </tbody>
    </table>
                                            </div>
                                            <div class="table-footer align-items-center">
                                                <!-- <p class="">Showing 1 to 3 of 3 entries</p> -->
                                                <nav class="" aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">
                                                        <?php echo $this->pagination->create_links(); ?>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/div-->
                                </div>
                            </div>
                            <!-- End Row-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- </div>
                </div>
            </div> -->
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>

    <!-- add standard -->
    <div class="modal" id="add_standard">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">Add Standard</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Select Board <span
                                        class="text-red">*</span></label>
                                <select required name="board_id" id="board_id" class="form-control text-center table-search-box" required="">
                                    <option value="" disabled="" selected>Select Board </option>
                                    <?php 
                                    if (!empty($board_list_1)) 
                                    {
                                        foreach ($board_list_1 as $std) 
                                        { 
                                            ?>
                                            <option value="<?php echo $std['board_id'] ?>">
                                                <?php echo $std['board_name'] ?>
                                            </option>
                                            <?php   
                                        }  
                                    } 
                                    else
                                    {
                                        echo "";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="bannername" class="form-label">Standard Name 
                                    <span class="text-red">*</span></label>

                                <input type="text" class="form-control" name="std_name" id="std_name" placeholder="Enter Standard Name" required >
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_standard_info" class="btn save-btn">Save changes</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
        // var $disabledResults = $(".search-input");
        // $disabledResults.select2();
    </script>
    
    <script>
        $(function() {
            setTimeout(function() { 
                $("#alert-msg").fadeOut(1500); 
            }, 2000)
        })
    </script>

    <!-- //status active-deactive -->
    <script type="text/javascript"> 
        function change_status(idd)
        {
            //console.log(std_id);
            var status=idd.value;
            var std_id= idd.id;
            
            var base_url = '<?php echo base_url() ?>';
            var std_id = std_id;

            if(status != '')
            {
                $.ajax({
                          url:base_url + "MainController/active_standard_status",
                          method:"POST",
                          data:{status:status,std_id:std_id},
                          
                          success: function(data)
                          {
                             // alert(data);
                              if (data == 1) 
                               {
                                   alert('Status Changed Sucessfully !..');
                                   location.reload();
                               } 
                               else 
                               {
                                   alert('Something Went Wrong !...')
                               }
                          }
                    });
            }
            }
    </script>

    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <!-- on change of board_id , it select std_name -->
    <script type="text/javascript">
    $("document").ready(function () 
    {
        $("#board_id").change(function () 
        {
            var board_id = $("#board_id").val();  //$(this).val();
            // alert(board_id);

            if (board_id !="") 
            {
                $.ajax({
                    url:'<?php echo base_url().'MainController/get_standard_list' ?>',
                    method:'POST',
                    data: {board_id:board_id},
                    success:function (data) {
                        $("#std_name").html(data);
                    }
                }); //end ajax
            }

        }); // end change(function)
    });  //  end js(function)
    </script> 
</body>

</html>