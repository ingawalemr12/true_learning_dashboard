<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learnings | Add Category </title>
    <?php $this->load->view('css'); ?>
    <style>
        @media (min-width: 320px) and (max-width: 600px){
            .tab-menu-heading .tabs-menu .nav{
                display: inline-flex;
            }
            .tabs-menu ul li{
                width: 50%;
                margin: 0;
            }
            .setting-menu .nav-pills-custom .nav-link{
                padding: 10%;
                font-size: 10px;
                margin: 0;
            }
        }
    </style>
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div classs="container p-5 ">
                <div class="row no-gutters ">
                    <div class="col-lg-4 col-md-12 ml-auto">
                        <div class="alert bg-info shadow alert-msg hide" role="alert" id="alert-msg">
                            <div class="row justify-content-center">
                                <div class="col-10  my-auto">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="True" style="color:#fff">&times;</span>
                                    </button>
                                    <h4 class="alert-heading-gradient mb-0">Save Info Successfully</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-content main-content">

                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                    <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Add Category</h4>
                            </span>
                            
                        </div>
                        <!-- <div class="page-leftheader">
                            <h4 class="page-title">Add Category</h4>
                        </div> -->
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add Category</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->

                    <div class="row justify-content-center">
                        <!-- <div class="card">
                            <div class="card-body"> 
                                <div class="row ">-->
                        <div class="col-md-2">
                            <?php $this->load->view('aside'); ?>
                        </div>

                        <div class="col-lg-10 col-xl-10 col-md-10 col-sm-12">
                            <div class="card ">
                                <div class="card-header">
                                    <div class="card-title">Category
                                        Info</div>
                                    <div class="card-options">
                                        <a type="button" class="btn btn-icon btn-primary" data-target="#add_category"
                                            data-toggle="modal"><i class="fa fa-plus"></i>&nbsp;Add
                                            Category</a>

                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive ">
                                        <!-- <div class="table-responsive" id="lightgallery"> -->
                                        <table class="table table-bordered  table-hover text-nowrap category-table">

                                            <thead class="text-center">
                                                <tr>
                                                    <th class="wd-15p border-bottom-0">
                                                        Sr. No.</th>
                                                    <th class="wd-15p border-bottom-0">
                                                        Category Name
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Active/Deactive
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                                <tr>
                                                    <td>1</td>
                                                    <td>Recoeded Course
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" data-size="sm" checked
                                                            data-toggle="toggle" data-on="Active" data-off="Deactive"
                                                            data-onstyle="success" data-offstyle="danger">

                                                    </td>
                                                    <td>
                                                        <a type="button" class="btn btn-sm btn-icon btn-primary"
                                                            data-target="#update_category" data-toggle="modal"><i
                                                                class="fa fa-edit"></i></a>
                                                        <a type="button" class="btn btn-sm btn-icon btn-secondary"
                                                            id='delete'><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Test Series
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" data-size="sm" checked
                                                            data-toggle="toggle" data-on="Active" data-off="Deactive"
                                                            data-onstyle="success" data-offstyle="danger">

                                                    </td>
                                                    <td>
                                                        <a type="button" class="btn btn-sm btn-icon btn-primary"><i
                                                                class="fa fa-edit"></i></a>
                                                        <a type="button" class="btn btn-sm btn-icon btn-secondary"><i
                                                                class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Free Course
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" data-size="sm" checked
                                                            data-toggle="toggle" data-on="Active" data-off="Deactive"
                                                            data-onstyle="success" data-offstyle="danger">

                                                    </td>
                                                    <td>
                                                        <a type="button" class="btn btn-sm btn-icon btn-primary"><i
                                                                class="fa fa-edit"></i></a>
                                                        <a type="button" class="btn btn-sm btn-icon btn-secondary"><i
                                                                class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-footer align-items-center">
                                        <p class="">Showing 1 to 3 of 3 entries</p>
                                        <nav class="" aria-label="Page navigation example">
                                            <ul class="pagination justify-content-end">
                                                <li class="page-item disabled">
                                                    <a class="page-link" href="#" tabindex="-1">
                                                        <i class="fa fa-angle-left"></i>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item active">
                                                    <a class="page-link" href="#">1</a>
                                                </li>
                                                <li class="page-item ">
                                                    <a class="page-link" href="#">2</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="#">3</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="#">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- </div>
                        </div> 
                    </div>-->
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    <div class="modal" id="add_category">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">Add Category</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Category Name <span
                                        class="text-red">*</span></label>
                                <input type="text" class="form-control" id="" placeholder="Enter Category Name">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn save-btn" type="button">Save changes</button> <button class="btn cancel-btn"
                        data-dismiss="modal" type="button">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="update_category">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">Update Category</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Category Name <span
                                        class="text-red">*</span></label>
                                <input type="text" class="form-control" id="" placeholder="Enter Category Name">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn save-btn" type="button">Save changes</button>
                    <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
    document.getElementById('delete').onclick = function() {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: "No, cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal(
                        "Deleted!",
                        "Your  file has been deleted!",
                        "success");
                } else {
                    swal(
                        "Cancelled",
                        "Your  file is safe !",
                        "error"
                    );
                }
            });
    };
    </script>
    <script>

$(function() {
    setTimeout(function() { 
        $("#alert-msg").fadeOut(1500); 
    }, 2000)
    
})
</script>
</body>

</html>