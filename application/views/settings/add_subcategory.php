<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Add Sub-Category </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div classs="container p-5 ">
                <div class="row no-gutters ">
                    <div class="col-lg-4 col-md-12 ml-auto">
                        <div class="alert bg-info shadow alert-msg hide" role="alert" id="alert-msg">
                            <div class="row justify-content-center">
                                <div class="col-10  my-auto">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="True" style="color:#fff">&times;</span>
                                    </button>
                                    <h4 class="alert-heading-gradient mb-0">Save Info Successfully</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Add Sub-Category</h4>
                            </span>
                            
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add Sub-Category</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <!-- <div class="card">
                            <div class="card-body">
                                <div class="row "> -->
                        <div class="col-md-2">
                            <?php $this->load->view('aside'); ?>
                        </div>
                        <div class="col-md-10">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                    <div class="card ">
                                        <div class="card-header">
                                            <div class="card-title">Sub-Category
                                                Info</div>
                                            <div class="card-options">
                                                <a type="button" class="btn btn-icon btn-primary"
                                                    data-target="#add_subCategory" data-toggle="modal"><i
                                                        class="fa fa-plus"></i>&nbsp;Add
                                                    Sub-Category</a>

                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row justify-content-center">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-5 col-5">
                                                            <div class="input-group">
                                                                <select
                                                                    class="form-control text-center table-search-box"
                                                                    required="">
                                                                    <option value="0" disable="">Select Categories
                                                                    </option>
                                                                    <option value="1">Recorded Course</option>
                                                                    <option value="2">Test Series</option>
                                                                    <option value="3">Free Course</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 col-5">
                                                            <div class="input-group">
                                                                <select
                                                                    class="form-control text-center table-search-box"
                                                                    required="">
                                                                    <option value="0" disable="">Select Sub-Categories
                                                                    </option>
                                                                    <option value="1">JEE Main</option>
                                                                    <option value="2">NEET</option>
                                                                    <option value="3">10th</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-2 pl-0">
                                                            <div class="input-group">
                                                                <button class="btn btn-primary"
                                                                    type="button">Go!</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive ">
                                                <!-- <div class="table-responsive" id="lightgallery"> -->
                                                <table class="table table-hover border table-vcenter text-nowrap">
                                                    <thead class="text-center">
                                                        <tr>
                                                            <th class="wd-15p border-bottom-0">
                                                                Sr. No.</th>
                                                            <th class="wd-15p border-bottom-0">
                                                                Category Name
                                                            </th>
                                                            <th class="wd-15p border-bottom-0">
                                                                Sub-Category Name
                                                            </th>
                                                            <th class="wd-15p border-bottom-0">
                                                                Image
                                                            </th>
                                                            <th class="wd-20p border-bottom-0">
                                                                Active/Deactive
                                                            </th>
                                                            <th class="wd-20p border-bottom-0">
                                                                Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-center">
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Recoeded Course
                                                            </td>
                                                            <td>JEE Main</td>
                                                            <td>
                                                                <div class="magnific-img">
                                                                    <a class="image-popup-vertical-fit"
                                                                        href="<?php echo base_url(''); ?>/assets/images/test-series.png"
                                                                        title="">
                                                                        <img src="<?php echo base_url(''); ?>/assets/images/test-series.png"
                                                                            alt="Sub-Category Image"
                                                                            style="width: 100px; height: 50px;" /></a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" data-size="sm" checked
                                                                    data-toggle="toggle" data-on="Active"
                                                                    data-off="Deactive" data-onstyle="success"
                                                                    data-offstyle="danger">

                                                            </td>
                                                            <td>
                                                                <a type="button" class="btn btn-sm btn-icon btn-primary"
                                                                    data-target="#edit_subCategory"
                                                                    data-toggle="modal"><i class="fa fa-edit"></i></a>
                                                                <a type="button"
                                                                    class="btn btn-sm btn-icon btn-secondary"
                                                                    id='delete'><i class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Test Series
                                                            </td>
                                                            <td>NEET</td>
                                                            <td>
                                                                <div class="magnific-img">
                                                                    <a class="image-popup-vertical-fit"
                                                                        href="<?php echo base_url(''); ?>/assets/images/test-series.png"
                                                                        title="">
                                                                        <img src="<?php echo base_url(''); ?>/assets/images/test-series.png"
                                                                            alt="Sub-Category Image"
                                                                            style="width: 100px; height: 50px;" /></a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" data-size="sm" checked
                                                                    data-toggle="toggle" data-on="Active"
                                                                    data-off="Deactive" data-onstyle="success"
                                                                    data-offstyle="danger">

                                                            </td>
                                                            <td>
                                                                <a type="button"
                                                                    class="btn btn-sm btn-icon btn-primary"><i
                                                                        class="fa fa-edit"></i></a>
                                                                <a type="button"
                                                                    class="btn btn-sm btn-icon btn-secondary"><i
                                                                        class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Free Course
                                                            </td>
                                                            <td>10th</td>
                                                            <td>
                                                                <div class="magnific-img">
                                                                    <a class="image-popup-vertical-fit"
                                                                        href="<?php echo base_url(''); ?>/assets/images/test-series.png"
                                                                        title="">
                                                                        <img src="<?php echo base_url(''); ?>/assets/images/test-series.png"
                                                                            alt="Sub-Category Image"
                                                                            style="width: 100px; height: 50px;" /></a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" data-size="sm" checked
                                                                    data-toggle="toggle" data-on="Active"
                                                                    data-off="Deactive" data-onstyle="success"
                                                                    data-offstyle="danger">

                                                            </td>
                                                            <td>
                                                                <a type="button"
                                                                    class="btn btn-sm btn-icon btn-primary"><i
                                                                        class="fa fa-edit"></i></a>
                                                                <a type="button"
                                                                    class="btn btn-sm btn-icon btn-secondary"><i
                                                                        class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="table-footer align-items-center">
                                                <p class="">Showing 1 to 3 of 3 entries</p>
                                                <nav class="" aria-label="Page navigation example">
                                                    <ul class="pagination justify-content-end">
                                                        <li class="page-item disabled">
                                                            <a class="page-link" href="#" tabindex="-1">
                                                                <i class="fa fa-angle-left"></i>
                                                                <span class="sr-only">Previous</span>
                                                            </a>
                                                        </li>
                                                        <li class="page-item active">
                                                            <a class="page-link" href="#">1</a>
                                                        </li>
                                                        <li class="page-item ">
                                                            <a class="page-link" href="#">2</a>
                                                        </li>
                                                        <li class="page-item">
                                                            <a class="page-link" href="#">3</a>
                                                        </li>
                                                        <li class="page-item">
                                                            <a class="page-link" href="#">
                                                                <i class="fa fa-angle-right"></i>
                                                                <span class="sr-only">Next</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- </div>
                        </div>
                    </div> -->
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    <div class="modal" id="add_subCategory">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Add Sub-Category</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Select Category <span
                                        class="text-red">*</span></label>
                                <select class="form-control " required="">
                                    <option value="0" disable="">Select Category
                                    </option>
                                    <option value="1">Recorded Course</option>
                                    <option value="2">Test Series</option>
                                    <option value="3">Free Course</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="bannername" class="form-label">Sub-Category Name <span
                                        class="text-red">*</span></label>
                                <input type="text" class="form-control" id="" placeholder="Enter Category Name">
                            </div>
                            <div class="form-group">
                                <label for="bannername" class="form-label">Upload Image<span
                                        class="text-red">*</span></label>
                                <input type="file" class="dropify" data-default-file="" data-height="150" required />
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn save-btn" type="button">Save changes</button>
                    <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="edit_subCategory">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Edit Sub-Category</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Select Category <span
                                        class="text-red">*</span></label>
                                <select class="form-control " required="">
                                    <option value="0" disable="">Select Category
                                    </option>
                                    <option value="1">Recorded Course</option>
                                    <option value="2">Test Series</option>
                                    <option value="3">Free Course</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="bannername" class="form-label">Sub-Category Name <span
                                        class="text-red">*</span></label>
                                <input type="text" class="form-control" id="" placeholder="Enter Category Name">
                            </div>
                            <div class="form-group">
                                <label for="bannername" class="form-label">Upload Image<span
                                        class="text-red">*</span></label>
                                <input type="file" class="dropify"
                                    data-default-file="<?php echo base_url(''); ?>/assets/images/test-series.png"
                                    data-height="150" required />
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn save-btn" type="button">Save changes</button> <button class="btn cancel-btn"
                        data-dismiss="modal" type="button">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
    document.getElementById('delete').onclick = function() {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: "No, cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal(
                        "Deleted!",
                        "Your  file has been deleted!",
                        "success");
                } else {
                    swal(
                        "Cancelled",
                        "Your  file is safe !",
                        "error"
                    );
                }
            });
    };
    </script>
<script>
    $(function() {
        setTimeout(function() { 
            $("#alert-msg").fadeOut(1500); 
        }, 2000)
    })
</script>
</body>

</html>