 <link rel="icon" href="<?php echo base_url(''); ?>/assets/images/brand/favicon32X32.png" type="image/x-icon" />

 <!-- Bootstrap css -->
 <link href="<?php echo base_url(''); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" />

 <!-- Style css -->
 <link href="<?php echo base_url(''); ?>assets/css/style.css" rel="stylesheet" />

 <!-- Animate css -->
 <link href="<?php echo base_url(''); ?>assets/css/animated.css" rel="stylesheet" />

 <!-- P-scroll bar css-->
 <link href="<?php echo base_url(''); ?>assets/plugins/p-scrollbar/p-scrollbar.css" rel="stylesheet" />



 <!---Sweetalert css-->
 <link href="<?php echo base_url(''); ?>assets/plugins/sweet-alert/jquery.sweet-modal.min.css" rel="stylesheet" />
 <link href="<?php echo base_url(''); ?>assets/plugins/sweet-alert/sweetalert.css" rel="stylesheet" />

 <!---Icons css-->
 
 <link href="<?php echo base_url(''); ?>assets/plugins/web-fonts/icons.css" rel="stylesheet" />
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/fontawesome.min.css" /> -->
 <link href="<?php echo base_url(''); ?>assets/plugins/web-fonts/plugin.css" rel="stylesheet" />

 <!-- <link href="<?php echo base_url(''); ?>assets/plugins/fontawesome/fontawesome.min.css" rel="stylesheet" /> -->
 
 <!-- File Uploads css-->
 <link href="<?php echo base_url(''); ?>assets/plugins/fileupload/css/fileupload.css" rel="stylesheet"
     type="text/css" />

 <!-- Data table css -->
 <link href="<?php echo base_url(''); ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />

 <!-- Magnify css -->
 <link href="<?php echo base_url(''); ?>assets/css/magnific.css" rel="stylesheet" />

 <!-- Dropify -->
<link href="<?php echo base_url(''); ?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet" />

<!-- summernote -->
 <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/summernote/summernote.min.css"/>

 <!-- bootstrap4-toggle -->
 <link href="<?php echo base_url(''); ?>assets/plugins/bootstrap4-toggle/bootstrap4-toggle.min.css" rel="stylesheet">



 <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> -->



  <!-- Switcher css -->
  <!-- <link href="<?php echo base_url(''); ?>assets/switcher/css/switcher.css" rel="stylesheet">
 <link href="<?php echo base_url(''); ?>assets/switcher/demo.css" rel="stylesheet"> -->

 <!-- Dark css -->
 <!-- <link href="../public/assets/css/dark.css" rel="stylesheet" /> -->

 <!-- Skins css -->
 <!-- <link href="<?php echo base_url(''); ?>assets/css/skins.css" rel="stylesheet" /> -->



 <!--Sidemenu css -->
 <!-- <link id="theme" href="<?php echo base_url(''); ?>assets/css/sidemenu4.css" rel="stylesheet"> -->

<script src="<?php echo base_url(''); ?>assets/js/vendors/jquery-3.5.1.min.js"></script>





 <!-- INTERNAL CSS START -->
 <!---jvectormap css-->
 <!-- <link href="<?php echo base_url(''); ?>assets/plugins/jvectormap/jqvmap.css" rel="stylesheet" /> -->




 <!-- Select2 css -->
 <link href="<?php echo base_url(''); ?>assets/plugins/select2/select2.min.css" rel="stylesheet" />
   <!--multi css-->
   <!-- <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/multi/multi.min.css"> -->

   <!--Sumoselect css-->
   <!-- <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/sumoselect/sumoselect.css"> -->

 <!-- Time picker css -->
 <!-- <link href="<?php echo base_url(''); ?>assets/plugins/time-picker/jquery.timepicker.css" rel="stylesheet" /> -->

 <!--Daterangepicker css-->
 <!-- <link href="<?php echo base_url(''); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" /> -->

 <!-- Date Picker css -->
 <link href="<?php echo base_url(''); ?>assets/plugins/date-picker/date-picker.css" rel="stylesheet" />


 <!-- Gallery css -->
 <!-- <link href="<?php echo base_url(''); ?>/assets/plugins/lightgallery/gallery.css" rel="stylesheet"> -->


 <!-- Forn-wizard css-->
 <!-- <link href="<?php echo base_url(''); ?>assets/plugins/form-wizard/css/form-wizard.html" rel="stylesheet" />
		<link href="<?php echo base_url(''); ?>assets/plugins/formwizard/smart_wizard.css" rel="stylesheet"/>
		<link href="<?php echo base_url(''); ?>assets/plugins/formwizard/smart_wizard_theme_dots.css" rel="stylesheet"/> -->

    <style>
         .col-ting {
  width: 100%;
  margin: 0 auto;
  margin-top: 3em;
  margin-bottom: 3em;
}

.file-upload .image-box {
    display: flex;
    justify-content: center;
    padding: 14px;
    height: 191px;
    width: 100%;
    background: #efefef;
    cursor: pointer;
    overflow: hidden;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgb(0 0 0 / 10%);
}
.file-upload .image-box:hover {
    content: "SElect";
    background: #0a050552;
   box-shadow: 0px 2px 4px 0px rgb(0 0 0 / 12%);
}
.file-upload .image-box img {
  height: 100%;
  display: block;
}
.file-upload .image-box img:hover {
    height: 100%;
    display: block;
    opacity: 0.7;
}
.file-upload .image-box p {
  position: relative;
  top: 45%;
  color: #fff;
}
        </style>