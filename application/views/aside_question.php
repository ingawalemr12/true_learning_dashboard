<aside class="back-menu">
    <div class="test-tab ">
        <div class="panel panel-primary back-menu-center">
            <div class="tab-menu-heading border-0">
                <div class="tabs-menu test-mgmt-menu">
                    <!-- Tabs -->
                    <ul class="nav panel-tabs nav-pills-custom nav-justified  d-flex">
                        <li class="nav-item">
                            <a class="nav-link" id="v-pills-sub-tab" href="<?php echo base_url('add_questions'); ?>">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add Question</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="v-pills-sub-tab" href="<?php echo base_url('manage_question'); ?>">
                                <i class="fa fa-list mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Manage Question </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="v-pills-addSubCat-tab"
                                href="<?php echo base_url('upload_question'); ?>">
                                <i class="fa fa-upload mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Upload Question</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="test-tab ">

        <div class="panel panel-primary toggle-menu">
            <div class="tab-menu-heading border-0">
                <div class="tabs-menu test-mgmt-menu">
                    <!-- Tabs -->
                    <label for="toggle-btn">
                        <i class="fa fa-bars toggle-icon"></i>
                    </label>
                    <input type="checkbox" name="" id="toggle-btn">
                    
                    <ul class="nav panel-tabs nav-pills-custom nav-justified  " id="toggle-items">
                        <li class="nav-item-1">
                            <a class="nav-link" id="v-pills-sub-tab" href="<?php echo base_url('add_questions'); ?>">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add Question</span>
                            </a>
                        </li>
                        <li class="nav-item-2">
                            <a class="nav-link" id="v-pills-sub-tab" href="<?php echo base_url('manage_question'); ?>">
                                <i class="fa fa-list mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Manage Question </span>
                            </a>
                        </li>
                        <li class="nav-item-3">
                            <a class="nav-link" id="v-pills-addSubCat-tab"
                                href="<?php echo base_url('upload_question'); ?>">
                                <i class="fa fa-upload mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Upload Question</span>
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </div>
</aside>