 <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Manage Questions </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Manage Questions</h4>
                            </span>

                        </div> 
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Manage Questions</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('aside_question'); ?>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <?php 
                            if (!empty($this->session->flashdata('create')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('create');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('edit')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('edit');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('exists')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-warning" id="alert_msg">
                                      <?php echo $this->session->flashdata('exists');?>
                                  </div>
                              </div>
                            <?php
                            }
                            ?>
                            <div class="card">
                                <!-- filter -->
                                <div class="card-header d-block">
                                    <form method="post" action="">
                                        <div class="row justify-content-center m-2">
                                            <div class="col-md-2">
                                                <div class="input-group mt-2">
                                                    <select name="std_id" id="std_id" class="form-control table-search-box" required="">
                                                        <option value="" selected disabled>Select Standard </option>
                                                        <?php 
                                                        if (!empty($standard_list)) 
                                                        {
                                                            foreach ($standard_list as $std) 
                                                            { 
                                                                ?>
                                                                <option value="<?php echo $std['std_id'] ?>">
                                                                    <?php echo $std['std_name'] ?>
                                                                </option>
                                                                <?php   
                                                            }  
                                                        } 
                                                        else
                                                        {
                                                            echo "";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="input-group mt-2">
                                                    <select name="subject_id" id="subject_id" class="form-control table-search-box" required>
                                                        <option value="" selected disabled>Select Subject</option>

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="input-group mt-2">
                                                    <select name="difficulty_id" id="difficulty_id" class="form-control table-search-box" required>
                                                         <option value="" selected disabled>Select Difficulty Level
                                                        </option>
                                                        <?php
                                                        if (!empty($difficulty_list)) 
                                                        {
                                                            foreach ($difficulty_list as $dif) 
                                                            {
                                                                ?>
                                                                <option value="<?php echo $dif['difficulty_id']; ?>">
                                                                    <?php echo $dif['difficulty_medium']; ?>
                                                                </option>
                                                                <?php
                                                            }
                                                        }else
                                                        {
                                                            echo " ";
                                                        }
                                                        ?>

                                                        <?php
                                                        if (!empty($language_list)) 
                                                        {
                                                            foreach ($language_list as $lag) 
                                                            {
                                                                ?>
                                                                <option value="<?php echo $lag['lang_id']; ?>">
                                                                    <?php echo $lag['language_name']; ?>
                                                                </option>
                                                                <?php
                                                            }
                                                        }else
                                                        {
                                                            echo " ";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="input-group mt-2">
                                                    <button type="submit" class="btn btn-primary" name="search_questions" type="button">Go!</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-body">
                                    <div class="row justify-content-end">
                                        <div class="col-md-2 mb-3">
                                            <input type="text" class="form-control" name="" id="myInput" onkeyup="search_table()" placeholder="Search">
                                        </div>
                                    </div>
                                    <div class="table-responsive ">
                                        <!-- <div class="table-responsive" id="lightgallery"> -->
                                        <table class="table table-hover border table-vcenter text-nowrap mng-qus mt-2" id="myTable">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="wd-15p border-bottom-0">Sr. No.</th>
                                                    <!-- <th class="wd-15p border-bottom-0">question_id</th> -->
                                                    <th class="wd-15p border-bottom-0">Questions</th>
                                                    <th class="wd-15p border-bottom-0">Questions Type</th>
                                                    <th class="wd-20p border-bottom-0">Standard</th>
                                                    <th class="wd-20p border-bottom-0">Subject</th>
                                                    <th class="wd-20p border-bottom-0">Difficulty</th>
                                                    <th class="wd-20p border-bottom-0">Update Date</th>
                                                    <th>Related Image</th>
                                                    <th class="wd-20p border-bottom-0">Status</th>
                                                    <th class="wd-20p border-bottom-0">Action</th>
                                                </tr>
                                            </thead>

                                            <tbody class="text-center">
                                            <?php 
                                            if (!empty($questions_list)) 
                                            {
                                                $i= 1+$this->uri->segment(2);
                                                foreach ($questions_list as $que) 
                                                {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $i++; ?></td>
                                                        <!-- <td><?php echo $que['question_id']; ?></td> -->
                                                        <td><?php echo $que['question_name']; ?></td>
                                                        <!-- answer_type -->
                                                        <td>
                                                            <?php  
                                                            if ($que['answer_type'] == 1) 
                                                            {
                                                                echo "Single Choice";
                                                            } elseif ($que['answer_type'] == 2) 
                                                            {
                                                                echo "Multiple Choice";
                                                            } elseif ($que['answer_type'] == 3) 
                                                            {
                                                                echo "Integer";
                                                            } elseif ($que['answer_type'] == 4) 
                                                            {
                                                                echo "True / False";
                                                            } elseif ($que['answer_type'] == 5) 
                                                            {
                                                                echo "Match Matrix";
                                                            }elseif ($que['answer_type'] == 6) 
                                                            {
                                                                echo "Match The Following";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($standard_list)) 
                                                            {
                                                                foreach ($standard_list as $std) 
                                                                {
                                                                    if ($std['std_id'] == $que['std_id']) 
                                                                    {
                                                                        echo $std['std_name'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?> 
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($subject_list)) 
                                                            {
                                                                foreach ($subject_list as $sub) 
                                                                {
                                                                    if ($sub['subject_id'] == $que['subject_id']) 
                                                                    {
                                                                        echo $sub['subject_name'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($difficulty_list)) 
                                                            {
                                                                foreach ($difficulty_list as $dif) 
                                                                {
                                                                    if ($dif['difficulty_id'] == $que['difficulty_id']) 
                                                                    {
                                                                        echo $dif['difficulty_medium'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td><?php echo date("d M Y", strtotime($que['updated_at'])); ?>
                                                        </td>
                                                        <td>
                                                            <div class="magnific-img">
                                                            <?php 
                                                            if ($que['images']) 
                                                            {
                                                                ?>
                                                                <a class="image-popup-vertical-fit" href="<?php echo $que['images']; ?>" title="">
                                                                    <img src="<?php echo $que['images']; ?>" alt="image" style="width: 50px !important;height: 50px !important;object-fit: contain;" />
                                                                </a>
                                                                <?php
                                                            } 
                                                            else
                                                            {
                                                                ?>
                                                                <a class="image-popup-vertical-fit" href="<?php echo base_url().'/assets/images/no_image.png' ?>" title="">
                                                                    <img src="<?php echo base_url().'/assets/images/no_image.png' ?>" alt="image" style="width: 50px !important;height: 50px !important;object-fit: contain;" />
                                                                </a>
                                                                <?php         
                                                            } 
                                                            ?> 
                                                            </div>
                                                        </td>
                                                        <!-- status -->
                                                        <td>
                                                            <?php 
                                                            if ($que['status'] == 1) 
                                                            {
                                                                ?>
                                                                <input type="checkbox" id="<?php echo $que['question_id']; ?>" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="0"> 
                                                                <?php
                                                            } 
                                                            else
                                                            {
                                                                ?>
                                                                <input type="checkbox" id="<?php echo $que['question_id']; ?>" data-size="sm" data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="1">
                                                                <?php         
                                                            } 
                                                            ?> 
                                                        </td>
                                                        <td>
                                                            <!-- edit -->
                                                            <a type="button" class="btn btn-sm btn-icon btn-primary" data-target="#edit-question" id="<?php echo $que['question_id']; ?>" onclick="get_records(this)"  data-toggle="modal"><i class="fa fa-edit"></i></a> 

                                                            <!-- delete -->
                                                            <a type="button" class="btn btn-sm btn-icon btn-secondary" id='delete_<?php echo $que['question_id']; ?>'><i class="fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>

                                                    <!-- delete -->
                                                    <script>
                                                    document.getElementById('delete_<?php echo $que['question_id']; ?>').onclick = function() 
                                                    {
                                                        var id = $("#<?php echo $que['question_id']; ?>").val(); 
                                                   
                                                        swal({
                                                                title: "Are you sure?",
                                                                text: "You will not be able to recover this file!",
                                                                type: "warning",
                                                                showCancelButton: true,
                                                                confirmButtonColor: '#DD6B55',
                                                                confirmButtonText: 'Yes, delete it!',
                                                                cancelButtonText: "No, cancel",
                                                                closeOnConfirm: false,
                                                                closeOnCancel: false
                                                        },
                                                        function(isConfirm) {
                                                            if (isConfirm) 
                                                            {
                                                                $.ajax({
                                                                           url: '<?php echo base_url().'delete_manage_question/'.$que['question_id']; ?>',
                                                                           type: "POST",
                                                                           data: {id:id},
                                                                           dataType:"HTML",
                                                                           success: function () {
                                                                            swal(
                                                                                    "Deleted!",
                                                                                    "Your file has been deleted!",
                                                                                    "success"
                                                                                ),
                                                                                $('.confirm').click(function()
                                                                                {
                                                                                    location.reload();
                                                                                });
                                                                            },
                                                                    });
                                                                
                                                            } else {
                                                                swal(
                                                                    "Cancelled",
                                                                    "Your  file is safe !",
                                                                    "error"
                                                                );
                                                            }
                                                        });
                                                    };
                                                    </script>

                                                    <?php
                                                }
                                            }
                                            else
                                            {
                                                ?>
                                                <p id="founder" class="text-danger">No matching records found</p>
                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-footer align-items-center">
                                        <!-- <p class="">Showing 1 to 3 of 3 entries</p> -->
                                        <nav class="" aria-label="Page navigation example">
                                            <ul class="pagination justify-content-end">
                                                <?php echo $this->pagination->create_links(); ?>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <input type="hidden" id="editId" value="" name="">
    
    <!-- edit -->
    <div class="modal" id="edit-question">
        <div class="modal-dialog model-fullwidth" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Update Question </h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form method="post"  action="<?php echo base_url().'MainController/edit_manage_question'?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <!-- start editing Question --> 
                        <div class="panel panel-default active">
                            <div id="collapseOne1" class="panel-collapse  ">
                                <div class="panel-body border-0">
                                    <div class="row">
                                      
                                        <input type="hidden" name="question_id" id="q_id"  value="" >
                                      
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Standard
                                                    <span class="text-red">*</span></label>
                                                <select name="std_id" id="std" class="form-control" tabindex="-1" aria-hidden="true" required>
                                                    <?php 
                                                    if (!empty($standard_list)) 
                                                    {
                                                        foreach ($standard_list as $std) 
                                                        { 
                                                            ?>
                                                            <option value="<?php echo $std['std_id'] ?>">
                                                                <?php echo $std['std_name']; ?>
                                                            </option>
                                                            <?php   
                                                        }  
                                                    } 
                                                    else
                                                    {
                                                        echo "";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Subject<span class="text-red">*</span></label>
                                                <select name="subject_id" id="sub" class="form-control" tabindex="-1" aria-hidden="true" required>
                                                    <?php 
                                                    if (!empty($subject_list)) 
                                                    {
                                                        foreach ($subject_list as $sub) 
                                                        { 
                                                            ?>
                                                            <option value="<?php echo $sub['subject_id'] ?>"><?php echo $sub['subject_name']; ?></option>
                                                            <?php   
                                                        }  
                                                    } 
                                                    else
                                                    {
                                                        echo "";
                                                    }
                                                    ?>
                                                </select>                              
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Topic</label>
                                                <input type="text" class="form-control" name="topic" id="topic" placeholder="Enter Topic Name" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Sub Topic</label>
                                                <input type="text" class="form-control"  name="sub_topic" id="sub_topic" placeholder="Enter Sub Topic Name">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Difficulty Level <span class="text-red">*</span></label>
                                                <!-- <select class="form-control" tabindex="-1" aria-hidden="true" required id="difficulty_level"> -->
                                                <select name="difficulty_id" id="difficulty_level" class="form-control" tabindex="-1" aria-hidden="true" required>
                                                <?php 
                                                if (!empty($difficulty_list)) 
                                                {
                                                    foreach ($difficulty_list as $dif) 
                                                    {
                                                        ?>
                                                        <option value="<?php echo $dif['difficulty_id'] ?>"> <?php echo $dif['difficulty_medium']; ?></option>
                                                        <?php   
                                                    }  
                                                } 
                                                else
                                                {
                                                     echo "";
                                                }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Language
                                                    <span class="text-red">*</span></label>
                                                <select name="lang_id" id="lang" class="form-control" tabindex="-1" aria-hidden="true" required>
                                                 <?php 
                                                 if (!empty($language_list)) 
                                                 {
                                                     foreach ($language_list as $lag) 
                                                     {
                                                         ?>
                                                         <option value="<?php echo $lag['lang_id'] ?>">
                                                             <?php echo $lag['language_name']; ?>
                                                         </option>
                                                         <?php   
                                                     }  
                                                 } 
                                                 else
                                                 {
                                                     echo "";
                                                 }
                                                 ?>
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Correct Marks <span class="text-red">*</span></label>
                                                <input type="text" name="correct_marks" id="cor_mark" step="0.01" minlength="1" class="form-control" placeholder="Enter Correct Marks" required  onkeypress="return onlyNumberKey(event)">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Negative Marks
                                                    <span class="text-red">*</span></label>
                                                <input type="text" name="negative_marks" id="neg_mark" step="0.01" minlength="1" class="form-control" placeholder="Enter Negative Marks" required  onkeypress="return onlyNumberKey(event)">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Not Attempt Marks
                                                    <span class="text-red">*</span></label>
                                                <input type="text" name="not_attempt_marks" id="not_att_mark" step="0.01" minlength="1" class="form-control" placeholder="Enter Not Attempt Marks" required  onkeypress="return onlyNumberKey(event)">     
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Questions<span
                                                        class="text-red">*</span></label>
                                                <textarea id="summernote1" class="summernote question_n" name="question_name" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Related Image<span class="text-red">*</span></label>

                                                <input type="file" name="images" id="rel_images" class="dropifyy" data-default-file="" data-height="150" accept="image/*" /> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end editing Question --> 

                        <!-- start editing answer --> 
                        <div class="panel panel-default mt-2">
                            <div id="answer" class="panel-collapse ">
                               
                              <div class="panel-body border-0">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group ">
                                      <label for="bannername" class="form-label ">Answer Type
                                        <span class="text-red">*</span></label>
                                      
                                        <select name="answer_type" id="answere_type" class="form-control" tabindex="-1" aria-hidden="true" onchange="showHideFun(this.value);" >
                                          <option value="" selected disabled>Select Answer Type</option>
                                          <option value="1">Single Choice</option>
                                          <option value="2">Multiple Choice</option>
                                          <option value="3">Integer</option>
                                          <option value="4">True / False</option>
                                          <option value="5">Match Matrix</option>
                                          <option value="6">Match The Following</option>
                                        </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="row " id="single_choice"></div>
                                <div class="row " id="multiple_choice"></div>
                                <div class="row " id="integer"></div>
                                <div class="row " id="true_false"></div>
                                <div class="row " id="match_matrix"></div>
                                <div class="row " id="match_the_following"></div>

                              </div>
                            </div>
                        </div>
                        <!-- end editing answer --> 
                    </div>  <!-- modal-body -->
                    <div class="modal-footer">
                       <button type="submit" class="btn save-btn">Update Info</button>
                       <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
     
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    
    <?php 
    /*
    $wh_opt = '(question_id = "'.$question_id.'")';
    $opt = $this->Main_Model->getAllData_not_order($tbl='answers_option_match_matrix', $wh_opt);
    print_r($opt[0]['opt_id']);*/
    ?>

    <script>
    function showHideFun(value)
    {
        console.log(value);
        if (value == 0) 
        {
            $('#single_choice').hide();
            $('#multiple_choice').hide();
            $('#integer').hide();
            $('#true_false').hide();
            $('#match_matrix').hide();
            $('#match_the_following').hide();
        }
      
        if(value==1)
        {
            console.log("type 1");
        
            // $('#type1').show();
            $('#single_choice').show().empty().append('<div class="col-md-6 form-group">' +
                '<div class=" d-flex">' + '<label for="bannername" class="form-label pr-3">A.</label>' +
                '<textarea id="type_1_A" class="summernote " name="options_A" required="require" ></textarea>' +
                '</div></div>' +
                '<div class="col-md-6 form-group">' +
                '<div class=" d-flex">' + '<label for="bannername" class="form-label pr-3">B.</label>' +
                '<textarea id="type_1_B" class="summernote" name="options_B" required="require"></textarea>' +
                '</div></div>' +
                '<div class="col-md-6 form-group">' +
                '<div class=" d-flex">' + '<label for="bannername" class="form-label pr-3">C.</label>' +
                '<textarea id="type_1_C" class="summernote" name="options_C" required="require"></textarea>' +
                '</div></div>' +
                '<div class="col-md-6 form-group">' +
                '<div class=" d-flex">' + '<label for="bannername" class="form-label pr-3">D.</label>' +
                '<textarea id="type_1_D" class="summernote" name="options_D" required="require"></textarea>' +
                '</div></div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>' +
                '<table class="table table-bordered text-center">' +
                '<thead style="background: #f5f5f5;">' +
                '<tr><th data-option="1" style="">A</th>' +
                '<th data-option="2" style="">B</th>' +
                '<th data-option="3" style="">C</th>' +
                '<th data-option="4" style="">D</th></tr>' +
                '</thead><tbody><tr>' +
                '<td data-option="1" style="">' +
                '<input type="radio" name="sc_correct_option" id="type_1_cur_opt_1" value="A" required="require"> </td>' +
                '<td data-option="2" style="">' +
                '<input type="radio" name="sc_correct_option" id="type_1_cur_opt_2" value="B" required="require"></td>' +
                '<td data-option="3" style="">' +
                '<input type="radio" name="sc_correct_option" id="type_1_cur_opt_3" value="C" required="require"> </td>' +
                '<td data-option="4" style="">' +
                '<input type="radio"name="sc_correct_option" id="type_1_cur_opt_4" value="D" required="require"></td>' +
                '</tr></tbody></table></div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Solution(Optional)</label>' +
                '<textarea id="type_1_soln" class="summernote" name="solution"></textarea>' +
                '</div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Video Solution (Optional)</label>' +
                '<input type="text" name="video_solution" id="type_1_vid_soln" class="form-control" placeholder="Enter Solution Video link">' +
                '</div>');
            $('#multiple_choice').hide();
            $('#integer').hide();
            $('#true_false').hide();
            $('#match_matrix').hide();
            $('#match_the_following').hide();
            $('.summernote').summernote({
                                           height:70,                 
                                           minHeight: 70,             
                                           maxHeight: 70,
                                         });
            if ($('.summernote').summernote('isEmpty')) 
            {
                // alert('contents is empty1');
            }

        }

        if (value == 2) 
        {
            console.log("type 2");
            // $('#type1').show();
        //  var sc_correct_option="sc_correct_option[]"
            $('#single_choice').hide();
            $('#integer').hide();
            $('#true_false').hide();
            $('#match_matrix').hide();
            $('#match_the_following').hide();
            $('#multiple_choice').show().empty().append(
                '<div class="col-md-6 form-group">' +
                '<div class=" d-flex">' + '<label for="bannername" class="form-label pr-3">A.</label>' +
                '<textarea id="type_2_A" class="summernote" name="options_A" required="require"></textarea>' +
                '</div></div>' +
                '<div class="col-md-6 form-group">' +
                '<div class=" d-flex">' + '<label for="bannername" class="form-label pr-3">B.</label>' +
                '<textarea id="type_2_B" class="summernote" name="options_B" required="require"></textarea>' +
                '</div></div>' +
                '<div class="col-md-6 form-group">' +
                '<div class=" d-flex">' + '<label for="bannername" class="form-label pr-3">C.</label>' +
                '<textarea id="type_2_C" class="summernote" name="options_C" required="require"></textarea>' +
                '</div></div>' +
                '<div class="col-md-6 form-group">' +
                '<div class=" d-flex">' + '<label for="bannername" class="form-label pr-3">D.</label>' +
                '<textarea id="type_2_D" class="summernote" name="options_D" required="require"></textarea>' +
                '</div></div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>' +
                '<table class="table table-bordered text-center">' +
                '<thead style="background: #f5f5f5;">' +
                '<tr><th data-option="1" style="">A</th>' +
                '<th data-option="2" style="">B</th>' +
                '<th data-option="3" style="">C</th>' +
                '<th data-option="4" style="">D</th></tr>' +
                '</thead><tbody><tr>' +
                '<td data-option="1" style="">' +
                '<input type="checkbox" name="sc_correct_option[]" id="type_2_cur_opt_1" value="A" required="require"> </td>' +
                '<td data-option="2" style="">' +
                '<input type="checkbox" name="sc_correct_option[]" id="type_2_cur_opt_2" value="B" required="require"></td>' +
                '<td data-option="3" style="">' +
                '<input type="checkbox" name="sc_correct_option[]" id="type_2_cur_opt_3" value="C" required="require"> </td>' +
                '<td data-option="4" style="">' +
                '<input type="checkbox" name="sc_correct_option[]" id="type_2_cur_opt_4" value="D" required="require"></td>' +
                '</tr></tbody></table></div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Solution(Optional)</label>' +
                '<textarea id="type_2_soln" class="summernote" name="solution"></textarea>' +
                '</div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Video Solution (Optional)</label>' +
                '<input type="text" name="video_solution" id="type_2_vid_soln" class="form-control" placeholder="Enter Solution Video link">' +
                '</div>');
            $('.summernote').summernote({
                                           height:70,                 
                                           minHeight: 70,             
                                           maxHeight: 70,
            });
            jQuery(function($) {
                var requiredCheckboxes = $(':checkbox[required]');
                requiredCheckboxes.on('change', function(e) {
                    var checkboxGroup = requiredCheckboxes.filter('[name="' + $(this).attr(
                        'name') + '"]');
                    var isChecked = checkboxGroup.is(':checked');
                    checkboxGroup.prop('required', !isChecked);
                });
                requiredCheckboxes.trigger('change');
            });
        }
      
        if (value == 3) 
        {
            $('#single_choice').hide();
            $('#multiple_choice').hide();
            $('#true_false').hide();
            $('#match_matrix').hide();
            $('#match_the_following').hide();
            $('#integer').show().empty().append('<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Correct Answer</label>' +
                '<textarea id="type_3_cur_ans" class="form-control" rows="6"  name="correct_answer" required="require">' +
                '</textarea></div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Solution(Optional)</label>' +
                '<textarea id="type_3_soln" class="summernote" name="solution"></textarea>' +
                '</div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Video Solution (Optional)</label>' +
                '<input type="text" name="video_solution" id="type_3_vid_soln" class="form-control" placeholder="Enter Solution Video link">' +
                '</div>');
             $('.summernote').summernote({
               height:70,                 
               minHeight: 70,             
               maxHeight: 70,
             });
        }

        if (value == 4) 
        {
            $('#single_choice').hide();
            $('#multiple_choice').hide();
            $('#integer').hide();
            $('#match_matrix').hide();
            $('#match_the_following').hide();
            $('#true_false').show().empty().append('<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>' +
                '<table class="table table-bordered text-center">' +
                '<thead style="background: #f5f5f5;">' +
                '<tr><th data-option="1" style="">True</th>' +
                '<th data-option="2" style="">False</th>' +
                '</tr></thead><tbody><tr>' +
                '<td data-option="1" style="">' +
                '<input type="radio" name="correct_answer" id="type_4_cur_opt_1" value="1" required="require"> </td>' +
                '<td data-option="2" style="">' +
                '<input type="radio" name="correct_answer" id="type_4_cur_opt_2" value="2" required="require"></td>' +
                '</tr></tbody></table></div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Solution(Optional)</label>' +
                '<textarea id="type_4_soln" class="summernote" name="solution"></textarea>' +
                '</div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Video Solution (Optional)</label>' +
                '<input type="text" name="video_solution" id="type_4_vid_soln" class="form-control" placeholder="Enter Solution Video link">' +
                '</div>');
             $('.summernote').summernote({
               height:70,                 
               minHeight: 70,             
               maxHeight: 70,
             });
        }

        if (value == 5) 
        {
            $('#single_choice').hide();
            $('#multiple_choice').hide();
            $('#integer').hide();
            $('#true_false').hide();
            $('#match_the_following').hide();
            $('#match_matrix').show().empty().append(
                '<div class="col-md-6 ">' +
                '<div class=" form-group">' +
                '<label for="bannername " class="form-label text-center pr-3">Left Side Options</label>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">1.</label>' +
                '<textarea id="type_5_lft_opt_1" class="summernote" name="left_opt_1" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">2.</label>' +
                '<textarea id="type_5_lft_opt_2" class="summernote" name="left_opt_2" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">3.</label>' +
                '<textarea id="type_5_lft_opt_3" class="summernote" name="left_opt_3" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">4.</label>' +
                '<textarea id="type_5_lft_opt_4" class="summernote" name="left_opt_4" required="require"></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6 ">' +
                '<div class=" form-group">' +
                '<label for="bannername " class="form-label text-center pr-3">Right Side Options</label>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">A.</label>' +
                '<textarea id="type_5_right_opt_A" class="summernote" name="right_opt_A" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">B.</label>' +
                '<textarea id="type_5_right_opt_B" class="summernote" name="right_opt_B" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">C.</label>' +
                '<textarea id="type_5_right_opt_C" class="summernote" name="right_opt_C" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">D.</label>' +
                '<textarea id="type_5_right_opt_D" class="summernote" name="right_opt_D" required="require"></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6"><div class="form-group">' +
                '<label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>' +
                '<table class="table table-bordered text-center">' +
                '<thead style="background: #f5f5f5;"><tr><td></td>' +
                '<th data-option="1" style="">A</th>' +
                '<th data-option="2" style="">B</th>' +
                '<th data-option="3" style="">C</th>' +
                '<th data-option="4" style="">D</th></tr></thead>' +
                '<tbody><tr class="matchMatrixCorrectOption1" data-option="1" style="">' +
                '<th>1</th><td data-option="1" style="">' +
                '<input type="checkbox" name="mm_correct_option_1[]" value="A" id="type_5_cur_opt_1_1" required="require"></td>' +
                '<td data-option="2" style="">' +
                '<input type="checkbox" name="mm_correct_option_1[]" value="B" id="type_5_cur_opt_1_2" required="require"></td>' +
                '<td data-option="3" style="">' +
                '<input type="checkbox" name="mm_correct_option_1[]" value="C" id="type_5_cur_opt_1_3" required="require"></td>' +
                '<td data-option="4" style="">' +
                '<input type="checkbox" name="mm_correct_option_1[]" value="D" id="type_5_cur_opt_1_4" required="require"></td>' +
                '</tr>'+
                '<tr class="matchMatrixCorrectOption1"data-option="1" style="">' +
                '<th>2</th><td data-option="1" style="">' +
                '<input type="checkbox" name="mm_correct_option_2[]" value="A" id="type_5_cur_opt_2_1" required="require"></td>' +
                '<td data-option="2" style="">' +
                '<input type="checkbox" name="mm_correct_option_2[]" value="B" id="type_5_cur_opt_2_2" required="require"> </td>' +
                '<td data-option="3" style="">' +
                '<input type="checkbox" name="mm_correct_option_2[]" value="C" id="type_5_cur_opt_2_3" required="require"> </td>' +
                '<td data-option="4" style="">' +
                '<input type="checkbox" name="mm_correct_option_2[]" value="D" id="type_5_cur_opt_2_4" required="require"></td>' +
                '</tr><tr class="matchMatrixCorrectOption1" data-option="1" style="">' +
                '<th>3</th><td data-option="1" style="">' +
                '<input type="checkbox" name="mm_correct_option_3[]" value="A" id="type_5_cur_opt_3_1" required="require"></td>' +
                '<td data-option="2" style="">' +
                '<input type="checkbox" name="mm_correct_option_3[]" value="B" id="type_5_cur_opt_3_2" required="require"></td>' +
                '<td data-option="3" style="">' +
                '<input type="checkbox" name="mm_correct_option_3[]" value="C" id="type_5_cur_opt_3_3" required="require"></td>' +
                ' <td data-option="4" style="">' +
                '<input type="checkbox" name="mm_correct_option_3[]" value="D" id="type_5_cur_opt_3_4" required="require"></td>' +
                '</tr><tr class="matchMatrixCorrectOption1" data-option="1" style="">' +
                '<th>4</th><td data-option="1" style="">' +
                '<input type="checkbox" name="mm_correct_option_4[]" value="A" id="type_5_cur_opt_4_1" required="require"></td>' +
                '<td data-option="2" style="">' +
                '<input type="checkbox" name="mm_correct_option_4[]" value="B" id="type_5_cur_opt_4_2" required="require"></td>' +
                '<td data-option="3" style="">' +
                '<input type="checkbox" name="mm_correct_option_4[]" value="C" id="type_5_cur_opt_4_3" required="require"> </td>' +
                '<td data-option="4" style="">' +
                '<input type="checkbox" name="mm_correct_option_4[]" value="D" id="type_5_cur_opt_4_4" required="require"> </td>' +
                '</tr></tbody></table></div></div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Solution(Optional)</label>' +
                '<textarea id="type_5_soln" class="summernote" name="solution"></textarea>' +
                '</div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Video Solution (Optional)</label>' +
                '<input type="text" name="video_solution" id="type_5_vid_soln" class="form-control" placeholder="Enter Solution Video link">' +
                '</div>');
            $('.summernote').summernote({
                                           height:70,                 
                                           minHeight: 70,             
                                           maxHeight: 70,
            });

            jQuery(function($) 
            {
                var requiredCheckboxes = $(':checkbox[required]');
                requiredCheckboxes.on('change', function(e) {
                    var checkboxGroup = requiredCheckboxes.filter('[name="' + $(this).attr(
                        'name') + '"]');
                    var isChecked = checkboxGroup.is(':checked');
                    checkboxGroup.prop('required', !isChecked);
                });
                requiredCheckboxes.trigger('change');
            });
        }

        if (value == 6) 
        {
            $('#single_choice').hide();
            $('#multiple_choice').hide();
            $('#integer').hide();
            $('#true_false').hide();
            $('#match_matrix').hide();
            $('#match_the_following').show().empty().append(
                '<div class="col-md-6 ">' +
                '<div class=" form-group">' +
                '<label for="bannername " class="form-label text-center pr-3">Left Side Options</label>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">1.</label>' +
                '<textarea id="type_6_lft_opt_1" class="summernote" name="left_opt_1" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">2.</label>' +
                '<textarea id="type_6_lft_opt_2" class="summernote" name="left_opt_2" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">3.</label>' +
                '<textarea id="type_6_lft_opt_3" class="summernote" name="left_opt_3" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">4.</label>' +
                '<textarea id="type_6_lft_opt_4" class="summernote" name="left_opt_4" required="require"></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6 ">' +
                '<div class=" form-group">' +
                '<label for="bannername " class="form-label text-center pr-3">Right Side Options</label>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">A.</label>' +
                '<textarea id="type_6_right_opt_A" class="summernote" name="right_opt_A" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">B.</label>' +
                '<textarea id="type_6_right_opt_B" class="summernote" name="right_opt_B" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">C.</label>' +
                '<textarea id="type_6_right_opt_C" class="summernote" name="right_opt_C" required="require"></textarea>' +
                '</div>' +
                '<div class=" form-group d-flex">' +
                '<label for="bannername" class="form-label pr-3">D.</label>' +
                '<textarea id="type_6_right_opt_D" class="summernote" name="right_opt_D" required="require"></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6"><div class="form-group">' +
                '<label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>' +
                '<table class="table table-bordered text-center">' +
                '<thead style="background: #f5f5f5;"><tr><th></th>' +
                '<th data-option="1" style="">A</th>' +
                '<th data-option="2" style="">B</th>' +
                '<th data-option="3" style="">C</th>' +
                '<th data-option="4" style="">D</th></tr></thead>' +
                '<tbody>'+
                '<tr class="matchMatrixCorrectOption1" data-option="1" style="">' +
                '<th>1 <input type="hidden" name="opt_id_1" value="" id="type_6_opt_1"></th><td data-option="1" style="">' +
                '<input type="radio" class="radio_1" name="correct_option1" value="A" id="type_6_cur_opt_1_1" required="require"></td>' +
                '<td data-option="2" style="">' +
                '<input type="radio" class="radio_2" name="correct_option1" value="B" id="type_6_cur_opt_1_2" required="require"></td>' +
                '<td data-option="3" style="">' +
                '<input type="radio" class="radio_3" name="correct_option1" value="C" id="type_6_cur_opt_1_3" required="require"></td>' +
                '<td data-option="4" style="">' +
                '<input type="radio" class="radio_4" name="correct_option1" value="D" id="type_6_cur_opt_1_4" required="require"></td>' +
                '</tr><tr class="matchMatrixCorrectOption1"data-option="1" style="">' +
                '<th>2 <input type="hidden" name="opt_id_2" value="" id="type_6_opt_2"></th><td data-option="1" style="">' +
                '<input type="radio" class="radio_1" name="correct_option2" value="A" id="type_6_cur_opt_2_1" required="require"></td>' +
                '<td data-option="2" style="">' +
                '<input type="radio" class="radio_2" name="correct_option2" value="B" id="type_6_cur_opt_2_2" required="require"> </td>' +
                '<td data-option="3" style="">' +
                '<input type="radio" class="radio_3" name="correct_option2" value="C" id="type_6_cur_opt_2_3" required="require"> </td>' +
                '<td data-option="4" style="">' +
                '<input type="radio" class="radio_4" name="correct_option2" value="D" id="type_6_cur_opt_2_4" required="require"></td>' +
                '</tr><tr class="matchMatrixCorrectOption1" data-option="1" style="">' +
                '<th>3 <input type="hidden" name="opt_id_3" value="" id="type_6_opt_3"></th><td data-option="1" style="">' +
                '<input type="radio" class="radio_1" name="correct_option3" value="A" id="type_6_cur_opt_3_1" required="require"></td>' +
                '<td data-option="2" style="">' +
                '<input type="radio" class="radio_2" name="correct_option3" value="B" id="type_6_cur_opt_3_2" required="require"></td>' +
                '<td data-option="3" style="">' +
                '<input type="radio" class="radio_3" name="correct_option3" value="C" id="type_6_cur_opt_3_3" required="require"></td>' +
                ' <td data-option="4" style="">' +
                '<input type="radio" class="radio_4" name="correct_option3" value="D" id="type_6_cur_opt_3_4" required="require"></td>' +
                '</tr><tr class="matchMatrixCorrectOption1" data-option="1" style="">' +
                '<th>4<input type="hidden" name="opt_id_4" value="" id="type_6_opt_4"></th><td data-option="1" style="">' +
                '<input type="radio" class="radio_1" name="correct_option4" value="A" id="type_6_cur_opt_4_1" required="require"></td>' +
                '<td data-option="2" style="">' +
                '<input type="radio" class="radio_2" name="correct_option4" value="B" id="type_6_cur_opt_4_2" required="require"></td>' +
                '<td data-option="3" style="">' +
                '<input type="radio" class="radio_3" name="correct_option4" value="C" id="type_6_cur_opt_4_3" required="require"> </td>' +
                '<td data-option="4" style="">' +
                '<input type="radio" class="radio_4" name="correct_option4" value="D" id="type_6_cur_opt_4_4" required="require"> </td>' +
                '</tr></tbody></table></div></div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Solution(Optional)</label>' +
                '<textarea id="type_6_soln" class="summernote" name="solution"></textarea>' +
                '</div>' +
                '<div class="col-md-6 form-group">' +
                '<label for="bannername" class="form-label">Video Solution (Optional)</label>' +
                '<input type="text" name="video_solution" id="type_6_vid_soln" class="form-control" placeholder="Enter Solution Video link">' +
                '</div>'
            );
             $('.summernote').summernote({
               height:70,                 
               minHeight: 70,             
               maxHeight: 70,
             });
                      $("input[type=radio]").click(function(){
                $("input."+this.className).not($(this)).each(function(){
                this.checked = false;
                });
            });
        }
    }
    </script>
  
    <script>
    $(document).ready(function() 
    {
        $('.summernote').summernote({
                                       height:100,                 
                                       minHeight: 100,             
                                       maxHeight: 100,
                                     });
    });
    </script>

    <script type="text/javascript">
    function get_records(idd) 
    {
        document.getElementById("editId").value=idd.id
         
        var base_url = '<?php echo base_url() ?>';
        $.ajax({
                    url:base_url + "MainController/get_manage_question_record",
                    type: "post", 
                    data: {
                            question_id:$("#editId").val(),
                            // uid: $("#uid").val(),
                        },
                    success: function(data) 
                    {
                        if(data==0)
                        {
                             // console.log("data no") 
                        }
                        else
                        {
                            const obj = JSON.parse(data);
                            console.log(obj);
                          
                          
                          
                          ///comman question block
                            document.getElementById('std').value = obj.std_id;
                            document.getElementById('sub').value = obj.subject_id;

                            document.getElementById('topic').value = obj.topic;
                            document.getElementById('sub_topic').value = obj.sub_topic;
                            document.getElementById('difficulty_level').value = obj.difficulty_id;
                            document.getElementById('lang').value = obj.lang_id;
                            document.getElementById('cor_mark').value = obj.correct_marks; 
                            document.getElementById('neg_mark').value = obj.negative_marks;
                            document.getElementById('not_att_mark').value = obj.not_attempt_marks;
                            $('.question_n').summernote('code',obj.question_name );
                            document.getElementById('q_id').value  = obj.question_id;
                           // $("#rel_images").addClass('dropifyy');
                            var img_path=obj.images;
                             img_up(img_path);
                                   function img_up(idd)
                                    {
                                      
                                       var drEvent = $('.dropifyy').dropify(
                                      {
                                          efaultFile: idd
                                      });
                                         
                                      drEvent = drEvent.data('dropify');
                                      drEvent.resetPreview();
                                      drEvent.clearElement();
                                      drEvent.settings.defaultFile = idd
                                      drEvent.destroy();
                                      drEvent.init();

                                    }
                          
                          document.getElementById('answere_type').value = obj.answer_type;
                          //answere type single choice
                          
                          
                          
                          
                          if(obj.answer_type==1){
                           showHideFun(obj.answer_type);
                            
                           //set option values
                             $('#type_1_A').summernote('code',obj.options_A );
                             $('#type_1_B').summernote('code',obj.options_B );
                             $('#type_1_C').summernote('code',obj.options_C );
                             $('#type_1_D').summernote('code',obj.options_D );
                            
                            //set correct answere
                            var input_count=$("#single_choice table tbody td input").length;
                            
                           for(var i=1; i<=input_count;i++){
                                var opt_c=document.getElementById('type_1_cur_opt_'+i).value;
                              
                                if(obj.correct_answer==opt_c){
                                  document.getElementById('type_1_cur_opt_'+i).checked = true;
                                }
                             
                            }
                            //set soln
                            $('#type_1_soln').summernote('code',obj.solution );
                            
                            //set vid soln
                            document.getElementById('type_1_vid_soln').value = obj.video_solution;
                          }
                          
                          
                          
                          
                          
                          
                          if(obj.answer_type==2){
                           
                            showHideFun(obj.answer_type);
                            
                            //set option values
                             $('#type_2_A').summernote('code',obj.options_A );
                             $('#type_2_B').summernote('code',obj.options_B );
                             $('#type_2_C').summernote('code',obj.options_C );
                             $('#type_2_D').summernote('code',obj.options_D );
                            
                            //set correct answere
                            var input_count=$("#multiple_choice table tbody td input").length;
                            var index_of=0;
                            //obj.options.length;
                           
                            console.log("arr_len=",obj.options.length);
                            for(var i=1; i<=input_count;i++)
                            {
                             if(index_of < obj.options.length){
                                  var currect_ans=obj.options[index_of];
                               }
                              
                                console.log("currect_ans=",currect_ans.correct_answer)
                                var opt_c=document.getElementById('type_2_cur_opt_'+i).value;
                                console.log("table head opt",opt_c)
                                if(currect_ans.correct_answer==opt_c)
                                {
                                  document.getElementById('type_2_cur_opt_'+i).checked = true;
                                }
                              
                                                          
                              index_of++;
                            }
                            //set soln
                            $('#type_2_soln').summernote('code',obj.solution );
                            
                            //set vid soln
                            document.getElementById('type_2_vid_soln').value = obj.video_solution;
                           }
                          
                          
                          
                          
                          
                           if(obj.answer_type==3){
                           
                            showHideFun(obj.answer_type);
                            //currect answere
                            document.getElementById('type_3_cur_ans').innerHTML  = obj.correct_answer;
                            //set soln
                            $('#type_3_soln').summernote('code',obj.solution );
                            
                            //set vid soln
                            document.getElementById('type_3_vid_soln').value = obj.video_solution;
                                                         
                          }
                          
                          
                          
                          
                          
                          
                          
                           if(obj.answer_type==4){
                            
                            showHideFun(obj.answer_type);
                             
                             //set correct answere
                            var input_count=$("#true_false table tbody td input").length;
                            for(var i=1; i<=input_count;i++){
                                var opt_c=document.getElementById('type_4_cur_opt_'+i).value;
                                 if(obj.correct_answer==opt_c){
                                    document.getElementById('type_4_cur_opt_'+i).checked = true;
                                }
                             
                            }
                            //set soln
                            $('#type_4_soln').summernote('code',obj.solution);
                             
                            //set vid soln
                            document.getElementById('type_4_vid_soln').value = obj.video_solution;
                          }
                          
                          
                          
                          
                          
                          
                           if(obj.answer_type==5){                           
                             showHideFun(obj.answer_type);
                             //set option values
                             $('#type_5_lft_opt_1').summernote('code',obj.left_opt_1);
                             $('#type_5_lft_opt_2').summernote('code',obj.left_opt_2);
                             $('#type_5_lft_opt_3').summernote('code',obj.left_opt_3);
                             $('#type_5_lft_opt_4').summernote('code',obj.left_opt_4);
                             $('#type_5_right_opt_A').summernote('code',obj.right_opt_A);
                             $('#type_5_right_opt_B').summernote('code',obj.right_opt_B);
                             $('#type_5_right_opt_C').summernote('code',obj.right_opt_C);
                             $('#type_5_right_opt_D').summernote('code',obj.right_opt_D);
                             $('#type_5_soln').summernote('code',obj.solution);
                             
                             
                            var thead=$("#match_matrix table thead th").length;
                            var tr=$("#match_matrix tbody tr").length;
                             console.log("thead  =",thead);
                            console.log("tr=", tr);
                             
                            console.log("arr_len=",obj.options.length);
                             
                            var index_of=0;
                            for(var i=1; i<=tr; i++)
                            {
                             
                                
                                for(var k=0;k < obj.options.length ;k++)
                                  {
                                    var get_in_op=obj.options[k];
                                    if(get_in_op.correct_options_l==i){
                                     // console.log("correct_options_r",get_in_op.correct_options_r);
                                      
                                      
                                       for(var j=1;j<=thead;j++)
                                       {
                                               //console.log("option get =", get_in_op);
                                              var head_opt = $('#match_matrix thead th:nth-child('+(j+1)+')');
                                              //console.log("i=",i);
                                              // console.log("get_in_op=",get_in_op);
                                              var thead_opt=head_opt .text();
                                              //console.log("thead_opt=",thead_opt)
                                             if(get_in_op.correct_options_r==thead_opt)
                                                {
                                                document.getElementById('type_5_cur_opt_'+i+'_'+j).checked = true;
                                              }
                                         }
                                
                                    }
                                    
                                  }
                                console.log("\n");
                              
                              
                             }
                             //set vid soln
                             document.getElementById('type_5_vid_soln').value = obj.video_solution;
                             
                          }
                           if(obj.answer_type==6){
                           
                            showHideFun(obj.answer_type);
                             //set option values
                             $('#type_6_lft_opt_1').summernote('code',obj.left_opt_1);
                             $('#type_6_lft_opt_2').summernote('code',obj.left_opt_2);
                             $('#type_6_lft_opt_3').summernote('code',obj.left_opt_3);
                             $('#type_6_lft_opt_4').summernote('code',obj.left_opt_4);
                             $('#type_6_right_opt_A').summernote('code',obj.right_opt_A);
                             $('#type_6_right_opt_B').summernote('code',obj.right_opt_B);
                             $('#type_6_right_opt_C').summernote('code',obj.right_opt_C);
                             $('#type_6_right_opt_D').summernote('code',obj.right_opt_D);
                             $('#type_6_soln').summernote('code',obj.solution); 
                             //set vid soln
                             document.getElementById('type_6_vid_soln').value = obj.video_solution;
                             
                             
                             
                             
                            var thead=$("#match_the_following table thead th").length;
                            var tr=$("#match_the_following tbody tr").length;
                            console.log("thead  =",thead);
                            console.log("tr=", tr);
                            var index_of=0;
                            for(var i=1; i<=tr; i++)
                            {
                              for(var j=1;j<thead;j++)
                              {
                                      var get_in_op=obj.options[index_of];
                                     // console.log("option get =", get_in_op);
                                      var head_opt = $('#match_the_following thead th:nth-child('+(j+1)+')');
                                      var thead_opt=head_opt .text();
                                      console.log("opt ids=",get_in_op.opt_id);
                                      document.getElementById('type_6_opt_'+i).value = get_in_op.opt_id;
                                      if(get_in_op.correct_options_l==i && get_in_op.correct_options_r==thead_opt)
                                      {                                   
                                       
                                        var opt_c=document.getElementById('type_6_cur_opt_'+i+'_'+j).checked = true;
                                        //var input=$('#match_matrix tbody tr['+i+'] td input').length;
                                        //console.log("options", opt_c);
                                      }
                                
                                }
                               index_of++;
                               console.log("\n");
                             }
                             
                          }
                          
                          
                            // document.getElementById('question_id').value = obj.question_id;
                        }
                    }
                });
            
        }
    </script>
   
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({
          height:70,                 
          minHeight: 70,             
          maxHeight: 70,
        });
    });
    </script>
   

     <!-- status active-deactive -->
    <script type="text/javascript"> 
        function change_status(idd)
        {
            var status_val=idd.value;
            var question_id= idd.id;
            
            var base_url = '<?php echo base_url() ?>';
            var question_id = question_id;

            if(status_val != '')
            {
                $.ajax({
                          url:base_url + "MainController/active_question_status",
                          method:"POST",
                          data:{status:status_val,question_id:question_id},
                          
                          success: function(data)
                          {
                               // alert(data);
                               if (data == 1) 
                               {
                                   alert('Status Changed Sucessfully !..');
                                   location.reload();
                               } 
                               else 
                               {
                                   alert('Something Went Wrong !...')
                               }
                          }
                    });
            }
        }
    </script>

    
    <!-- on change of Standard , it select Subject -->
    <script type="text/javascript">
        $("document").ready(function () 
        {
            // filter record code
            $("#std_id").change(function () 
            {
                var std_id = $("#std_id").val();  //$(this).val();
                // alert(std_id);

                if (std_id !="") 
                {
                    $.ajax({
                        url:'<?php echo base_url().'MainController/get_subject_list' ?>',
                        method:'POST',
                        data: {std_id:std_id},
                        success:function (data) {
                            $("#subject_id").html(data);
                        }
                    }); //end ajax
                }

            }); // end change(function)



            // json format code
            $("#std").change(function () 
            {
             // console.log("g", $("#std").val())
                var std_id = $("#std").val();  //$(this).val();
                // alert(std_id);

                if (std_id !="") 
                {
                    $.ajax({
                        url:'<?php echo base_url().'MainController/get_subject_list' ?>',
                        method:'POST',
                        data: {std_id:std_id},
                        success:function (data) {
                          console.log(data);
                            $("#sub").html(data);
                        }
                    }); //end ajax
                }
              else
                {
                    console.log("no data"); 
                }

            }); // end change(function)
        });  //  end js(function)
    </script>

     <!-- setTimeout -->
    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <script>
        function onlyNumberKey(evt) 
        {
            // Only ASCII character in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
    </script>   

</body>

</html>