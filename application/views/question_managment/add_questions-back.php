<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Add Questions </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Add Questions</h4>
                            </span>
                            
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add Questions</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('aside_question'); ?>
                        </div>
                        <div class="col-md-12">
                                    <div class="card p-5">
                                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                                        <form>
                                            <div class="panel panel-default active">
                                                <div class="panel-heading " role="tab" id="headingOne1">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" href="#collapseOne1"
                                                            aria-expanded="true" aria-controls="collapseOne1">

                                                            Question Details
                                                            <span class="float-right"><i
                                                                    class=" fa fa-arrow-down"></i></span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne1" class="panel-collapse collapse " role="tabpanel"
                                                    aria-labelledby="headingOne1">
                                                    <div class="panel-body border-0">

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Standard
                                                                        <span class="text-red">*</span></label>
                                                                    <select class="form-control" tabindex="-1"
                                                                        aria-hidden="true" required>
                                                                        <option value="0">
                                                                            --Select--
                                                                        </option>
                                                                        <option value="1">
                                                                            9Th</option>
                                                                        <option value="2">
                                                                            10Th</option>
                                                                        <option value="3">12
                                                                            th</option>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername"
                                                                        class="form-label">Subject<span
                                                                            class="text-red">*</span></label>
                                                                    <select class="form-control" tabindex="-1"
                                                                        aria-hidden="true" required>
                                                                        <option value="0">
                                                                            --Select--
                                                                        </option>
                                                                        <option value="1">
                                                                            Sub1</option>
                                                                        <option value="2">
                                                                            Sub2</option>
                                                                        <option value="3">
                                                                            Sub3 </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername"
                                                                        class="form-label">Topic</label>
                                                                    <input type="text" class="form-control" id=""
                                                                        placeholder="Enter Topic Name">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Sub
                                                                        Topic</label>
                                                                    <input type="text" class="form-control" id=""
                                                                        placeholder="Enter Sub Topic Name">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername"
                                                                        class="form-label">Difficulty
                                                                        Level <span class="text-red">*</span></label>
                                                                    <select class="form-control" tabindex="-1"
                                                                        aria-hidden="true" required>
                                                                        <option value="0">
                                                                            --Select--
                                                                        </option>
                                                                        <option value="1">
                                                                            Easy</option>
                                                                        <option value="2">
                                                                            Medium</option>
                                                                        <option value="3">
                                                                            Hard </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Language
                                                                        <span class="text-red">*</span></label>
                                                                    <select class="form-control" tabindex="-1"
                                                                        aria-hidden="true" required>
                                                                        <option value="0">
                                                                            --Select--
                                                                        </option>
                                                                        <option value="1">
                                                                            Hindi</option>
                                                                        <option value="2">
                                                                            English</option>
                                                                        <option value="3">
                                                                            Marathi
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Attempt
                                                                        Duration (minutes)
                                                                        <span class="text-red">*</span></label>
                                                                    <input type="number" class="form-control" id=""
                                                                        placeholder="Enter Attempt Duration (minutes)"
                                                                        min="0" value="0" required>
                                                                </div>
                                                            </div> -->
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Correct
                                                                        Marks
                                                                        <span class="text-red">*</span></label>
                                                                    <input type="number" step="0.01" min="0"
                                                                        class="form-control" id=""
                                                                        placeholder="Enter Correct Marks" value="0"
                                                                        required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Negative
                                                                        Marks<span class="text-red">*</span></label>
                                                                    <input type="number" step="0.01" min="0"
                                                                        class="form-control" id=""
                                                                        placeholder="Enter Negative Marks" value="0"
                                                                        required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Not
                                                                        Attempt
                                                                        Marks<span class="text-red">*</span></label>
                                                                    <input type="number" step="0.01" min="0"
                                                                        class="form-control" id=""
                                                                        placeholder="Enter Not Attempt Marks" value="0"
                                                                        required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="bannername"
                                                                        class="form-label">Questions<span
                                                                            class="text-red">*</span></label>
                                                                    <textarea id="summernote" class="summernote"
                                                                        name="example" required></textarea>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Related
                                                                        Image<span class="text-red">*</span></label>
                                                                    <input type="file" class="dropify"
                                                                        data-default-file="" data-height="150"
                                                                        required />

                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default mt-2">
                                                <div class="panel-heading " role="tab" id="headingOne1">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent=""
                                                            href="#answer" aria-expanded="true" aria-controls="answer">

                                                            Answer Details
                                                            <span class="float-right"><i
                                                                    class="fa fa-arrow-down"></i></span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="answer" class="panel-collapse collapse" role="tabpanel"
                                                    aria-labelledby="headingOne1">
                                                    <div class="panel-body border-0">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group ">
                                                                    <label for="bannername" class="form-label ">Answer
                                                                        Type
                                                                        <span class="text-red">*</span></label>
                                                                    <select class="form-control" tabindex="-1"
                                                                        aria-hidden="true" id="typeop"
                                                                        onClick="show_typewise()">
                                                                        <option value="0">
                                                                            --Select--
                                                                        </option>
                                                                        <option value="1">
                                                                            Single Choice
                                                                        </option>
                                                                        <option value="2">
                                                                            Multiple Choice
                                                                        </option>
                                                                        <option value="3">
                                                                            Integer
                                                                        </option>
                                                                        <option value="4">
                                                                            True / False
                                                                        </option>
                                                                        <option value="5">
                                                                            Match Matrix
                                                                        </option>
                                                                        <option value="6">
                                                                            Match The
                                                                            Following
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row justify-content-around" id="type1"
                                                            style="display:none">
                                                            <div class="col-md-6 form-group">
                                                                <div class=" d-flex">
                                                                    <label for="bannername"
                                                                        class="form-label pr-3">A.</label>
                                                                    <textarea id="summernote" class="summernote"
                                                                        name="example"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 form-group">
                                                                <div class=" d-flex">
                                                                    <label for="bannername"
                                                                        class="form-label pr-3">B.</label>
                                                                    <textarea id="summernote" class="summernote"
                                                                        name="example"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 form-group">
                                                                <div class=" d-flex">
                                                                    <label for="bannername"
                                                                        class="form-label pr-3">C.</label>
                                                                    <textarea id="summernote" class="summernote"
                                                                        name="example"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 form-group">
                                                                <div class=" d-flex">
                                                                    <label for="bannername"
                                                                        class="form-label pr-3">D.</label>
                                                                    <textarea id="summernote" class="summernote"
                                                                        name="example"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row justify-content-around" id="type7"
                                                            style="display:none">
                                                            <div class="col-md-12">
                                                                <div class="row justify-content-around">
                                                                    <div class="col-md-6 form-group">
                                                                        <label for="bannername "
                                                                            class="form-label text-center pr-3">Left
                                                                            Side
                                                                            Options</label>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">

                                                                        <label for="bannername"
                                                                            class="form-label text-center pr-3">Right
                                                                            Side
                                                                            Options</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row justify-content-around">
                                                                    <div class="col-md-6 form-group">
                                                                        <div class=" d-flex">
                                                                            <label for="bannername"
                                                                                class="form-label pr-3">1.</label>
                                                                            <textarea id="summernote" class="summernote"
                                                                                name="example"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <div class=" d-flex">
                                                                            <label for="bannername"
                                                                                class="form-label pr-3">A.</label>
                                                                            <textarea id="summernote" class="summernote"
                                                                                name="example"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row justify-content-around">
                                                                    <div class="col-md-6 form-group">
                                                                        <div class=" d-flex">
                                                                            <label for="bannername"
                                                                                class="form-label pr-3">2.</label>
                                                                            <textarea id="summernote" class="summernote"
                                                                                name="example"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <div class=" d-flex">
                                                                            <label for="bannername"
                                                                                class="form-label pr-3">B.</label>
                                                                            <textarea id="summernote" class="summernote"
                                                                                name="example"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row justify-content-around">
                                                                    <div class="col-md-6 form-group">
                                                                        <div class=" d-flex">
                                                                            <label for="bannername"
                                                                                class="form-label pr-3">3.</label>
                                                                            <textarea id="summernote" class="summernote"
                                                                                name="example"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <div class=" d-flex">
                                                                            <label for="bannername"
                                                                                class="form-label pr-3">C.</label>
                                                                            <textarea id="summernote" class="summernote"
                                                                                name="example"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row justify-content-around">
                                                                    <div class="col-md-6 form-group">
                                                                        <div class=" d-flex">
                                                                            <label for="bannername"
                                                                                class="form-label pr-3">4.</label>
                                                                            <textarea id="summernote" class="summernote"
                                                                                name="example"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 form-group">
                                                                        <div class=" d-flex">
                                                                            <label for="bannername"
                                                                                class="form-label pr-3">D.</label>
                                                                            <textarea id="summernote" class="summernote"
                                                                                name="example"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6" id="type2" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Correct
                                                                        Option<span class="text-red">*</span></label>
                                                                    <table class="table table-bordered text-center">
                                                                        <thead style="background: #f5f5f5;">
                                                                            <tr>
                                                                                <th data-option="1" style="">
                                                                                    A</th>
                                                                                <th data-option="2" style="">
                                                                                    B</th>
                                                                                <th data-option="3" style="">
                                                                                    C</th>
                                                                                <th data-option="4" style="">
                                                                                    D</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td data-option="1" style="">
                                                                                    <input type="radio"
                                                                                        name="sc_correct_option"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="radio"
                                                                                        name="sc_correct_option"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="radio"
                                                                                        name="sc_correct_option"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="radio"
                                                                                        name="sc_correct_option"
                                                                                        value="4">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="type3" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Correct
                                                                        Option<span class="text-red">*</span></label>
                                                                    <table class="table table-bordered text-center">
                                                                        <thead style="background: #f5f5f5;">
                                                                            <tr>
                                                                                <th data-option="1" style="">
                                                                                    A</th>
                                                                                <th data-option="2" style="">
                                                                                    B</th>
                                                                                <th data-option="3" style="">
                                                                                    C</th>
                                                                                <th data-option="4" style="">
                                                                                    D</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mc_correct_option[]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mc_correct_option[]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mc_correct_option[]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mc_correct_option[]"
                                                                                        value="1">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="type4" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Correct
                                                                        Option<span class="text-red">*</span></label>
                                                                    <table class="table table-bordered text-center">
                                                                        <thead style="background: #f5f5f5;">
                                                                            <tr>
                                                                                <th data-option="1" style="">
                                                                                    True
                                                                                </th>
                                                                                <th data-option="2" style="">
                                                                                    False
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td data-option="1" style="">
                                                                                    <input type="radio"
                                                                                        name="mc_correct_option[]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="radio"
                                                                                        name="mc_correct_option[]"
                                                                                        value="1">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="type5" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Correct
                                                                        Option<span class="text-red">*</span></label>
                                                                    <table class="table table-bordered text-center">
                                                                        <thead style="background: #f5f5f5;">
                                                                            <tr>
                                                                                <th></th>
                                                                                <th data-option="1" style="">
                                                                                    A</th>
                                                                                <th data-option="2" style="">
                                                                                    B</th>
                                                                                <th data-option="3" style="">
                                                                                    C</th>
                                                                                <th data-option="4" style="">
                                                                                    D</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>1</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>2</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>3</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>4</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="type8" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Correct
                                                                        Option<span class="text-red">*</span></label>
                                                                    <table class="table table-bordered text-center">
                                                                        <thead style="background: #f5f5f5;">
                                                                            <tr>
                                                                                <th></th>
                                                                                <th data-option="1" style="">
                                                                                    A</th>
                                                                                <th data-option="2" style="">
                                                                                    B</th>
                                                                                <th data-option="3" style="">
                                                                                    C</th>
                                                                                <th data-option="4" style="">
                                                                                    D</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>1</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>2</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>3</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>4</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="checkbox"
                                                                                        name="mm_correct_option[1][]"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="type9" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Correct
                                                                        Option<span class="text-red">*</span></label>
                                                                    <table class="table table-bordered text-center">
                                                                        <thead style="background: #f5f5f5;">
                                                                            <tr>
                                                                                <th></th>
                                                                                <th data-option="1" style="">
                                                                                    A</th>
                                                                                <th data-option="2" style="">
                                                                                    B</th>
                                                                                <th data-option="3" style="">
                                                                                    C</th>
                                                                                <th data-option="4" style="">
                                                                                    D</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>1</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option1"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option1"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option1"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option1"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>2</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option2"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option2"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option2"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option2"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>3</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option3"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option3"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option3"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option3"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                            <tr class="matchMatrixCorrectOption1"
                                                                                data-option="1" style="">
                                                                                <th>4</th>
                                                                                <td data-option="1" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option4"
                                                                                        value="1">
                                                                                </td>
                                                                                <td data-option="2" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option4"
                                                                                        value="2">
                                                                                </td>
                                                                                <td data-option="3" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option4"
                                                                                        value="3">
                                                                                </td>
                                                                                <td data-option="4" style="">
                                                                                    <input type="radio"
                                                                                        name="correct_option4"
                                                                                        value="4">
                                                                                </td>

                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="type10" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Correct Answer</label>
                                                                    <textarea  class="form-control" rows="6" oninput="this.value = this.value.replace(/\D+/g, '')"
                                                                        name="example" ></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="type6" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Solution
                                                                        (Optional)</label>
                                                                    <textarea id="summernote" class="summernote"
                                                                        name="example"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="type11" style="display:none">
                                                                <div class="form-group">
                                                                    <label for="bannername" class="form-label">Video Solution
                                                                        (Optional)</label>
                                                                        <input type="text" name="" id="" class="form-control" placeholder="Enter Solution Video link">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="">
                                                <div class="form-group text-center">
                                                    <button type="submit"
                                                        class="btn btn-primary add-ques mt-4 mb-0 text-center"><i
                                                            class="fa fa-plus"> </i> Save
                                                        Question</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div><!-- panel-group -->
                                </div>
                            </div></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({

        });
    });
    </script>
    <script>
    function show_typewise() {
        var e = document.getElementById("typeop");
        var strUser = e.options[e.selectedIndex].value;
        var div1 = document.getElementById("type1");
        var div2 = document.getElementById("type2");
        var div3 = document.getElementById("type3");
        var div4 = document.getElementById("type4");
        var div5 = document.getElementById("type5");
        var div6 = document.getElementById("type6");
        var div7 = document.getElementById("type7");
        var div8 = document.getElementById("type8");
        var div9 = document.getElementById("type9");
        var div10 = document.getElementById("type10");
        var div11 = document.getElementById("type11");
        if (strUser == 1) {
            div1.style.display = "flex";
            div2.style.display = "block";
            div3.style.display = "none";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "none";
            div8.style.display = "none";
            div9.style.display = "none";
            div10.style.display = "none";
            div11.style.display = "block";
        }

        if (strUser == 2) {
            div1.style.display = "flex";
            div2.style.display = "none";
            div3.style.display = "block";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "none";
            div8.style.display = "none";
            div9.style.display = "none";
            div10.style.display = "none";
            div11.style.display = "block";
        }
        if (strUser == 3) {
            div1.style.display = "none";
            div2.style.display = "none";
            div3.style.display = "none";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "none";
            div8.style.display = "none";
            div9.style.display = "none";
            div10.style.display = "block";
            div11.style.display = "block";
        }
        if (strUser == 4) {
            div1.style.display = "none";
            div2.style.display = "none";
            div3.style.display = "none";
            div4.style.display = "block";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "none";
            div8.style.display = "none";
            div9.style.display = "none";
            div10.style.display = "none";
            div11.style.display = "block";
        }
        if (strUser == 5) {
            div1.style.display = "none";
            div2.style.display = "none";
            div3.style.display = "none";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "block";
            div8.style.display = "block";
            div9.style.display = "none";
            div10.style.display = "none";
            div11.style.display = "block";
        }
        if (strUser == 6) {
            div1.style.display = "none";
            div2.style.display = "none";
            div3.style.display = "none";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "block";
            div8.style.display = "none";
            div9.style.display = "block";
            div10.style.display = "none";
            div11.style.display = "block";
        }
    }
    </script>
</body>

</html>