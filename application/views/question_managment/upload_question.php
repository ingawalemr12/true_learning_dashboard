<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Upload Questions </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
    <style>
    /* input[type=file] {
        display: none
    }

    .choosefile,
    .fileuploadspan {
        font-family: "Comic Sans MS"
    } */
    
    </style>
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Upload Questions</h4>
                            </span>

                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center "> <i class="breadcrumb-item-icon fa fa-home"></i> <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Upload Questions</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('aside_question'); ?>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">
                                <div class="card-header ">
                                    <div class="card-title">
                                        Upload Question
                                    </div>
                                </div>
                                <div class="card-body">
                                    <p><b>Upload Your File (.xls Only)*</b></p>
                                    <p class="font-italic text-muted mb-2">
                                        You can now upload images with the questions inside the excel sheet. Images will be extracted from question, all the options and feedback columns only.
                                    </p><br>
                                    <div class="form-group ">
                                        <p>
                                            Refer the following sample file for import Question :
                                            <!-- <a href="<?php echo base_url().'/assets/images/admin/upload_question_integer.xlsx'; ?>"  target="_blank" class="btn btn-primary add-ques mt-2 ml-2 mb-0 text-center"><i class="fa fa-download" aria-hidden="true"></i> Download Sample Files</a> -->
                                        </p>
                                        <div class="d-flex">
                                            <!-- <label>Select Answer Type</label> -->
                                            <select name="std_id" id="ans_type" class="form-control" tabindex="-1"
                                                aria-hidden="true" required="" style="width:200px">
                                                <option value="" selected="" disabled="">Select Answer Type</option>
                                                <option value="1">Single Choice</option>
                                                <option value="2">Multiple Choice</option>
                                                <option value="3">Integer</option>
                                                <option value="4">True / False</option>
                                                <option value="5">Match Matrix</option>
                                                <option value="6">Match The Following</option>
                                            </select>
                                            <a id="single_choice" href="<?php echo base_url().'/assets/images/admin/upload_question_single_choice.xlsx'; ?>"  target="_blank" class="btn btn-primary add-ques mt-2 ml-2 mb-0 text-center" style="display:none;"><i class="fa fa-download" aria-hidden="true"></i> Download Single Choice</a>

                                            <a id="multiple_choice" href="<?php echo base_url().'/assets/images/admin/upload_question_multiple_choice.xlsx'; ?>"  target="_blank" class="btn btn-primary add-ques mt-2 ml-2 mb-0 text-center" style="display:none;"><i class="fa fa-download" aria-hidden="true"></i> Download Multiple Choice</a>

                                            <a id="integer_choice" href="<?php echo base_url().'/assets/images/admin/upload_question_integer.xlsx'; ?>"  target="_blank" class="btn btn-primary add-ques mt-2 ml-2 mb-0 text-center" style="display:none;"><i class="fa fa-download" aria-hidden="true"></i> Download Integer Type</a>

                                            <a id="truefalse_choice" href="<?php echo base_url().'/assets/images/admin/upload_question_tf.xlsx'; ?>"  target="_blank" class="btn btn-primary add-ques mt-2 ml-2 mb-0 text-center" style="display:none;"><i class="fa fa-download" aria-hidden="true"></i> Download True / False Type</a>

                                            <a id="match_matrix" href="<?php echo base_url().'/assets/images/admin/upload_question_match_matrix.xlsx'; ?>"  target="_blank" class="btn btn-primary add-ques mt-2 ml-2 mb-0 text-center" style="display:none;"><i class="fa fa-download" aria-hidden="true"></i> Download Match Matrix Type</a>

                                            <a id="match_the_following" href="<?php echo base_url().'/assets/images/admin/upload_question_match_the_following.xlsx'; ?>"  target="_blank" class="btn btn-primary add-ques mt-2 ml-2 mb-0 text-center" style="display:none;"><i class="fa fa-download" aria-hidden="true"></i> Download Match the following Type</a>

                                            <!-- <a type="button" id="single_choice" onclick="DownloadFile('upload_question_single_choice_type.xlsx')"
                                                class="btn btn-primary add-ques ml-2 mb-0 text-center" style="display:none;">
                                                <i class="fa fa-download" aria-hidden="true"></i> Download
                                                Sample Files</a> 
                                            <a type="button" id="multiple_choice" onclick="DownloadFile('upload_question_multiple_choice.xlsx')"
                                                class="btn btn-primary add-ques ml-2 mb-0 text-center" style="display:none;">
                                                <i class="fa fa-download" aria-hidden="true"></i> Download
                                                Sample Files</a>
                                            <a type="button" id="integer_choice" onclick="DownloadFile('upload_question_integer.xlsx')"
                                                class="btn btn-primary add-ques ml-2 mb-0 text-center" style="display:none;">
                                                <i class="fa fa-download" aria-hidden="true"></i> Download
                                                Sample Files</a>
                                            <a type="button" id="truefalse_choice" onclick="DownloadFile('upload_question_truefalse.xlsx')"
                                                class="btn btn-primary add-ques ml-2 mb-0 text-center" style="display:none;">
                                                <i class="fa fa-download" aria-hidden="true"></i> Download
                                                Sample Files</a>
                                            <a type="button" id="match_matrix" onclick="DownloadFile('upload_question_match_matrix.xlsx')"
                                                class="btn btn-primary add-ques ml-2 mb-0 text-center" style="display:none;">
                                                <i class="fa fa-download" aria-hidden="true"></i> Download
                                                Sample Files</a>
                                                <a type="button" id="match_the_following" onclick="DownloadFile('upload_question_match_the_following.xlsx')"
                                                class="btn btn-primary add-ques ml-2 mb-0 text-center" style="display:none;">
                                                <i class="fa fa-download" aria-hidden="true"></i> Download
                                                Sample Files</a>-->
                                            <!-- <a type="submit" id="demo"
                                                class="btn btn-primary add-ques ml-2 mb-0 text-center">
                                                <i class="fa fa-download" aria-hidden="true"></i> Download
                                                Sample Files</a> -->
                                        </div>
                                    </div>
                                   
                                    <div class="fileUploadWrap">
                                        
                                        <!-- upload excel form -->
                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRSZJ5Qtnz_eutCQeLs-5tnHPFubgFz2TjnpkkMwgIIqIG93t1z" alt="">

                                        <form method="post" enctype="multipart/form-data" action="<?php //echo base_url().'upload_excel_file_driver'; ?>">
                                            <input  type="file" name="uploadFile" required>
                                            <p class="fileName">Choose File</p>
                                            <p class="pt-5">
                                                <button type="submit" name="questions_upload" class="btn btn-primary add-ques  mb-0 text-center"> <i class="fa fa-upload " aria-hidden="true"></i> Import Files</button>
                                            </p>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Tabs Containers -->

                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.18.5/xlsx.full.min.js"></script>
    <script>
    document.getElementById("demo").onclick = () => {
        // GENERATE DEMO EXCEL FILE
        var data = [
            ["Sr. No","Subject", "Difficulty Level","Language ",  "Questions", "Answer Type ","Option 1","Option 2", "Option 3", "Option 4", "Currect Option", "Solution"]
           
        ];
        // (C2) CREATE NEW EXCEL "FILE"
        var workbook = XLSX.utils.book_new(),
            worksheet = XLSX.utils.aoa_to_sheet(data);
        workbook.SheetNames.push("First");
        workbook.Sheets["First"] = worksheet;
        // (C3) TO BINARY STRING
        var xlsbin = XLSX.write(workbook, {
            bookType: "xlsx",
            type: "binary"
        });

        // (C4) TO BLOB OBJECT
        var buffer = new ArrayBuffer(xlsbin.length),
            array = new Uint8Array(buffer);
        for (var i = 0; i < xlsbin.length; i++) {
            array[i] = xlsbin.charCodeAt(i) & 0XFF;
        }
        var xlsblob = new Blob([buffer], {
            type: "application/octet-stream"
        });
        delete array;
        delete buffer;
        delete xlsbin;
        // (C5) "FORCE DOWNLOAD"
        var url = window.URL.createObjectURL(xlsblob),
            anchor = document.createElement("a");
        anchor.href = url;
        anchor.download = "demo.xlsx";
        anchor.click();
        window.URL.revokeObjectURL(url);
        delete anchor;
    };
    </script>
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({

        });
    });
    </script>
    <script>
    // $('.choosefile').click(function() {
    //     $('#fileInput').click();
    // });
    // $('#fileInput').change(function(e) {
    //     var filename = this.files[0].name;
    //     $('.fileuploadspan').text(filename);
    // });
    $(document).on('change', ".fileUploadWrap input[type='file']", function() {
        if ($(this).val()) {

            var filename = $(this).val().split("\\");

            filename = filename[filename.length - 1];

            $('.fileName').text(filename);
        }
    });
    </script>

    <script>
            $('#ans_type').on('change',function() {
                if (this.value == 0) {
                    $('#single_choice').hide();
                    $('#multiple_choice').hide();
                    $('#integer_choice').hide();
                    $('#truefalse_choice').hide();
                    $('#match_matrix').hide();
                    $('#match_the_following').hide();
                }
                if (this.value == 1) {
                    $('#single_choice').show();
                    $('#multiple_choice').hide();
                    $('#integer_choice').hide();
                    $('#truefalse_choice').hide();
                    $('#match_matrix').hide();
                    $('#match_the_following').hide();
                }
                if (this.value == 2) {
                    $('#single_choice').hide();
                    $('#multiple_choice').show();
                    $('#integer_choice').hide();
                    $('#truefalse_choice').hide();
                    $('#match_matrix').hide();
                    $('#match_the_following').hide();
                }
                if (this.value == 3) {
                    $('#single_choice').hide();
                    $('#multiple_choice').hide();
                    $('#integer_choice').show();
                    $('#truefalse_choice').hide();
                    $('#match_matrix').hide();
                    $('#match_the_following').hide();
                }
                if (this.value == 4) {
                    $('#single_choice').hide();
                    $('#multiple_choice').hide();
                    $('#integer_choice').hide();
                    $('#truefalse_choice').show();
                    $('#match_matrix').hide();
                    $('#match_the_following').hide();
                }
                if (this.value == 5) {
                    $('#single_choice').hide();
                    $('#multiple_choice').hide();
                    $('#integer_choice').hide();
                    $('#truefalse_choice').hide();
                    $('#match_matrix').show();
                    $('#match_the_following').hide();
                }
                if (this.value == 6) {
                    $('#single_choice').hide();
                    $('#multiple_choice').hide();
                    $('#integer_choice').hide();
                    $('#truefalse_choice').hide();
                    $('#match_matrix').hide();
                    $('#match_the_following').show();
                }
            });
    </script>
</body>

</html>