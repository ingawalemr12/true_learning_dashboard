<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>True Learning | Add Questions </title>
  <?php $this->load->view('css'); ?>
  <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>
<body class="app sidebar-mini light-mode default-sidebar">
  <div class="wrapper">
     <?php $this->load->view('header'); ?>
     <section class="content">
        <div class="app-content main-content">
           <div class="side-app">
              <!--Page header-->
              <div class="page-header">
                 <div class="page-leftheader">
                    <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                    <span class="d-flex">
                       <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                       <h4 class="page-title">Add Questions</h4>
                    </span>
                 </div>
                 <div class="page-rightheader ml-auto d-lg-flex d-none">
                    <ol class="breadcrumb">
                       <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                          class="d-flex align-items-center ">
                          <i class="breadcrumb-item-icon fa fa-home"></i>
                          <span class="breadcrumb-icon"> Home</span></a>
                       </li>
                       <li class="breadcrumb-item active" aria-current="page">Add Questions</li>
                    </ol>
                 </div>
              </div>
              <!--End Page header-->
              <div class="row">
                 <div class="col-md-12">
                    <?php $this->load->view('aside_question'); ?>
                 </div>
                 <div class="col-md-12">
                    <?php 
                    if (!empty($this->session->flashdata('create')) )
                    { ?>
                      <div class="col-sm-12">
                          <div class="alert alert-success" id="alert_msg">
                              <?php echo $this->session->flashdata('create');?>
                          </div>
                      </div>
                    <?php
                    }

                    if (!empty($this->session->flashdata('edit')) )
                    { ?>
                      <div class="col-sm-12">
                          <div class="alert alert-success" id="alert_msg">
                              <?php echo $this->session->flashdata('edit');?>
                          </div>
                      </div>
                    <?php
                    }

                    if (!empty($this->session->flashdata('exists')) )
                    { ?>
                      <div class="col-sm-12">
                          <div class="alert alert-warning" id="alert_msg">
                              <?php echo $this->session->flashdata('exists');?>
                          </div>
                      </div>
                    <?php
                    }
                    ?>
                    <div class="card p-5">
                        <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">

<!-- form submit -->
<form method="post" enctype="multipart/form-data" action="">
    <div class="panel panel-default active">
        <div class="panel-heading " role="tab" id="headingOne1">
           <h4 class="panel-title">
              <a role="button" data-toggle="collapse" href="#collapseOne1"
                 aria-expanded="true" aria-controls="collapseOne1">
              Question Details
              <span class="float-right"><i
                 class=" fa fa-arrow-down"></i></span>
              </a>
           </h4>
        </div>
        <!-- Add Question -->                             
        <div id="collapseOne1" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne1">
            <div class="panel-body border-0">
              <div class="row">
                 <div class="col-md-3">
                    <div class="form-group">
                       <label for="bannername" class="form-label">Standard
                       <span class="text-red">*</span></label>

                       <select name="std_id" id="std_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Standard</option>
                            <?php 
                            if (!empty($standard_list)) 
                            {
                                foreach ($standard_list as $std) 
                                { 
                                    ?>
                                    <option value="<?php echo $std['std_id'] ?>">
                                        <?php echo $std['std_name']; ?>
                                    </option>
                                    <?php   
                                }  
                            } 
                            else
                            {
                                echo "";
                            }
                            ?>
                       </select>
                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="form-group">
                       <label for="bannername" class="form-label">Subject<span class="text-red">*</span></label>

                        <select name="subject_id" id="subject_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Subject</option>

                        </select>
                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="form-group">
                       <label for="bannername" class="form-label">Topic</label>
                       <input type="text" class="form-control" name="topic" id="topic" placeholder="Enter Topic Name" >
                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="form-group">
                       <label for="bannername" class="form-label">Sub Topic</label>
                       <input type="text" class="form-control"  name="sub_topic" id="sub_topic" placeholder="Enter Sub Topic Name">
                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="form-group">
                       <label for="bannername" class="form-label">Difficulty Level 
                        <span class="text-red">*</span></label>
                        <select name="difficulty_id" id="difficulty_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Difficulty Level</option>
                            <?php 
                            if (!empty($difficulty_list)) 
                            {
                                foreach ($difficulty_list as $dif) 
                                {
                                    ?>
                                    <option value="<?php echo $dif['difficulty_id'] ?>">
                                        <?php echo $dif['difficulty_medium']; ?>
                                    </option>
                                    <?php   
                                }  
                            } 
                            else
                            {
                                echo "";
                            }
                            ?>
                        </select>
                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="form-group">
                       <label for="bannername" class="form-label">Language <span class="text-red">*</span></label>
                       <select name="lang_id" id="lang_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Language </option>
                            <?php 
                            if (!empty($language_list)) 
                            {
                                foreach ($language_list as $lag) 
                                {
                                    ?>
                                    <option value="<?php echo $lag['lang_id'] ?>">
                                        <?php echo $lag['language_name']; ?>
                                    </option>
                                    <?php   
                                }  
                            } 
                            else
                            {
                                echo "";
                            }
                            ?>
                       </select>
                    </div>
                 </div>
                 <!-- 
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Attempt Duration (minutes)
                            <span class="text-red">*</span></label>
                        <input type="number" class="form-control" id="" placeholder="Enter Attempt Duration (minutes)" min="0" value="0" required>
                    </div>
                </div> -->
                 <div class="col-md-3">
                    <div class="form-group">
                       <label for="bannername" class="form-label">Correct Marks <span class="text-red">*</span></label>
                       <input type="text" name="correct_marks" id="correct_marks" step="0.01" minlength="1" class="form-control" placeholder="Enter Correct Marks" required  onkeypress="return onlyNumberKey(event)">
                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="form-group">
                       <label for="bannername" class="form-label">Negative Marks<span class="text-red">*</span></label>
                       <input type="text" name="negative_marks" id="negative_marks" step="0.01" minlength="1" class="form-control" placeholder="Enter Negative Marks" required  onkeypress="return onlyNumberKey(event)">
                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Not Attempt Marks<span class="text-red">*</span></label>
                        <input type="text" name="not_attempt_marks" id="not_attempt_marks" step="0.01" minlength="1" class="form-control" placeholder="Enter Not Attempt Marks" required  onkeypress="return onlyNumberKey(event)">
                    </div>
                 </div>
              </div>
              <div class="row">
                 <div class="col-md-6">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Questions<span class="text-red">*</span></label>
                        <textarea required id="summernote" class="summernote" name="question_name"></textarea>
                    </div>
                 </div>
                 <div class="col-md-6">
                    <div class="form-group"> 
                       <label for="bannername" class="form-label">Related Image<span class="text-red"></span></label>
                        
                        <input type="file" name="images" class="dropify" data-default-file="" data-height="150" accept="image/*" />
                    </div>
                 </div>
              </div>
            </div>
        </div>
        <!-- end Add Question -->
    </div>

    <div class="panel panel-default mt-2">
        <div class="panel-heading " role="tab" id="headingOne1">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="" href="#answer" aria-expanded="true" aria-controls="answer"> Answer Details <span class="float-right"><i class="fa fa-arrow-down"></i></span>
                </a>
           </h4>
        </div>

        <!-- start add answer -->
        <div id="answer" class="panel-collapse collapse" role="tabpanel"
           aria-labelledby="headingOne1">
           <div class="panel-body border-0">
              <div class="row">
                 <div class="col-md-6">
                    <div class="form-group ">
                       <label for="bannername" class="form-label ">Answer Type
                       <span class="text-red">*</span></label>
                       
                       <select name="answer_type" class="form-control" tabindex="-1" aria-hidden="true" id="typeop" onClick="show_typewise()" required>
                          <option selected disabled value="">
                             - Select Answer Type -
                          </option>
                          <option value="1">Single Choice</option>
                          <option value="2">Multiple Choice</option>
                          <option value="3">Integer</option>
                          <option value="4">True / False</option>
                          <option value="5">Match Matrix</option>
                          <option value="6">Match The Following</option>
                       </select>
                    </div>
                 </div>
              </div>

               <!-- Single Choice ( options ) -->
               <div class="row justify-content-around" id="type1" style="display:none">
                  <div class="col-md-6 form-group">
                    <div class=" d-flex">
                       <label for="bannername"
                          class="form-label pr-3">A.</label>
                        <textarea  id="summernote" class="summernote" name="options_A"></textarea >
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class=" d-flex">
                       <label for="bannername"
                          class="form-label pr-3">B.</label>
                        <textarea id="summernote" class="summernote" name="options_B"></textarea>
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class=" d-flex">
                       <label for="bannername"
                          class="form-label pr-3">C.</label>
                        <textarea id="summernote" class="summernote" name="options_C"></textarea>
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class=" d-flex">
                       <label for="bannername"
                          class="form-label pr-3">D.</label>
                        <textarea id="summernote" class="summernote" name="options_D"></textarea>
                    </div>
                  </div>
               </div>

               <!-- Left Side Options, Right Side Options -->
               <div class="row justify-content-around" id="type7" style="display:none">
                  <div class="col-md-12">
                     <div class="row justify-content-around">
                       <div class="col-md-6 form-group">
                          <label for="bannername " class="form-label text-center pr-3">Left
                          Side Options</label>
                       </div>
                       <div class="col-md-6 form-group">
                          <label for="bannername" class="form-label text-center pr-3">Right
                          Side Options</label>
                       </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="row justify-content-around">
                        <div class="col-md-6 form-group">
                          <div class=" d-flex">
                             <label for="bannername"
                                class="form-label pr-3">A.</label>
                             <textarea id="summernote" class="summernote"
                                name="example"></textarea>
                          </div>
                       </div>
                       <div class="col-md-6 form-group">
                          <div class=" d-flex">
                             <label for="bannername"
                                class="form-label pr-3">1.</label>
                             <textarea id="summernote" class="summernote"
                                name="example"></textarea>
                          </div>
                       </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="row justify-content-around">
                       <div class="col-md-6 form-group">
                          <div class=" d-flex">
                             <label for="bannername"
                                class="form-label pr-3">B.</label>
                             <textarea id="summernote" class="summernote"
                                name="example"></textarea>
                          </div>
                       </div>
                       <div class="col-md-6 form-group">
                          <div class=" d-flex">
                             <label for="bannername"
                                class="form-label pr-3">2.</label>
                             <textarea id="summernote" class="summernote"
                                name="example"></textarea>
                          </div>
                       </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="row justify-content-around">
                        <div class="col-md-6 form-group">
                          <div class=" d-flex">
                             <label for="bannername"
                                class="form-label pr-3">C.</label>
                             <textarea id="summernote" class="summernote"
                                name="example"></textarea>
                          </div>
                       </div>
                       <div class="col-md-6 form-group">
                          <div class=" d-flex">
                             <label for="bannername"
                                class="form-label pr-3">3.</label>
                             <textarea id="summernote" class="summernote"
                                name="example"></textarea>
                          </div>
                       </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="row justify-content-around">
                       <div class="col-md-6 form-group">
                          <div class=" d-flex">
                             <label for="bannername"
                                class="form-label pr-3">D.</label>
                             <textarea id="summernote" class="summernote"
                                name="example"></textarea>
                          </div>
                       </div>
                       <div class="col-md-6 form-group">
                          <div class=" d-flex">
                             <label for="bannername"
                                class="form-label pr-3">4.</label>
                             <textarea id="summernote" class="summernote"
                                name="example"></textarea>
                          </div>
                       </div>
                    </div>
                 </div>
               </div>

               <div class="row">
                  <!--  Single Choice - Correct Option -->
                  <div class="col-md-6" id="type2" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>

                       <table class="table table-bordered text-center">
                          <thead style="background: #f5f5f5;">
                             <tr>
                                <th data-option="1" style=""> A </th>
                                <th data-option="2" style=""> B </th>
                                <th data-option="3" style=""> C </th>
                                <th data-option="4" style=""> D </th>
                             </tr>
                          </thead>
                          <tbody>
                             <tr>
                                <td data-option="1" style="">
                                   <input type="radio" name="sc_correct_option" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="radio" name="sc_correct_option" value="2">
                                </td>
                                <td data-option="3" style="">
                                   <input type="radio" name="sc_correct_option" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="radio" name="sc_correct_option" value="4">
                                </td>
                             </tr>
                          </tbody>
                       </table>
                     </div>
                  </div>

                  <!-- Correct Option ( multiple )-->
                  <div class="col-md-6" id="type3" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>
                       <table class="table table-bordered text-center">
                          <thead style="background: #f5f5f5;">
                             <tr>
                                <th data-option="1" style=""> A </th>
                                <th data-option="2" style=""> B </th>
                                <th data-option="3" style=""> C </th>
                                <th data-option="4" style=""> D </th>
                             </tr>
                          </thead>
                          <tbody>
                             <tr>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mc_correct_option[]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mc_correct_option[]" value="2">
                                </td>
                                <td data-option="3" style="">
                                   <input type="checkbox" name="mc_correct_option[]" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mc_correct_option[]" value="4">
                                </td>
                             </tr>
                          </tbody>
                       </table>
                    </div>
                  </div>

                   <!-- Correct Option ( True / False )-->
                  <div class="col-md-6" id="type4" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>
                       <table class="table table-bordered text-center">
                          <thead style="background: #f5f5f5;">
                             <tr>
                                <th data-option="1" style=""> True </th>
                                <th data-option="2" style=""> False </th>
                             </tr>
                          </thead>
                          <tbody>
                             <tr>
                                <td data-option="1" style="">
                                   <input type="radio" name="mc_correct_option[]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="radio" name="mc_correct_option[]" value="1">
                                </td>
                             </tr>
                          </tbody>
                       </table>
                     </div>
                  </div>

                  <!-- Correct Option ( match Matrix - checkbox )-->
                  <div class="col-md-6" id="type5" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>
                       <table class="table table-bordered text-center">
                          <thead style="background: #f5f5f5;">
                             <tr>
                                <th></th>
                                <th data-option="1" style=""> A </th>
                                <th data-option="2" style=""> B </th>
                                <th data-option="3" style=""> C </th>
                                <th data-option="4" style=""> D </th>
                             </tr>
                          </thead>
                           <tbody>
                              <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                <th>1</th>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="2">
                                </td>
                                <td data-option="3" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="4">
                                </td>
                             </tr>

                             <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                <th>2</th>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="2"> 
                                </td>
                                <td data-option="3" style="">
                                    <input type="checkbox" name="mm_correct_option[1][]" value="3"> 
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="4"> 
                                </td>
                             </tr>

                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>3</th>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="2"> 
                                </td>
                                <td data-option="3" style="">
                                    <input type="checkbox" name="mm_correct_option[1][]" value="3"> 
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="4"> 
                                </td>
                             </tr>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>4</th>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="2"> 
                                </td>
                                <td data-option="3" style="">
                                    <input type="checkbox" name="mm_correct_option[1][]" value="3"> 
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="4"> 
                                </td>
                             </tr>
                          </tbody>
                        </table>
                     </div>
                  </div>

                  <!-- Correct Option ( match Matrix - type8)-->
                  <div class="col-md-6" id="type8" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>
                       <table class="table table-bordered text-center">
                          <thead style="background: #f5f5f5;">
                             <tr>
                                <th></th>
                                <th data-option="1" style=""> A </th>
                                <th data-option="2" style=""> B </th>
                                <th data-option="3" style=""> C </th>
                                <th data-option="4" style=""> D </th>
                             </tr>
                          </thead>
                          <tbody>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>1</th>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="2">
                                </td>
                                <td data-option="3" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="4">
                                </td>
                             </tr>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>2</th>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="2">
                                </td>
                                <td data-option="3" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="4">
                                </td>
                             </tr>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>3</th>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="2">
                                </td>
                                <td data-option="3" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="4">
                                </td>
                             </tr>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>4</th>
                                <td data-option="1" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="1">
                                </td>
                                <td data-option="2" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="2">
                                </td>
                                <td data-option="3" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="checkbox" name="mm_correct_option[1][]" value="4">
                                </td>
                             </tr>
                          </tbody>
                       </table>
                     </div>
                  </div>

                  <!-- Correct Option ( match the following - radio )-->
                  <div class="col-md-6" id="type9" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Correct Option<span class="text-red">*</span></label>
                       <table class="table table-bordered text-center">
                          <thead style="background: #f5f5f5;">
                             <tr>
                                <th></th>
                                <th data-option="1" style=""> A </th>
                                <th data-option="2" style=""> B </th>
                                <th data-option="3" style=""> C </th>
                                <th data-option="4" style=""> D </th>
                             </tr>
                          </thead>
                          <tbody>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>1</th>
                                <td data-option="1" style="">
                                   <input type="radio" name="correct_option1" value="1">
                                </td>
                                <td data-option="2" style="">
                                    <input type="radio" name="correct_option1" value="2">
                                </td>
                                <td data-option="3" style="">
                                    <input type="radio" name="correct_option1" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="radio" name="correct_option1" value="4">
                                </td>
                             </tr>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>2</th>
                                <td data-option="1" style="">
                                   <input type="radio" name="correct_option2" value="1">
                                </td>
                                <td data-option="2" style="">
                                    <input type="radio" name="correct_option2" value="2">
                                </td>
                                <td data-option="3" style="">
                                    <input type="radio" name="correct_option2" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="radio" name="correct_option2" value="4">
                                </td>
                             </tr>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>3</th>
                                <td data-option="1" style="">
                                   <input type="radio" name="correct_option3" value="1">
                                </td>
                                <td data-option="2" style="">
                                    <input type="radio" name="correct_option3" value="2">
                                </td>
                                <td data-option="3" style="">
                                    <input type="radio" name="correct_option3" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="radio" name="correct_option3" value="4">
                                </td>
                             </tr>
                             <tr class="matchMatrixCorrectOption1"
                                data-option="1" style="">
                                <th>4</th>
                                <td data-option="1" style="">
                                   <input type="radio" name="correct_option4" value="1">
                                </td>
                                <td data-option="2" style="">
                                    <input type="radio" name="correct_option4" value="2">
                                </td>
                                <td data-option="3" style="">
                                    <input type="radio" name="correct_option4" value="3">
                                </td>
                                <td data-option="4" style="">
                                   <input type="radio" name="correct_option4" value="4">
                                </td>
                             </tr>
                          </tbody>
                       </table>
                    </div>
                  </div>

                  <!-- Correct Answer (integer) -->
                  <div class="col-md-6" id="type10" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Correct Answer</label>
                       
                       <textarea name="correct_answer" class="form-control" rows="6" ></textarea>
                       <!-- <textarea name="correct_answer" class="form-control" rows="6" oninput="this.value = this.value.replace(/\D+/g, '')"></textarea> -->
                     </div>
                  </div>

                  <!-- Solution (Optional) -->
                  <div class="col-md-6" id="type6" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Solution (Optional)</label>
                        
                        <textarea id="summernote" class="summernote" name="solution"></textarea>
                     </div>
                  </div>

                  <!-- Video Solution (Optional) -->
                  <div class="col-md-6" id="type11" style="display:none">
                     <div class="form-group">
                       <label for="bannername" class="form-label">Video Solution (Optional)</label>
                       
                       <input type="text" name="video_solution" id="video_solution" class="form-control" placeholder="Enter Solution Video link">
                     </div>
                  </div>

               </div> <!-- class="row" -->
            </div>
         </div>
         <!-- end answer -->
      </div>
      <div class="">
         <div class="form-group text-center">
            <button type="submit" name="add_question_answer" class="btn btn-primary add-ques mt-4 mb-0 text-center"><i class="fa fa-plus"> </i> Save Question</button>
         </div>
      </div>
</form> 
<!-- form end -->
                        </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
  </div>
  </div>
  </section>
  </div>
    
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    
    <script>
     $(document).ready(function() {
         $('.summernote').summernote({
     
         });
     });
    </script>

    <script>
        function show_typewise() 
        {
            var e = document.getElementById("typeop");
            var strUser = e.options[e.selectedIndex].value;
            var div1 = document.getElementById("type1");
            var div2 = document.getElementById("type2");
            var div3 = document.getElementById("type3");
            var div4 = document.getElementById("type4");
            var div5 = document.getElementById("type5");
            var div6 = document.getElementById("type6");
            var div7 = document.getElementById("type7");
            var div8 = document.getElementById("type8");
            var div9 = document.getElementById("type9");
            var div10 = document.getElementById("type10");
            var div11 = document.getElementById("type11");
         
                if (strUser == 1) {
                 div1.style.display = "flex";
                 div2.style.display = "block";
                 div3.style.display = "none";
                 div4.style.display = "none";
                 div5.style.display = "none";
                 div6.style.display = "block";
                 div7.style.display = "none";
                 div8.style.display = "none";
                 div9.style.display = "none";
                 div10.style.display = "none";
                 div11.style.display = "block";
                }

                if (strUser == 2) {
                 div1.style.display = "flex";
                 div2.style.display = "none";
                 div3.style.display = "block";
                 div4.style.display = "none";
                 div5.style.display = "none";
                 div6.style.display = "block";
                 div7.style.display = "none";
                 div8.style.display = "none";
                 div9.style.display = "none";
                 div10.style.display = "none";
                 div11.style.display = "block";
                }
                if (strUser == 3) {
                 div1.style.display = "none";
                 div2.style.display = "none";
                 div3.style.display = "none";
                 div4.style.display = "none";
                 div5.style.display = "none";
                 div6.style.display = "block";
                 div7.style.display = "none";
                 div8.style.display = "none";
                 div9.style.display = "none";
                 div10.style.display = "block";
                 div11.style.display = "block";
                }
                if (strUser == 4) {
                 div1.style.display = "none";
                 div2.style.display = "none";
                 div3.style.display = "none";
                 div4.style.display = "block";
                 div5.style.display = "none";
                 div6.style.display = "block";
                 div7.style.display = "none";
                 div8.style.display = "none";
                 div9.style.display = "none";
                 div10.style.display = "none";
                 div11.style.display = "block";
                }
                if (strUser == 5) {
                 div1.style.display = "none";
                 div2.style.display = "none";
                 div3.style.display = "none";
                 div4.style.display = "none";
                 div5.style.display = "none";
                 div6.style.display = "block";
                 div7.style.display = "block";
                 div8.style.display = "block";
                 div9.style.display = "none";
                 div10.style.display = "none";
                 div11.style.display = "block";
                }
                if (strUser == 6) {
                 div1.style.display = "none";
                 div2.style.display = "none";
                 div3.style.display = "none";
                 div4.style.display = "none";
                 div5.style.display = "none";
                 div6.style.display = "block";
                 div7.style.display = "block";
                 div8.style.display = "none";
                 div9.style.display = "block";
                 div10.style.display = "none";
                 div11.style.display = "block";
                }
        }
    </script>

    <!-- on change of Standard , it select Subject -->
    <script type="text/javascript">
        $("document").ready(function () 
        {
            $("#std_id").change(function () 
            {
                var std_id = $("#std_id").val();  //$(this).val();
                // alert(std_id);

                if (std_id !="") 
                {
                    $.ajax({
                        url:'<?php echo base_url().'MainController/get_subject_list' ?>',
                        method:'POST',
                        data: {std_id:std_id},
                        success:function (data) {
                            $("#subject_id").html(data);
                        }
                    }); //end ajax
                }

            }); // end change(function)
        });  //  end js(function)
    </script>

    <!-- setTimeout -->
    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <script>
        function onlyNumberKey(evt) 
        {
            // Only ASCII character in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
    </script>   

</body>
</html>