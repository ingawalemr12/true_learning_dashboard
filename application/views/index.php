<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Login</title>

    <?php $this->load->view('css'); ?>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

</head>

<body class="h-100vh page-style1 light-mode default-sidebar">
    <div class="page">

        <!-- <div classs="container p-5 ">
            <div class="row no-gutters ">
                <div class="col-lg-4 col-md-12 ml-auto">
                    <div class="alert alert-gradient shadow alert-msg " role="alert" id="success-msg">
                        <div class="row justify-content-center">
                            <div class="col-10  my-auto">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="True" style="color:#fff">&times;</span>
                                </button>
                                <h4 class="alert-heading-gradient mb-0">Login Successfully</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div classs="container p-5 ">
            <div class="row no-gutters ">
                <div class="col-lg-4 col-md-12 ml-auto">
                    <div class="alert alert-danger shadow alert-msg" role="alert" id="error-msg">
                        <div class="row justify-content-center">
                            <div class="col-10  my-auto">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="True" style="color:#fff">&times;</span>
                                </button>
                                <h4 class="alert-heading-gradient mb-0">Either Username Or Password Incorrect</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col mx-auto">
                        <div class="row justify-content-center">
                            <div class="col-md-7 col-lg-5">
                                <?php 
                                if (!empty($this->session->flashdata('login_fail')) )
                                { ?>
                                    <div class="alert alert-danger"  id="alert_msg">
                                      <?php echo $this->session->flashdata('login_fail');?>
                                    </div>
                                <?php
                                }

                                if (!empty($this->session->flashdata('log_out')) )
                                { ?>
                                    <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('log_out');?>
                                    </div>
                                <?php
                                }
                                ?>  
                                <div class="card card-group mb-0">
                                    <div class="card p-4">
                                        <div class="card-body">
                                            <div class="text-center title-style mb-6">
                                                <h1 class="mb-2">Login</h1>
                                                <hr>
                                            </div>

                                            <form method="post" action="" enctype="multipart/form-data" >
                                                <div class="input-group mb-3">
                                                    <span class="input-group-addon"><i class="svg-icon fa fa-user"></i></span>
                                                    <input type="text" name="user_name" id="user_name" placeholder="Username" class="form-control <?php echo (form_error('user_name') !="") ? 'is-invalid' : ''; ?>" autocomplete="off">

                                                    <?php echo form_error('user_name'); ?>
                                                </div>
                                                <div class="input-group mb-4">
                                                    <span class="input-group-addon">
                                                        <i class="svg-icon fa fa-lock" onclick="showPass()"></i></span>
                                                    <input type="password" name="password" id="password" placeholder="Password" class="form-control <?php echo (form_error('password') !="") ? 'is-invalid' : ''; ?>"> 
                                                    <?php echo form_error('password'); ?>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" name="login" class="btn btn-lg btn-primary btn-block submit-btn"><i class="fa fa-arrow-right"></i> Login</button>
                                                    </div>
                                                    <div class="col-12">
                                                        <a href="<?php echo base_url('forgot_password'); ?>" class="btn btn-link box-shadow-0 px-0">Forgot password?</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="text-center pt-4">
                                    <div class="font-weight-normal fs-16">You Don't have an account <a
                                            class="btn-link font-weight-normal" href="#">Register Here</a></div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery js-->
    <?php $this->load->view('js'); ?>

    <script>

    $(function() {
        setTimeout(function() { 
            $("#success-msg").fadeOut(1500); 
        }, 2000)
        setTimeout(function() { 
            $("#error-msg").fadeOut(1500); 
        }, 2000)
    })
    </script>
    
    <script type="text/javascript">
    const myTimeout = setTimeout(close, 2000);

    function close() 
    {
        document.getElementById("alert_msg").style.display = "none";
    }
    </script>

    <script>
        function showPass() 
        {
          var x = document.getElementById("password");
          
          if (x.type === "password") {
            x.type = "text";
          } else {
            x.type = "password";
          }
        }
    </script>
    
</body>


</html>