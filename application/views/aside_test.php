<aside class="">
    <div class="test-tab ">
        <div class="panel panel-primary ">
            <div class="tab-menu-heading border-0">
                <div class="tabs-menu test-mgmt-menu">
                    <!-- Tabs -->
                    <ul class="nav panel-tabs nav-pills-custom  d-flex">
                        <li class="nav-item">
                            <a class="nav-link" id="v-pills-sub-tab" href="<?php echo base_url('manage_testSeries'); ?>">
                                <i class="fa fa-list mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Test Series</span>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" id="v-pills-addSubCat-tab" href="<?php echo base_url('add_test'); ?>">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Add Test</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="v-pills-sub-tab" href="<?php echo base_url('manage_test'); ?>">
                                <i class="fa fa-list-alt mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Manage Test </span>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="v-pills-difficulty-tab" href="<?php echo base_url('test_profile_setting'); ?>">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Test Profile Setting</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " id="v-pills-board-tab" href="<?php echo base_url('evaluation'); ?>">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Evaluation</span>
                            </a>
                        </li> -->
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="v-pills-board-tab" href="<?php echo base_url('result'); ?>">
                                <i class="fa fa-plus-circle mr-2"></i>
                                <span class="font-weight-bold small text-uppercase">Result</span>
                            </a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</aside>