<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learnings | Test Management </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
    <style>
    .show {
        display: block;
    }

    .hide {
        display: none;
    }
       hr {
        margin-top: 1rem;
        margin-bottom: 1rem;
    }
    </style>
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">a
                <div class="side-app">

                    <?php 
                    $test_id = $this->uri->segment(2);

                    $where = '(test_id="'.$test_id.'")';
                    $test_info = $this->Main_Model->getData($tbl='test_list',$where);
                    // print_r($test_info);
                    ?>

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Test Managment</h4>
                            </span>
                            
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Create Section</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center add_test">
                        <div class="col-md-2">
                        <?php $this->load->view('aside_update_test'); ?>
                        </div>
                        <div class="col-md-10">
                            <?php 
                            if (!empty($this->session->flashdata('create')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('create');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('edit')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('edit');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('exists')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-warning" id="alert_msg">
                                      <?php echo $this->session->flashdata('exists');?>
                                  </div>
                              </div>
                            <?php
                            }
                            ?>
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        Create Section
                                    </div>
                                    <div class="card-options ">
                                        <a type="button" id="" class="btn btn-icon btn-primary"> &nbsp;Save Test</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!-- hide and show div -->
                                    <section class="test_section">
<!-- get Section records  -->
<?php 
if (!empty($section_list)) 
{
    ?>
    <!-- section list -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                <?php
                foreach ($section_list as $sec) 
                {
                    ?>
                    <div class="panel panel-default active">
                        
                        <!-- get section record -->
                        <div class="panel-heading " role="tab" id="headingOne1">
                            <h4 class="panel-title section-panel align-items-center">
                                <span>
                                    <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#section-1_<?php echo $sec['section_id'] ; ?>" aria-expanded="false" aria-controls="section-1_<?php echo $sec['section_id'] ; ?>" class="collapsed">
                                    <?php echo $sec['section_name'] ; ?> : (10 Marks, 2 Questions)</a>
                                </span>

                                <span class="float-right d-flex">
                                    <a role="button" data-target="#update_section_<?php echo $sec['section_id'] ; ?>" data-toggle="modal" class=" btn-sm btn-icon btn-primary mr-1"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    
                                    <a id="delete1_section_<?php echo $sec['section_id'] ; ?>" role="button" class=" btn-sm btn-icon btn-warning "> <i class="fa fa-trash"></i></a>
                                </span>
                            </h4>
                        </div>
                        <!-- ended get section record -->

                        <!-- Let’s add questions -->
                        <?php 
                        $order_by = ('imp_id desc');
                        $where = '(section_id="'.$sec['section_id'].'")';
                        $questions_per_section =$this->Main_Model->getAllData_as_per_order($tbl='import_question', $where, $order_by);
                        // print_r($questions_per_section);
                        ?>
                        <div id="section-1_<?php echo $sec['section_id']; ?>" class="panel-collapse collapse"
                            role="tabpanel" aria-labelledby="headingOne1">
                            <div class="panel-body border-0 ">
                            <?php 
                            if (empty($questions_per_section)) 
                            {
                                ?>
                                <div class="card-body text-center">
                                    <i class="section-icon fa fa-puzzle-piece"
                                        aria-hidden="true"></i>
                                    <h2> Let’s add questions</h2>
                                    <p>You can use the‘Import Questions’ panel to add questions to a section</p>

                                    <?php 
                                    /*$test_name = $test_info['test_name'];
                                    echo $test_name;$test_id;*/
                                    ?>

                                    <!-- import_question -->
                                    <form method="post"  action="<?php echo base_url().'import_question'; ?>" >
                                        <input type="hidden" name="section_id" value="<?php echo $sec['section_id'] ; ?>">
                                        <input type="hidden" name="test_id" value="<?php echo $test_id; ?>">
                                        <button type="submit" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>&nbsp; Import Question</button>
                                    </form>
                                </div>
                                <?php 
                            } else
                            {
                                ?>
                                
                                <!-- list of questions -->
                                <div class="card-body text-center border-0 p-0">
                                    <!-- question list as per section  -->
                                    <div class="table-responsive ">
                                        <table class="table table-hover border table-vcenter text-nowrap mng-qus">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="wd-15p border-bottom-0">
                                                        Sr. No.
                                                    </th>
                                                    <th class="wd-15p border-bottom-0">
                                                        Questions
                                                    </th>
                                                    <th class="wd-15p border-bottom-0">
                                                        Questions
                                                        Type</th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Standard
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Subject
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Difficulty
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Marks
                                                    </th>

                                                    <th class="wd-20p border-bottom-0">
                                                        Action
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                                <?php
                                                $i = 1;
                                                foreach ($questions_per_section as $que) 
                                                {
                                                    $wh_q = '(question_id="'.$que['question_id'].'")';
                                                    $question_info = $this->Main_Model->getData($tbl='questions',$wh_q);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i++; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $question_info['question_name']; ?>
                                                        </td>
                                                        <td>
                                                            <?php  
                                                            if ($question_info['answer_type'] == 1) 
                                                            {
                                                                echo "Single Choice";
                                                            } elseif ($question_info['answer_type'] == 2) 
                                                            {
                                                                echo "Multiple Choice";
                                                            } elseif ($question_info['answer_type'] == 3) 
                                                            {
                                                                echo "Integer";
                                                            } elseif ($question_info['answer_type'] == 4) 
                                                            {
                                                                echo "True / False";
                                                            } elseif ($question_info['answer_type'] == 5) 
                                                            {
                                                                echo "Match Matrix";
                                                            }elseif ($question_info['answer_type'] == 6) 
                                                            {
                                                                echo "Match The Following";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($standard_list)) 
                                                            {
                                                                foreach ($standard_list as $std) 
                                                                {
                                                                    if ($std['std_id'] == $question_info['std_id']) 
                                                                    {
                                                                        echo $std['std_name'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?> 
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($subject_list)) 
                                                            {
                                                                foreach ($subject_list as $sub) 
                                                                {
                                                                    if ($sub['subject_id'] == $question_info['subject_id']) 
                                                                    {
                                                                        echo $sub['subject_name'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($difficulty_list)) 
                                                            {
                                                                foreach ($difficulty_list as $dif) 
                                                                {
                                                                    if ($dif['difficulty_id'] == $question_info['difficulty_id']) 
                                                                    {
                                                                        echo $dif['difficulty_medium'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-sm btn-icon btn-success" data-target="#edit_marks_<?php echo $que['imp_id']; ?>" data-toggle="modal"><i class="fa fa-edit"></i></a>
                                                        </td>
                                                        <td>
                                                            <!-- edit question-->
                                                            <a type="button" class="btn btn-sm btn-icon btn-warning" data-target="#view_question" id="<?php echo $que['question_id']; ?>" onclick="get_records(this)"  data-toggle="modal"><i class="fa fa-eye"></i></a> 

                                                            <!-- <a class="btn btn-sm btn-icon btn-warning" data-target="#view_question" data-toggle="modal"><i class="fa fa-eye"></i></a> -->

                                                            <a id="delete_que_<?php echo $que['imp_id']; ?>" class="btn btn-sm btn-icon btn-secondary">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>

                                                    <!-- Marks in MODAL -->
                                                    <div class="modal" id="edit_marks_<?php echo $que['imp_id']; ?>">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content modal-content-demo">
                                                                <div class="modal-header">
                                                                    <h6 class="modal-title ">Edit Marks</h6>
                                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row justify-content-around">
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="bannername" class="form-label">Correct Marks
                                                                                    <span class="text-red">*</span></label>
                                                                                <input type="number" step="0.01" min="0" class="form-control" placeholder="Enter Correct Marks" value="<?php echo $question_info['correct_marks']; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="bannername" class="form-label">Negative
                                                                                    Marks<span class="text-red">*</span></label>
                                                                                <input type="number" step="0.01" min="0" class="form-control" placeholder="Enter Negative Marks" value="<?php echo $question_info['negative_marks']; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="bannername" class="form-label">Not Attempt
                                                                                    Marks<span class="text-red">*</span></label>
                                                                                <input type="number" step="0.01" min="0" class="form-control" placeholder="Enter Not Attempt Marks" value="<?php echo $question_info['not_attempt_marks']; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn save-btn">Add Section</button> 
                                                                    <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Marks in MODAL -->


                                                    <!-- delete section questions -->
                                                    <script>
                                                    document.getElementById('delete_que_<?php echo $que['imp_id']; ?>').onclick = function() 
                                                    {
                                                        var id = $("#<?php echo $que['imp_id']; ?>").val(); 
                                                   
                                                        swal({
                                                                title: "Are you sure?",
                                                                text: "You will not be able to recover this file!",
                                                                type: "warning",
                                                                showCancelButton: true,
                                                                confirmButtonColor: '#DD6B55',
                                                                confirmButtonText: 'Yes, delete it!',
                                                                cancelButtonText: "No, cancel",
                                                                closeOnConfirm: false,
                                                                closeOnCancel: false
                                                        },
                                                        function(isConfirm) {
                                                            if (isConfirm) 
                                                            {
                                                                $.ajax({
                                                                           url: '<?php echo base_url().'delete_questions_as_per_section/'.$que['imp_id']; ?>',
                                                                           type: "POST",
                                                                           data: {id:id},
                                                                           dataType:"HTML",
                                                                           success: function () {
                                                                            swal(
                                                                                    "Deleted!",
                                                                                    "Your file has been deleted!",
                                                                                    "success"
                                                                                ),
                                                                                $('.confirm').click(function()
                                                                                {
                                                                                    location.reload();
                                                                                });
                                                                            },
                                                                    });
                                                                
                                                            } else {
                                                                swal(
                                                                    "Cancelled",
                                                                    "Your  file is safe !",
                                                                    "error"
                                                                );
                                                            }
                                                        });
                                                    };
                                                    </script>
                                                    <!-- delete section questions -->

                                                    <?php 
                                                } // end foreach
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- pagination -->
                                    <div class="table-footer align-items-center mb-3">
                                        <!-- <p class="">Showing 1 to 3 of 3 entries</p> -->
                                        <nav class=""
                                            aria-label="Page navigation example">
                                            <ul class="pagination justify-content-end">
                                                <?php //echo $this->pagination->create_links(); ?>
                                            </ul>
                                        </nav>
                                    </div>
                                    <!-- pagination -->
                                </div>
                                <?php  
                            }
                            ?>
                            </div>
                        </div>

                    </div>

                    <!-- edit section -->
                    <div class="modal" id="update_section_<?php echo $sec['section_id']; ?>">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content modal-content-demo">
                                <div class="modal-header">
                                    <h6 class="modal-title ">Edit Section</h6>
                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post"  action="<?php echo base_url().'edit_section/'.$sec['section_id']; ?>" >
                                    <div class="modal-body">
                                        <div class="">

                                            <input type="hidden" name="test_id" value="<?php echo $test_id; ?>">

                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Section Name <span class="text-red">*</span></label>

                                                <input type="text" class="form-control" name="section_name" id="section_name" placeholder="Enter Section Name" required value="<?php echo $sec['section_name'] ; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Section Instruction <span class="text-red">*</span></label>
                                               
                                                <textarea id="summernote" class="summernote_<?php echo $sec['section_id'] ; ?>" name="instruction" required><?php echo $sec['instruction'] ; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn save-btn">Update Info</button> 
                                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- description script -->
                    <script>
                        $(document).ready(function() {
                            $('.summernote_<?php echo $sec['section_id'] ; ?>').summernote({

                            });
                        });

                        /*function showDiv() {
                            document.getElementById('welcomeDiv').style.display = "block";
                        }*/
                    </script>

                    <!-- delete -->
                    <script>
                    document.getElementById('delete1_section_<?php echo $sec['section_id'] ; ?>').onclick = function() 
                    {
                        var id = $("#<?php echo $sec['section_id']; ?>").val(); 
                   
                        swal({
                                title: "Are you sure?",
                                text: "You will not be able to recover this file!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, delete it!',
                                cancelButtonText: "No, cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                        },
                        function(isConfirm) {
                            if (isConfirm) 
                            {
                                $.ajax({
                                           url: '<?php echo base_url().'delete_section/'.$sec['section_id']; ?>',
                                           type: "POST",
                                           data: {id:id},
                                           dataType:"HTML",
                                           success: function () {
                                            swal(
                                                    "Deleted!",
                                                    "Your file has been deleted!",
                                                    "success"
                                                ),
                                                $('.confirm').click(function()
                                                {
                                                    location.reload();
                                                });
                                            },
                                    });
                                
                            } else {
                                swal(
                                    "Cancelled",
                                    "Your  file is safe !",
                                    "error"
                                );
                            }
                        });
                    };
                    </script>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}
else
{
?>           
    <!-- add section-->
    <div class="row justify-content-center">
        <div class="col-md-6 mt-5">
            <div class="card" id="info-section">
                <div class="card-body text-center "><i
                        class="section-icon fa fa-puzzle-piece"
                        aria-hidden="true"></i>
                    <h2> Start by adding a section</h2>
                    <p>You can add & manage sections</p>
                </div>
                <div class="card-footer text-center">
                    <a type="button" id="add_new_section" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>&nbsp;Add New Section</a>
                </div>
            </div>
            <div class="card" id="form_newSection" style="display:none;">
                <!-- add section -->
                <form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Section Name 
                                    <span class="text-red">*</span></label>

                                <input type="text" class="form-control" name="section_name" id="section_name" placeholder="Enter Section Name" required>

                                <!-- <input type="hidden" name="test_id" value="<?php echo $test_id; ?>"> -->
                            </div>

                            <div class="form-group">
                                <label for="bannername" class="form-label">Section Instruction <span class="text-red">*</span></label>

                                <textarea id="summernote" class="summernote" name="instruction" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_section_info" class="btn save-btn" >Add Section</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php 
}
?>                                        
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    

    <!-- add section in MODAL -->
    <div class="modal" id="add_section">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">Add New Section</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Section Name 
                                    <span class="text-red">*</span></label>

                                <input type="text" class="form-control" name="section_name" id="section_name" placeholder="Enter Section Name" required>

                                <!-- <input type="hidden" name="test_id" value="<?php echo $test_id; ?>"> -->
                            </div>

                            <div class="form-group">
                                <label for="bannername" class="form-label">Section Instruction <span class="text-red">*</span></label>

                                <textarea id="summernote" class="summernote" name="instruction" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_section_info" class="btn save-btn" >Add Section</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Select section in MODAL -->
    <div class="modal" id="select_section">
        <div class="modal-dialog " role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">Choose Section</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">&times;</span></button>
                </div>
                <form action="<?php echo base_url('import_question'); ?>" method="post">
                    <div class="modal-body">
                        <div class="row justify-content-around">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select name="section_id" id="section_id" class="form-control" tabindex="-1" aria-hidden="true" required="require" onchange="sec_get_id(this.value)">
                                        <option value="" selected disabled>Select Section</option>
                                        <?php 
                                        if (!empty($section_list)) 
                                        {
                                            foreach ($section_list as $sec1) 
                                            {
                                                ?>
                                                <option value="<?php echo $sec1['section_id'] ; ?>">
                                                    <?php echo $sec1['section_name'] ?>
                                                </option>
                                                <?php
                                            }
                                        }else
                                        {
                                            echo " ";
                                        }
                                        ?>  
                                    </select>
                                </div>

                                <input type="hidden" name="test_id" value="<?php echo $test_id; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn save-btn" type="submit" name="save_import_questions">Import Questions</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <input type="hidden" id="editId" value="" name="">

    <!-- section wise QUESTIONS view -->
    <div class="modal" id="view_question">
        <div class="modal-dialog model-fullwidth" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">View Question</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <div class="row">
                        <div class="col-md-3">
                          <b>Topic :</b>  <p >  &nbsp; Topic 1</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Sub Topic :</b> &nbsp; Sub Topic 1</p>
                        </div>
                       
                        <div class="col-md-3">
                            <p><b>Languagel :</b> &nbsp;English</p>
                        </div>
                    </div>
                    <!-- Questions -->
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <p><b>Questions :</b></p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis, tempore harum
                                eius, nisi molestiae culpa error quae laudantium quas cupiditate ut modi, adipisci
                                fugit maxime optio quasi. Porro, cum itaque!</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Related Image:</b></p>
                            <div class="qus_image" style="width:150px; height: 150">
                                <img src="https://testingsites.in/True_Learning_website/assets/img/course/1.jpg"
                                    alt="course" style="object-fit: cover;">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- Single Choice -->
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;Single Choice</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option A :</b> &nbsp; A</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option B :</b> &nbsp; B</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option C :</b> &nbsp;C</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option D :</b> &nbsp;D</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Correct Option :</b> &nbsp;D</p>
                        </div>
                    </div>
                    <!-- Multiple Choice -->
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;Multiple Choice</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option A :</b> &nbsp; A</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option B :</b> &nbsp; B</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option C :</b> &nbsp;C</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option D :</b> &nbsp;D</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Correct Option :</b> &nbsp;D</p>
                        </div>
                    </div>
                    <!-- Integer -->
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;Integer</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Correct Answer :</b> &nbsp; 30</p>
                        </div>
                    </div>
                    <!-- True / False -->
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;True / False</p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Correct Option :</b> &nbsp; A</p>
                        </div>
                    </div>
                    <!-- Match Matrix -->
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;Match Matrix</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Left Side Option :</b></p>
                            <p><b>Option 1 :</b> &nbsp; 1</p>
                            <p><b>Option 1 :</b> &nbsp; 1</p>
                            <p><b>Option 1 :</b> &nbsp; 1</p>
                            <p><b>Option 1 :</b> &nbsp; 1</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Right Side Option :</b></p>
                            <p><b>Option A :</b> &nbsp; 1</p>
                            <p><b>Option B :</b> &nbsp; 1</p>
                            <p><b>Option C :</b> &nbsp; 1</p>
                            <p><b>Option B :</b> &nbsp; 1</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Correct Option :</b> &nbsp; (1)-(A,B);(2)-(B,C);(3)-(A,C,D);(4)-(C,B)</p>
                        </div>
                    </div>
                    <!-- Match The Following -->
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;Match The Following</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Left Side Option :</b></p>
                            <p><b>Option 1 :</b> &nbsp; 1</p>
                            <p><b>Option 1 :</b> &nbsp; 1</p>
                            <p><b>Option 1 :</b> &nbsp; 1</p>
                            <p><b>Option 1 :</b> &nbsp; 1</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Right Side Option :</b></p>
                            <p><b>Option A :</b> &nbsp; 1</p>
                            <p><b>Option B :</b> &nbsp; 1</p>
                            <p><b>Option C :</b> &nbsp; 1</p>
                            <p><b>Option B :</b> &nbsp; 1</p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Correct Option :</b> &nbsp; 1-A, 2-D, 3-B, 4-C</p>
                        </div>
                    </div>
                    <!-- Solution -->
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <p><b>Solution (Optional):</b> </p>
                                <p> &nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing
                                elit. A quia, libero odio tenetur, sunt asperiores pariatur delectus ut impedit non
                                voluptatem quas repellendus dolor magnam enim hic accusamus? Autem, similique?</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Video Solution (Optional):</b> </p>
                            <p>
                                &nbsp;https://testingsites.in/TrueLearning/create_section/5</p>
                        </div>
                    </div>
                    <div class="panel panel-default mt-2">
                        <div id="answer" class="panel-collapse ">
                            <div class="panel-body border-0">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label for="bannername" class="form-label ">Question
                                                Type
                                                <span class="text-red">*</span></label>
                                            <select class="form-control" tabindex="-1" aria-hidden="true"
                                                id="typeop_edit" onClick="show_typewise_edit()" readonly>
                                                <option value="0">--Select--</option>
                                                <option value="1">Single Choice</option>
                                                <option value="2">Multiple Choice</option>
                                                <option value="3">Integer</option>
                                                <option value="4">True / False</option>
                                                <option value="5">Match Matrix</option>
                                                <option value="6">Match The Following</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-around" id="type_edit1" style="display:none">
                                    <div class="col-md-6 form-group">
                                        <div class=" d-flex">
                                            <label for="bannername" class="form-label pr-3">A.</label>
                                            <textarea rows="5" name="example" class="form-control" required
                                                readonly></textarea>

                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <div class=" d-flex">
                                            <label for="bannername" class="form-label pr-3">B.</label>
                                            <textarea rows="5" name="example" class="form-control" required
                                                readonly></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <div class=" d-flex">
                                            <label for="bannername" class="form-label pr-3">C.</label>
                                            <textarea rows="5" name="example" class="form-control" required
                                                readonly></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <div class=" d-flex">
                                            <label for="bannername" class="form-label pr-3">D.</label>
                                            <textarea rows="5" name="example" class="form-control" required
                                                readonly></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-around" id="type_edit7" style="display:none">
                                    <div class="col-md-12">
                                        <div class="row justify-content-around">
                                            <div class="col-md-6 form-group">
                                                <label for="bannername " class="form-label text-center pr-3">Left
                                                    Side
                                                    Options</label>
                                            </div>
                                            <div class="col-md-6 form-group">

                                                <label for="bannername" class="form-label text-center pr-3">Right
                                                    Side
                                                    Options</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row justify-content-around">
                                            <div class="col-md-6 form-group">
                                                <div class=" d-flex">
                                                    <label for="bannername" class="form-label pr-3">1.</label>
                                                    <textarea rows="5" name="example" class="form-control" required
                                                        readonly></textarea>

                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div class=" d-flex">
                                                    <label for="bannername" class="form-label pr-3">A.</label>
                                                    <textarea rows="5" name="example" class="form-control" required
                                                        readonly></textarea>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row justify-content-around">
                                            <div class="col-md-6 form-group">
                                                <div class=" d-flex">
                                                    <label for="bannername" class="form-label pr-3">2.</label>
                                                    <textarea rows="5" name="example" class="form-control" required
                                                        readonly></textarea>

                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div class=" d-flex">
                                                    <label for="bannername" class="form-label pr-3">B.</label>
                                                    <textarea rows="5" name="example" class="form-control" required
                                                        readonly></textarea>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row justify-content-around">
                                            <div class="col-md-6 form-group">
                                                <div class=" d-flex">
                                                    <label for="bannername" class="form-label pr-3">3.</label>
                                                    <textarea rows="5" name="example" class="form-control" required
                                                        readonly></textarea>

                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div class=" d-flex">
                                                    <label for="bannername" class="form-label pr-3">C.</label>
                                                    <textarea rows="5" name="example" class="form-control" required
                                                        readonly></textarea>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row justify-content-around">
                                            <div class="col-md-6 form-group">
                                                <div class=" d-flex">
                                                    <label for="bannername" class="form-label pr-3">4.</label>
                                                    <textarea rows="5" name="example" class="form-control" required
                                                        readonly></textarea>

                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div class=" d-flex">
                                                    <label for="bannername" class="form-label pr-3">D.</label>
                                                    <textarea rows="5" name="example" class="form-control" required
                                                        readonly></textarea>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" id="type_edit2" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Correct
                                                Option<span class="text-red">*</span></label>
                                            <table class="table table-hover border text-center">
                                                <thead style="background: #f5f5f5;">
                                                    <tr>
                                                        <th data-option="1" style="">A</th>
                                                        <th data-option="2" style="">B</th>
                                                        <th data-option="3" style="">C</th>
                                                        <th data-option="4" style="">D</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td data-option="1" style="">
                                                            <input type="radio" name="sc_correct_option" value="1">
                                                        </td>
                                                        <td data-option="2" style="">
                                                            <input type="radio" name="sc_correct_option" value="2">
                                                        </td>
                                                        <td data-option="3" style="">
                                                            <input type="radio" name="sc_correct_option" value="3">
                                                        </td>
                                                        <td data-option="4" style="">
                                                            <input type="radio" name="sc_correct_option" value="4">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="type_edit3" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Correct
                                                Option<span class="text-red">*</span></label>
                                            <table class="table table-bordered text-center">
                                                <thead style="background: #f5f5f5;">
                                                    <tr>
                                                        <th data-option="1" style="">A</th>
                                                        <th data-option="2" style="">B</th>
                                                        <th data-option="3" style="">C</th>
                                                        <th data-option="4" style="">D</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td data-option="1" style="">
                                                            <input type="checkbox" name="mc_correct_option[]"
                                                                value="1">
                                                        </td>
                                                        <td data-option="2" style="">
                                                            <input type="checkbox" name="mc_correct_option[]"
                                                                value="1">
                                                        </td>
                                                        <td data-option="3" style="">
                                                            <input type="checkbox" name="mc_correct_option[]"
                                                                value="1">
                                                        </td>
                                                        <td data-option="4" style="">
                                                            <input type="checkbox" name="mc_correct_option[]"
                                                                value="1">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="type_edit4" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Correct
                                                Option<span class="text-red">*</span></label>
                                            <table class="table table-bordered text-center">
                                                <thead style="background: #f5f5f5;">
                                                    <tr>
                                                        <th data-option="1" style="">True</th>
                                                        <th data-option="2" style="">False</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td data-option="1" style="">
                                                            <input type="radio" name="mc_correct_option[]"
                                                                value="1">
                                                        </td>
                                                        <td data-option="2" style="">
                                                            <input type="radio" name="mc_correct_option[]"
                                                                value="1">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="type_edit5" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Correct
                                                Option<span class="text-red">*</span></label>
                                            <table class="table table-bordered text-center">
                                                <thead style="background: #f5f5f5;">
                                                    <tr>
                                                        <th></th>
                                                        <th data-option="1" style="">A</th>
                                                        <th data-option="2" style="">B</th>
                                                        <th data-option="3" style="">C</th>
                                                        <th data-option="4" style="">D</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>1</th>
                                                        <td data-option="1" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="1"></td>
                                                        <td data-option="2" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="2"></td>
                                                        <td data-option="3" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="3"></td>
                                                        <td data-option="4" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>2</th>
                                                        <td data-option="1" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="1"></td>
                                                        <td data-option="2" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="2"></td>
                                                        <td data-option="3" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="3"></td>
                                                        <td data-option="4" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>3</th>
                                                        <td data-option="1" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="1"></td>
                                                        <td data-option="2" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="2"></td>
                                                        <td data-option="3" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="3"></td>
                                                        <td data-option="4" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>4</th>
                                                        <td data-option="1" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="1"></td>
                                                        <td data-option="2" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="2"></td>
                                                        <td data-option="3" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="3"></td>
                                                        <td data-option="4" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="4"></td>

                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="type_edit8" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Correct
                                                Option<span class="text-red">*</span></label>
                                            <table class="table table-bordered text-center">
                                                <thead style="background: #f5f5f5;">
                                                    <tr>
                                                        <th></th>
                                                        <th data-option="1" style="">A</th>
                                                        <th data-option="2" style="">B</th>
                                                        <th data-option="3" style="">C</th>
                                                        <th data-option="4" style="">D</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>1</th>
                                                        <td data-option="1" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="1"></td>
                                                        <td data-option="2" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="2"></td>
                                                        <td data-option="3" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="3"></td>
                                                        <td data-option="4" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>2</th>
                                                        <td data-option="1" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="1"></td>
                                                        <td data-option="2" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="2"></td>
                                                        <td data-option="3" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="3"></td>
                                                        <td data-option="4" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>3</th>
                                                        <td data-option="1" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="1"></td>
                                                        <td data-option="2" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="2"></td>
                                                        <td data-option="3" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="3"></td>
                                                        <td data-option="4" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>4</th>
                                                        <td data-option="1" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="1"></td>
                                                        <td data-option="2" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="2"></td>
                                                        <td data-option="3" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="3"></td>
                                                        <td data-option="4" style=""><input type="checkbox"
                                                                name="mm_correct_option[1][]" value="4"></td>

                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="type_edit9" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Correct
                                                Option<span class="text-red">*</span></label>
                                            <table class="table table-bordered text-center">
                                                <thead style="background: #f5f5f5;">
                                                    <tr>
                                                        <th></th>
                                                        <th data-option="1" style="">A</th>
                                                        <th data-option="2" style="">B</th>
                                                        <th data-option="3" style="">C</th>
                                                        <th data-option="4" style="">D</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>1</th>
                                                        <td data-option="1" style=""><input type="radio"
                                                                name="correct_option1" value="1"></td>
                                                        <td data-option="2" style=""><input type="radio"
                                                                name="correct_option1" value="2"></td>
                                                        <td data-option="3" style=""><input type="radio"
                                                                name="correct_option1" value="3"></td>
                                                        <td data-option="4" style=""><input type="radio"
                                                                name="correct_option1" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>2</th>
                                                        <td data-option="1" style=""><input type="radio"
                                                                name="correct_option2" value="1"></td>
                                                        <td data-option="2" style=""><input type="radio"
                                                                name="correct_option2" value="2"></td>
                                                        <td data-option="3" style=""><input type="radio"
                                                                name="correct_option2" value="3"></td>
                                                        <td data-option="4" style=""><input type="radio"
                                                                name="correct_option2" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>3</th>
                                                        <td data-option="1" style=""><input type="radio"
                                                                name="correct_option3" value="1"></td>
                                                        <td data-option="2" style=""><input type="radio"
                                                                name="correct_option3" value="2"></td>
                                                        <td data-option="3" style=""><input type="radio"
                                                                name="correct_option3" value="3"></td>
                                                        <td data-option="4" style=""><input type="radio"
                                                                name="correct_option3" value="4"></td>

                                                    </tr>
                                                    <tr class="matchMatrixCorrectOption1" data-option="1" style="">
                                                        <th>4</th>
                                                        <td data-option="1" style=""><input type="radio"
                                                                name="correct_option4" value="1"></td>
                                                        <td data-option="2" style=""><input type="radio"
                                                                name="correct_option4" value="2"></td>
                                                        <td data-option="3" style=""><input type="radio"
                                                                name="correct_option4" value="3"></td>
                                                        <td data-option="4" style=""><input type="radio"
                                                                name="correct_option4" value="4"></td>

                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="type_edit6" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Solution
                                                (Optional)</label>
                                            <textarea rows="5" name="example" class="form-control" required
                                                readonly></textarea>

                                        </div>
                                    </div>
                                    <div class="col-md-6" id="type_edit10" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Correct Answer</label>
                                            <textarea class="form-control" rows="6"
                                                oninput="this.value = this.value.replace(/\D+/g, '')" name="example"
                                                readonly></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="type_edit11" style="display:none">
                                        <div class="form-group">
                                            <label for="bannername" class="form-label">Video Solution
                                                (Optional)</label>
                                            <input type="text" name="" id="" class="form-control"
                                                placeholder="Enter Solution Video link" readonly>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn save-btn" type="button">Save changes</button> <button class="btn cancel-btn"
                        data-dismiss="modal" type="button">Close</button>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
   

    <script>
      function sec_get_id(idd){
        console.log(idd);
      }
    </script>
  
    <script>
    $(document).ready(
        function() {
            $("#create_section").click(function() {
                $("#invoice").show("slow");
            });

        });
    </script>
    
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({

        });
    });

    function showDiv() {
        document.getElementById('welcomeDiv').style.display = "block";
    }
    </script>
  
    <script>
    $(function() {
        $('#add_new_section').click(function() {
            $('#form_newSection').show();
            $('#info-section').hide();
        });
        $('#close_section').click(function() {
            $('#form_newSection').hide();
            $('#info-section').show();
        });
        $('#save_section').click(function() {
            $('#form_newSection').hide();
            $('#info-section').hide();
        });
    });
    </script>
    <script>
    $('.slide-toggle').on("click", function() {
        $('.slide-toggle ').toggleClass(' fa fa-angle-down fa fa-angle-up');
        $('.headerbottombar').toggleClass('hide show');
    });
    </script>
    <script>
    function show_typewise_edit() 
    {
        var e = document.getElementById("typeop_edit");
        var strUser = e.options[e.selectedIndex].value;
        var div1 = document.getElementById("type_edit1");
        var div2 = document.getElementById("type_edit2");
        var div3 = document.getElementById("type_edit3");
        var div4 = document.getElementById("type_edit4");
        var div5 = document.getElementById("type_edit5");
        var div6 = document.getElementById("type_edit6");
        var div7 = document.getElementById("type_edit7");
        var div8 = document.getElementById("type_edit8");
        var div9 = document.getElementById("type_edit9");
        var div10 = document.getElementById("type_edit10");
        var div11 = document.getElementById("type_edit11");

        if (strUser == 1) {
            div1.style.display = "flex";
            div2.style.display = "block";
            div3.style.display = "none";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "none";
            div8.style.display = "none";
            div9.style.display = "none";
            div10.style.display = "none";
            div11.style.display = "block";
        }

        if (strUser == 2) {
            div1.style.display = "flex";
            div2.style.display = "none";
            div3.style.display = "block";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "none";
            div8.style.display = "none";
            div9.style.display = "none";
            div10.style.display = "none";
            div11.style.display = "block";
        }
        if (strUser == 3) {
            div1.style.display = "none";
            div2.style.display = "none";
            div3.style.display = "none";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "none";
            div8.style.display = "none";
            div9.style.display = "none";
            div10.style.display = "block";
            div11.style.display = "block";
        }
        if (strUser == 4) {
            div1.style.display = "none";
            div2.style.display = "none";
            div3.style.display = "none";
            div4.style.display = "block";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "none";
            div8.style.display = "none";
            div9.style.display = "none";
            div10.style.display = "none";
            div11.style.display = "block";
        }
        if (strUser == 5) {
            div1.style.display = "none";
            div2.style.display = "none";
            div3.style.display = "none";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "block";
            div8.style.display = "block";
            div9.style.display = "none";
            div10.style.display = "none";
            div11.style.display = "block";
        }
        if (strUser == 6) {
            div1.style.display = "none";
            div2.style.display = "none";
            div3.style.display = "none";
            div4.style.display = "none";
            div5.style.display = "none";
            div6.style.display = "block";
            div7.style.display = "block";
            div8.style.display = "none";
            div9.style.display = "block";
            div10.style.display = "none";
            div11.style.display = "block";
        }
    }
    </script>

    <!-- setTimeout -->
    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <script type="text/javascript">
    function get_records(idd) 
    {
        // console.log(idd.id);
        document.getElementById("editId").value=idd.id
         
        var base_url = '<?php echo base_url() ?>';
        $.ajax({
                    url:base_url + "MainController/get_question_record_as_per_section",
                    type: "post", 
                    data: {
                            question_id:$("#editId").val(),
                            // uid: $("#uid").val(),
                        },
                    success: function(data) 
                    {
                        if(data==0)
                        {
                             // console.log("data no") 
                        }
                        else
                        {
                            const obj = JSON.parse(data);
                            
                            // get json response
                            console.log(obj);

                            
                            document.getElementById('topic').value = obj.topic;
                            document.getElementById('sub_topic').value = obj.sub_topic;
                            document.getElementById('lang').value = obj.lang_id;

                            document.getElementById('answer_type').value = obj.answer_type;
                            $('.question_n').summernote('code',obj.question_name );
                            document.getElementById('q_id').value  = obj.question_id;

                            var img_path=obj.images;
                            img_up(img_path);
                            function img_up(idd)
                            {
                                var drEvent = $('.dropifyy').dropify(
                                {
                                    efaultFile: idd
                                });

                                drEvent = drEvent.data('dropify');
                                drEvent.resetPreview();
                                drEvent.clearElement();
                                drEvent.settings.defaultFile = idd
                                drEvent.destroy();
                                drEvent.init();
                            }
                          
                            document.getElementById('answere_type').value = obj.answer_type;
                            
                            //answere type single choice
                            if(obj.answer_type==1)
                            {
                                showHideFun(obj.answer_type);
                            
                                //set option values
                                $('#type_1_A').summernote('code',obj.options_A );
                                $('#type_1_B').summernote('code',obj.options_B );
                                $('#type_1_C').summernote('code',obj.options_C );
                                $('#type_1_D').summernote('code',obj.options_D );
                            
                                //set correct answere
                                var input_count=$("#single_choice table tbody td input").length;
                                
                               for(var i=1; i<=input_count;i++){
                                    var opt_c=document.getElementById('type_1_cur_opt_'+i).value;
                                  
                                    if(obj.correct_answer==opt_c){
                                      document.getElementById('type_1_cur_opt_'+i).checked = true;
                                    }
                                 
                                }
                                //set soln
                                $('#type_1_soln').summernote('code',obj.solution );
                                
                                //set vid soln
                                document.getElementById('type_1_vid_soln').value = obj.video_solution;
                            }
                          
                            if(obj.answer_type==2)
                            {
                           
                                showHideFun(obj.answer_type);
                            
                                //set option values
                                $('#type_2_A').summernote('code',obj.options_A );
                                $('#type_2_B').summernote('code',obj.options_B );
                                $('#type_2_C').summernote('code',obj.options_C );
                                $('#type_2_D').summernote('code',obj.options_D );

                                //set correct answere
                                var input_count=$("#multiple_choice table tbody td input").length;
                                var index_of=0;
                                //obj.options.length;
                           
                                console.log("arr_len=",obj.options.length);
                                for(var i=1; i<=input_count;i++)
                                {
                                    if(index_of < obj.options.length){
                                          var currect_ans=obj.options[index_of];
                                       }
                                  
                                    console.log("currect_ans=",currect_ans.correct_answer)
                                    var opt_c=document.getElementById('type_2_cur_opt_'+i).value;
                                    console.log("table head opt",opt_c)
                                    if(currect_ans.correct_answer==opt_c)
                                    {
                                      document.getElementById('type_2_cur_opt_'+i).checked = true;
                                    }                                                          
                                    index_of++;
                                }
                                //set soln
                                $('#type_2_soln').summernote('code',obj.solution );
                                
                                //set vid soln
                                document.getElementById('type_2_vid_soln').value = obj.video_solution;
                            }
                          
                            if(obj.answer_type==3)
                            {
                                showHideFun(obj.answer_type);

                                document.getElementById('type_3_cur_ans').innerHTML  = obj.correct_answer;
                                
                                //set soln
                                $('#type_3_soln').summernote('code',obj.solution );

                                //set vid soln
                                document.getElementById('type_3_vid_soln').value = obj.video_solution;
                            }
                          
                            if(obj.answer_type==4)
                            {
                                showHideFun(obj.answer_type);
                             
                                //set correct answere
                                var input_count=$("#true_false table tbody td input").length;
                                for(var i=1; i<=input_count;i++){
                                    var opt_c=document.getElementById('type_4_cur_opt_'+i).value;
                                     if(obj.correct_answer==opt_c){
                                        document.getElementById('type_4_cur_opt_'+i).checked = true;
                                    }
                             
                                }
                                //set soln
                                $('#type_4_soln').summernote('code',obj.solution);
                                 
                                //set vid soln
                                document.getElementById('type_4_vid_soln').value = obj.video_solution;
                            }
                          
                            if(obj.answer_type==5)
                            {                           
                                showHideFun(obj.answer_type);
                                //set option values
                                $('#type_5_lft_opt_1').summernote('code',obj.left_opt_1);
                                $('#type_5_lft_opt_2').summernote('code',obj.left_opt_2);
                                $('#type_5_lft_opt_3').summernote('code',obj.left_opt_3);
                                $('#type_5_lft_opt_4').summernote('code',obj.left_opt_4);
                                $('#type_5_right_opt_A').summernote('code',obj.right_opt_A);
                                $('#type_5_right_opt_B').summernote('code',obj.right_opt_B);
                                $('#type_5_right_opt_C').summernote('code',obj.right_opt_C);
                                $('#type_5_right_opt_D').summernote('code',obj.right_opt_D);
                                $('#type_5_soln').summernote('code',obj.solution);
                             
                             
                                var thead=$("#match_matrix table thead th").length;
                                var tr=$("#match_matrix tbody tr").length;
                                // console.log("option get =", obj.options[0]);
                                console.log("tr=", tr);
                                var index_of=0;

                                for(var i=1; i<=tr; i++)
                                {
                                    for(var j=1;j<thead;j++)
                                    {
                                        var get_in_op=obj.options[index_of];
                                        console.log("option get =", get_in_op);
                                        var head_opt = $('#match_matrix thead th:nth-child('+(j+1)+')');
                                        var thead_opt=head_opt .text();
                                        console.log(thead_opt);
                                      
                                        if(get_in_op.correct_options_l==i && get_in_op.correct_options_r==thead_opt)
                                        {                                   
                                            var opt_c=document.getElementById('type_5_cur_opt_'+i+'_'+j).checked = true;
                                            //var input=$('#match_matrix tbody tr['+i+'] td input').length;
                                            //console.log("options", opt_c);
                                        }
                                
                                    }
                                    index_of++;
                                    console.log("\n");
                                }
                                
                                //set vid soln
                                document.getElementById('type_5_vid_soln').value = obj.video_solution;
                            }

                            if(obj.answer_type==6)
                            {
                                showHideFun(obj.answer_type);
                                //set option values
                                $('#type_6_lft_opt_1').summernote('code',obj.left_opt_1);
                                $('#type_6_lft_opt_2').summernote('code',obj.left_opt_2);
                                $('#type_6_lft_opt_3').summernote('code',obj.left_opt_3);
                                $('#type_6_lft_opt_4').summernote('code',obj.left_opt_4);
                                $('#type_6_right_opt_A').summernote('code',obj.right_opt_A);
                                $('#type_6_right_opt_B').summernote('code',obj.right_opt_B);
                                $('#type_6_right_opt_C').summernote('code',obj.right_opt_C);
                                $('#type_6_right_opt_D').summernote('code',obj.right_opt_D);
                                $('#type_6_soln').summernote('code',obj.solution); 
                                //set vid soln
                                document.getElementById('type_6_vid_soln').value = obj.video_solution;
                             
                                var thead=$("#match_the_following table thead th").length;
                                var tr=$("#match_the_following tbody tr").length;
                                // console.log("option get =", obj.options[0]);
                                console.log("tr=", tr);
                                var index_of=0;

                                for(var i=1; i<=tr; i++)
                                {
                                    for(var j=1;j<thead;j++)
                                    {
                                        var get_in_op=obj.options[index_of];
                                        // console.log("option get =", get_in_op);
                                        var head_opt = $('#match_the_following thead th:nth-child('+(j+1)+')');
                                        var thead_opt=head_opt .text();
                                        console.log("opt ids=",get_in_op.opt_id);
                                        document.getElementById('type_6_opt_'+i).value = get_in_op.opt_id;
                                        
                                        if(get_in_op.correct_options_l==i && get_in_op.correct_options_r==thead_opt)
                                        {                                   
                                            var opt_c=document.getElementById('type_6_cur_opt_'+i+'_'+j).checked = true;
                                            //var input=$('#match_matrix tbody tr['+i+'] td input').length;
                                            //console.log("options", opt_c);
                                        }
                                    }
                                    index_of++;
                                    console.log("\n");
                                }
                            }
                        } //else
                    }
                });
            
    }
    </script>
</body>

</html>