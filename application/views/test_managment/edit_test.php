<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learnings | Test Management </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
    <style>
    .show {
        display: block;
    }

    .hide {
        display: none;
    }
    </style>
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <?php 
                    $test_id = $this->uri->segment(2);

                    $where = '(test_id="'.$test_id.'")';
                    $test_info = $this->Main_Model->getData($tbl='test_list',$where);
                    // print_r($test_info);
                    // print_r($category_list);
                    ?>

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Test Managment</h4>
                            </span>
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center "><i class="breadcrumb-item-icon fa fa-home"></i> <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Edit Test</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-2">
                            <?php $this->load->view('aside_update_test'); ?>
                        </div>
                        <div class="col-md-10">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        Edit Test Info
                                    </div>
                                </div>
    <form method="post"  action="<?php echo base_url().'update_test_info/'.$test_id; ?>" enctype="multipart/form-data">                               
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Category Name
                            <span class="text-red">*</span></label>
                        <select  name="category_id" id="category_id_<?php echo $test_info['test_id']; ?>" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Category </option>
                            <?php
                            if (!empty($category_list)) 
                            {
                                foreach ($category_list as $cat_list) 
                                { 
                                    // echo  $test_info['category_id']." ".$cat_list['category_id'] ; 
                                    if ($cat_list['category_id'] == $test_info['category_id'] ) 
                                    { 
                                        ?>
                                        <option selected="selected" value="<?php echo $cat_list['category_id'] ?>"><?php echo $cat_list['category_name'] ?>
                                        </option>
                                        <?php   
                                    } 
                                    else 
                                    { 
                                        ?>
                                        <option value="<?php echo $cat_list['category_id'] ?>"><?php echo $cat_list['category_name'] ?>
                                        </option>
                                        <?php
                                    }
                                }  
                            } 
                            else
                            {
                                echo " ";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php 
                        $wh_cat = '(category_id = "'.$test_info['category_id'].'")';
                        $test_series_list_1= $this->Main_Model->getAllData_not_order($tbl='test_series',$wh_cat);
                        ?>
                        
                        <label for="bannername" class="form-label">Test Series
                            <span class="text-red">*</span></label>
                        <select class="form-control" name="test_series_id" id="test_series_id_<?php echo $test_info['test_id']; ?>" tabindex="-1" aria-hidden="true" required>
                            <?php
                            if (!empty($test_series_list_1)) 
                            {
                               foreach ($test_series_list_1 as $t_s_li_1) 
                               { 
                                  if ($t_s_li_1['test_series_id'] == $test_info['test_series_id'] ) 
                                  { ?>
                                      <option selected="selected" value="<?php echo $t_s_li_1['test_series_id'] ?>">
                                        <?php echo $t_s_li_1['test_series_name']; ?>
                                      </option>
                                  <?php   
                                  } else { ?>
                                      <option value="<?php echo $t_s_li_1['test_series_id'] ?>">
                                        <?php echo $t_s_li_1['test_series_name']; ?>
                                      </option>
                                      <?php
                                  }
                               }  
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Test Name <span class="text-red">*</span></label>
                        <input type="text" class="form-control" name="test_name" id="test_name" placeholder="Enter Test Name" required value="<?php echo $test_info['test_name']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Select Standard
                            <span class="text-red">*</span></label>
                        <select name="std_id" id="std_id_<?php echo $test_info['test_id']; ?>" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Standard </option>
                            <?php
                            if (!empty($standard_list)) 
                            {
                                foreach ($standard_list as $std) 
                                { 
                                    // echo  $test_info['category_id']." ".$cat_list['category_id'] ; 
                                    if ($std['std_id'] == $test_info['std_id'] ) 
                                    { 
                                        ?>
                                        <option selected="selected" value="<?php echo $std['std_id'] ?>"><?php echo $std['std_name'] ?>
                                        </option>
                                        <?php   
                                    } 
                                    else 
                                    { 
                                        ?>
                                        <option value="<?php echo $std['std_id'] ?>"><?php echo $std['std_name'] ?>
                                        </option>
                                        <?php
                                    }
                                }  
                            } 
                            else
                            {
                                echo " ";
                            }
                            ?>
                        </select>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php 
                        $wh_sub = '(subject_id = "'.$test_info['subject_id'].'")';
                        $sub_list_1= $this->Main_Model->getAllData_not_order($tbl='subjects',$wh_sub);
                        ?>
                        
                        <label for="bannername" class="form-label">Select Subject</label>
                        <select  name="subject_id" id="subject_id_<?php echo $test_info['test_id']; ?>" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <?php
                            if (!empty($sub_list_1)) 
                            {
                               foreach ($sub_list_1 as $sub_1) 
                               { 
                                  if ($sub_1['subject_id'] == $test_info['subject_id'] ) 
                                  { ?>
                                      <option selected="selected" value="<?php echo $sub_1['subject_id'] ?>">
                                        <?php echo $sub_1['subject_name']; ?>
                                      </option>
                                  <?php   
                                  } else { ?>
                                      <option value="<?php echo $sub_1['subject_id'] ?>">
                                        <?php echo $sub_1['subject_name']; ?>
                                      </option>
                                      <?php
                                  }
                               }  
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Topic</label>
                        <input type="text" class="form-control" name="topic" id="topic" placeholder="Enter Topic Name" value="<?php echo $test_info['topic']; ?>" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Sub Topic</label>
                        <input type="text" class="form-control"  name="sub_topic" id="sub_topic" placeholder="Enter Sub Topic Name" value="<?php echo $test_info['sub_topic']; ?>" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Difficulty Level <span class="text-red">*</span></label>
                        
                        <select name="difficulty_id" id="difficulty_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Difficulty Level</option>
                            <?php 
                            if (!empty($difficulty_list)) 
                            {
                                foreach ($difficulty_list as $dif) 
                                {
                                    if ($dif['difficulty_id'] == $test_info['difficulty_id'] ) 
                                    { 
                                        ?>
                                        <option selected="selected" value="<?php echo $dif['difficulty_id'] ?>">
                                            <?php echo $dif['difficulty_medium']; ?>
                                        </option>
                                        <?php   
                                    } 
                                    else 
                                    { 
                                        ?>
                                        <option value="<?php echo $dif['difficulty_id'] ?>">
                                            <?php echo $dif['difficulty_medium']; ?>
                                        </option>
                                        <?php
                                    }
                                }  
                            } 
                            else
                            {
                                echo "";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Language
                            <span class="text-red">*</span></label>
                            <select name="lang_id" id="lang_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                                <option value="" selected disabled>Select Language </option>
                                <?php 
                                if (!empty($language_list)) 
                                {
                                    foreach ($language_list as $lag) 
                                    {
                                        if ($lag['lang_id'] == $test_info['lang_id'] ) 
                                        { 
                                            ?>
                                            <option selected="selected" value="<?php echo $lag['lang_id'] ?>">
                                                <?php echo $lag['language_name']; ?>
                                            </option>
                                            <?php   
                                        } 
                                        else 
                                        { 
                                            ?>
                                            <option value="<?php echo $lag['lang_id'] ?>">
                                                <?php echo $lag['language_name']; ?>
                                            </option>
                                            <?php
                                        }
                                    }  
                                } 
                                else
                                {
                                    echo "";
                                }
                                ?>
                           </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Test Series Type 
                            <span class="text-red">*</span></label>
                        <select name="test_series_type" class="form-control" tabindex="-1" aria-hidden="true" required> 
                           <option value="" selected disable>Select Test Series Type</option>
                            <?php
                            if ($test_info['test_series_type'] == 1) 
                            { 
                                ?>
                                <option value="1" selected>Free</option>
                                <option value="2">Paid</option>
                                <?php
                            } 

                            if ($test_info['test_series_type'] == 2) 
                            { 
                                ?>
                                <option value="1">Free</option>
                                <option value="2" selected>Paid</option>
                                <?php
                            }   
                            ?> 
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Total Questions <span class="text-red">*</span></label>

                        <input type="text" class="form-control" name="t_questions" id="t_questions" placeholder="Enter Numbers of Questions" required value="<?php echo $test_info['t_questions']; ?>" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Total Test Marks<span class="text-red">*</span></label>
                        
                        <input type="text" class="form-control" name="t_marks" id="t_marks"  placeholder="Enter Total Test Marks" required value="<?php echo $test_info['t_marks']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Attempt Duration (minutes) <span class="text-red">*</span></label>
                        
                        <input type="text" minlength="1" class="form-control" name="t_duration" id="t_duration" placeholder="Enter Attempt Duration" required  onkeypress="return onlyNumberKey(event)" value="<?php echo $test_info['t_duration']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Correct Marks
                            <span class="text-red">*</span></label>

                        <input type="text" name="correct_marks" id="correct_marks" minlength="1" class="form-control" placeholder="Enter Correct Marks" required  onkeypress="return onlyNumberKey(event)" value="<?php echo $test_info['correct_marks']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Negative Marks<span class="text-red">*</span></label>
                        
                        <input type="text" name="negative_marks" id="negative_marks" minlength="1" class="form-control" placeholder="Enter Negative Marks" required  onkeypress="return onlyNumberKey(event)" value="<?php echo $test_info['negative_marks']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Not Attempt Marks<span class="text-red">*</span></label>
                        
                        <input type="text" name="not_attempt_marks" id="not_attempt_marks" minlength="1" class="form-control" placeholder="Enter Not Attempt Marks" required  onkeypress="return onlyNumberKey(event)" value="<?php echo $test_info['not_attempt_marks']; ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Test Instructions<span class="text-red">*</span></label>
                        
                        <textarea id="summernote" class="summernote" name="t_instructions" required><?php echo $test_info['t_instructions']; ?></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="bannername" class="form-label">Test Image<span class="text-red">*</span></label>
                        <div class="control-group file-upload" id="file-upload1">
                          <div class="image-box text-center">
                              <img src="<?php echo $test_info['test_image'];?>" alt="img">
                          </div>
                          <div class="controls" style="display: none;">
                              <input type="file" name="test_image" id="test_image" accept="image/*" />
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <!-- <a href="<?php echo base_url('create_section'); ?>" type="button" class="btn  btn-primary float-right">Save & Next</a> -->
            <button type="submit" class="btn  btn-primary float-right">Update & Next</button>
        </div>
    </form>
                            </div>
                        </div>
                    </div>

                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>

    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>


    <script>
    $(document).ready(function() {
        $('.summernote').summernote({

        });
    });
    </script>
    <script>
    // $("a[href$='#demo']").click(function() {

    //     let attr = $("#demo").attr("class");
    //     if (attr == "collapse show") {
    //         $("i").removeClass("fa fa-chevron-up");
    //         $("i").addClass("fa fa-chevron-down");
    //     } else {
    //         $("i").removeClass("fa fa-chevron-down");
    //         $("i").addClass("fa fa-chevron-up");
    //     }


    // })
    $('.slide-toggle').on("click", function() {
        $('.slide-toggle i').toggleClass(' fa fa-angle-down fa fa-angle-up');
        $('.headerbottombar').toggleClass('hide show');
    });
    </script>

    <script type="text/javascript">
    $("document").ready(function () 
    {
        // on change of std_id , it select subject 
        $("#std_id_<?php echo $test_info['test_id']; ?>").change(function () 
        {
            var std_id = $("#std_id_<?php echo $test_info['test_id']; ?>").val();  //$(this).val();
            // console.log(std_id);

            if (std_id !="") 
            {
                $.ajax({
                    url:'<?php echo base_url().'MainController/get_subject_list_for_edit' ?>',
                    method:'POST',
                    data: {std_id:std_id},
                    success:function (data) {
                        $("#subject_id_<?php echo $test_info['test_id']; ?>").html(data);
                    }
                }); //end ajax
            }

        }); // end change(function)


        // on change of category, it select test_series_name 
        $("#category_id_<?php echo $test_info['test_id']; ?>").change(function () 
        {
            var category_id = $("#category_id_<?php echo $test_info['test_id']; ?>").val();  //$(this).val();
            // console.log(category_id);

            if (category_id !="") 
            {
                $.ajax({
                    url:'<?php echo base_url().'MainController/get_test_series_list_for_edit' ?>',
                    method:'POST',
                    data: {category_id:category_id},
                    success:function (data) {
                        $("#test_series_id_<?php echo $test_info['test_id']; ?>").html(data);
                    }
                }); //end ajax
            }

        }); // end change(function)

    });  //  end js(function)
    </script> 

</body>

</html>