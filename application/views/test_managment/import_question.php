<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Add Board </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <section class="content">
            <div class="app-content main-content ">
                <div class="side-app">
                    
                    <?php 

                    // echo $section_id ;echo $test_id ;
                    $where = '(section_id="'.$section_id.'")';
                    $section_id_info = $this->Main_Model->getData($tbl='section',$where);
                    // print_r($section_id_info); 
                    ?>
                    <input type="hidden" id="section_id" name="section_id" value="<?php echo $section_id;?>" >
                    
                    <input type="hidden" id="test_id" name="test_id" value="<?php echo $test_id;?>" >

                    <div class="container-fluid p-0">
                        <div class="import-header ">
                            <h4 class="page-title float-left"><i onclick="goBack()" class="fa fa-arrow-left" aria-hidden="true"> </i>Import Questions</h4>
                            <div class="page-rightheader ml-auto d-flex align-items-center  float-right">
                                <p class="m-0 mr-4">
                                    <small class="counter"></small> Question Selected  
                                </p>
                               
                                <p class="mb-0">
                                    <a type='submit' id="Submit" name="submit" disabled class="btn btn-icon btn-primary import-btn">Import Questions</a>
                                </p>
                            </div>
                        </div>

                    </div>
                   
                    <div class="row justify-content-center mt-4">
                        <div class="card ">

                            <div class="card-header">
                                <div class="card-title">Section Name : 
                                    <?php echo $section_id_info['section_name']; ?>
                                 </div>
                            </div>
                            <div class="card-body">

                                <!-- filter -->
                                <div class="row ">
                                    <div class="col-md-2 col-6">
                                        <div class="input-group mt-2">
                                            <select name="std_id" id="std_id" class="form-control table-search-box" required>
                                                <option value="" selected disabled>Select Standard</option>
                                                <?php 
                                                if (!empty($standard_list)) 
                                                {
                                                    foreach ($standard_list as $std) 
                                                    { 
                                                        ?>
                                                        <option value="<?php echo $std['std_id'] ?>">
                                                            <?php echo $std['std_name']; ?>
                                                        </option>
                                                        <?php   
                                                    }  
                                                } 
                                                else
                                                {
                                                    echo "";
                                                }
                                                ?>
                                           </select>

                                        </div>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <div class="input-group mt-2">
                                           <select name="subject_id" id="subject_id" class="form-control table-search-box" required>
                                                <option value="" selected disabled>Select Subject</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <div class="input-group mt-2">
                                            <select name="topic" id="topic" class="form-control table-search-box" >
                                                <option value="" selected disabled>Select Topic </option>
                                                <?php 
                                                if (!empty($questions_list)) 
                                                {
                                                    foreach ($questions_list as $que) 
                                                    {
                                                        ?>
                                                        <option value="<?php echo $que['topic'] ?>">
                                                            <?php echo $que['topic']; ?>
                                                        </option>
                                                        <?php   
                                                    }  
                                                } 
                                                else
                                                {
                                                    echo "";
                                                }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <div class="input-group mt-2">
                                            <select name="sub_topic" id="sub_topic" class="form-control table-search-box" >
                                                <option value="" selected disabled>Select Sub-Topic </option>
                                                <?php 
                                                if (!empty($questions_list)) 
                                                {
                                                    foreach ($questions_list as $que) 
                                                    {
                                                        ?>
                                                        <option value="<?php echo $que['sub_topic'] ?>">
                                                            <?php echo $que['sub_topic']; ?>
                                                        </option>
                                                        <?php   
                                                    }  
                                                } 
                                                else
                                                {
                                                    echo "";
                                                }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <div class="input-group mt-2">
                                            <select name="answer_type" class="form-control table-search-box" >
                                                <option selected disabled value="">Select Question Type</option>
                                                <option value="1">Single Choice</option>
                                                <option value="2">Multiple Choice</option>
                                                <option value="3">Integer</option>
                                                <option value="4">True / False</option>
                                                <option value="5">Match Matrix</option>
                                                <option value="6">Match The Following</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <div class="input-group mt-2">
                                            <select name="difficulty_id" id="difficulty_id" class="form-control table-search-box" tabindex="-1" aria-hidden="true" >
                                                <option value="" selected disabled>Select Difficulty Level</option>
                                                <?php 
                                                if (!empty($difficulty_list)) 
                                                {
                                                    foreach ($difficulty_list as $dif) 
                                                    {
                                                        ?>
                                                        <option value="<?php echo $dif['difficulty_id'] ?>">
                                                            <?php echo $dif['difficulty_medium']; ?>
                                                        </option>
                                                        <?php   
                                                    }  
                                                } 
                                                else
                                                {
                                                    echo "";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <div class="input-group mt-2">
                                            <select name="lang_id" id="lang_id" class="form-control table-search-box" tabindex="-1" aria-hidden="true" required>
                                                <option value="" selected disabled>Select Language </option>
                                                <?php 
                                                if (!empty($language_list)) 
                                                {
                                                    foreach ($language_list as $lag) 
                                                    {
                                                        ?>
                                                        <option value="<?php echo $lag['lang_id'] ?>">
                                                            <?php echo $lag['language_name']; ?>
                                                        </option>
                                                        <?php   
                                                    }  
                                                } 
                                                else
                                                {
                                                    echo "";
                                                }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <div class="input-group mt-2">
                                            <select class="form-control table-search-box" required="">
                                                <option value="0" disable="">Exclude Test Questions
                                                </option>
                                                <option value="1">Test 1</option>
                                                <option value="2">Test 2</option>
                                                <option value="3">Test 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-12">
                                        <div class="input-group mt-2">

                                            <button class="btn btn-primary" type="button"><i class="fa fa-search"
                                                    aria-hidden="true"></i></button>

                                        </div>
                                    </div>
                                </div>

                                <div class="float-right mb-3">
                                    <input type="text" class="form-control" name="" id="myInput" onkeyup="search_table()" placeholder="Search">
                                </div>

<!-- get QUESTIONS records -->
<div class="table-responsive ">
    <table class="table table-hover border table-vcenter text-nowrap mng-qus questions" id="myTable">
        <thead class="text-center">
            <tr>
                <th class="wd-15p border-bottom-0">
                    Sr. No. 
                    <input id="select-all" type="checkbox" name="" class="check" value="">

                    <!-- <input  name="question_id" id="question_id" type="checkbox" value="<?php echo $que['question_id']; ?>"> -->
                </th>
                <th class="wd-15p border-bottom-0">Questions Id</th>
                <th class="wd-15p border-bottom-0">Questions</th>
                <th class="wd-15p border-bottom-0">Questions Type</th>
                <th class="wd-20p border-bottom-0">Standard</th>
                <th class="wd-20p border-bottom-0">Subject</th>
                <th class="wd-20p border-bottom-0">Difficulty</th>
            </tr>
        </thead>
        <tbody class="text-center">
            <?php 
            // print_r($questions_list);
            if (!empty($questions_list)) 
            {
                $i= 1+$this->uri->segment(2);
                // $i= 1;
                foreach ($questions_list as $que) 
                {
                    ?>
                    <tr>
                        <td><?php echo $i++; ?>
                            <input type="checkbox" name="question_id[]" id="question_id" class="check" value="<?php echo $que['question_id']; ?>" required>
                        </td>
                        <td><?php echo $que['question_id']; ?></td>
                        <td><?php echo $que['question_name']; ?></td>
                        <td>
                            <?php  
                            if ($que['answer_type'] == 1) 
                            {
                                echo "Single Choice";
                            } elseif ($que['answer_type'] == 2) 
                            {
                                echo "Multiple Choice";
                            } elseif ($que['answer_type'] == 3) 
                            {
                                echo "Integer";
                            } elseif ($que['answer_type'] == 4) 
                            {
                                echo "True / False";
                            } elseif ($que['answer_type'] == 5) 
                            {
                                echo "Match Matrix";
                            }elseif ($que['answer_type'] == 6) 
                            {
                                echo "Match The Following";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($standard_list)) 
                            {
                                foreach ($standard_list as $std) 
                                {
                                    if ($std['std_id'] == $que['std_id']) 
                                    {
                                        echo $std['std_name'];
                                    }
                                }
                            }else
                            {
                                echo " ";
                            }
                            ?> 
                        </td>
                        <td>
                            <?php
                            if (!empty($subject_list)) 
                            {
                                foreach ($subject_list as $sub) 
                                {
                                    if ($sub['subject_id'] == $que['subject_id']) 
                                    {
                                        echo $sub['subject_name'];
                                    }
                                }
                            }else
                            {
                                echo " ";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            //echo $sub['std_id']; 
                            if (!empty($difficulty_list)) 
                            {
                                foreach ($difficulty_list as $dif) 
                                {
                                    if ($dif['difficulty_id'] == $que['difficulty_id']) 
                                    {
                                        echo $dif['difficulty_medium'];
                                    }
                                }
                            }else
                            {
                                echo " ";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            else
            {
                ?>
                 <p id="founder" class="text-danger">No matching records found</p>
                <?php
            }
            ?> 
        </tbody>
    </table>
</div>
<!-- end QUESTIONS list -->

                                <div class="table-footer align-items-center mb-3">
                                    <!-- <p class="">Showing 1 to 3 of 3 entries</p> -->
                                    <nav class="" aria-label="Page navigation example">
                                        <ul class="pagination justify-content-end">
                                            <?php echo $this->pagination->create_links(); ?>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    <script>
    function goBack() {
        window.history.back();
    }
    </script>

    <!-- on change of Standard , it select Subject -->
    <script type="text/javascript">
        $("document").ready(function () 
        {
            $("#std_id").change(function () 
            {
                var std_id = $("#std_id").val();  //$(this).val();
                // alert(std_id);

                if (std_id !="") 
                {
                    $.ajax({
                        url:'<?php echo base_url().'MainController/get_subject_list' ?>',
                        method:'POST',
                        data: {std_id:std_id},
                        success:function (data) {
                            $("#subject_id").html(data);
                        }
                    }); //end ajax
                }

            }); // end change(function)
        });  //  end js(function)
    </script>

    <script>
    $(function() {
        $('p .counter').text(' ');

        var generallen = $("#example1 input[name='questions']:checked").length;
        if (generallen > 0) {
            $("p .counter").text('(' + generallen + ')');
        } else {
            $("p .counter").text(' ');
        }
        // submitbutton();
    })

    function updateCounter() {
        var len = $("#example1 input[name='questions']:checked").length;
        if (len > 0) {
            $("p .counter").text(len);
        } else {
            $("p .counter").text(' ');
        }
    }

    $("#example1 input:checkbox").on("change", function() {
        updateCounter();
    });

    $(function() {
        var selectAllItems = "#select-all";
        var checkboxItem = ":checkbox";
        $(selectAllItems).change(function() 
        {
            if (this.checked) {
                $(checkboxItem).each(function() 
                {
                    this.checked = true;
                });
            } 
            else 
            {
                $(checkboxItem).each(function() 
                {
                    this.checked = false;
                });
            }
            updateCounter();
            // submitbutton();
        });
    })
    </script>

    <script>
    var url = this.href;
    document.getElementById('Submit').onclick = function() 
    {
      console.log($("#section_id").val());
      var Q_id_arry = []
      var checkboxes = document.querySelectorAll('.questions tbody input[type=checkbox]:checked')

      for (var i = 0; i < checkboxes.length; i++) {
        Q_id_arry.push(checkboxes[i].value)
      }
      var arr_l=checkboxes.length;
      //console.log("arr_l",arr_l);
     // console.log("Q_id_arry",Q_id_arry);
      
        swal({
                title: ''+arr_l+' Questions Imported',
                text: "Successfully imported questions on SectionName ",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Submit and Go Back ',
                cancelButtonText: "Import More Question",
                closeOnConfirm: false,
                //closeOnCancel: false
            },
            function(isConfirm) 
            {
                if (isConfirm) 
                {
                    $.ajax({
                                url: '<?php echo base_url().'add_imported_questions'; ?>',
                                type: "POST",
                                data: { 
                                        test_id: $("#test_id").val(),
                                        section_id: $("#section_id").val(),
                                        question_id: Q_id_arry,
                                    },
                                dataType:"HTML",
                                success: function () {
                                    swal(
                                            "Questions details has been added!",
                                            "success"
                                        ),
                                        $('.confirm').click(function()
                                        {
                                           location.reload();
                                           // window.location.href = '<?php echo base_url().'create_section/'; ?>'+test_id
                                        });
                                },
                        });
                    // window.location = "create_section";
                }
            });
    };
    </script>

</body>
</html>