<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learnings | Test Management </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Test Managment</h4>
                            </span>
                            
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add Test</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <!-- <div class="card">
                            <div class="card-body">
                                <div class="row "> -->
                                    <div class="col-md-12">
                                        <?php $this->load->view('aside_test'); ?>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row add_test">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            Create New Test
                                                        </div>
                                                    </div>
<?php
$series_name = $this->uri->segment(2); 
$series_id = $this->uri->segment(3); 
?>
<form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <?php 
                $wh_t = '(test_series_id = "'.$series_id.'" )';
                $test = $this->Main_Model->getData($tbl='test_series', $wh_t);
                // print_r($test);

                if ($series_id) 
                {
                    ?>
                    <div class="form-group">
                        <label for="bannername" class="form-label">Category Name
                            <span class="text-red">*</span></label>
                        <select  name="category_id" id="category_id" class="form-control"  tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Category Name</option>
                            <?php 
                            if (!empty($category_list)) 
                            {
                                foreach ($category_list as $cat_list) 
                                {
                                    if ($cat_list['category_id'] == $test['category_id']) 
                                    {
                                        ?>
                                        <option selected="selected" value="<?php echo $cat_list['category_id']; ?>">
                                            <?php echo $cat_list['category_name']; ?>
                                        </option>
                                        <?php   
                                    } 
                                    else
                                    {
                                        ?>
                                        <option value="<?php echo $cat_list['category_id'];?>"><?php echo $cat_list['category_name']; ?></option>
                                        <?php   
                                    }
                                }  
                            } 
                            else
                            {
                                echo " ";
                            }
                            ?>
                            
                        </select>
                    </div>
                    <?php
                } 
                else
                {
                    ?>
                    <div class="form-group">
                        <label for="bannername" class="form-label">Category Name
                            <span class="text-red">*</span></label>
                        <select  name="category_id" id="category_id" class="form-control"  tabindex="-1" aria-hidden="true" required>
                            <option value="" selected disabled>Select Category Name</option>
                            <?php 
                            if (!empty($category_list)) 
                            {
                                foreach ($category_list as $cat_list) 
                                { 
                                    ?>
                                    <option value="<?php echo $cat_list['category_id'] ?>">
                                        <?php echo $cat_list['category_name'] ?>
                                    </option>
                                    <?php   
                                }  
                            } 
                            else
                            {
                                echo " ";
                            }
                            ?>
                        </select>
                    </div>
                    <?php
                }
                ?>
                
            </div>
            <div class="col-md-3">
                <?php 
                // print_r($test);
                if ($series_id) 
                {
                    ?>
                    <div class="form-group">
                        <label for="bannername" class="form-label">Test Series
                            <span class="text-red">*</span></label>
                        <select name="test_series_id" id="test_series_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option selected="selected" value="<?php echo $test['test_series_id']; ?>">
                                <?php echo $test['test_series_name'];?>
                            </option>
                        </select>
                    </div>
                    <?php
                } 
                else
                {
                    ?>
                    <div class="form-group">
                        <label for="bannername" class="form-label">Test Series
                            <span class="text-red">*</span></label>
                        <select name="test_series_id" id="test_series_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                            <option  value="" selected disabled>Select Test Series</option>
                            
                        </select>
                    </div>
                    <?php
                }
                ?>
                
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Test Name 
                        <span class="text-red">*</span></label>
                    <input type="text" class="form-control" name="test_name" id="test_name" placeholder="Enter Test Name" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername"
                        class="form-label">Standard <span class="text-red">*</span></label>
                    <select name="std_id" id="std_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                        <option value="" selected disabled>Select Standard</option>
                        <?php 
                        if (!empty($standard_list)) 
                        {
                            foreach ($standard_list as $std) 
                            { 
                                ?>
                                <option value="<?php echo $std['std_id'] ?>">
                                    <?php echo $std['std_name']; ?>
                                </option>
                                <?php   
                            }  
                        } 
                        else
                        {
                            echo "";
                        }
                        ?>
                   </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Select Subject</label>
                    <select name="subject_id" id="subject_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                        <option value="" selected disabled>Select Subject</option>

                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Topic</label>
                    <input type="text" class="form-control" name="topic" id="topic" placeholder="Enter Topic Name" >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Sub Topic</label>
                    <input type="text" class="form-control"  name="sub_topic" id="sub_topic" placeholder="Enter Sub Topic Name">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Difficulty Level 
                        <span class="text-red">*</span></label>
                    <select name="difficulty_id" id="difficulty_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                        <option value="" selected disabled>Select Difficulty Level</option>
                        <?php 
                        if (!empty($difficulty_list)) 
                        {
                            foreach ($difficulty_list as $dif) 
                            {
                                ?>
                                <option value="<?php echo $dif['difficulty_id'] ?>">
                                    <?php echo $dif['difficulty_medium']; ?>
                                </option>
                                <?php   
                            }  
                        } 
                        else
                        {
                            echo "";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Language 
                        <span class="text-red">*</span></label>
                    <select name="lang_id" id="lang_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                        <option value="" selected disabled>Select Language </option>
                        <?php 
                        if (!empty($language_list)) 
                        {
                            foreach ($language_list as $lag) 
                            {
                                ?>
                                <option value="<?php echo $lag['lang_id'] ?>">
                                    <?php echo $lag['language_name']; ?>
                                </option>
                                <?php   
                            }  
                        } 
                        else
                        {
                            echo "";
                        }
                        ?>
                   </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Test Series Type 
                        <span class="text-red">*</span></label>
                    <select name="test_series_type" class="form-control" tabindex="-1" aria-hidden="true" required>
                        <option value="" selected disable>Select Series Type </option>
                        <option value="1">Free</option>
                        <option value="2">Paid</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Total Questions 
                        <span class="text-red">*</span></label>

                    <input type="text" class="form-control" name="t_questions" id="t_questions" placeholder="Enter Numbers of Questions" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Total Test Marks
                        <span class="text-red">*</span></label>

                    <input type="text" class="form-control" name="t_marks" id="t_marks"  placeholder="Enter Total Test Marks" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername"
                        class="form-label">Attempt Duration (minutes) 
                        <span class="text-red">*</span></label>

                    <input type="text" minlength="1" class="form-control" name="t_duration" id="t_duration" placeholder="Enter Attempt Duration" required  onkeypress="return onlyNumberKey(event)">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername"
                        class="form-label">Correct Marks 
                        <span class="text-red">*</span></label>

                    <input type="text" name="correct_marks" id="correct_marks" minlength="1" class="form-control" placeholder="Enter Correct Marks" required  onkeypress="return onlyNumberKey(event)">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername"
                        class="form-label">Negative Marks<span class="text-red">*</span></label>

                    <input type="text" name="negative_marks" id="negative_marks" minlength="1" class="form-control" placeholder="Enter Negative Marks" required  onkeypress="return onlyNumberKey(event)">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="bannername" class="form-label">Not Attempt Marks
                        <span class="text-red">*</span></label>

                    <input type="text" name="not_attempt_marks" id="not_attempt_marks" minlength="1" class="form-control" placeholder="Enter Not Attempt Marks" required  onkeypress="return onlyNumberKey(event)">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="bannername" class="form-label">Test Instructions
                        <span class="text-red">*</span></label>

                    <textarea id="summernote" class="summernote" name="t_instructions" required></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="bannername" class="form-label">Test Image
                        <span class="text-red">*</span></label>

                    <input type="file" name="test_image" id="test_image" class="dropify" data-default-file="" data-height="150" required accept="image/*" required />
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" name="add_test_info" class="btn  btn-primary float-right"> Save & Next</button>
        <!-- <a href="<?php echo base_url('create_section'); ?>" type="button" class="btn  btn-primary float-right" >Save & Next</a> -->
    </div>
</form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- </div>
                        </div>
                    </div> -->
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>

    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>


    <script>
    $(document).ready(function() {
        $('.summernote').summernote({

        });
    });
    </script>

     <!-- on change of Category Name , it select Test Series -->
    <script type="text/javascript">
        $("document").ready(function () 
        {
            $("#category_id").change(function () 
            {
                var category_id = $("#category_id").val();  //$(this).val();
                // alert(category_id);

                if (category_id !="") 
                {
                    $.ajax({
                        url:'<?php echo base_url().'MainController/get_test_series_list' ?>',
                        method:'POST',
                        data: {category_id:category_id},
                        success:function (data) {
                            $("#test_series_id").html(data);
                        }
                    }); //end ajax
                }

            }); // end change(function)

            // on change of Standard , it select Subject
            $("#std_id").change(function () 
            {
                var std_id = $("#std_id").val();  //$(this).val();
                // alert(std_id);

                if (std_id !="") 
                {
                    $.ajax({
                        url:'<?php echo base_url().'MainController/get_subject_list' ?>',
                        method:'POST',
                        data: {std_id:std_id},
                        success:function (data) {
                            $("#subject_id").html(data);
                        }
                    }); //end ajax
                }

            }); // end change(function)

        });  //  end js(function)
    </script>

    <!-- setTimeout -->
    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <script>
        function onlyNumberKey(evt) 
        {
            // Only ASCII character in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
    </script>   

</body>

</html>