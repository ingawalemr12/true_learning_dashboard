<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learnings | Test Management </title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
    <style>
    .show {
        display: block;
    }

    .hide {
        display: none;
    }
       hr {
        margin-top: 1rem;
        margin-bottom: 1rem;
    }
    </style>
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <?php 
                    $test_id = $this->uri->segment(2);

                    $where = '(test_id="'.$test_id.'")';
                    $test_info = $this->Main_Model->getData($tbl='test_list',$where);
                    // print_r($test_info);
                    ?>

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                            <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i class="fa fa-angle-left"></i></a>
                            <h4 class="page-title">Test Managment</h4>
                            </span>
                            
                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Create Section</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center add_test">
                        <div class="col-md-2">
                        <?php $this->load->view('aside_update_test'); ?>
                        </div>
                        <div class="col-md-10">
                            <?php 
                            if (!empty($this->session->flashdata('create')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('create');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('edit')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('edit');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('exists')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-warning" id="alert_msg">
                                      <?php echo $this->session->flashdata('exists');?>
                                  </div>
                              </div>
                            <?php
                            }
                            ?>
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        Create Section
                                    </div>
                                    <div class="card-options ">
                                        <a type="button" id="" class="btn btn-icon btn-primary"> &nbsp;Save Test</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!-- hide and show div -->
                                    <section class="test_section">
<!-- get Section records  -->
<?php 
if (!empty($section_list)) 
{
    ?>
    <!-- section list -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                <?php
                foreach ($section_list as $sec) 
                {
                    ?>
                    <div class="panel panel-default active">
                        
                        <!-- get section record -->
                        <div class="panel-heading " role="tab" id="headingOne1">
                            <h4 class="panel-title section-panel align-items-center">
                                <span>
                                    <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#section-1_<?php echo $sec['section_id'] ; ?>" aria-expanded="false" aria-controls="section-1_<?php echo $sec['section_id'] ; ?>" class="collapsed">
                                    <?php echo $sec['section_name'] ; ?> : (10 Marks, 2 Questions)</a>
                                </span>

                                <span class="float-right d-flex">
                                    <a role="button" data-target="#update_section_<?php echo $sec['section_id'] ; ?>" data-toggle="modal" class=" btn-sm btn-icon btn-primary mr-1"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    
                                    <a id="delete1_section_<?php echo $sec['section_id'] ; ?>" role="button" class=" btn-sm btn-icon btn-warning "> <i class="fa fa-trash"></i></a>
                                </span>
                            </h4>
                        </div>
                        <!-- ended get section record -->

                        <!-- Let’s add questions -->
                        <?php 
                        $order_by = ('imp_id desc');
                        $where = '(section_id="'.$sec['section_id'].'")';
                        $questions_per_section =$this->Main_Model->getAllData_as_per_order($tbl='import_question', $where, $order_by);
                        // print_r($questions_per_section);
                        ?>
                        <div id="section-1_<?php echo $sec['section_id']; ?>" class="panel-collapse collapse"
                            role="tabpanel" aria-labelledby="headingOne1">
                            <div class="panel-body border-0 ">
                            <?php 
                            if (empty($questions_per_section)) 
                            {
                                ?>
                                <div class="card-body text-center">
                                    <i class="section-icon fa fa-puzzle-piece"
                                        aria-hidden="true"></i>
                                    <h2> Let’s add questions</h2>
                                    <p>You can use the‘Import Questions’ panel to add questions to a section</p>

                                    <?php 
                                    /*$test_name = $test_info['test_name'];
                                    echo $test_name;$test_id;*/
                                    ?>

                                    <!-- import_question -->
                                    <form method="post"  action="<?php echo base_url().'import_question'; ?>" >
                                        <input type="hidden" name="section_id" value="<?php echo $sec['section_id'] ; ?>">
                                        <input type="hidden" name="test_id" value="<?php echo $test_id; ?>">
                                        <button type="submit" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>&nbsp; Import Question</button>
                                    </form>
                                </div>
                                <?php 
                            } else
                            {
                                ?>
                                
                                <!-- list of questions -->
                                <div class="card-body text-center border-0 p-0">
                                    <!-- question list as per section  -->
                                    <div class="table-responsive ">
                                        <table class="table table-hover border table-vcenter text-nowrap mng-qus">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="wd-15p border-bottom-0">
                                                        Sr. No.
                                                    </th>
                                                    <th class="wd-15p border-bottom-0">
                                                        Questions
                                                    </th>
                                                    <th class="wd-15p border-bottom-0">
                                                        Questions
                                                        Type</th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Standard
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Subject
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Difficulty
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Marks
                                                    </th>

                                                    <th class="wd-20p border-bottom-0">
                                                        Action
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                                <?php
                                                $i = 1;
                                                foreach ($questions_per_section as $que) 
                                                {
                                                    $wh_q = '(question_id="'.$que['question_id'].'")';
                                                    $question_info = $this->Main_Model->getData($tbl='questions',$wh_q);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i++; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $question_info['question_name']; ?>
                                                        </td>
                                                        <td>
                                                            <?php  
                                                            if ($question_info['answer_type'] == 1) 
                                                            {
                                                                echo "Single Choice";
                                                            } elseif ($question_info['answer_type'] == 2) 
                                                            {
                                                                echo "Multiple Choice";
                                                            } elseif ($question_info['answer_type'] == 3) 
                                                            {
                                                                echo "Integer";
                                                            } elseif ($question_info['answer_type'] == 4) 
                                                            {
                                                                echo "True / False";
                                                            } elseif ($question_info['answer_type'] == 5) 
                                                            {
                                                                echo "Match Matrix";
                                                            }elseif ($question_info['answer_type'] == 6) 
                                                            {
                                                                echo "Match The Following";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($standard_list)) 
                                                            {
                                                                foreach ($standard_list as $std) 
                                                                {
                                                                    if ($std['std_id'] == $question_info['std_id']) 
                                                                    {
                                                                        echo $std['std_name'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?> 
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($subject_list)) 
                                                            {
                                                                foreach ($subject_list as $sub) 
                                                                {
                                                                    if ($sub['subject_id'] == $question_info['subject_id']) 
                                                                    {
                                                                        echo $sub['subject_name'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            //echo $sub['std_id']; 
                                                            if (!empty($difficulty_list)) 
                                                            {
                                                                foreach ($difficulty_list as $dif) 
                                                                {
                                                                    if ($dif['difficulty_id'] == $question_info['difficulty_id']) 
                                                                    {
                                                                        echo $dif['difficulty_medium'];
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                echo " ";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-sm btn-icon btn-success" data-target="#edit_marks_<?php echo $que['imp_id']; ?>" data-toggle="modal"><i class="fa fa-edit"></i></a>
                                                        </td>
                                                        <td>
                                                            <!-- edit question-->
                                                            <a type="button" class="btn btn-sm btn-icon btn-warning" data-target="#view_question" id="<?php echo $que['question_id']; ?>" onclick="get_records(this)"  data-toggle="modal"><i class="fa fa-eye"></i></a> 

                                                            <!-- <a class="btn btn-sm btn-icon btn-warning" data-target="#view_question" data-toggle="modal"><i class="fa fa-eye"></i></a> -->

                                                            <a id="delete_que_<?php echo $que['imp_id']; ?>" class="btn btn-sm btn-icon btn-secondary">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>

                                                    <!-- Marks in MODAL -->
                                                    <div class="modal" id="edit_marks_<?php echo $que['imp_id']; ?>">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content modal-content-demo">
                                                                <div class="modal-header">
                                                                    <h6 class="modal-title ">Edit Marks</h6>
                                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row justify-content-around">
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="bannername" class="form-label">Correct Marks
                                                                                    <span class="text-red">*</span></label>
                                                                                <input type="number" step="0.01" min="0" class="form-control" placeholder="Enter Correct Marks" value="<?php echo $question_info['correct_marks']; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="bannername" class="form-label">Negative
                                                                                    Marks<span class="text-red">*</span></label>
                                                                                <input type="number" step="0.01" min="0" class="form-control" placeholder="Enter Negative Marks" value="<?php echo $question_info['negative_marks']; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="bannername" class="form-label">Not Attempt
                                                                                    Marks<span class="text-red">*</span></label>
                                                                                <input type="number" step="0.01" min="0" class="form-control" placeholder="Enter Not Attempt Marks" value="<?php echo $question_info['not_attempt_marks']; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn save-btn">Add Section</button> 
                                                                    <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Marks in MODAL -->


                                                    <!-- delete section questions -->
                                                    <script>
                                                    document.getElementById('delete_que_<?php echo $que['imp_id']; ?>').onclick = function() 
                                                    {
                                                        var id = $("#<?php echo $que['imp_id']; ?>").val(); 
                                                   
                                                        swal({
                                                                title: "Are you sure?",
                                                                text: "You will not be able to recover this file!",
                                                                type: "warning",
                                                                showCancelButton: true,
                                                                confirmButtonColor: '#DD6B55',
                                                                confirmButtonText: 'Yes, delete it!',
                                                                cancelButtonText: "No, cancel",
                                                                closeOnConfirm: false,
                                                                closeOnCancel: false
                                                        },
                                                        function(isConfirm) {
                                                            if (isConfirm) 
                                                            {
                                                                $.ajax({
                                                                           url: '<?php echo base_url().'delete_questions_as_per_section/'.$que['imp_id']; ?>',
                                                                           type: "POST",
                                                                           data: {id:id},
                                                                           dataType:"HTML",
                                                                           success: function () {
                                                                            swal(
                                                                                    "Deleted!",
                                                                                    "Your file has been deleted!",
                                                                                    "success"
                                                                                ),
                                                                                $('.confirm').click(function()
                                                                                {
                                                                                    location.reload();
                                                                                });
                                                                            },
                                                                    });
                                                                
                                                            } else {
                                                                swal(
                                                                    "Cancelled",
                                                                    "Your  file is safe !",
                                                                    "error"
                                                                );
                                                            }
                                                        });
                                                    };
                                                    </script>
                                                    <!-- delete section questions -->

                                                    <?php 
                                                } // end foreach
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- pagination -->
                                    <div class="table-footer align-items-center mb-3">
                                        <!-- <p class="">Showing 1 to 3 of 3 entries</p> -->
                                        <nav class=""
                                            aria-label="Page navigation example">
                                            <ul class="pagination justify-content-end">
                                                <?php //echo $this->pagination->create_links(); ?>
                                            </ul>
                                        </nav>
                                    </div>
                                    <!-- pagination -->
                                </div>
                                <?php  
                            }
                            ?>
                            </div>
                        </div>

                    </div>

                    <!-- edit section -->
                    <div class="modal" id="update_section_<?php echo $sec['section_id']; ?>">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content modal-content-demo">
                                <div class="modal-header">
                                    <h6 class="modal-title ">Edit Section</h6>
                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post"  action="<?php echo base_url().'edit_section/'.$sec['section_id']; ?>" >
                                    <div class="modal-body">
                                        <div class="">

                                            <input type="hidden" name="test_id" value="<?php echo $test_id; ?>">

                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Section Name <span class="text-red">*</span></label>

                                                <input type="text" class="form-control" name="section_name" id="section_name" placeholder="Enter Section Name" required value="<?php echo $sec['section_name'] ; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="bannername" class="form-label">Section Instruction <span class="text-red">*</span></label>
                                               
                                                <textarea id="summernote" class="summernote_<?php echo $sec['section_id'] ; ?>" name="instruction" required><?php echo $sec['instruction'] ; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn save-btn">Update Info</button> 
                                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- description script -->
                    <script>
                        $(document).ready(function() {
                            $('.summernote_<?php echo $sec['section_id'] ; ?>').summernote({

                            });
                        });

                        /*function showDiv() {
                            document.getElementById('welcomeDiv').style.display = "block";
                        }*/
                    </script>

                    <!-- delete -->
                    <script>
                    document.getElementById('delete1_section_<?php echo $sec['section_id'] ; ?>').onclick = function() 
                    {
                        var id = $("#<?php echo $sec['section_id']; ?>").val(); 
                   
                        swal({
                                title: "Are you sure?",
                                text: "You will not be able to recover this file!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, delete it!',
                                cancelButtonText: "No, cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                        },
                        function(isConfirm) {
                            if (isConfirm) 
                            {
                                $.ajax({
                                           url: '<?php echo base_url().'delete_section/'.$sec['section_id']; ?>',
                                           type: "POST",
                                           data: {id:id},
                                           dataType:"HTML",
                                           success: function () {
                                            swal(
                                                    "Deleted!",
                                                    "Your file has been deleted!",
                                                    "success"
                                                ),
                                                $('.confirm').click(function()
                                                {
                                                    location.reload();
                                                });
                                            },
                                    });
                                
                            } else {
                                swal(
                                    "Cancelled",
                                    "Your  file is safe !",
                                    "error"
                                );
                            }
                        });
                    };
                    </script>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}
else
{
?>           
    <!-- add section-->
    <div class="row justify-content-center">
        <div class="col-md-6 mt-5">
            <div class="card" id="info-section">
                <div class="card-body text-center "><i
                        class="section-icon fa fa-puzzle-piece"
                        aria-hidden="true"></i>
                    <h2> Start by adding a section</h2>
                    <p>You can add & manage sections</p>
                </div>
                <div class="card-footer text-center">
                    <a type="button" id="add_new_section" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>&nbsp;Add New Section</a>
                </div>
            </div>
            <div class="card" id="form_newSection" style="display:none;">
                <!-- add section -->
                <form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Section Name 
                                    <span class="text-red">*</span></label>

                                <input type="text" class="form-control" name="section_name" id="section_name" placeholder="Enter Section Name" required>

                                <!-- <input type="hidden" name="test_id" value="<?php echo $test_id; ?>"> -->
                            </div>

                            <div class="form-group">
                                <label for="bannername" class="form-label">Section Instruction <span class="text-red">*</span></label>

                                <textarea id="summernote" class="summernote" name="instruction" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_section_info" class="btn save-btn" >Add Section</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php 
}
?>                                        
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    

    <!-- add section in MODAL -->
    <div class="modal" id="add_section">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">Add New Section</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="">
                            <div class="form-group">
                                <label for="bannername" class="form-label">Section Name 
                                    <span class="text-red">*</span></label>

                                <input type="text" class="form-control" name="section_name" id="section_name" placeholder="Enter Section Name" required>

                                <!-- <input type="hidden" name="test_id" value="<?php echo $test_id; ?>"> -->
                            </div>

                            <div class="form-group">
                                <label for="bannername" class="form-label">Section Instruction <span class="text-red">*</span></label>

                                <textarea id="summernote" class="summernote" name="instruction" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_section_info" class="btn save-btn" >Add Section</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Select section in MODAL -->
    <div class="modal" id="select_section">
        <div class="modal-dialog " role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">Choose Section</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">&times;</span></button>
                </div>
                <form action="<?php echo base_url('import_question'); ?>" method="post">
                    <div class="modal-body">
                        <div class="row justify-content-around">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select name="section_id" id="section_id" class="form-control" tabindex="-1" aria-hidden="true" required="require" onchange="sec_get_id(this.value)">
                                        <option value="" selected disabled>Select Section</option>
                                        <?php 
                                        if (!empty($section_list)) 
                                        {
                                            foreach ($section_list as $sec1) 
                                            {
                                                ?>
                                                <option value="<?php echo $sec1['section_id'] ; ?>">
                                                    <?php echo $sec1['section_name'] ?>
                                                </option>
                                                <?php
                                            }
                                        }else
                                        {
                                            echo " ";
                                        }
                                        ?>  
                                    </select>
                                </div>

                                <input type="hidden" name="test_id" value="<?php echo $test_id; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn save-btn" type="submit" name="save_import_questions">Import Questions</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <input type="hidden" id="editId" value="" name="">

    <!-- section wise QUESTIONS view -->
    <div class="modal" id="view_question">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">View Question</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-2">
                              <div class="d-flex"> <b>Topic : </b> &nbsp; <span id="topic_name"></span></div>
                            </div>
                            <div class="mb-2">
                              <div class="d-flex"><b>Sub Topic : </b> &nbsp; <span id="sub_topic_name"></span></div>
                            </div>

                            <div class="mb-2">
                              <div class="d-flex"><b>Languagel : </b> &nbsp; <span id="lang"></span></div>
                            </div>
                            <div class="mb-2">
                              <div class="d-flex"><b>Questions : </b> <span id="quest"></span></div>
                              
                            </div>
                        </div>
                       <div class="col-md-6">
                            <p><b>Related Image:</b></p>
                            <div class="qus_image">
                                <img id="rel_img" src="" alt="" style="width: 100%; height: 110px; object-fit: contain;">
                            </div>
                       </div>
                        
                    </div>
                  
                    <hr>
                    <!-- Single Choice -->
                    <div class="row" id="ans_type_1" style="display:none;">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;<span id="q_type_1"></span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option A :</b> &nbsp; <span id="opt_A_1"> </span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option B :</b> &nbsp; <span id="opt_B_1"> </span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option C :</b> &nbsp; <span id="opt_C_1"> </span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option D :</b> &nbsp; <span id="opt_D_1"> </span> </p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Correct Option :</b> &nbsp;<span id="curr_opt_1"> </span></p>
                        </div>
                    </div>
                    <!-- Multiple Choice -->
                    <div class="row" id="ans_type_2" style="display:none;">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;<span id="q_type_2"></span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option A :</b> &nbsp; <span id="opt_A_2"> </span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option B :</b> &nbsp; <span id="opt_B_2"> </span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option C :</b> &nbsp; <span id="opt_C_2"> </span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Option D :</b> &nbsp; <span id="opt_D_2"> </span> </p>
                        </div>
                        <div class="col-md-12">
                            <p><b>Correct Option :</b> &nbsp;<span id="curr_opt_2"> </span></p>
                        </div>
                    </div>
                    <!-- Integer -->
                    <div class="row" id="ans_type_3" style="display:none;">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;<span id="q_type_3"></span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Correct Answer :</b> &nbsp; <span id="curr_opt_3"> </span></p>
                        </div>
                    </div>
                    <!-- True / False -->
                    <div class="row" id="ans_type_4" style="display:none;">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp;<span id="q_type_4"></span></p>
                        </div>
                        <div class="col-md-3">
                            <p><b>Correct Option :</b> &nbsp; <span id="curr_opt_4"></span></p>
                        </div>
                    </div>
                    <!-- Match Matrix -->
                    <div class="row" id="ans_type_5" style="display:none;">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp; <span id="q_type_5"></span></p>
                        </div>
                       <div class="d-flex">
                          <div class="col-md-6">
                            <p><b>Left Side Option :</b></p>
                            <p><b>Option 1 :</b> &nbsp; <span id="type_5_lft_opt_1"></span></p>
                            <p><b>Option 2:</b> &nbsp;  <span id="type_5_lft_opt_2"></span></p>
                            <p><b>Option 3 :</b> &nbsp; <span id="type_5_lft_opt_3"></span></p>
                            <p><b>Option 4 :</b> &nbsp; <span id="type_5_lft_opt_4"></span></p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Right Side Option :</b></p>
                            <p><b>Option A :</b> &nbsp;  <span id="type_5_right_opt_A"></span></p>
                            <p><b>Option B :</b> &nbsp;  <span id="type_5_right_opt_B"></span></p>
                            <p><b>Option C :</b> &nbsp;  <span id="type_5_right_opt_C"></span></p>
                            <p><b>Option D :</b> &nbsp;  <span id="type_5_right_opt_D"></span></p>
                        </div>
                      </div>
                        <div class="col-md-12">
                            <p><b>Correct Option :</b> &nbsp; <span id="curr_opt_5"></span></p>
                        </div>
                    </div>
                    <!-- Match The Following -->
                    <div class="row" id="ans_type_6" style="display:none;">
                        <div class="col-md-12">
                            <p><b>Question Type :</b> &nbsp; <span id="q_type_6"></span></p>
                        </div>
                         <div class="d-flex">
                            <div class="col-md-6">
                                <p><b>Left Side Option :</b></p>
                                <div class="d-flex"><b>Option 1 :</b> &nbsp;  <span id="type_6_lft_opt_1"></span></div>
                                <div class="d-flex"><b>Option 2 :</b> &nbsp;  <span id="type_6_lft_opt_2"></span></div>
                                <div class="d-flex"><b>Option 3 :</b> &nbsp;  <span id="type_6_lft_opt_3"></span></div>
                                <div class="d-flex"><b>Option 4 :</b> &nbsp;  <span id="type_6_lft_opt_4"></span></div>
                            </div>
                            <div class="col-md-6">
                                <p><b>Right Side Option :</b></p>
                                <div class="d-flex"><b>Option A :</b> &nbsp;  <span id="type_6_right_opt_A"></span></div>
                                <div class="d-flex"><b>Option B :</b> &nbsp;  <span id="type_6_right_opt_B"></span></div>
                                <div class="d-flex"><b>Option C :</b> &nbsp;  <span id="type_6_right_opt_C"></span></div>
                                <div class="d-flex"><b>Option D :</b> &nbsp;  <span id="type_6_right_opt_D"></span></div>
                            </div>
                         </div>
                         <div class="col-md-12">
                                <p><b>Correct Option :</b> &nbsp; <span id="curr_opt_6"></span></p>
                         </div>
                    </div>
                    <!-- Solution -->
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <p><b>Solution (Optional):</b> </p>
                                <p id="soln_opt"></p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Video Solution (Optional):</b> </p>
                            <p id="vid_soln"></p>
                        </div>
                    </div>
                   
                </div>
                <div class="modal-footer">
                   <button class="btn cancel-btn"
                        data-dismiss="modal" type="button">Close</button>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
   

    <script>
      function sec_get_id(idd){
        console.log(idd);
      }
    </script>
  
    <script>
    $(document).ready(
        function() {
            $("#create_section").click(function() {
                $("#invoice").show("slow");
            });

        });
    </script>
    
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({

        });
    });

    function showDiv() {
        document.getElementById('welcomeDiv').style.display = "block";
    }
    </script>
  
    <script>
    $(function() {
        $('#add_new_section').click(function() {
            $('#form_newSection').show();
            $('#info-section').hide();
        });
        $('#close_section').click(function() {
            $('#form_newSection').hide();
            $('#info-section').show();
        });
        $('#save_section').click(function() {
            $('#form_newSection').hide();
            $('#info-section').hide();
        });
    });
    </script>
    <script>
    $('.slide-toggle').on("click", function() {
        $('.slide-toggle ').toggleClass(' fa fa-angle-down fa fa-angle-up');
        $('.headerbottombar').toggleClass('hide show');
    });
    </script>
   

    <!-- setTimeout -->
    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <script type="text/javascript">
    function get_records(idd) 
    {
        // console.log(idd.id);
        document.getElementById("editId").value=idd.id
         
        var base_url = '<?php echo base_url() ?>';
        $.ajax({
                    url:base_url + "MainController/get_question_record_as_per_section",
                    type: "post", 
                    data: {
                            question_id:$("#editId").val(),
                            // uid: $("#uid").val(),
                        },
                    success: function(data) 
                    {
                        if(data==0)
                        {
                             // console.log("data no") 
                        }
                        else
                        {
                           const obj = JSON.parse(data);
                            
                            // get json response
                           console.log(obj);
						   var blank_="_";
                           document.getElementById('ans_type_1').style.display="none";  
                           document.getElementById('ans_type_2').style.display="none";
                           document.getElementById('ans_type_3').style.display="none";
                           document.getElementById('ans_type_4').style.display="none";
                           document.getElementById('ans_type_5').style.display="none";
                           document.getElementById('ans_type_6').style.display="none";
                           // document.getElementById('soln_opt').innerHTML  = blank_;
                           //document.getElementById('vid_soln').innerHTML  = blank_;
                           //document.getElementById('q_type').innerHTML  = blank_;
                          
                          
                          
                            document.getElementById('topic_name').innerHTML= obj.topic;
                            document.getElementById('sub_topic_name').innerHTML= obj.sub_topic;
                            document.getElementById('lang').innerHTML = obj.lang_id;
                            document.getElementById('quest').innerHTML = obj.question_name;
                            document.getElementById("rel_img").src = obj.images;
                           

                           
                          
                           
                            //answere type single choice
                            if(obj.answer_type==1)
                            {
                                document.getElementById('ans_type_'+obj.answer_type).style.display="flex";
                                document.getElementById('q_type_1').innerHTML  = obj.answer_type;
                                
                                document.getElementById('opt_A_1').innerHTML  = obj.options_A;
                                document.getElementById('opt_B_1').innerHTML  = obj.options_B;
                                document.getElementById('opt_C_1').innerHTML  = obj.options_C;
                                document.getElementById('opt_D_1').innerHTML  = obj.options_D;
                                document.getElementById('curr_opt_1').innerHTML  = obj.correct_answer;
                                document.getElementById('soln_opt').innerHTML  = obj.solution;
                                document.getElementById('vid_soln').innerHTML  = obj.video_solution;
                                                            
                            }
                          
                            if(obj.answer_type==2)
                            {
                               document.getElementById("curr_opt_2").innerHTML = "";
                                document.getElementById('ans_type_'+obj.answer_type).style.display="flex";
                                document.getElementById('q_type_2').innerHTML  = obj.answer_type;
                                
                                document.getElementById('opt_A_2').innerHTML  = obj.options_A;
                                document.getElementById('opt_B_2').innerHTML  = obj.options_B;
                                document.getElementById('opt_C_2').innerHTML  = obj.options_C;
                                document.getElementById('opt_D_2').innerHTML  = obj.options_D;
                                
                               console.log("length", obj.options.length)
                               
                               for(var i=0; i<  obj.options.length;i++){
                                
                                 var get_in_op=obj.options[i]; 
                                  //document.getElementById('curr_opt_2').innerText  = get_in_op.correct_answer;
                                 var curr_opt_2 = document.getElementById("curr_opt_2");
                                 var opts = document.createTextNode(get_in_op.correct_answer);

                                  curr_opt_2.appendChild(opts);
                                 if(i<  obj.options.length-1){
                                    var optss = document.createTextNode(",");

                                  curr_opt_2.appendChild(optss);
                                 }
                                 
                               }
                                
                              
                               
                                document.getElementById('soln_opt').innerHTML  = obj.solution;
                                document.getElementById('vid_soln').innerHTML  = obj.video_solution;
                            }
                          
                            if(obj.answer_type==3)
                            { 
                                document.getElementById('ans_type_'+obj.answer_type).style.display="block";
                                document.getElementById('q_type_3').innerHTML  = obj.answer_type;
                                document.getElementById('curr_opt_3').innerHTML  = obj.correct_answer;
                                document.getElementById('soln_opt').innerHTML  = obj.solution;
                                document.getElementById('vid_soln').innerHTML  = obj.video_solution;
                            }
                          
                            if(obj.answer_type==4)
                            {
                                document.getElementById('ans_type_'+obj.answer_type).style.display="block";
                                document.getElementById('q_type_4').innerHTML  = obj.answer_type;
                                document.getElementById('curr_opt_4').innerHTML  = obj.correct_answer;
                                document.getElementById('soln_opt').innerHTML  = obj.solution;
                                document.getElementById('vid_soln').innerHTML  = obj.video_solution;
                            }
                          
                            if(obj.answer_type==5)
                            {    
                               document.getElementById("curr_opt_5").innerHTML = "";
                               document.getElementById('ans_type_'+obj.answer_type).style.display="block";
                              document.getElementById('q_type_5').innerHTML  = obj.answer_type;
                               document.getElementById('type_5_lft_opt_1').innerHTML  = obj.left_opt_1;
                               document.getElementById('type_5_lft_opt_2').innerHTML  = obj.left_opt_2;
                               document.getElementById('type_5_lft_opt_3').innerHTML  = obj.left_opt_3;
                               document.getElementById('type_5_lft_opt_4').innerHTML  = obj.left_opt_4;
                              
                              
                               document.getElementById('type_5_right_opt_A').innerHTML  = obj.right_opt_A;
                               document.getElementById('type_5_right_opt_B').innerHTML  = obj.right_opt_B;
                               document.getElementById('type_5_right_opt_C').innerHTML  = obj.right_opt_C;
                               document.getElementById('type_5_right_opt_D').innerHTML  = obj.right_opt_D;
                               document.getElementById('soln_opt').innerHTML  = obj.solution;
                               document.getElementById('vid_soln').innerHTML  = obj.video_solution;
                              
                               for(var i=1; i<=4; i++)
                                {

                                var curr_opt_5 = document.getElementById("curr_opt_5");
                                 var opts = document.createTextNode("("+i+") - { ");
                                  
                                  curr_opt_5.appendChild(opts);
                                  
                                  
                                    for(var k=0;k < obj.options.length ;k++)
                                      {
                                        var get_in_op=obj.options[k];
                                        if(get_in_op.correct_options_l==i)
                                        {
                                        
                                              var opts = document.createTextNode("("+get_in_op.correct_options_r+")");
                                              curr_opt_5.appendChild(opts);

                                          
 											   
                                        }
                                        

                                      }
                                     var opts1 = document.createTextNode(" } ");
                                     curr_opt_5.appendChild(opts1);

                                  
                                  if(i < 4){
                                    var opts1 = document.createTextNode(" , ");
                                     curr_opt_5.appendChild(opts1);
                                  }

                                 }  
                             
                            }

                            if(obj.answer_type==6)
                            {
                               document.getElementById("curr_opt_6").innerHTML = "";
                               document.getElementById('ans_type_'+obj.answer_type).style.display="block";
                              document.getElementById('q_type_6').innerHTML  = obj.answer_type;
                               document.getElementById('type_6_lft_opt_1').innerHTML  = obj.left_opt_1;
                               document.getElementById('type_6_lft_opt_2').innerHTML  = obj.left_opt_2;
                               document.getElementById('type_6_lft_opt_3').innerHTML  = obj.left_opt_3;
                               document.getElementById('type_6_lft_opt_4').innerHTML  = obj.left_opt_4;
                              
                              
                               document.getElementById('type_6_right_opt_A').innerHTML  = obj.right_opt_A;
                               document.getElementById('type_6_right_opt_B').innerHTML  = obj.right_opt_B;
                               document.getElementById('type_6_right_opt_C').innerHTML  = obj.right_opt_C;
                               document.getElementById('type_6_right_opt_D').innerHTML  = obj.right_opt_D;
                               document.getElementById('soln_opt').innerHTML  = obj.solution;
                               document.getElementById('vid_soln').innerHTML  = obj.video_solution;
                              
                               for(var i=1; i<=4; i++)
                                {

                                var curr_opt_6 = document.getElementById("curr_opt_6");
                                 var opts = document.createTextNode("("+i+") - { ");
                                  
                                  curr_opt_6.appendChild(opts);
                                  
                                  
                                    for(var k=0;k < obj.options.length ;k++)
                                      {
                                        var get_in_op=obj.options[k];
                                        if(get_in_op.correct_options_l==i)
                                        {
                                        
                                              var opts = document.createTextNode("("+get_in_op.correct_options_r+")");
                                              curr_opt_6.appendChild(opts);

                                          
 											   
                                        }
                                        

                                      }
                                     var opts1 = document.createTextNode(" } ");
                                     curr_opt_6.appendChild(opts1);

                                  
                                  if(i < 4){
                                    var opts1 = document.createTextNode(" , ");
                                     curr_opt_6.appendChild(opts1);
                                  }

                                 }  
                            }
                        } //else
                    }
                });
            
    }
    </script>
</body>

</html>