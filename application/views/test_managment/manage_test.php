<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learnings | Test Management</title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode default-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Test Management</h4>
                            </span>

                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Manage Test</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <!-- <div class="card">
                            <div class="card-body">
                                <div class="row "> -->
                        <div class="col-md-12">
                            <?php $this->load->view('aside_test'); ?>
                        </div>
                        <div class="col-md-12">
                            <div class="row manage_test">
                                <div class="col-md-12">
                                    <?php 
                                    if (!empty($this->session->flashdata('create')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-success" id="alert_msg">
                                              <?php echo $this->session->flashdata('create');?>
                                          </div>
                                      </div>
                                    <?php
                                    }

                                    if (!empty($this->session->flashdata('edit')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-success" id="alert_msg">
                                              <?php echo $this->session->flashdata('edit');?>
                                          </div>
                                      </div>
                                    <?php
                                    }

                                    if (!empty($this->session->flashdata('exists')) )
                                    { ?>
                                      <div class="col-sm-12">
                                          <div class="alert alert-warning" id="alert_msg">
                                              <?php echo $this->session->flashdata('exists');?>
                                          </div>
                                      </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title">
                                                Manage Tests(Count)
                                            </div>
                                            <div class="card-options">
                                                <a href="<?php echo base_url('add_test'); ?>" type="button"
                                                    class="btn btn-icon btn-primary"> <i class="fa fa-plus">
                                                    </i>&nbsp; Create New
                                                    Test</a>

                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row justify-content-center">
                                                <div class="col-md-6 col-12">
    <!-- filter -->
    <form method="post" action="">
        <div class="row justify-content-center">
            <div class="col-md-5 col-12">
                <div class="input-group">
                    <select name="test_series_id" id="test_series_id"  class="form-control search-input table-search-box" tabindex="-1" aria-hidden="true" required>
                        <option value="" selected disabled>Select Test Series Name </option>
                        <?php 
                        if (!empty($test_series_list)) 
                        {
                            foreach ($test_series_list as $t) 
                            { 
                                ?>
                                <option value="<?php echo $t['test_series_id'] ?>">
                                    <?php echo $t['test_series_name'] ?>
                                </option>
                                <?php   
                            }  
                        } 
                        else
                        {
                            echo " ";
                        }
                        ?>
                    </select>

                    <button class="btn btn-primary" type="submit" name="search_test">Go!</button>
                </div>
            </div>
        </div>
        <!-- submit -->
    </form>
    
                                                </div>
                                            </div>
                                        </div>
    <div class="card-body">
        <?php 
        if (!empty($test_lists)) 
        {
            $i= 1+$this->uri->segment(2);
            foreach ($test_lists as $t) 
            {
                ?>    
                <div class="card test-box">
                    <div class="card-body p-3">
                        <div class="row align-items-center">
                            <div class="col-md-1 text-center file-icon">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-2">
                                <div class="font-weight-bold mb-1 fs-16 text-muted">

                                    <!-- edit_test here -->
                                    <a href="<?php echo base_url().'edit_test/'.$t['test_id']; ?>">
                                        <?php echo $t['test_name']; ?>
                                    </a>
                                </div>
                                <span class="text-primary">
                                    <?php 
                                    if (!empty($test_series_list)) 
                                    {
                                        foreach ($test_series_list as $test) 
                                        { 
                                            if ($test['test_series_id'] == $t['test_series_id'])
                                            {
                                                echo $test['test_series_name'];
                                            }
                                        }  
                                    } 
                                    else
                                    {
                                        echo " ";
                                    }
                                     // echo $t['test_series_id']; ?>
                                </span>
                                <p class="text-primary">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <?php 
                                    echo date("d/m/Y", strtotime($t['created_at'])); 
                                    ?>
                                </p>
                                <hr class="d-md-none d-sm-block">
                            </div>
                            <div class="col-md-9 card-bg">
                                <div class="row align-items-center">
                                    <div class="col-md-2 col-6">
                                        <p><?php echo $t['t_duration']; ?> min</p>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <p>
                                            <?php 
                                            if ($t['test_series_type'] == 1)
                                            {
                                               echo "Free";
                                            } 
                                            elseif ($t['test_series_type'] == 2)
                                            {
                                               echo "Paid" ;
                                            }
                                            ?>  
                                        </p>
                                    </div>
                                    <div class="col-md-2 col-6">
                                        <?php 
                                        if (!empty($difficulty_list)) 
                                        {
                                            foreach ($difficulty_list as $dif) 
                                            { 
                                                if ($dif['difficulty_id'] == $t['difficulty_id'])
                                                {
                                                   ?>
                                                   <span class="badge btn-sm btn-default"><?php echo $dif['difficulty_medium']; ?></span>
                                                   <?php
                                                }
                                            }  
                                        } 
                                        else
                                        {
                                            echo " ";
                                        }
                                        // echo $test['test_series_name']; 
                                        ?>

                                    </div>
                                    <div class="col-md-2 col-6">
                                        <?php 
                                        if ($t['status'] == 1) 
                                        {
                                            ?>
                                            <input type="checkbox" id="<?php echo $t['test_id']; ?>" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="0"> 
                                            <?php
                                        } 
                                        else
                                        {
                                            ?>
                                            <input type="checkbox" id="<?php echo $t['test_id']; ?>" data-size="sm" data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="1">
                                            <?php         
                                        } 
                                        ?>
                                    </div>
                                    
                                    <div class="col-md-3 col-12  test-btn-section ">
                                    <hr class="d-md-none d-sm-block">
                                        <div class="d-flex justify-content-center">
                                            <?php 
                                            $t_name = $t['test_name'];
                                            ?>
                                            <a href="<?php echo base_url().'result/'.$t_name.'/'.$t['test_id']; ?>" class="btn btn-primary d-flex  mr-2 align-items-center" data-placement="top" data-toggle="tooltip" title="Result"><i class="fa fa-area-chart"></i></a>

                                            <a href="<?php echo base_url().'edit_test/'.$t['test_id']; ?>" class="btn btn-warning d-flex  mr-2 align-items-center" data-placement="top" data-toggle="tooltip" title="Edit Test"><i class="fa fa-edit"></i> </a>

                                            <a id="delete_<?php echo $t['test_id']; ?>" class="btn btn-danger d-flex align-items-center mr-2"><i class="fa fa-trash"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- delete -->
                <script>
                    document.getElementById('delete_<?php echo $t['test_id']; ?>').onclick = function() 
                    {
                        var id = $("#<?php echo $t['test_id']; ?>").val(); 
                   
                        swal({
                                title: "Are you sure?",
                                text: "You will not be able to recover this file!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, delete it!',
                                cancelButtonText: "No, cancel",
                                closeOnConfirm: false,
                                closeOnCancel: false
                        },
                        function(isConfirm) {
                            if (isConfirm) 
                            {
                                $.ajax({
                                           url: '<?php echo base_url().'delete_test/'.$t['test_id']; ?>',
                                           type: "POST",
                                           data: {id:id},
                                           dataType:"HTML",
                                           success: function () {
                                            swal(
                                                    "Deleted!",
                                                    "Your file has been deleted!",
                                                    "success"
                                                ),
                                                $('.confirm').click(function()
                                                {
                                                    location.reload();
                                                });
                                            },
                                    });
                                
                            } else {
                                swal(
                                    "Cancelled",
                                    "Your  file is safe !",
                                    "error"
                                );
                            }
                        });
                    };
                </script>
                <!-- End delete -->

                <?php
            }
        }
        else
        {
            echo "Record not available. ";
        }
        ?>
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
                <?php echo $this->pagination->create_links(); ?>
            </ul>
        </nav>
    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- </div>
                        </div>
                    </div> -->
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>

    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
   
  
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({

        });
    });
    </script>

    <!-- setTimeout -->
    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <!-- status active-deactive -->
    <script type="text/javascript"> 
    function change_status(idd)
    {
        var status=idd.value;
        var test_id= idd.id;
        var base_url = '<?php echo base_url() ?>';
        var test_id = test_id;

        if(status != '')
        {
            $.ajax({
                      url:base_url + "MainController/active_test_status",
                      method:"POST",
                      data:{status:status,test_id:test_id},
                      
                      success: function(data)
                      {
                         // alert(data);
                          if (data == 1) 
                           {
                               alert('Status Changed Sucessfully !..');
                               location.reload();
                           } 
                           else 
                           {
                               alert('Something Went Wrong !...')
                           }
                      }
                });
        }
    }
    </script>


</body>

</html>