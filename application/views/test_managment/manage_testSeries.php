<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learnings | Test Management</title>
    <?php $this->load->view('css'); ?>
    <link href="<?php echo base_url(''); ?>/assets/css/magnific.css" rel="stylesheet" />
</head>

<body class="app sidebar-mini light-mode light-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Test Management</h4>
                            </span>

                        </div>

                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item "><a href="<?php echo base_url('home'); ?>"
                                        class="d-flex align-items-center ">
                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Manage Test Series</li>
                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->
                    <!-- Row -->
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <?php $this->load->view('aside_test'); ?>
                        </div>
                        <div class="col-md-12">
                            <?php 
                            if (!empty($this->session->flashdata('create')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('create');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('edit')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-success" id="alert_msg">
                                      <?php echo $this->session->flashdata('edit');?>
                                  </div>
                              </div>
                            <?php
                            }

                            if (!empty($this->session->flashdata('exists')) )
                            { ?>
                              <div class="col-sm-12">
                                  <div class="alert alert-warning" id="alert_msg">
                                      <?php echo $this->session->flashdata('exists');?>
                                  </div>
                              </div>
                            <?php
                            }
                            ?>
                            <div class="row manage_test">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title">
                                                Manage Test Series(Count)
                                            </div>
                                            <div class="card-options">
                                                <a type="button" class="btn btn-icon btn-primary"
                                                    data-target="#add_testSeries" data-toggle="modal"><i class="fa fa-plus"></i>&nbsp;Create New Test Series</a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row justify-content-center">
                                                <div class="col-md-6 col-12">
                                                    <div class="row justify-content-center">
                                                        <!-- filter  -->
                                                        <div class="col-md-5 col-12">
                                                            <form method="post" action="">
                                                                <div class="input-group">
                                                                    <select  name="category_id" id="category_id" class="form-control">
                                                                        <option value="" selected disabled>Select Category </option>
                                                                        <?php 
                                                                        if (!empty($category_list)) 
                                                                        {
                                                                            foreach ($category_list as $cat_list) 
                                                                            { 
                                                                                ?>
                                                                                <option value="<?php echo $cat_list['category_id'] ?>">
                                                                                    <?php echo $cat_list['category_name'] ?>
                                                                                </option>
                                                                                <?php   
                                                                            }  
                                                                        } 
                                                                        else
                                                                        {
                                                                            echo " ";
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                    <button class="btn btn-primary" type="submit" name="search_test_series">Go!</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
  <?php 
  if (!empty($test_series_list)) 
  {
      $i= 1+$this->uri->segment(2);
      foreach ($test_series_list as $test) 
      {
          ?>
          <div class="card test-box">
              <div class="card-body p-3">
                  <div class="row align-items-center">
                      <div class="col-md-1 text-center file-icon">
                          <i class="fa fa-file-text-o" aria-hidden="true"></i>
                      </div>
                      <div class="col-md-2">
                          <div class="font-weight-bold mb-1 fs-16 text-muted">
                              <a href="<?php // echo base_url('manage_test'); ?>">
                                  <?php echo $test['test_series_name']; ?></a>
                          </div>
                          <span class="text-primary">
                              <?php 
                              if (!empty($category_list)) 
                              {
                                  foreach ($category_list as $cat_list) 
                                  { 
                                      if ($cat_list['category_id'] == $test['category_id'])
                                      {
                                          echo $cat_list['category_name'];
                                      }
                                  }  
                              } 
                              else
                              {
                                  echo " ";
                              }
                              // echo $test['test_series_name']; ?>
                          </span><br>
                        <span><i class="fa fa-clock-o" > </i> 
                            <?php 
                            echo date("d-m-Y", strtotime($test['start_date'])); 
                            ?>
                            <i class="fa fa-clock-o" > </i> 
                            <?php
                            echo date("d-m-Y", strtotime($test['end_date'])); 
                            ?>
                        </span>
                        <hr class="d-md-none d-sm-block">
                      </div>
                      <div class="col-md-7 card-bg">
                          <div class="row align-items-center">
                              <div class="col-md-2 col-6">
                                  <p>  <!-- free / paid test -->
                                      <?php 
                                      if ($test['offer'])
                                      {
                                        $offer = " (".$test['offer']." % )";
                                      } else{
                                        $offer = "";
                                      }

                                      if ($test['test_series_type'] == 1)
                                      {
                                         echo "Free Test";
                                      } 
                                      elseif ($test['test_series_type'] == 2)
                                      {
                                        echo "<i class='fa fa-inr'></i> ".$test['price'].$offer;
                                      }
                                      ?> 
                                  </p>
                              </div>
                            <div class="col-md-2 col-6">
                                  <p> 
                                    <?php 
                                    $where = '(test_series_id="'.$test['test_series_id'].'")';

                                    $test_list = $this->Main_Model->getAllData_not_order($tbl='test_list',$where); 
                                    echo count($test_list);
                                    ?>
                                   Test </p>
                              </div>
                              <div class="col-md-2 col-6">
                                  <p>  <!-- is_trending_series  -->
                                      <?php 
                                      if ($test['is_trending_series'] == 1)
                                      {
                                         ?>
                                         <span class="badge badge-success">Trending</span>
                                         <?php
                                      } 
                                      else
                                      {
                                         ?>
                                         <span class="badge badge-warning">No Trending</span>
                                         <?php
                                      }
                                      ?> 
                                  </p>
                              </div>
                              <div class="col-md-2 col-6">
                                  <!-- icon  -->
                                  <?php 
                                  if ($test['icon_image'])
                                  {
                                     ?>
                                     <div class="magnific-img">
                                        <a class="image-popup-vertical-fit" href="<?php echo $test['icon_image']; ?>" title="">
                                            <img src="<?php echo $test['icon_image']; ?>" alt="Sub-Category Image" style="width: 50px; height: 50px; object-fit: contain;" />
                                        </a>
                                    </div>
                                     <?php
                                  } 
                                  ?> 
                              </div>
                              <!-- banner_image  -->
                              <div class="col-md-2 col-6">
                                  <?php 
                                  if ($test['banner_image'])
                                  {
                                     ?>
                                     <div class="magnific-img">
                                        <a class="image-popup-vertical-fit" href="<?php echo $test['banner_image']; ?>" title="">
                                            <img src="<?php echo $test['banner_image']; ?>" alt="Sub-Category Image" style="width: 50px; height: 50px; object-fit: contain;" />
                                        </a>
                                    </div>
                                     <?php
                                  } 
                                  ?> 
                              </div>
                             
                              <div class="col-md-2 col-6">
                                  <?php 
                                  if ($test['status'] == 1) 
                                  {
                                      ?>
                                      <input type="checkbox" id="<?php echo $test['test_series_id']; ?>" data-size="sm" checked data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="0"> 
                                      <?php
                                  } 
                                  else
                                  {
                                      ?>
                                      <input type="checkbox" id="<?php echo $test['test_series_id']; ?>" data-size="sm" data-toggle="toggle" data-on="Active" data-off="Deactive" data-onstyle="success" data-offstyle="danger" onchange="change_status(this);" value="1">
                                      <?php         
                                  } 
                                  ?>

                              </div>
                          </div>
                          <hr class="d-md-none d-sm-block">
                      </div>
                      <div class="col-md-2 col-12 text-center test-btn-section ">
                          <div class="d-flex justify-content-center">

                              <!-- view description -->
                              <a class="btn btn-primary d-flex  mr-2 align-items-center" data-target="#view_des_<?php echo $test['test_series_id']; ?>" data-toggle="modal" type="button"><i class="fa fa-file-text"></i>
                              </a>
                              <?php 
                               $series_name = $test['test_series_name'];
                               // echo $series_name;
                               ?>
                                
                                <a href="<?php echo base_url().'add_test/'.$series_name.'/'.$test['test_series_id']; ?>" class="btn btn-info d-flex  mr-2 align-items-center" data-placement="top" data-toggle="tooltip" title="Add Test" type="button"><i class="fa fa-plus"></i></a>
                                
                                 <a href="<?php echo base_url().'manage_test_list/'.$series_name.'/'.$test['test_series_id']; ?>" class="btn btn-success d-flex  mr-2 align-items-center" data-placement="top" data-toggle="tooltip" title="View All Test" type="button"><i class="fa fa-eye"></i> </a>

                                <!-- <a href="<?php //echo base_url().'manage_test_list/'.$test['test_series_id']; ?>" class="btn btn-success d-flex  mr-2 align-items-center" data-placement="top" data-toggle="tooltip" title="View All Test" type="button"><i class="fa fa-eye"></i> </a> -->
                            
                                <a class="btn btn-primary d-flex  mr-2 align-items-center" data-target="#edit_testSeries_<?php echo $test['test_series_id']; ?>" data-toggle="modal" type="button"><i class="fa fa-edit"></i> </a>
                            
                                <a id="delete_<?php echo $test['test_series_id']; ?>" class="btn btn-danger d-flex align-items-center mr-2"><i class="fa fa-trash"></i> </a>

                          </div>

                      </div>
                  </div>
              </div>
          </div>

          <!-- view description -->
          <div class="modal" id="view_des_<?php echo $test['test_series_id']; ?>">
              <div class="modal-dialog model-fullwidth" role="document">
                  <div class="modal-content modal-content-demo">
                      <div class="modal-header">
                          <h6 class="modal-title">View Description </h6>
                          <button aria-label="Close" class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">&times;</span> </button>
                      </div>
                      <div class="modal-body">
                          <div class="row">
                              <div class="col-md-12 form-group">
                                  <label for="bannername" class="form-label">Description
                                      <span class="text-red">*</span></label>
                                  <textarea id="summ_des_<?php echo $test['test_series_id']; ?>" class="" readonly><?php echo $test['description']; ?></textarea>
                              </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                      </div>
                  </div>
              </div>
          </div>

          <script>
          $(document).ready(function() {
              $('.summernote_<?php echo $test['test_series_id']; ?>').summernote({

              });
              $('#summ_des_<?php echo $test['test_series_id']; ?>').summernote(
                  {
                  height: 380,
                  disableResizeEditor: true,

              });
              $('#summ_des_<?php echo $test['test_series_id']; ?>').summernote('disable');
          });
          </script>

          <!-- delete -->
          <script>
              document.getElementById('delete_<?php echo $test['test_series_id']; ?>').onclick = function() 
              {
                  var id = $("#<?php echo $test['test_series_id']; ?>").val(); 

                  swal({
                          title: "Are you sure?",
                          text: "You will not be able to recover this file!",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: '#DD6B55',
                          confirmButtonText: 'Yes, delete it!',
                          cancelButtonText: "No, cancel",
                          closeOnConfirm: false,
                          closeOnCancel: false
                  },
                  function(isConfirm) {
                      if (isConfirm) 
                      {
                          $.ajax({
                                     url: '<?php echo base_url().'delete_testSeries/'.$test['test_series_id']; ?>',
                                     type: "POST",
                                     data: {id:id},
                                     dataType:"HTML",
                                     success: function () {
                                      swal(
                                              "Deleted!",
                                              "Your file has been deleted!",
                                              "success"
                                          ),
                                          $('.confirm').click(function()
                                          {
                                              location.reload();
                                          });
                                      },
                              });

                      } else {
                          swal(
                              "Cancelled",
                              "Your  file is safe !",
                              "error"
                          );
                      }
                  });
              };
          </script>

          <!-- edit -->
          <div class="modal" id="edit_testSeries_<?php echo $test['test_series_id']; ?>">
              <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content modal-content-demo">
                      <div class="modal-header">
                          <h6 class="modal-title">Edit Test Series </h6>
                          <button aria-label="Close" class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">&times;</span> </button>
                      </div>
                      <form method="post"  action="<?php echo base_url().'edit_testSeries/'.$test['test_series_id']; ?>" enctype="multipart/form-data">
                          <div class="modal-body">
                              <div class="row">
                                  <div class="col-md-6 form-group">
                                      <label for="bannername" class="form-label">Select Category <span class="text-red">*</span></label>

                                      <select  name="category_id" id="category_id" class="form-control">
                                          <option value="" selected disabled>Select Category </option>
                                          <?php 
                                          if (!empty($category_list)) 
                                          {
                                              foreach ($category_list as $cat_list) 
                                              { 
                                                  if ($cat_list['category_id'] == $test['category_id'] ) 
                                                  { ?>
                                                    <option selected="selected" value="<?php echo $cat_list['category_id'] ?>">
                                                      <?php echo  $cat_list['category_name']; ?>
                                                    </option>
                                                      <?php   
                                                  } 
                                                  else { ?>
                                                    <option value="<?php echo $cat_list['category_id'] ?>">
                                                      <?php echo $cat_list['category_name']; ?>
                                                    </option>
                                                    <?php
                                                  }
                                              }  
                                          } 
                                          else
                                          {
                                              echo " ";
                                          }
                                          ?>
                                      </select>
                                  </div>

                                  <div class="col-md-6 form-group">
                                      <label for="bannername" class="form-label">Test Series Name <span class="text-red">*</span></label>

                                      <input type="text" class="form-control" name="test_series_name" id="test_series_name" placeholder="Enter Test Series Name" value="<?php echo $test['test_series_name']; ?>" >
                                  </div>
                                  <!--<div class="col-md-6 form-group">
                                      <label for="bannername" class="form-label">Test Series Validity <span class="text-red">*</span></label>

                                      <input type="number" class="form-control" name="validity" id="validity" placeholder="Enter Test Series Validity" value="<?php echo $test['validity']; ?>" >
                                  </div>
                                  <div class="col-md-6 form-group">
                                      <label for="bannername" class="form-label">Validity Time<span class="text-red">*</span></label>

                                      <select  name="validity_time" id="validity_time" class="form-control">
                                          <option value="" selected disable>Select Validity Time</option>
                                          <?php
                                          if ($test['validity_time'] == 1) 
                                          { 
                                              ?>
                                              <option value="1" selected>Day's</option>
                                              <option value="2">Week's</option>
                                              <option value="3">Month's</option>
                                              <option value="4">Unlimited</option>
                                              <?php
                                          } 

                                          if ($test['validity_time'] == 2) 
                                          { 
                                              ?>
                                              <option value="1">Day's</option>
                                              <option value="2" selected>Week's</option>
                                              <option value="3">Month's</option>
                                              <option value="4">Unlimited</option>
                                              <?php
                                          }   

                                          if ($test['validity_time'] == 3) 
                                          { 
                                              ?>
                                              <option value="1">Day's</option>
                                              <option value="2">Week's</option>
                                              <option value="3" selected>Month's</option>
                                              <option value="4">Unlimited</option>
                                              <?php
                                          }   

                                          if ($test['validity_time'] == 4) 
                                          { 
                                              ?>
                                              <option value="1">Day's</option>
                                              <option value="2">Week's</option>
                                              <option value="3">Month's</option>
                                              <option value="4" selected>Unlimited</option>
                                              <?php
                                          }   
                                          ?> 
                                      </select>
                                  </div>-->
                                <div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">Start Date <span class="text-red">*</span></label>

                                <input type="date" class="form-control s_date" name="start_date" placeholder="Enter Test Series Start Date" required value="<?php echo $test['start_date']; ?>">
                            </div>
                          <div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">End Date <span class="text-red">*</span></label>

                                <input type="date" class="form-control s_date" name="end_date" id="validity" placeholder="Enter Test Series End Date" required value="<?php echo $test['end_date']; ?>">
                            </div>
                                <div class="col-md-6 form-group">
                                  <label for="" class="form-label">Language<span class="text-red">*</span></label>
                                    <select name="lang_id" id="lang_id" class="form-control" tabindex="-1" aria-hidden="true" required>
                                        <option value="" selected disabled>Select Language </option>
                                        <?php 
                                        if (!empty($language_list)) 
                                        {
                                            foreach ($language_list as $lag) 
                                            {
                                                if ($lag['lang_id'] == $test['lang_id'] ) 
                                                { 
                                                    ?>
                                                    <option selected="selected" value="<?php echo $lag['lang_id'] ?>">
                                                        <?php echo $lag['language_name']; ?>
                                                    </option>
                                                    <?php   
                                                } 
                                                else 
                                                { 
                                                    ?>
                                                    <option value="<?php echo $lag['lang_id'] ?>">
                                                        <?php echo $lag['language_name']; ?>
                                                    </option>
                                                    <?php
                                                }
                                            }  
                                        } 
                                        else
                                        {
                                            echo "";
                                        }
                                        ?>
                                    </select>
                                </div>
                                
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="form-label">This Series Is Trending?<span class="text-red">
                                      *</span></label>
                                    <div class="form-check">
                                       <?php  
                                       if ($test['is_trending_series']==1) 
                                       {
                                          ?>
                                          <input class="form-check-input" type="checkbox" checked name="is_trending_series" value="1"  id="trending_series" value="<?php echo $test['is_trending_series'];?>">

                                          <label class="form-check-label" for="trending_series">Trending Series</label>
                                          <?php
                                       }
                                       else
                                       {
                                           ?>                                              
                                           <input class="form-check-input" type="checkbox"  name="is_trending_series" value="1"  id="trending_series" value="<?php echo $test['is_trending_series'];?>">

                                            <label class="form-check-label" for="trending_series">Trending Series</label>
                                          <?php
                                       }
                                       ?>
                                    </div>
                                  </div>
                                </div>

                                  <div class="col-md-6 form-group">
                                      <label for="bannername" class="form-label">Test Series Type <span class="text-red">*</span></label>

                                      <select name="test_series_type" class="form-control" id="select_price_1"> 
                                         <option value="" selected disable>Select Series Type </option>
                                          <?php
                                          if ($test['test_series_type'] == 1) 
                                          { 
                                              ?>
                                              <option value="1" selected>Free</option>
                                              <option value="2">Paid</option>
                                              <?php
                                          } 

                                          if ($test['test_series_type'] == 2) 
                                          { 
                                              ?>
                                              <option value="1">Free</option>
                                              <option value="2" selected>Paid</option>
                                              <?php
                                          }   
                                          ?> 
                                      </select>
                                  </div>
                            
                                
                                  <div class="col-md-6 form-group" id="type2" >
                                      <label for="bannername" class="form-label">Test Series Price <span class="text-red">*</span></label>

                                      <input type="text" class="form-control" name="price" id="price" placeholder="Enter Test Series Price"  value="<?php echo $test['price']; ?>" >
                                  </div>
                                <div class="col-md-6 form-group" id="type_2">
                                <label for="bannername" class="form-label">Test Series Offers ( % )</label>
                                <input type="text" class="form-control" name="offer" id="offer" placeholder="Enter Test Series Offers" value="<?php echo $test['offer']; ?>" >
                            </div>
                                </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="form-label">Upload Icon<span class="text-red"> *</span></label>
                                    <div class="col-ting">
                                      <div class="control-group file-upload" id="file-upload1">
                                        <div class="image-box text-center">
                                           <img src="<?php echo $test['icon_image'];?>" alt="img">
                                        </div>
                                        <div class="controls" style="display: none;">
                                            <input type="file" name="icon_image" accept="image/*"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="form-label">Upload Banner<span class="text-red"> *</span></label>
                                    <div class="col-ting">
                                      <div class="control-group file-upload" id="file-upload1">
                                        <div class="image-box text-center">
                                           <img src="<?php echo $test['banner_image'];?>" alt="img">
                                        </div>
                                        <div class="controls" style="display: none;">
                                          <input type="file" name="banner_image" accept="image/*"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                  <div class="col-md-12 form-group">
                                      <label for="bannername" class="form-label">Description <span class="text-red">*</span></label>

                                      <textarea id="summernote" class="summernote" name="description" ><?php echo $test['description']; ?></textarea>
                                  </div>
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn save-btn">Update Test Series</button> 
                              <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>

          <?php
      }
  }
  else
  {
      echo "Record not available. ";
  }
  ?>

                                            <nav aria-label="Page navigation example">
                                                <ul class="pagination justify-content-end">
                                                    <?php echo $this->pagination->create_links(); ?>
                                                </ul>
                                            </nav>
                                          </div>  <!-- card-body -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Row-->
                </div>
            </div>
            <!-- end app-content-->
        </section>
    </div>
    
    <!-- add test series -->
    <div class="modal" id="add_testSeries">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Add Test Series </h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="<?php //echo base_url(); ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">Select Category <span class="text-red">*</span></label>
                                
                                <select  name="category_id" id="category_id" class="form-control">
                                    <option value="" selected disabled>Select Category </option>
                                    <?php 
                                    if (!empty($category_list)) 
                                    {
                                        foreach ($category_list as $cat_list) 
                                        { 
                                            ?>
                                            <option value="<?php echo $cat_list['category_id'] ?>">
                                                <?php echo $cat_list['category_name'] ?>
                                            </option>
                                            <?php   
                                        }  
                                    } 
                                    else
                                    {
                                        echo " ";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">Test Series Name 
                                    <span class="text-red">*</span></label>

                                <input type="text" class="form-control" name="test_series_name" id="test_series_name" placeholder="Enter Test Series Name"  >
                            </div>
                            <!--<div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">Test Series Validity <span class="text-red">*</span></label>

                                <input type="number" class="form-control" name="validity" id="validity" placeholder="Enter Test Series Validity" >
                            </div>-->
                            <div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">Start Date <span class="text-red">*</span></label>

                                <input type="date" class="form-control s_date" name="start_date" id="validity" placeholder="Enter Test Series Start Date" required >
                            </div>
                          <div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">End Date <span class="text-red">*</span></label>

                                <input type="date" class="form-control s_date" name="end_date" id="validity" placeholder="Enter Test Series End Date" required >
                            </div>
                            <!--<div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">Validity Time
                                    <span class="text-red">*</span></label>
                               
                                <select  name="validity_time" id="validity_time" class="form-control">
                                    <option value="" selected disable>Select Validity Time</option>
                                    <option value="1">Day's</option>
                                    <option value="2">Week's</option>
                                    <option value="3">Month's</option>
                                    <option value="4">Unlimited</option>
                                </select>
                            </div>-->
                          <div class="col-md-6 form-group">
                                <label for="" class="form-label">Language<span
                                        class="text-red">*</span></label>
                               
                                <select name="lang_id" id="lang_id" class="form-control">
                                    <option value="" selected disabled>Select Language</option>
                                    <?php 
                                    if (!empty($language_list)) 
                                    {
                                        foreach ($language_list as $lang) 
                                        { 
                                            ?>
                                            <option value="<?php echo $lang['lang_id'] ?>">
                                                <?php echo $lang['language_name'] ?>
                                            </option>
                                            <?php   
                                        }  
                                    } 
                                    else
                                    {
                                        echo " ";
                                    }
                                    ?>
                                </select>
                            </div>
                          <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">This Series Is Trending?
                                        <span class="text-red">*</span></label>
                                    <div class="form-check">
                                        <input type="checkbox" name="is_trending_series" value="1"  class="form-check-input" id="edit_trending_series">
                                        <label class="form-check-label" for="edit_trending_series">Trending Series</label>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="col-md-6 form-group">
                                <label for="bannername" class="form-label">Test Series Type
                                    <span class="text-red">*</span></label>

                                <select name="test_series_type" class="form-control"  id="select_price">
                                    <option value="" selected disable>Select Series Type </option>
                                    <option value="1">Free</option>
                                    <option value="2">Paid</option>
                                </select>
                            </div>

                            <div class="col-md-6 form-group" id="type1" >
                                <label for="bannername" class="form-label">Test Series Price 
                                    <span class="text-red">*</span></label>

                                <input type="text" class="form-control" name="price" id="price" placeholder="Enter Test Series Price" >
                            </div>
                            <div class="col-md-6 form-group" id="type_1" >
                                <label for="bannername" class="form-label">Test Series Offers (%)</label>
                                <input type="text" class="form-control" name="offer" id="offer" placeholder="Enter Test Series Offers" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Upload Icon<span class="text-red"> *</span></label>
                                    <div class="col-ting">
                                      <div class="control-group file-upload" id="file-upload1">
                                        <div class="image-box text-center">
                                            <img src="<?php echo base_url('assets/images/no_file.png');?>" alt="img">
                                        </div>
                                        <div class="controls" style="display: none;">
                                            <input type="file" name="icon_image" accept="image/*"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Upload Banner<span class="text-red"> *</span></label>
                                        <div class="col-ting">
                                          <div class="control-group file-upload" id="file-upload1">
                                            <div class="image-box text-center">
                                              <img src="<?php echo base_url('assets/images/no_file.png');?>" alt="img">
                                            </div>
                                            <div class="controls" style="display: none;">
                                              <input type="file" name="banner_image" accept="image/*" />
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-12 form-group">
                                <label for="bannername" class="form-label">Description
                                    <span class="text-red">*</span></label>
                                <textarea id="summernote" class="summernote" name="description" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="add_test_series" class="btn save-btn">Add Test Series</button>
                        <button class="btn cancel-btn" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- add test series -->

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>
    
    <script>
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 100,
            disableResizeEditor: true,
        });
        $('#summ_des').summernote(
            {
            height: 500,
            disableResizeEditor: true,

        });
        $('#summ_des').summernote('disable');
    });
    </script>
    <script>
    $('#select_price').on('change', function() {
        if (this.value == 0) {
            $('#type1').hide().find(':input').attr('required', false);
            $('#type_1').hide();
        }
        if (this.value == 1) {
            $('#type1').hide().find(':input').attr('required', false);
          $('#type_1').hide();
        }
        if (this.value == 2) {
            $("#type1").show().find(':input').attr('required', true);
          $("#type_1").show();
        }
    });
    $('#select_price_1').on('change', function() {
        if (this.value == 0) {
            $('#type2').hide().find(':input').attr('required', false);
          $('#type_2').hide();
        }
        if (this.value == 1) {
            $('#type2').hide().find(':input').attr('required', false);
          $('#type_2').hide();
        }
        if (this.value == 2) {
            $("#type2").show().find(':input').attr('required', true);
          $("#type_2").show();
        }
    });
    </script>
    <script>
        function show_typewise() {
            var e = document.getElementById("typeop");
            var strUser = e.options[e.selectedIndex].value;
            var div1 = document.getElementById("type1");
            if (strUser == 1) {
                div1.style.display = "none";
            }
            if (strUser == 2) {
                div1.style.display = "block";
            }

        }

        function show_typewise1() {
            var e = document.getElementById("typeop1");
            var strUser = e.options[e.selectedIndex].value;
            var div1 = document.getElementById("type_1");
            if (strUser == 1) {
                div1.style.display = "none";
            }
            if (strUser == 2) {
                div1.style.display = "block";
            }

        }
    </script>

    <!-- status active-deactive -->
    <script type="text/javascript"> 
    function change_status(idd)
    {
        var status=idd.value;
        var test_series_id= idd.id;
        var base_url = '<?php echo base_url() ?>';
        var test_series_id = test_series_id;

        if(status != '')
        {
            $.ajax({
                      url:base_url + "MainController/active_test_series_status",
                      method:"POST",
                      data:{status:status,test_series_id:test_series_id},
                      
                      success: function(data)
                      {
                         // alert(data);
                          if (data == 1) 
                           {
                               alert('Status Changed Sucessfully !..');
                               location.reload();
                           } 
                           else 
                           {
                               alert('Something Went Wrong !...')
                           }
                      }
                });
        }
    }
    </script>

    <script type="text/javascript">
        const myTimeout = setTimeout(close, 2000);

        function close() 
        {
            document.getElementById("alert_msg").style.display = "none";
        }
    </script>

    <script>
      $(function(){
        var dtToday = new Date();
        
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        
        var maxDate = year + '-' + month + '-' + day;

        // or instead:
        // var maxDate = dtToday.toISOString().substr(0, 10);

        //alert(maxDate);
        $('.s_date').attr('min', maxDate);
    });
      </script>
</body>

</html>