<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>True Learning | Result </title>

    <?php $this->load->view('css'); ?>
</head>

<!-- <body class="app sidebar-mini light-mode default-sidebar"> -->

<body class="app sidebar-mini light-mode light-sidebar">
    <div class="wrapper">
        <?php $this->load->view('header'); ?>
        <section class="content">
            <div class="app-content main-content">
                <div class="side-app">

                    <!--Page header-->
                    <div class="page-header">
                        <div class="page-leftheader">
                            <!-- <span> <a class="button-effect effect effect-4" href="#" >Back</a><h4 class="page-title">About Us</h4></span> -->
                            <span class="d-flex">
                                <a class="back-icon icon-fill" href="<?php echo base_url('home'); ?>"><i
                                        class="fa fa-angle-left"></i></a>
                                <h4 class="page-title">Result - Test Name</h4>
                            </span>

                        </div>
                        <div class="page-rightheader ml-auto d-lg-flex d-none">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('home'); ?>" class="d-flex align-items-center ">

                                        <i class="breadcrumb-item-icon fa fa-home"></i>
                                        <span class="breadcrumb-icon"> Home</span></a>
                                </li>
                                <li class="breadcrumb-item ">
                                    <a href="<?php echo base_url('manage_test'); ?>" >

                                        
                                        <span class="breadcrumb-icon"> Test Management</span></a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Result</li>

                            </ol>
                        </div>
                    </div>
                    <!--End Page header-->

                    <!--Row-->
                    <div class="row justify-content-around">
                        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                            <div class="card ">
                                <div class="card-header">
                                    <div class="card-title">Test Result
                                    </div>
                                    <!-- <div class="card-options">
                                        <a type="button" onclick="generateExcel('Report')" id="excel" class="btn btn-icon btn-primary" 
                                            >Excel</a>
                                    </div> -->
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive ">
                                        <!-- <div class="table-responsive" id="lightgallery"> -->
                                        

                                        <table class="table table-hover border table-vcenter  text-nowrap category-table" id="table-data">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="wd-15p border-bottom-0">
                                                        Sr. No.</th>
                                                    <th class="wd-15p border-bottom-0">
                                                        User Name
                                                    </th>
                                                    <th class="wd-20p border-bottom-0">
                                                        Test Date
                                                    </th>
                                                    <th class="wd-15p border-bottom-0">
                                                        Score out of 100
                                                    </th>
                                                    <th class="wd-15p border-bottom-0">
                                                        Attempt Duration(30:00 min)
                                                    </th>
                                                    
                                                    <th class="wd-20p border-bottom-0">
                                                        Percentage
                                                    </th>
                                                    <th class="wd-20p border-bottom-0 action">
                                                        Action</th>
                                                </tr>
                                            </thead>
    <tbody class="text-center">
        <?php  
        // print_r($test_result);
        if (!empty($test_result)) 
        { 
            $i =1;
            foreach ($test_result as $res) 
            {
                $wh_u = '(uid="'.$res['uid'].'")';
                $user = $this->Main_Model->getData($tbl='user_details',$wh_u);
                ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $user['full_name']; ?></td>
                    <td>11-07-2022</td>
                    <td>1000</td>
                    <td>25:00</td>
                    <td>
                        <div class="mx-auto chart-circle chart-circle-xs chart-circle-secondary mt-sm-0 mb-0 icon-dropshadow-secondary"
                            data-value="0.45" data-thickness="5" data-color="#f72d66">
                            <div class="mx-auto chart-circle-value text-center">45%
                            </div>
                        </div>

                    </td>
                    <td class="action">
                        <!-- <a type="button" class="btn btn-sm btn-icon btn-warning"><i
                                class="fa fa-download"></i></a> -->
                        <a type="button" class="btn btn-sm btn-icon btn-primary" data-target="#view_report" data-toggle="modal">
                            <i class="fa fa-eye"></i></a>
                       

                    </td>
                </tr>
                <?php
            }
        }
        else
        {
            ?>
            <tr>
                <td colspan="7" class="text-center"><?php    echo "Record not available. "; ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
                                        </table>
                                    </div>
                                    
                                    <!-- Pagination -->
                                    <div class="table-footer align-items-center">
                                        <!-- <p class="">Showing 1 to 3 of 10 entries</p> -->
                                        <nav class="" aria-label="Page navigation example">
                                            <ul class="pagination justify-content-end">
                                                <li class="page-item disabled">
                                                    <a class="page-link" href="#" tabindex="-1">
                                                        <i class="fa fa-angle-left"></i>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item active">
                                                    <a class="page-link" href="#">1</a>
                                                </li>
                                                <li class="page-item ">
                                                    <a class="page-link" href="#">2</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="#">3</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="#">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end app-content-->
        </section>
        <div class="modal" id="view_report">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title ">View Report</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="result-details text-left">
                        <div class="row align-items-center justify-content-center mb-4">
                            <div class="col-md-2 col-2 text-center">
                                <p class="icon icon-shape bg-info rounded-circle text-white mb-0 mr-3">
                                    <i class="fa fa-question fa-lg text-white" aria-hidden="true"></i>
                                </p>
                            </div>
                            <div class="col-md-6 col-6">
                                <p>Total Questions</p>
                            </div>
                            <div class="col-md-2 col-2">
                                <p>100</p>
                            </div>
                        </div>
                        <div class="row align-items-center justify-content-center mb-4">
                            <div class="col-md-2 col-2 text-center">
                                <!-- <i class="fa fa-check fa-lg" aria-hidden="true"></i> -->
                                <p class="icon icon-shape bg-success rounded-circle text-white mb-0 mr-3">
                                    <i class="fa fa-check fa-lg text-white" aria-hidden="true"></i>
                                </p>
                            </div>
                            <div class="col-md-6 col-6">
                                <p>Correct Answers</p>
                            </div>
                            <div class="col-md-2 col-2">
                                <p>90</p>
                            </div>
                        </div>
                        <div class="row align-items-center justify-content-center mb-4">
                            <div class="col-md-2 col-2 text-center">
                                <!-- <i class="fa fa-times fa-lg" aria-hidden="true"></i> -->
                                <p class="icon icon-shape bg-secondary rounded-circle text-white mb-0 mr-3">
                                    <i class="fa fa-times fa-lg text-white" aria-hidden="true"></i>
                                </p>
                            </div>
                            <div class="col-md-6 col-6">
                                <p>Incorrect Answers</p>
                            </div>
                            <div class="col-md-2 col-2">
                                <p>5</p>
                            </div>
                        </div>
                        <div class="row align-items-center justify-content-center mb-4">
                            <div class="col-md-2 col-2 text-center">
                                <p class="icon icon-shape bg-info rounded-circle text-white mb-0 mr-3">
                                    <i class="fa fa-minus fa-lg text-white" aria-hidden="true"></i>
                                </p>
                            </div>
                            <div class="col-md-6 col-6">
                                <p>Negative Answers</p>
                            </div>
                            <div class="col-md-2 col-2">
                                <p>5</p>
                            </div>
                        </div>
                        <div class="row align-items-center justify-content-center mb-4">
                            <div class="col-md-2 col-2 text-center">
                                <!-- <i class="fa fa-exclamation fa-lg" aria-hidden="true"></i> -->
                                <p class="icon icon-shape bg-warning rounded-circle text-white mb-0 mr-3">
                                    <i class="fa fa-exclamation fa-lg text-white" aria-hidden="true"></i>
                                </p>
                            </div>
                            <div class="col-md-6 col-6">
                                <p>Unattempted Questions</p>
                            </div>
                            <div class="col-md-2 col-2">
                                <p>5</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    </div>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('js'); ?>

    <script>
    // $("#excel").click(function(e) {
    //     $('#table-data').find('th.action, td.action').remove();
    //     window.open('data:application/vnd.ms-excel,' +
    //         '<table>' + $('#table-data > table').html() + '</table>');
    //     e.preventDefault();
    // });

    function generateExcel(name) {
        //getting data from our table
        $('#table-data').find('th.action, td.action').remove();
        var data_type = "data:application/vnd.ms-excel";
        var table_div = document.getElementById("table-data");
        var table_html = table_div.outerHTML.replace(/ /g, "%20");

        var a = document.createElement("a");
        a.href = data_type + ", " + table_html;
        a.download = name + ".xls";
        a.click();
        window.location.reload();
    }
    </script>
    <script>
    document.getElementById('delete').onclick = function() {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: "No, cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal(
                        "Deleted!",
                        "Your  file has been deleted!",
                        "success");
                } else {
                    swal(
                        "Cancelled",
                        "Your  file is safe !",
                        "error"
                    );
                }
            });
    };
    </script>

    
</body>

</html>