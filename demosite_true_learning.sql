-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2022 at 05:28 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demosite_true_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `heading` mediumtext NOT NULL,
  `about_us` mediumtext NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `heading`, `about_us`, `updated_at`) VALUES
(1, 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene', '<h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p>', '2022-08-26 09:59:30');

-- --------------------------------------------------------

--
-- Table structure for table `answers_integer`
--

CREATE TABLE `answers_integer` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1-Single Choice,2-Multiple Choice,3-Integer,4-True / False,5-Match Matrix,6-Match The Following',
  `correct_answer` varchar(255) NOT NULL,
  `solution` varchar(10000) NOT NULL,
  `video_solution` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_integer`
--

INSERT INTO `answers_integer` (`answer_id`, `question_id`, `answer_type`, `correct_answer`, `solution`, `video_solution`, `created_at`, `updated_at`) VALUES
(3, 9, 3, 'demo ans', 'demo solution', 'demo video link google.com', '2022-09-14 12:19:54', '2022-09-14 06:49:54');

-- --------------------------------------------------------

--
-- Table structure for table `answers_match_matrix`
--

CREATE TABLE `answers_match_matrix` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1-Single Choice,2-Multiple Choice,3-Integer,4-True / False,5-Match Matrix,6-Match The Following',
  `left_opt_1` varchar(255) NOT NULL,
  `left_opt_2` varchar(255) NOT NULL,
  `left_opt_3` varchar(255) NOT NULL,
  `left_opt_4` varchar(255) NOT NULL,
  `right_opt_A` varchar(255) NOT NULL,
  `right_opt_B` varchar(255) NOT NULL,
  `right_opt_C` varchar(255) NOT NULL,
  `right_opt_D` varchar(255) NOT NULL,
  `solution` varchar(10000) NOT NULL,
  `video_solution` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_match_matrix`
--

INSERT INTO `answers_match_matrix` (`answer_id`, `question_id`, `answer_type`, `left_opt_1`, `left_opt_2`, `left_opt_3`, `left_opt_4`, `right_opt_A`, `right_opt_B`, `right_opt_C`, `right_opt_D`, `solution`, `video_solution`, `created_at`, `updated_at`) VALUES
(1, 14, 5, 'A1', 'B1', 'C1', 'D1', 'd1x', 'c1x', 'b1x', 'a1x', 'demo solution', 'https://www.google.com/', '2022-09-14 12:20:24', '2022-10-12 09:09:25'),
(2, 15, 5, 'P1up 5', 'Q1up 5', 'R1up 5', 'S1up 5', 'p1up 5', 'q1up 5', 'r1up 5', 's1up 5', 'up 5', 'up 5', '2022-09-14 12:20:25', '2022-09-21 07:26:51');

-- --------------------------------------------------------

--
-- Table structure for table `answers_match_the_following`
--

CREATE TABLE `answers_match_the_following` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1-Single Choice,2-Multiple Choice,3-Integer,4-True / False,5-Match Matrix,6-Match The Following',
  `left_opt_1` varchar(255) NOT NULL,
  `left_opt_2` varchar(255) NOT NULL,
  `left_opt_3` varchar(255) NOT NULL,
  `left_opt_4` varchar(255) NOT NULL,
  `right_opt_A` varchar(255) NOT NULL,
  `right_opt_B` varchar(255) NOT NULL,
  `right_opt_C` varchar(255) NOT NULL,
  `right_opt_D` varchar(255) NOT NULL,
  `solution` varchar(10000) NOT NULL,
  `video_solution` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_match_the_following`
--

INSERT INTO `answers_match_the_following` (`answer_id`, `question_id`, `answer_type`, `left_opt_1`, `left_opt_2`, `left_opt_3`, `left_opt_4`, `right_opt_A`, `right_opt_B`, `right_opt_C`, `right_opt_D`, `solution`, `video_solution`, `created_at`, `updated_at`) VALUES
(1, 16, 6, 'P1', 'Q1', 'R1', 'S1', 'p1', 'r1', 'q1', 's1', 'demo solution', 'https://www.google.com/', '2022-09-14 12:20:59', '2022-09-14 06:50:59');

-- --------------------------------------------------------

--
-- Table structure for table `answers_multiple_choice`
--

CREATE TABLE `answers_multiple_choice` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1-Single Choice,2-Multiple Choice,3-Integer,4-True / False,5-Match Matrix,6-Match The Following',
  `options_A` varchar(255) NOT NULL,
  `options_B` varchar(255) NOT NULL,
  `options_C` varchar(255) NOT NULL,
  `options_D` varchar(255) NOT NULL,
  `solution` varchar(10000) NOT NULL,
  `video_solution` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_multiple_choice`
--

INSERT INTO `answers_multiple_choice` (`answer_id`, `question_id`, `answer_type`, `options_A`, `options_B`, `options_C`, `options_D`, `solution`, `video_solution`, `created_at`, `updated_at`) VALUES
(6, 23, 2, '<p>Questions1<br></p>', '<p>Questions2<br></p>', '<p>Questions3<br></p>', '<p>Questions4<br></p>', 'demo solution', 'demo video link google.com', '2022-09-21 17:57:14', '2022-09-22 11:24:17'),
(9, 26, 2, '<p>google</p>', '<p>instagram</p>', '<p>youtube</p>', '<p>facebook</p>', '<p>demoq</p>', 'demo1', '2022-09-22 10:28:27', '2022-09-22 07:16:16'),
(10, 34, 2, '<ul style=\"box-sizing: inherit; color: rgb(0, 0, 0); font-family: Verdana, sans-serif; font-size: 15px;\"><li style=\"box-sizing: inherit;\">HTML stands for Hyper Text Markup Language</li></ul>', '<ul style=\"box-sizing: inherit; color: rgb(0, 0, 0); font-family: Verdana, sans-serif; font-size: 15px;\"><li style=\"box-sizing: inherit;\">HTML elements label pieces of content such as \"this is a heading\", \"this is a paragraph\", \"this is a link\", etc.</li>', '<p>Other</p>', '<p>Nine</p>', '<p><span style=\"color: rgb(3, 3, 3); font-family: Roboto, Arial, sans-serif; white-space: pre-wrap; background-color: rgb(249, 249, 249);\">HTML is understood by all browsers, from browsers on phones, to tablets to desktop computers. That\'s why every website and web app you use is made using HTML – it\'s the language of the web!</span><br></p>', 'https://www.youtube.com/watch?v=W-6OY9eI3hk', '2022-10-11 15:01:40', '2022-10-11 09:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `answers_option_match_matrix`
--

CREATE TABLE `answers_option_match_matrix` (
  `opt_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '5-Match Matrix',
  `answer_id` int(11) NOT NULL,
  `correct_options_l` int(11) NOT NULL,
  `correct_options_r` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_option_match_matrix`
--

INSERT INTO `answers_option_match_matrix` (`opt_id`, `question_id`, `answer_type`, `answer_id`, `correct_options_l`, `correct_options_r`, `created_at`, `updated_at`) VALUES
(38, 15, 5, 2, 1, 'A', '2022-09-22 15:57:28', '2022-09-22 10:27:28'),
(39, 15, 5, 2, 1, 'B', '2022-09-22 15:57:28', '2022-09-22 10:27:28'),
(40, 15, 5, 2, 1, 'C', '2022-09-22 15:57:28', '2022-09-22 10:27:28'),
(41, 15, 5, 2, 1, 'D', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(42, 15, 5, 2, 2, 'A', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(43, 15, 5, 2, 2, 'B', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(44, 15, 5, 2, 2, 'C', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(45, 15, 5, 2, 2, 'D', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(46, 15, 5, 2, 3, 'A', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(47, 15, 5, 2, 3, 'B', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(48, 15, 5, 2, 3, 'C', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(49, 15, 5, 2, 3, 'D', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(50, 15, 5, 2, 4, 'A', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(51, 15, 5, 2, 4, 'B', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(52, 15, 5, 2, 4, 'C', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(53, 15, 5, 2, 4, 'D', '2022-09-22 15:57:29', '2022-09-22 10:27:29'),
(54, 14, 5, 1, 1, 'D', '2022-09-22 16:54:55', '2022-09-22 11:24:55'),
(55, 14, 5, 1, 2, 'C', '2022-09-22 16:54:55', '2022-09-22 11:24:55'),
(56, 14, 5, 1, 3, 'B', '2022-09-22 16:54:55', '2022-09-22 11:24:55'),
(57, 14, 5, 1, 4, 'A', '2022-09-22 16:54:55', '2022-09-22 11:24:55');

-- --------------------------------------------------------

--
-- Table structure for table `answers_option_match_the_following`
--

CREATE TABLE `answers_option_match_the_following` (
  `opt_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '6-Match The Following',
  `answer_id` int(11) NOT NULL,
  `correct_options_l` int(11) NOT NULL,
  `correct_options_r` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_option_match_the_following`
--

INSERT INTO `answers_option_match_the_following` (`opt_id`, `question_id`, `answer_type`, `answer_id`, `correct_options_l`, `correct_options_r`, `created_at`, `updated_at`) VALUES
(1, 16, 6, 1, 1, 'A', '2022-09-14 12:20:59', '2022-09-14 06:50:59'),
(2, 16, 6, 1, 2, 'B', '2022-09-14 12:20:59', '2022-09-14 06:50:59'),
(3, 16, 6, 1, 3, 'C', '2022-09-14 12:20:59', '2022-09-14 06:50:59'),
(4, 16, 6, 1, 4, 'D', '2022-09-14 12:20:59', '2022-09-14 06:50:59');

-- --------------------------------------------------------

--
-- Table structure for table `answers_option_multiple_choice`
--

CREATE TABLE `answers_option_multiple_choice` (
  `opt_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '2-Multiple Choice',
  `answer_id` int(11) NOT NULL,
  `correct_answer` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_option_multiple_choice`
--

INSERT INTO `answers_option_multiple_choice` (`opt_id`, `question_id`, `answer_type`, `answer_id`, `correct_answer`, `created_at`, `updated_at`) VALUES
(28, 23, 2, 6, 'A', '2022-09-22 16:54:17', '2022-09-22 11:24:17'),
(29, 23, 2, 6, 'B', '2022-09-22 16:54:17', '2022-09-22 11:24:17'),
(30, 23, 2, 6, 'C', '2022-09-22 16:54:17', '2022-09-22 11:24:17'),
(31, 23, 2, 6, 'D', '2022-09-22 16:54:17', '2022-09-22 11:24:17'),
(32, 26, 2, 9, 'A', '2022-09-22 16:58:00', '2022-09-22 11:28:00'),
(33, 26, 2, 9, 'B', '2022-09-22 16:58:00', '2022-09-22 11:28:00'),
(34, 26, 2, 9, 'C', '2022-09-22 16:58:00', '2022-09-22 11:28:00'),
(35, 34, 2, 10, 'A', '2022-10-11 15:01:40', '2022-10-11 09:31:40'),
(36, 34, 2, 10, 'B', '2022-10-11 15:01:40', '2022-10-11 09:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `answers_single_choice`
--

CREATE TABLE `answers_single_choice` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL,
  `options_A` varchar(255) NOT NULL,
  `options_B` varchar(255) NOT NULL,
  `options_C` varchar(255) NOT NULL,
  `options_D` varchar(255) NOT NULL,
  `correct_answer` varchar(255) NOT NULL,
  `solution` varchar(10000) NOT NULL,
  `video_solution` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_single_choice`
--

INSERT INTO `answers_single_choice` (`answer_id`, `question_id`, `answer_type`, `options_A`, `options_B`, `options_C`, `options_D`, `correct_answer`, `solution`, `video_solution`, `created_at`, `updated_at`) VALUES
(3, 3, 1, 'xy', 'pq', 'mn', 'ij', 'D', 'demo solution', 'https://www.google.com/', '2022-09-14 12:19:18', '2022-09-22 11:23:52');

-- --------------------------------------------------------

--
-- Table structure for table `answers_true_false`
--

CREATE TABLE `answers_true_false` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1-Single Choice,2-Multiple Choice,3-Integer,4-True / False,5-Match Matrix,6-Match The Following',
  `correct_answer` varchar(255) NOT NULL COMMENT '1-True,2-False',
  `solution` varchar(10000) NOT NULL,
  `video_solution` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_true_false`
--

INSERT INTO `answers_true_false` (`answer_id`, `question_id`, `answer_type`, `correct_answer`, `solution`, `video_solution`, `created_at`, `updated_at`) VALUES
(2, 12, 4, '1', 'demo solution', 'demo video link google.com', '2022-09-14 12:20:06', '2022-09-14 06:50:06');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `title`, `banner_image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'experiences, objects, or imaginings', 'http://localhost/true_learning_dashboard/assets/images/admin/banner/ezgif_com-gif-maker.gif', 1, '2022-08-29 10:23:38', '2022-09-28 09:50:31'),
(5, 'The Purpose of Description in Writing', 'http://localhost/true_learning_dashboard/assets/images/admin/banner/dummy.png', 1, '2022-08-29 10:30:35', '2022-10-03 04:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

CREATE TABLE `board` (
  `board_id` int(11) NOT NULL,
  `board_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `board`
--

INSERT INTO `board` (`board_id`, `board_name`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Maharashtra Board', 1, '2022-08-25 11:12:23', '2022-08-26 06:57:57'),
(9, 'CBSE Board', 1, '2022-08-26 09:51:29', '2022-08-26 06:57:59');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `cat_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `cat_image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'JEE', 'http://localhost/true_learning_dashboard/assets/images/admin/category/jee.png', 1, '2022-08-25 10:50:21', '2022-08-26 06:36:47'),
(2, 'NEET', 'http://localhost/true_learning_dashboard/assets/images/admin/category/neett.jpg', 1, '2022-08-25 10:50:28', '2022-08-25 05:20:28'),
(4, 'NDA', 'http://localhost/true_learning_dashboard/assets/images/admin/category/nda.jpg', 1, '2022-09-06 11:02:25', '2022-09-06 05:32:25'),
(5, 'Development & Design', 'http://localhost/true_learning_dashboard/assets/images/admin/category/web.jpg', 1, '2022-09-28 16:16:53', '2022-09-28 10:46:53');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `phone`, `email`, `address`, `created_at`, `updated_at`) VALUES
(1, '9930112562', 'info@truelearning.co.in', '<p>M.K. Educational Services LLP, Kanpur, Uttar Pradesh (INDIA)</p>', '2022-08-26 15:45:03', '2022-09-28 05:54:42');

-- --------------------------------------------------------

--
-- Table structure for table `difficulty_level`
--

CREATE TABLE `difficulty_level` (
  `difficulty_id` int(11) NOT NULL,
  `difficulty_medium` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `difficulty_level`
--

INSERT INTO `difficulty_level` (`difficulty_id`, `difficulty_medium`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Easy', 1, '2022-08-26 14:31:39', '2022-08-26 09:14:12'),
(3, 'Medium', 1, '2022-08-26 14:39:14', '2022-08-26 09:09:14'),
(4, 'Hard', 1, '2022-08-26 14:44:16', '2022-08-26 09:14:16');

-- --------------------------------------------------------

--
-- Table structure for table `import_question`
--

CREATE TABLE `import_question` (
  `imp_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `import_question`
--

INSERT INTO `import_question` (`imp_id`, `test_id`, `section_id`, `question_id`, `status`, `created_at`, `updated_at`) VALUES
(16, 6, 13, 12, 1, '2022-09-23 12:58:44', '2022-09-23 07:28:44'),
(17, 6, 13, 9, 1, '2022-09-23 12:58:44', '2022-09-23 07:28:44'),
(21, 2, 12, 23, 1, '2022-09-23 15:59:19', '2022-09-23 10:29:19'),
(22, 2, 12, 16, 1, '2022-09-23 15:59:19', '2022-09-23 10:29:19'),
(26, 6, 9, 23, 1, '2022-09-23 16:50:06', '2022-09-23 11:20:06'),
(27, 6, 9, 16, 1, '2022-09-23 16:50:06', '2022-09-23 11:20:06'),
(28, 6, 9, 15, 1, '2022-09-23 16:50:06', '2022-09-23 11:20:06'),
(29, 6, 9, 26, 1, '2022-09-23 16:51:25', '2022-09-23 11:21:25'),
(30, 6, 9, 14, 1, '2022-09-23 16:51:26', '2022-09-23 11:21:26'),
(31, 2, 12, 23, 1, '2022-09-23 16:54:12', '2022-09-23 11:24:12'),
(32, 5, 6, 26, 1, '2022-09-23 17:07:47', '2022-09-23 11:37:47'),
(33, 5, 6, 23, 1, '2022-09-23 17:07:47', '2022-09-23 11:37:47'),
(34, 5, 6, 16, 1, '2022-09-23 17:07:47', '2022-09-23 11:37:47'),
(35, 5, 6, 15, 1, '2022-09-23 17:07:47', '2022-09-23 11:37:47'),
(36, 5, 6, 14, 1, '2022-09-23 17:07:47', '2022-09-23 11:37:47'),
(37, 7, 15, 12, 1, '2022-09-26 11:03:19', '2022-09-26 05:33:19'),
(38, 7, 15, 3, 1, '2022-09-26 11:03:19', '2022-09-26 05:33:19'),
(39, 7, 15, 23, 1, '2022-09-26 12:20:56', '2022-09-26 06:50:56'),
(40, 7, 15, 16, 1, '2022-09-26 12:20:56', '2022-09-26 06:50:56'),
(41, 7, 15, 14, 1, '2022-09-26 12:20:56', '2022-09-26 06:50:56'),
(42, 7, 15, 9, 1, '2022-09-26 12:20:56', '2022-09-26 06:50:56'),
(43, 7, 16, 26, 1, '2022-09-26 14:20:50', '2022-09-26 08:50:50'),
(44, 7, 16, 23, 1, '2022-09-26 14:20:50', '2022-09-26 08:50:50'),
(45, 8, 17, 26, 1, '2022-09-29 10:02:12', '2022-09-29 04:32:12'),
(46, 8, 17, 3, 1, '2022-09-29 10:02:12', '2022-09-29 04:32:12'),
(53, 9, 20, 34, 1, '2022-10-12 14:20:54', '2022-10-12 08:50:54'),
(54, 9, 20, 26, 1, '2022-10-12 14:20:54', '2022-10-12 08:50:54'),
(55, 9, 20, 16, 1, '2022-10-12 14:20:54', '2022-10-12 08:50:54'),
(56, 9, 20, 14, 1, '2022-10-12 14:20:54', '2022-10-12 08:50:54'),
(57, 9, 20, 12, 1, '2022-10-12 14:20:55', '2022-10-12 08:50:55'),
(58, 9, 20, 9, 1, '2022-10-12 14:20:55', '2022-10-12 08:50:55'),
(59, 9, 20, 3, 1, '2022-10-12 14:20:55', '2022-10-12 08:50:55');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `lang_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`lang_id`, `language_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'English', 1, '2022-08-26 12:48:06', '2022-08-26 07:33:24'),
(3, 'Hindi', 1, '2022-08-26 12:50:58', '2022-08-26 07:33:30'),
(4, 'Marathi', 1, '2022-09-19 16:17:45', '2022-09-19 10:47:45'),
(5, 'Tamil', 1, '2022-09-22 16:51:34', '2022-09-22 11:21:34');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `test_series_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `send_to` varchar(255) NOT NULL,
  `send_date` date NOT NULL,
  `notification_for` int(11) NOT NULL COMMENT '2- student/user',
  `notification_get_flag` int(11) NOT NULL COMMENT '1-send/received notify, 0-seen notify',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `uid`, `test_series_id`, `subject`, `description`, `send_to`, `send_date`, `notification_for`, `notification_get_flag`, `created_at`, `updated_at`) VALUES
(3, 6, 6, 'Test Series Purchased', 'Your Selected Test Series Purchased successfully on 20-09-2022', 'Mahadev Ingawale', '2022-09-20', 2, 0, '2022-09-20 10:47:29', '2022-09-27 11:21:18'),
(4, 6, 5, 'Test Series Purchased', 'Your Selected Test Series Purchased successfully on 20-09-2022', 'Mahadev Ingawale', '2022-09-20', 2, 0, '2022-09-20 10:51:27', '2022-09-27 11:19:21'),
(5, 6, 14, 'Test Series Purchased', 'Your Selected Test Series Purchased successfully on 30-09-2022', 'Mahadev Ingawale', '2022-09-30', 2, 0, '2022-09-30 10:27:10', '2022-09-30 06:37:27'),
(6, 6, 15, 'Test Series Purchased', 'Your Selected Test Series Purchased successfully on 30-09-2022', 'Mahadev Ingawale', '2022-09-30', 2, 0, '2022-09-30 10:41:56', '2022-09-30 06:37:27'),
(7, 7, 15, 'Test Series Purchased', 'Your Selected Test Series Purchased successfully on 30-09-2022', 'mahadev123', '2022-09-30', 2, 1, '2022-09-30 10:59:26', '2022-09-30 05:29:26'),
(8, 6, 12, 'Test Series Purchased', 'Your Selected Test Series Purchased successfully on \'.date(\'d-m-Y\')', 'Mahadev Ingawale', '2022-10-03', 2, 0, '2022-10-03 15:11:54', '2022-10-03 10:00:14');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `o_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `test_series_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `offer` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL COMMENT '1:-wallet, 2:-online',
  `payment_date` date NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL COMMENT '0:-Fail, 1:-Success',
  `payment_mode` varchar(255) NOT NULL,
  `buy_series` int(11) NOT NULL COMMENT '0:-No, 1:-Yes',
  `completed_series` int(11) NOT NULL COMMENT '0:-No, 1:-Yes',
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`o_id`, `uid`, `test_series_id`, `category_id`, `offer`, `amount`, `payment_type`, `payment_date`, `transaction_id`, `order_id`, `payment_status`, `payment_mode`, `buy_series`, `completed_series`, `full_name`, `email`, `mobile_no`, `created_at`, `updated_at`) VALUES
(1, 6, 14, 1, '10', '899', 2, '2022-09-30', 'txt21%4hh9dsxa', 'ord_1dh8u93u', 1, 'upi', 1, 0, 'Mahadev Ingawale', 'ingawalemr12@gmail.com', '9970410333', '2022-09-30 10:27:10', '2022-09-30 05:10:42'),
(2, 6, 15, 4, '00', '1499', 2, '2022-09-30', 'txt454hh9dsxcs', 'ord_1dh8u93u', 1, 'upi', 1, 0, 'Mahadev Ingawale', 'ingawalemr12@gmail.com', '9970410333', '2022-09-30 10:41:55', '2022-09-30 09:15:28'),
(3, 7, 15, 4, '00', '1499', 2, '2022-09-30', 'tx32h9svsv', 'ord_1dh8u93u', 1, 'upi', 1, 0, 'mahadev123', 'mingawale02@gmail.com', '7770410333', '2022-09-30 10:59:25', '2022-09-30 05:29:25'),
(4, 6, 12, 5, '10', '809', 2, '2022-10-03', 'txt_12$#rb2673y3', 'ord_1d23434u93u', 1, 'upi', 1, 0, 'Mahadev Ingawale', 'ingawalemr12@gmail.com', '9970410333', '2022-10-03 15:11:54', '2022-10-03 09:41:54');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy`
--

CREATE TABLE `privacy_policy` (
  `id` int(11) NOT NULL,
  `privacy_policy` mediumtext NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `privacy_policy`
--

INSERT INTO `privacy_policy` (`id`, `privacy_policy`, `updated_at`) VALUES
(1, '<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</span>', '2022-08-26 10:51:30');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `answer_type` int(11) NOT NULL COMMENT '1-Single Choice,2-Multiple Choice,3-Integer,4-True / False,5-Match Matrix,6-Match The Following',
  `std_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `sub_topic` varchar(255) NOT NULL,
  `difficulty_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `correct_marks` int(11) NOT NULL,
  `negative_marks` int(11) NOT NULL,
  `not_attempt_marks` int(11) NOT NULL,
  `question_name` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `answer_type`, `std_id`, `subject_id`, `topic`, `sub_topic`, `difficulty_id`, `lang_id`, `correct_marks`, `negative_marks`, `not_attempt_marks`, `question_name`, `images`, `status`, `created_at`, `updated_at`) VALUES
(3, 1, 9, 8, 'demo-topic', 'demo sub-topic', 3, 1, 3, 2, 1, 'What was the name of Blackbeard / Edward Teach\'s ship?', 'http://localhost/true_learning_dashboard/assets/images/admin/questions/1378793.jpg', 1, '2022-09-14 12:19:18', '2022-09-22 11:23:52'),
(9, 3, 1, 9, 'demo-topic', 'demo sub-topic', 3, 1, 3, 2, 1, 'demo interger question 3 ?', 'http://localhost/true_learning_dashboard/assets/images/admin/questions/1378793.jpg', 1, '2022-09-14 12:19:54', '2022-09-22 11:24:28'),
(12, 4, 1, 9, 'demo-topic', 'demo sub-topic', 3, 1, 2, 2, 1, 'demo False question2 ?', 'http://localhost/true_learning_dashboard/assets/images/admin/questions/3296323.jpg', 1, '2022-09-14 12:20:06', '2022-09-22 11:24:40'),
(14, 5, 9, 8, 'demo-topic', 'demo sub-topic', 2, 1, 2, 2, 0, 'How the hello world?', 'http://localhost/true_learning_dashboard/assets/images/admin/questions/c1.jpg', 1, '2022-09-14 12:20:24', '2022-09-22 11:24:55'),
(15, 5, 1, 5, 'up 5', 'up 5', 4, 4, 1, 1, 1, 'up 5 ?', '', 1, '2022-09-14 12:20:25', '2022-09-21 07:25:10'),
(16, 6, 9, 8, 'demo-topic', 'demo sub-topic', 2, 1, 2, 2, 0, 'demo  hello world 1 ?', 'http://localhost/true_learning_dashboard/assets/images/admin/questions/dummy.png', 1, '2022-09-14 12:20:58', '2022-09-22 11:25:05'),
(23, 2, 1, 5, 'Recreations of experiences', 'Description encourages a more concrete or sensory experience of a subject', 3, 1, 1, 1, 1, '<p>demo MC Questions</p>', 'http://localhost/true_learning_dashboard/assets/images/admin/questions/1378793.jpg', 1, '2022-09-21 17:57:14', '2022-09-22 11:24:17'),
(26, 2, 9, 8, 'Recreations of experiences', '', 3, 4, 1, 1, 1, 'How did you find out about our product ?', 'http://localhost/true_learning_dashboard/assets/images/admin/questions/3296323.jpg', 1, '2022-09-22 10:28:27', '2022-10-11 11:46:08'),
(34, 2, 1, 5, 'Recreations of experiences', '', 2, 1, 2, 2, 0, '<p>What is HTML ?</p>', 'http://localhost/true_learning_dashboard/assets/images/admin/questions/dummy.png', 1, '2022-10-11 15:01:40', '2022-10-11 11:46:02');

-- --------------------------------------------------------

--
-- Table structure for table `recent_list`
--

CREATE TABLE `recent_list` (
  `r_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `test_series_id` int(11) NOT NULL,
  `is_recently_opened` int(11) NOT NULL COMMENT '1:-Yes, 0:-No	',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recent_list`
--

INSERT INTO `recent_list` (`r_id`, `uid`, `test_series_id`, `is_recently_opened`, `created_at`, `updated_at`) VALUES
(1, 6, 5, 1, '2022-09-19 11:34:06', '2022-09-19 06:04:06');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `section_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `instruction` varchar(10000) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`section_id`, `test_id`, `section_name`, `instruction`, `created_at`, `updated_at`) VALUES
(6, 5, 'Geology is a branch of natural science concerned with Earth', '<p><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">Also known as \'geoscience\' or \'Earth science\', geology is </span><b style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">the study of the structure, evolution and dynamics of the Earth and its natural mineral and energy resources</b><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">. Geology investigates the processes that have shaped the Earth through its 4500 million (approximate!)</span><br></p>', '2022-09-06 15:58:38', '2022-09-26 03:59:37'),
(9, 6, 'demo', '<p>r</p>', '2022-09-08 10:02:38', '2022-09-08 04:32:38'),
(10, 5, 'demo', '<p>demo2</p>', '2022-09-08 10:05:51', '2022-09-08 04:35:51'),
(12, 2, 'sec for test 2', '<p>demo</p>', '2022-09-08 10:44:44', '2022-09-08 05:14:44'),
(13, 6, 'Kids Poems In English', '<p>demo</p>', '2022-09-22 11:44:51', '2022-09-22 06:14:51'),
(14, 5, 'test-5', '<p>test<br></p>', '2022-09-23 11:43:38', '2022-09-23 06:13:38'),
(15, 7, 'Section 1 - India and Climate Change', '<p style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; font-family: AvertaStd, -apple-system, BlinkMacSystemFont, sans-serif; line-height: 24px; color: rgb(60, 72, 82); font-size: medium; background-color: rgb(252, 252, 252);\">This paper consists of 10 questions.</p><p style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; font-family: AvertaStd, -apple-system, BlinkMacSystemFont, sans-serif; line-height: 24px; color: rgb(60, 72, 82); font-size: medium; background-color: rgb(252, 252, 252);\">Each question carries 2 mark.</p><p style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; font-family: AvertaStd, -apple-system, BlinkMacSystemFont, sans-serif; line-height: 24px; color: rgb(60, 72, 82); font-size: medium; background-color: rgb(252, 252, 252);\">There is negative marking.</p><p style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin-right: 0px; margin-left: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; font-family: AvertaStd, -apple-system, BlinkMacSystemFont, sans-serif; line-height: 24px; color: rgb(60, 72, 82); font-size: medium; background-color: rgb(252, 252, 252);\">Test duration is of 30 mins.</p>', '2022-09-26 10:33:51', '2022-09-26 05:03:51'),
(16, 7, 'NDA 2022-Mock test 2', '<ol style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; font-family: AvertaStd, -apple-system, BlinkMacSystemFont, sans-serif !important;\"><li style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; line-height: 24px; color: var(--color-text-primary);\">This Test booklet consists of 200 questions out of which 180 questions can be attempted.&nbsp;</li><li style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; line-height: 24px; color: var(--color-text-primary);\">There are five parts in the question paper.&nbsp;Physics, Chemistry having two sections each i.e Section A and Section B&nbsp;and Biology.</li><li style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; line-height: 24px; color: var(--color-text-primary);\">For Physics and chemistry, all of the 35 questions from&nbsp;section A can be attempted. But from Section B, 10 out of 15 questions can be attempted as per the new NEET UG pattern.&nbsp;</li><li style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; line-height: 24px; color: var(--color-text-primary);\">Each question is allotted +4 (four) marks for each correct response.</li><li style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; line-height: 24px; color: var(--color-text-primary);\">There is -1 (one) negative marking for each wrong attempt.</li><li style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; line-height: 24px; color: var(--color-text-primary);\">The total duration of the test is 3 hrs.</li><li style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; line-height: 24px; color: var(--color-text-primary);\">There is no negative marking for un-attempted questions.</li><li style=\"text-rendering: optimizelegibility; word-break: break-word; outline: none; margin: 0px; -webkit-font-smoothing: antialiased; overscroll-behavior: none; line-height: 24px; color: var(--color-text-primary);\">Maximum marks can be attempted are of 720.&nbsp;</li></ol>', '2022-09-26 14:18:38', '2022-09-26 08:48:38'),
(17, 8, 'PHP Data Types', '<p>demo</p>', '2022-09-29 10:01:56', '2022-09-29 04:31:56'),
(20, 9, 'AJAX Object', '<p>demo</p>', '2022-10-12 14:20:12', '2022-10-12 08:50:12');

-- --------------------------------------------------------

--
-- Table structure for table `standard`
--

CREATE TABLE `standard` (
  `std_id` int(11) NOT NULL,
  `board_id` int(11) NOT NULL,
  `std_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `standard`
--

INSERT INTO `standard` (`std_id`, `board_id`, `std_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '10', 1, '2022-08-25 12:59:44', '2022-08-25 07:29:44'),
(9, 2, '9', 1, '2022-08-26 11:45:19', '2022-08-26 06:51:12'),
(10, 0, '2', 1, '2022-08-29 11:19:19', '2022-08-29 05:49:56'),
(11, 0, '11', 1, '2022-08-29 11:28:23', '2022-08-29 05:58:23');

-- --------------------------------------------------------

--
-- Table structure for table `student_resourse_category`
--

CREATE TABLE `student_resourse_category` (
  `cat_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_resourse_category`
--

INSERT INTO `student_resourse_category` (`cat_id`, `category_name`, `status`, `created_at`, `updated_at`) VALUES
(4, 'State Board', 1, '2022-09-26 17:54:07', '2022-09-28 08:42:59'),
(5, 'Competitive Exam', 1, '2022-09-26 17:55:31', '2022-09-28 08:43:31'),
(6, 'Free Study Material', 1, '2022-09-26 17:55:39', '2022-09-28 08:43:46');

-- --------------------------------------------------------

--
-- Table structure for table `student_resourse_sub_category`
--

CREATE TABLE `student_resourse_sub_category` (
  `sub_cat_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sub_cat_name` varchar(255) NOT NULL,
  `upload_details` mediumtext NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive,1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_resourse_sub_category`
--

INSERT INTO `student_resourse_sub_category` (`sub_cat_id`, `cat_id`, `sub_cat_name`, `upload_details`, `status`, `created_at`, `updated_at`) VALUES
(8, 4, 'MH Board', '<p>demo</p>', 1, '2022-09-26 17:54:34', '2022-09-26 12:24:34'),
(9, 4, 'Bihar Board', '<p>Test</p>', 1, '2022-09-28 12:46:48', '2022-09-28 07:16:48'),
(10, 5, 'JEE', '<p>demo</p>', 1, '2022-09-28 12:51:57', '2022-09-28 07:21:57'),
(11, 5, 'NEET', '<p>demo</p>', 1, '2022-09-28 12:52:07', '2022-09-28 07:22:07'),
(12, 6, 'Free Study Material - JEE Main', '<ul style=\"margin-bottom: 0px; list-style-position: initial; list-style-image: initial; color: rgb(119, 119, 119); font-size: 16px; background-color: rgb(251, 251, 251);\"><li style=\"display: block; padding: 0px; position: relative;\"><a href=\"http://localhost/True_Learning_website/#\" style=\"color: rgb(102, 102, 102); transition: all 0.3s ease-in-out 0s; font-size: 14px; line-height: 24px; letter-spacing: 0.75px; text-transform: capitalize; padding: 6px 0px; display: block;\">Free Study Material - JEE Main</a></li><li style=\"display: block; padding: 0px; position: relative;\"><a href=\"http://localhost/True_Learning_website/#\" style=\"color: rgb(102, 102, 102); transition: all 0.3s ease-in-out 0s; font-size: 14px; line-height: 24px; letter-spacing: 0.75px; text-transform: capitalize; padding: 6px 0px; display: block;\">Free Study Material - JEE Advanced</a></li><li style=\"display: block; padding: 0px; position: relative;\"><br></li></ul>', 1, '2022-09-28 12:52:34', '2022-09-28 07:22:34'),
(13, 6, 'Free Study Material - NEET', '<p>demo</p>', 1, '2022-09-28 12:53:20', '2022-09-28 07:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL,
  `board_id` int(11) NOT NULL,
  `std_id` int(11) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `board_id`, `std_id`, `subject_name`, `status`, `created_at`, `updated_at`) VALUES
(4, 2, 1, 'Hindi', 1, '2022-08-26 10:53:52', '2022-08-26 05:23:52'),
(5, 2, 1, 'Marathi', 1, '2022-08-26 10:54:05', '2022-08-26 05:41:34'),
(7, 0, 1, 'History', 1, '2022-08-26 11:45:36', '2022-08-29 06:10:38'),
(8, 0, 9, 'Digital Art', 1, '2022-08-29 11:38:57', '2022-08-29 06:08:57'),
(9, 0, 1, 'Geo', 1, '2022-08-29 11:40:55', '2022-08-29 06:10:55'),
(10, 0, 10, 'Maths', 1, '2022-09-22 16:51:02', '2022-09-22 11:21:02'),
(11, 0, 11, 'History', 1, '2022-09-22 16:51:22', '2022-09-22 11:21:22');

-- --------------------------------------------------------

--
-- Table structure for table `terms_and_conditions`
--

CREATE TABLE `terms_and_conditions` (
  `id` int(11) NOT NULL,
  `terms_conditions` mediumtext NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `terms_and_conditions`
--

INSERT INTO `terms_and_conditions` (`id`, `terms_conditions`, `updated_at`) VALUES
(1, '<p><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">Terms of service are the legal agreements between a service provider and a person who wants to use that service. The person must agree to abide by the terms of service in order to use the offered service. Terms of service can also be merely a disclaimer, especially regarding the use of websites.</span><br></p>', '2022-08-26 10:40:02');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `t_id` int(11) NOT NULL,
  `testimonial_name` varchar(255) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `testimol_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '	0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`t_id`, `testimonial_name`, `qualification`, `description`, `testimol_image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'How To Write a Testimonial', 'Ph.D', '<span style=\"color: rgb(46, 71, 93); font-family: \" lexend=\"\" deca\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\">Ccustomers read testimonials, they’re stepping into the person’s shoes in the testimonial and see themselves using your product.&nbsp;</span><sp', 'http://localhost/true_learning_dashboard/assets/images/admin/testimonial/proo.jpg', 1, '2022-08-26 17:00:36', '2022-09-28 09:24:35'),
(6, 'What is a testimonial?', 'M.E(E&TC)', '<p><span style=\"color: rgb(46, 71, 93); font-family: \" lexend=\"\" deca\",=\"\" sans-serif;=\"\" font-size:=\"\" 16px;\"=\"\">Your testimonial page is an excellent opportunity to convince website visitors of the merits of your organization.</span></p>', 'http://localhost/true_learning_dashboard/assets/images/admin/testimonial/c1.jpg', 1, '2022-08-26 17:03:44', '2022-08-26 12:06:19'),
(7, 'Examples of Great Customer Testimonial Pages', 'B.E(E&TC', '<p><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.</span><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">Descriptive d</span><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.</span><br></p>', 'http://localhost/true_learning_dashboard/assets/images/admin/testimonial/j111.jpg', 1, '2022-09-28 14:43:45', '2022-09-28 09:53:34');

-- --------------------------------------------------------

--
-- Table structure for table `test_details`
--

CREATE TABLE `test_details` (
  `test_detail_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `test_series_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `imp_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `opt_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `test_list`
--

CREATE TABLE `test_list` (
  `test_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `test_series_id` int(11) NOT NULL,
  `test_name` varchar(255) NOT NULL,
  `std_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `sub_topic` varchar(255) NOT NULL,
  `difficulty_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `test_series_type` int(11) NOT NULL COMMENT '1-Free 2-Paid',
  `t_questions` int(11) NOT NULL,
  `t_marks` varchar(255) NOT NULL,
  `t_duration` varchar(255) NOT NULL COMMENT 'in minutes',
  `correct_marks` varchar(255) NOT NULL,
  `negative_marks` varchar(255) NOT NULL,
  `not_attempt_marks` varchar(255) NOT NULL,
  `t_instructions` varchar(10000) NOT NULL,
  `test_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test_list`
--

INSERT INTO `test_list` (`test_id`, `category_id`, `test_series_id`, `test_name`, `std_id`, `subject_id`, `topic`, `sub_topic`, `difficulty_id`, `lang_id`, `test_series_type`, `t_questions`, `t_marks`, `t_duration`, `correct_marks`, `negative_marks`, `not_attempt_marks`, `t_instructions`, `test_image`, `status`, `created_at`, `updated_at`) VALUES
(8, 5, 12, 'PHP Test', 1, 4, 'Recreations of experiences', 'Description encourages a more concrete or sensory experience of a subject', 3, 1, 2, 25, '50', '60', '2', '2', '0', '<p><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">Descriptive details </span><b style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">allow sensory recreations of experiences, objects, or imaginings</b><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.</span><br></p>', 'http://localhost/true_learning_dashboard/assets/images/admin/test_list/dummy.png', 1, '2022-09-29 10:00:29', '2022-09-29 10:32:33'),
(9, 5, 12, 'AJAX Test', 9, 8, 'Ajax Topic', 'Encourages a more concrete or sensory experience of a subject', 4, 3, 1, 30, '30', '20', '1', '1', '0', '<p>demos</p>', 'http://localhost/true_learning_dashboard/assets/images/admin/test_list/du.png', 1, '2022-09-29 10:38:57', '2022-10-11 09:57:17'),
(10, 1, 14, 'Organic Chemistry', 10, 10, 'Problems in Mathematics by S.S. Krotov.', 'Description encourages a more concrete or sensory experience of a subject', 3, 1, 2, 20, '20', '30', '1', '1', '0', '<p><b style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">No, JEE Advanced preparation is not much different from JEE Main</b><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;\">. You have to study the same concepts and topics. The difference only comes when you solve mock tests and previous years papers of JEE Main and JEE Advanced as both the exams have slightly different exam patterns.</span><br></p>', 'http://localhost/true_learning_dashboard/assets/images/admin/test_list/m.webp', 1, '2022-09-29 14:55:54', '2022-09-29 09:25:54');

-- --------------------------------------------------------

--
-- Table structure for table `test_series`
--

CREATE TABLE `test_series` (
  `test_series_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `test_series_name` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `lang_id` int(11) NOT NULL,
  `test_series_type` int(11) NOT NULL COMMENT '1-Free 2-Paid',
  `price` varchar(255) NOT NULL,
  `offer` varchar(255) NOT NULL COMMENT 'in %',
  `description` mediumtext NOT NULL,
  `is_trending_series` int(11) NOT NULL COMMENT '1:-Yes, 0:-No',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-Deactive, 1-Active',
  `icon_image` varchar(255) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test_series`
--

INSERT INTO `test_series` (`test_series_id`, `category_id`, `test_series_name`, `start_date`, `end_date`, `lang_id`, `test_series_type`, `price`, `offer`, `description`, `is_trending_series`, `status`, `icon_image`, `banner_image`, `created_at`, `updated_at`) VALUES
(12, 5, 'Web development Series ', '2022-10-03', '2022-10-31', 1, 2, '899', '10', '<p><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">Web development is the work involved in developing a website for the Internet or an intranet. Web development can range from developing a simple single static page of plain text to complex web applications, electronic businesses, and social network services.</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\"> </span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">Web development is the work involved in developing a website for the Internet or an intranet. Web development can range from developing a simple single static page of plain text to complex web applications, electronic businesses, and social network services.</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\"> </span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">Web development is the work involved in developing a website for the Internet or an intranet. Web development can range from developing a simple single static page of plain text to complex web applications, electronic businesses, and social network services.</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\"> </span><br></p>', 1, 1, 'http://localhost/true_learning_dashboard/assets/images/admin/test_management/webb-icon.jpeg', 'http://localhost/true_learning_dashboard/assets/images/admin/test_management/web.jpg', '2022-09-28 16:30:21', '2022-10-03 05:05:18'),
(13, 2, 'NEET Crash course series ', '2022-10-01', '2022-10-31', 1, 1, '', '', '<p><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">NEET</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">&nbsp;UG - All India&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">test series</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">&nbsp;for&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">NEET 2022</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">&nbsp;available on Unacademy is a topic-wise&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">test series</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">&nbsp;in English covering Multiple Choice Questions from high.</span><br></p>', 1, 1, 'http://localhost/true_learning_dashboard/assets/images/admin/test_management/neet.webp', 'http://localhost/true_learning_dashboard/assets/images/admin/test_management/neett.jpg', '0000-00-00 00:00:00', '2022-09-29 07:06:42'),
(14, 1, 'JEE Main and Advanced Preparation Series - 2022', '2022-10-03', '2022-10-30', 1, 2, '999', '5', '<p><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">JEE</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\"> 2022: All you need for the preparation of </span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">IIT JEE</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\"> exam 2022 is available on Unacademy. Get study material, Live </span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">Classes</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\"> and guidance for </span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">IIT JEE</span><span style=\"font-weight: bold; font-family: arial, sans-serif;\"><font color=\"#4d5156\">. </font></span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">In this </span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">course</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\">, Top Educators will share Revision Strategy, Tips & Tricks, Practice and Time Management Techniques for the </span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif;\">JEE</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif;\"> exam. </span><br></p>', 1, 1, 'http://localhost/true_learning_dashboard/assets/images/admin/test_management/jee.png', 'http://localhost/true_learning_dashboard/assets/images/admin/test_management/j111.jpg', '0000-00-00 00:00:00', '2022-10-03 06:11:24'),
(15, 4, 'NDA Main course series', '2022-10-01', '2022-12-31', 1, 2, '1499', '', '<p><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif;\">The NDA exam is&nbsp;</span><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif;\">conducted by UPSC for admission to the Army, Navy and Air Force wings of the NDA</span><span style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif;\">. This exam is a gateway for candidates looking forward to joining Defence Forces including Army, Navy and Air Force</span><br></p>', 1, 1, 'http://localhost/true_learning_dashboard/assets/images/admin/test_management/nda.jpg', 'http://localhost/true_learning_dashboard/assets/images/admin/test_management/ndad.jpg', '0000-00-00 00:00:00', '2022-09-29 07:14:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `uid` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1:-admin, 2:-student',
  `full_name` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_text` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `mob_otp` int(11) NOT NULL,
  `mob_otp_verfied` int(11) NOT NULL DEFAULT 0 COMMENT '0-No, 1-Yes',
  `photo` varchar(255) NOT NULL,
  `gcm_id` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `pincode` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `is_verfied` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active',
  `is_registered` int(11) NOT NULL DEFAULT 0 COMMENT '0:-No, 1:-Yes',
  `registration_date` date NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`uid`, `user_type`, `full_name`, `user_name`, `email`, `password`, `password_text`, `mobile_no`, `mob_otp`, `mob_otp_verfied`, `photo`, `gcm_id`, `gender`, `dob`, `address`, `city`, `pincode`, `country`, `is_verfied`, `status`, `is_registered`, `registration_date`, `created_at`, `updated_at`) VALUES
(1, 1, 'True Learning', 'admin', 'admin@gmail.com', '202cb962ac59075b964b07152d234b70', '123', '9876543210', 0, 0, 'http://localhost/true_learning_dashboard/assets/images/users/proo.jpg', '', '', '0000-00-00', 'Vimannagar , Pune', 'Pune', 411014, 'India', 0, 1, 0, '2022-08-24', '2022-08-24 11:18:07', '2022-08-25 05:22:16'),
(6, 2, 'Mahadev Ingawale', 'ingawalemr12', 'ingawalemr12@gmail.com', '', '', '9970410333', 2197, 1, 'http://localhost/True_Learning_website/assets/img/user_profile/m.jpg', '', '', '0000-00-00', 'Viman Nagar, Pune - 411047', 'Pune', 411047, '', 1, 1, 1, '2022-10-12', '2022-09-12 10:57:30', '2022-10-12 03:39:49'),
(7, 2, 'mahadev123', 'mingawale02', 'mingawale02@gmail.com', '', '', '7770410333', 6150, 0, 'http://localhost/true_learning_dashboard/assets/images/users/85c94f1d11091147d56cfcd76f2b622f.png', '', '', '0000-00-00', 'A/P Pune', 'Satara', 415002, '', 0, 0, 1, '2022-09-26', '2022-09-15 09:35:04', '2022-09-29 10:17:45'),
(10, 2, 'demo abc', '', 'abc@gmail.com', '', '', '8877665509', 7809, 1, 'http://localhost/True_Learning_website/assets/img/user_profile/img_(1).jpg', '', '', '0000-00-00', '', '', 0, '', 0, 1, 1, '2022-09-28', '2022-09-27 12:41:22', '2022-09-28 05:07:50'),
(11, 2, '', '', '', '', '', '9970410111', 1710, 0, '', '', '', '0000-00-00', '', '', 0, '', 0, 1, 0, '0000-00-00', '2022-09-27 15:27:45', '2022-09-27 10:00:15');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `wishlist_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `test_series_id` int(11) NOT NULL,
  `wishlist` int(11) NOT NULL DEFAULT 0 COMMENT '1:-added wishlist, 0:-no wishlist',
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`wishlist_id`, `uid`, `test_series_id`, `wishlist`, `created_at`) VALUES
(2, 6, 14, 0, '2022-09-30 16:33:30'),
(5, 6, 13, 1, '2022-09-30 16:39:24'),
(6, 6, 15, 0, '2022-09-30 17:34:00'),
(7, 6, 12, 1, '2022-10-03 11:28:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answers_integer`
--
ALTER TABLE `answers_integer`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_match_matrix`
--
ALTER TABLE `answers_match_matrix`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_match_the_following`
--
ALTER TABLE `answers_match_the_following`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_multiple_choice`
--
ALTER TABLE `answers_multiple_choice`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_option_match_matrix`
--
ALTER TABLE `answers_option_match_matrix`
  ADD PRIMARY KEY (`opt_id`);

--
-- Indexes for table `answers_option_match_the_following`
--
ALTER TABLE `answers_option_match_the_following`
  ADD PRIMARY KEY (`opt_id`);

--
-- Indexes for table `answers_option_multiple_choice`
--
ALTER TABLE `answers_option_multiple_choice`
  ADD PRIMARY KEY (`opt_id`);

--
-- Indexes for table `answers_single_choice`
--
ALTER TABLE `answers_single_choice`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `answers_true_false`
--
ALTER TABLE `answers_true_false`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `board`
--
ALTER TABLE `board`
  ADD PRIMARY KEY (`board_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `difficulty_level`
--
ALTER TABLE `difficulty_level`
  ADD PRIMARY KEY (`difficulty_id`);

--
-- Indexes for table `import_question`
--
ALTER TABLE `import_question`
  ADD PRIMARY KEY (`imp_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `recent_list`
--
ALTER TABLE `recent_list`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`section_id`);

--
-- Indexes for table `standard`
--
ALTER TABLE `standard`
  ADD PRIMARY KEY (`std_id`);

--
-- Indexes for table `student_resourse_category`
--
ALTER TABLE `student_resourse_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `student_resourse_sub_category`
--
ALTER TABLE `student_resourse_sub_category`
  ADD PRIMARY KEY (`sub_cat_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `terms_and_conditions`
--
ALTER TABLE `terms_and_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `test_details`
--
ALTER TABLE `test_details`
  ADD PRIMARY KEY (`test_detail_id`);

--
-- Indexes for table `test_list`
--
ALTER TABLE `test_list`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `test_series`
--
ALTER TABLE `test_series`
  ADD PRIMARY KEY (`test_series_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`wishlist_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `answers_integer`
--
ALTER TABLE `answers_integer`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `answers_match_matrix`
--
ALTER TABLE `answers_match_matrix`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `answers_match_the_following`
--
ALTER TABLE `answers_match_the_following`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `answers_multiple_choice`
--
ALTER TABLE `answers_multiple_choice`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `answers_option_match_matrix`
--
ALTER TABLE `answers_option_match_matrix`
  MODIFY `opt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `answers_option_match_the_following`
--
ALTER TABLE `answers_option_match_the_following`
  MODIFY `opt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `answers_option_multiple_choice`
--
ALTER TABLE `answers_option_multiple_choice`
  MODIFY `opt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `answers_single_choice`
--
ALTER TABLE `answers_single_choice`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `answers_true_false`
--
ALTER TABLE `answers_true_false`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `board`
--
ALTER TABLE `board`
  MODIFY `board_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `difficulty_level`
--
ALTER TABLE `difficulty_level`
  MODIFY `difficulty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `import_question`
--
ALTER TABLE `import_question`
  MODIFY `imp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `recent_list`
--
ALTER TABLE `recent_list`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `standard`
--
ALTER TABLE `standard`
  MODIFY `std_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `student_resourse_category`
--
ALTER TABLE `student_resourse_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_resourse_sub_category`
--
ALTER TABLE `student_resourse_sub_category`
  MODIFY `sub_cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `terms_and_conditions`
--
ALTER TABLE `terms_and_conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `test_details`
--
ALTER TABLE `test_details`
  MODIFY `test_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `test_list`
--
ALTER TABLE `test_list`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `test_series`
--
ALTER TABLE `test_series`
  MODIFY `test_series_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `wishlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
