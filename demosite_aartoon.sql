-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2022 at 05:27 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demosite_aartoon`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1:-student, 2:-school',
  `about_us` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `user_type`, `about_us`, `updated_at`) VALUES
(1, 1, 'demo', '0000-00-00 00:00:00'),
(2, 2, 'demo 2', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_for_web`
--

CREATE TABLE `about_us_for_web` (
  `id` int(11) NOT NULL,
  `about_us` mediumtext NOT NULL,
  `images` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about_us_for_web`
--

INSERT INTO `about_us_for_web` (`id`, `about_us`, `images`, `updated_at`) VALUES
(1, '<p><span style=\"color: rgb(51, 71, 91); font-family: proxima_nova_rgregular; font-size: 18px; text-align: justify;\">A great About Us page not just portrays your story, qualities and provides an insight on how your business started, but it also helps you sell.</span></p><p><span style=\"color: rgb(51, 71, 91); font-family: proxima_nova_rgregular; font-size: 18px; text-align: justify;\"> When visitors become familiar with your story and connect with it, they\'re probably going to purchase from you. </span></p><p><span style=\"color: rgb(51, 71, 91); font-family: proxima_nova_rgregular; font-size: 18px; text-align: justify;\">A well-planned About Us page can do this! </span><span style=\"color: rgb(51, 71, 91); font-family: proxima_nova_rgregular; font-size: 18px; text-align: justify;\">Think for a moment: would you rather purchase from a business you know nothing about, or would you go for somebody with a friendly face shared on their About page and a story that you find exciting? The latter one, right? </span><br></p>', 'http://localhost/aartoon/control_panel/dist/img/about_us_web/about-us.jpg', '2022-05-25 14:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `app_banner_screen_images`
--

CREATE TABLE `app_banner_screen_images` (
  `id` int(11) NOT NULL,
  `documents_file` varchar(255) NOT NULL,
  `file_type` int(11) NOT NULL COMMENT '1:-image, 2:-video_link',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active',
  `check_type` int(11) NOT NULL COMMENT '0:-vdo, 1:-img',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `app_banner_screen_images`
--

INSERT INTO `app_banner_screen_images` (`id`, `documents_file`, `file_type`, `status`, `check_type`, `created_at`) VALUES
(14, 'http://localhost/aartoon/dist/img/app_banner_screen_images/li.png', 1, 1, 1, '2022-05-04 11:06:57'),
(15, 'http://localhost/aartoon/dist/img/app_banner_screen_images/doc31.png', 1, 0, 1, '2022-05-04 11:06:57'),
(17, 'http://localhost/aartoon/dist/img/app_banner_screen_images/img_(1).jpg', 1, 1, 1, '2022-05-04 11:35:45'),
(18, 'http://localhost/aartoon/dist/img/app_banner_screen_images/ab.jpg', 1, 1, 1, '2022-05-26 06:10:39'),
(19, 'http://localhost/aartoon/dist/img/app_banner_screen_images/w3.jpg', 1, 1, 1, '2022-05-26 07:20:46'),
(20, 'https://www.youtube.com/watch?v=YnN7_9UQJrE', 2, 1, 0, '2022-05-28 18:08:01'),
(21, 'http://localhost/aartoon/dist/img/app_banner_screen_images/marathi-compulsory.jpg', 1, 1, 1, '2022-05-28 18:09:57');

-- --------------------------------------------------------

--
-- Table structure for table `app_home_screen_images`
--

CREATE TABLE `app_home_screen_images` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `app_home_screen_images`
--

INSERT INTO `app_home_screen_images` (`id`, `title`, `images`, `description`, `updated_at`) VALUES
(1, 'Top Video Production Courses Online', 'http://localhost/aartoon/dist/img/app_home_screen_images/1.jpg', 'you\'ll learn the essential skills and software behind producing engaging video content', '2022-05-02 13:02:06'),
(2, 'learning from the best ', 'http://localhost/aartoon/dist/img/app_home_screen_images/2.png', 'High quality example sentences with “learning from the best” in context from reliable sources.', '2022-05-02 11:54:57'),
(3, 'Learn Anywhere is an easy-to-use and affordable', 'http://localhost/aartoon/dist/img/app_home_screen_images/3.png', 'collaboration solution that brings students into the classroom - even when that is not physically possible.', '2022-05-02 11:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `app_login_screen_images`
--

CREATE TABLE `app_login_screen_images` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `app_login_screen_images`
--

INSERT INTO `app_login_screen_images` (`id`, `title`, `images`, `updated_at`) VALUES
(1, '', 'http://localhost/aartoon/dist/img/app_login_screen_images/111.jpg', '2022-05-02 08:52:59');

-- --------------------------------------------------------

--
-- Table structure for table `app_review`
--

CREATE TABLE `app_review` (
  `review_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `review_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_review`
--

INSERT INTO `app_review` (`review_id`, `uid`, `rating`, `remark`, `review_date`, `created_at`, `updated_at`) VALUES
(1, 2, '3', 'demo 2', '2022-04-27 11:13:32', '2022-04-27 05:43:32', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `blog_id` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `blog_type` varchar(255) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `popular_post` int(11) NOT NULL COMMENT '0:-No, 1:-Yes',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`blog_id`, `images`, `title`, `date`, `author_name`, `blog_type`, `description`, `popular_post`, `created_at`, `updated_at`) VALUES
(1, 'http://localhost/aartoon/control_panel/dist/img/blogs/car1.jpg', 'Few tips for get better results in examination', '2022-05-23', 'Mark anthem', 'ducation', 'Lorem ipsum gravida nibh vel velit auctor aliquetn sollicitudirem quibibendum auci elit cons equat ipsutis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus .', 0, '2022-05-23 15:45:10', '0000-00-00 00:00:00'),
(5, 'http://localhost/aartoon/control_panel/dist/img/blogs/du.png', 'Start with the keywords and work your way outwards', '2022-05-23', 'ABC', 'All', 'Your keywords will not only play a huge part in getting your post ranked on search engine results pages, they’ll also be responsible for helping readers decide if the post is relevant to them. As such, it’s a good idea to start with some keyword research before you even write your blog post. ', 1, '2022-05-23 17:37:21', '0000-00-00 00:00:00'),
(6, 'http://localhost/aartoon/control_panel/dist/img/blogs/download.jpg', 'Title Generator for Blog, Headline, and More', '2022-08-19', 'Mark anthem', ' Blog Title Generator', '<p><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">Not sure what to&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">blog</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;about? Fill in the blanks with your own keywords and have an endless supply of&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">blog</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;topics. ... Generate&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">titles</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;Generate&nbsp;</span><span style=\"font-weight: bold; color: rgb(95, 99, 104); font-family: arial, sans-serif; font-size: 14px;\">Titles</span><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">&nbsp;Arrow.&nbsp;</span><span style=\"color: rgb(77, 81, 86); font-size: 14px; font-family: arial, sans-serif;\">Our free</span><span style=\"color: rgb(77, 81, 86); font-size: 14px; font-family: arial, sans-serif;\">&nbsp;</span><span style=\"font-size: 14px; font-family: arial, sans-serif; font-weight: bold; color: rgb(95, 99, 104);\">Blog Title Generator</span><span style=\"color: rgb(77, 81, 86); font-size: 14px; font-family: arial, sans-serif;\">&nbsp;</span><span style=\"color: rgb(77, 81, 86); font-size: 14px; font-family: arial, sans-serif;\">allows you to create catchy, SEO optimized blog post titles in seconds.</span></p><div class=\"MjjYud\" style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: medium;\"><div class=\"g Ww4FFb vt6azd tF2Cxc\" lang=\"en\" data-hveid=\"CCkQAA\" data-ved=\"2ahUKEwjArKap1dL5AhU8UfUHHYS9BLcQFSgAegQIKRAA\" style=\"font-size: 14px; line-height: 1.58; width: 600px; margin: 0px 0px 30px; position: relative; border: 0px; border-radius: 0px;\"><div class=\"kvH3mc BToiNc UK95Uc\" data-sokoban-container=\"ih6Jnb_BiLPdc\" style=\"display: flex; flex-direction: column; justify-content: flex-start; position: relative; contain: layout paint; overflow: hidden;\"><div class=\"Z26q7c UK95Uc\" style=\"contain: layout paint; overflow: hidden; flex: 0 0 auto;\"><div jscontroller=\"K6HGfd\" id=\"eob_9\" jsdata=\"fxg5tf;_;AjI0oo\" jsaction=\"rcuQ6b:npT2md\" data-ved=\"2ahUKEwjArKap1dL5AhU8UfUHHYS9BLcQ2Z0BegQIGBAA\"></div></div></div></div></div><div class=\"MjjYud\" style=\"color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: medium;\"><div class=\"g Ww4FFb vt6azd tF2Cxc\" lang=\"en\" data-hveid=\"CCwQAA\" data-ved=\"2ahUKEwjArKap1dL5AhU8UfUHHYS9BLcQFSgAegQILBAA\" style=\"font-size: 14px; line-height: 1.58; width: 600px; margin: 0px 0px 30px; position: relative; border: 0px; border-radius: 0px;\"><div class=\"kvH3mc BToiNc UK95Uc\" data-sokoban-container=\"ih6Jnb_ZAV3l\" style=\"display: flex; flex-direction: column; justify-content: flex-start; position: relative; contain: layout paint; overflow: hidden;\"><div class=\"Z26q7c UK95Uc uUuwM jGGQ5e\" data-header-feature=\"0\" style=\"contain: layout paint; overflow: hidden; flex: 1 1 100%; min-width: 0px;\"></div></div></div></div>', 1, '2022-08-19 15:43:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `branch_name`, `created_at`, `updated_at`) VALUES
(1, 'PCB Design  Software Branch', '2022-04-27 05:08:31', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `career_growth`
--

CREATE TABLE `career_growth` (
  `c_g_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `image_video` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `career_growth`
--

INSERT INTO `career_growth` (`c_g_id`, `title`, `description`, `image_video`, `created_at`, `updated_at`) VALUES
(11, 'demo', 'fsdfwrrrfr', '', '2022-05-24 22:49:13', '2022-05-24 17:19:13'),
(13, '3D and 2D Animator', 'Breathing life into a film character.Video referencing the acting of a character before starting with an animation.Breathing life into a film character.Video referencing the acting of a character before starting with an animation.', '', '2022-05-25 09:31:06', '2022-05-25 04:01:06'),
(48, 'demo', '<p>demo<br></p>', '', '2022-05-30 14:49:47', '2022-05-30 09:19:47'),
(51, 'demo video 2', '<p>demo video</p>', '', '2022-05-30 15:37:36', '2022-05-30 10:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `career_growth_files`
--

CREATE TABLE `career_growth_files` (
  `c_g_f_id` int(11) NOT NULL,
  `c_g_id` int(11) NOT NULL,
  `image_video` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `career_growth_files`
--

INSERT INTO `career_growth_files` (`c_g_f_id`, `c_g_id`, `image_video`, `created_at`, `updated_at`) VALUES
(3, 11, 'http://localhost/aartoon/control_panel/dist/img/career_growth/Videos2.mp4', '2022-05-24 22:49:14', 0),
(6, 13, 'http://localhost/aartoon/control_panel/dist/img/career_growth/ezgif_com-gif-maker.gif', '2022-05-25 09:31:06', 0),
(7, 13, 'http://localhost/aartoon/control_panel/dist/img/career_growth/Videos3.mp4', '2022-05-25 09:31:06', 0),
(36, 48, 'http://localhost/aartoon/control_panel/dist/img/career_growth/du.png', '2022-05-30 14:49:47', 0),
(37, 48, 'http://localhost/aartoon/control_panel/dist/img/career_growth/lenovo-tablet.webp', '2022-05-30 14:49:48', 0),
(42, 51, 'http://localhost/aartoon/dist/img/career_growth/Videos2.mp4', '2022-05-30 15:37:37', 0),
(43, 51, 'http://localhost/aartoon/dist/img/career_growth/video4.mp4', '2022-05-30 15:37:37', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'PCB Design Category', '2022-04-27 05:05:42', '0000-00-00 00:00:00'),
(2, 'iOS Category', '2022-04-28 09:57:14', '2022-06-29 14:55:13'),
(3, '3 D Category', '2022-05-11 11:10:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `communication_management`
--

CREATE TABLE `communication_management` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `school_uid` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `send_to` varchar(255) NOT NULL,
  `send_date` date NOT NULL,
  `course_id` int(11) NOT NULL,
  `notification_for` int(11) NOT NULL COMMENT '1:-All, 2:-Multiple,3:-course wise , 4:-school wise, 5:-Live class amount, 6:-push_notification, 7:-signUp',
  `notification_get_flag` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_management`
--

INSERT INTO `communication_management` (`id`, `uid`, `school_uid`, `subject`, `description`, `send_to`, `send_date`, `course_id`, `notification_for`, `notification_get_flag`, `created_at`) VALUES
(1, 0, 0, 'all stu', '<p>dddd</p>', 'All Students', '2022-04-27', 0, 1, 0, '2022-04-27 17:54:03'),
(7, 2, 0, 'Live Class Request', 'd 4', 'Mahadev stu', '2022-04-28', 1, 5, 1, '2022-04-28 10:36:55'),
(9, 14, 0, 'Course Purchased', 'Your Selected Course Purchased successfully on 05-05-2022', 'Bishop School', '2022-05-05', 1, 6, 0, '2022-05-05 17:03:24'),
(10, 14, 0, 'Course Purchased', 'Your Selected Course Purchased successfully on 05-05-2022', 'Bishop School', '2022-05-05', 1, 6, 0, '2022-05-05 17:13:15'),
(11, 6, 0, 'Course Purchased', 'Your Selected Course Purchased successfully on 05-05-2022', 'Bishop School', '2022-05-05', 1, 6, 1, '2022-05-05 17:13:18'),
(12, 2, 0, 'Course Purchased', 'Your Selected Course Purchased successfully on 05-05-2022', 'Bishop School', '2022-05-05', 1, 6, 1, '2022-05-05 17:14:32'),
(13, 4, 0, 'Student Registered', 'You got Rs. 490 as wallet point, as your refferal code used by new user.', 'Bishop School', '2022-05-05', 0, 6, 0, '2022-05-05 17:25:56'),
(19, 2, 0, 'Live Class Request', 'd 3 See Live Class Lecture', 'Mahadev stu', '2022-05-11', 1, 5, 1, '2022-05-11 11:37:01'),
(20, 2, 0, 'Live Class Request', 'd 2', 'Mahadev stu', '2022-05-11', 1, 5, 1, '2022-05-11 18:18:55'),
(21, 3, 0, 'Student Registered', 'You got Rs. 160 as wallet point, as your refferal code used by new user.', 'PCB School', '2022-05-13', 0, 6, 0, '2022-05-13 16:44:58'),
(22, 1, 0, 'Student Registered', 'You got Rs. 100 as wallet point, as your refferal code used by new user.', 'Mahade Admin', '2022-05-13', 0, 6, 0, '2022-05-13 17:00:58'),
(23, 1, 0, 'Student Registered', 'You got Rs. 200 as wallet point, as your refferal code used by new user.', 'Mahade Admin', '2022-05-13', 0, 6, 0, '2022-05-13 17:10:43'),
(24, 1, 0, 'Student Registered', 'You got Rs. 300 as wallet point, as your refferal code used by new user.', 'Mahade Admin', '2022-05-13', 0, 6, 0, '2022-05-13 17:13:27'),
(25, 1, 0, 'Student Registered', 'You got Rs. 400 as wallet point, as your refferal code used by new user.', 'Mahade Admin', '2022-05-13', 0, 6, 0, '2022-05-13 17:14:08'),
(26, 3, 0, 'Student Registered', 'You got Rs. 260 as wallet point, as your refferal code used by new user.', 'PCB School', '2022-05-15', 0, 6, 0, '2022-05-15 09:51:03'),
(27, 4, 0, 'Student Registered', 'You got Rs. 590 as wallet point, as your refferal code used by new user.', 'Bishop School', '2022-05-15', 0, 6, 0, '2022-05-15 10:05:16'),
(28, 4, 0, 'Student Registered', 'You got Rs. 690 as wallet point, as your refferal code used by new user.', 'Bishop School', '2022-05-15', 0, 6, 0, '2022-05-15 11:49:56'),
(29, 3, 0, 'Student Registered', 'You got Rs. 360 as wallet point, as your refferal code used by new user.', 'PCB School', '2022-05-21', 0, 6, 0, '2022-05-21 17:06:18'),
(31, 61, 3, 'demo course', '<p>demo</p>', 'mahadevTest1', '2022-05-31', 0, 4, 0, '2022-05-31 09:39:20'),
(33, 61, 3, 'demo course 1', '<p>11</p>', 'All Students', '2022-05-31', 0, 4, 0, '2022-05-31 09:43:48'),
(34, 61, 3, 'demo course 2', '<p>22</p>', 'All Students', '2022-05-31', 0, 4, 0, '2022-05-31 09:44:51'),
(36, 2, 0, 'Live Class Request', 'd 1', 'Mahadev stu', '2022-06-17', 1, 5, 1, '2022-06-17 14:18:53'),
(37, 6, 0, 'Live Class Request', 'Pay above charges and see live demo lecture', 'mahadev stu_pcb', '2022-06-17', 2, 5, 1, '2022-06-17 14:35:42'),
(38, 6, 0, 'Live Class Request', 'pay 180 for PCB advance class', 'mahadev stu_pcb', '2022-06-17', 6, 5, 1, '2022-06-17 15:27:51'),
(39, 6, 0, 'Live Class Request', 'test 199', 'mahadev stu_pcb', '2022-06-21', 3, 5, 1, '2022-06-21 17:11:52'),
(40, 2, 0, 'Live Class Request', 'pay 99', 'Mahadev stu', '2022-06-23', 3, 5, 1, '2022-06-23 16:40:25'),
(41, 3, 0, 'Student Registered', 'You got Rs. 460 as wallet point, as your refferal code used by new user.', 'PCB School', '2022-06-27', 0, 6, 0, '2022-06-27 17:04:19'),
(42, 4, 0, 'Student Registered', 'You got Rs. 790 as wallet point, as your refferal code used by new user.', 'Bishop School', '2022-06-27', 0, 6, 0, '2022-06-27 17:49:38'),
(51, 61, 0, 'Live Class Request', 'pay and enjoy', 'mahadevTest1', '2022-06-29', 2, 5, 0, '2022-06-29 13:01:04'),
(53, 60, 0, 'Live Class Request', 'pay 10', 'mahadevTest', '2022-06-30', 2, 5, 1, '2022-06-30 16:47:17'),
(54, 60, 0, 'Live Class Request', 'pay 51', 'mahadevTest', '2022-06-30', 2, 5, 1, '2022-06-30 17:40:30'),
(55, 60, 0, 'Live Class Request', 'pay 49', 'mahadevTest', '2022-06-30', 2, 5, 1, '2022-06-30 17:40:45'),
(56, 6, 0, 'sending notification to individual', '<p>sending notification to individual<br></p>', 'mahadev stu_pcb', '2022-08-17', 6, 3, 0, '2022-08-17 16:42:37'),
(57, 2, 0, 'sending notification to individual', '<p>sending notification to individual<br></p>', 'Mahadev stu', '2022-08-17', 6, 3, 1, '2022-08-17 16:42:37'),
(63, 2, 0, 'multiple stu', '<p>multiple&nbsp; stu<br></p>', 'Mahadev stu', '2022-08-18', 0, 2, 1, '2022-08-18 09:31:36'),
(64, 14, 0, 'multiple stu', '<p>multiple&nbsp; stu<br></p>', 'M R Ingawale', '2022-08-18', 0, 2, 0, '2022-08-18 09:31:36'),
(65, 60, 0, 'multiple stu', '<p>multiple&nbsp; stu<br></p>', 'mahadevTest', '2022-08-18', 0, 2, 1, '2022-08-18 09:31:36'),
(66, 2, 0, 'PCB course wise', '<p>course wise<br></p>', 'Mahadev stu', '2022-08-18', 1, 3, 1, '2022-08-18 09:33:37'),
(67, 60, 0, 'PCB course wise', '<p>course wise<br></p>', 'mahadevTest', '2022-08-18', 1, 3, 1, '2022-08-18 09:33:37'),
(68, 61, 3, 'school wise- PCB', '<p>&nbsp;PCB school<br></p>', 'All School Students', '2022-08-18', 0, 4, 0, '2022-08-18 09:34:53'),
(69, 14, 0, 'demo course', '<p>admin<br></p>', 'M R Ingawale', '2022-09-01', 0, 2, 0, '2022-09-01 14:39:15'),
(70, 6, 0, 'sending notification to multiple', '<p>get_school_name[\'full_name\'];<br></p>', 'mahadev stu_pcb', '2022-09-01', 0, 2, 0, '2022-09-01 14:57:56'),
(71, 13, 0, 'sending notification to multiple', '<p>get_school_name[\'full_name\'];<br></p>', '', '2022-09-01', 0, 2, 0, '2022-09-01 14:57:56'),
(72, 14, 0, 'sending notification to multiple', '<p>get_school_name[\'full_name\'];<br></p>', 'M R Ingawale', '2022-09-01', 0, 2, 0, '2022-09-01 14:57:56'),
(73, 46, 0, 'sending notification to multiple', '<p>get_school_name[\'full_name\'];<br></p>', 'mahadevSchool_web', '2022-09-01', 0, 2, 0, '2022-09-01 14:57:57');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1:-student, 2:-school, 3:-web_contact_us',
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `user_type`, `phone`, `email`, `address`) VALUES
(1, 1, '1234567890', 'StudentMail@gmail.com', 'Pune'),
(2, 2, '0987654321', 'SchoolMail@gmail.com', 'Pune'),
(3, 3, '9823709888', 'contact.aartoon@gmail.com', 'South Main Road, opposite Ashok Chakra Society, Iricen Railway Colony, Koregaon Park, Pune 411001.');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_price` varchar(255) NOT NULL,
  `artist_name` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `course_hrs` varchar(255) NOT NULL,
  `course_rewards` varchar(255) NOT NULL,
  `short_description` varchar(10000) NOT NULL,
  `long_description` varchar(10000) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `best_sellar` int(11) DEFAULT 0 COMMENT '1:-Yes, 0:-No',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `category_id`, `course_name`, `course_price`, `artist_name`, `language`, `course_hrs`, `course_rewards`, `short_description`, `long_description`, `image`, `video_link`, `best_sellar`, `created_at`) VALUES
(1, 1, 'PCB Design Basic Course ', '2999', 'Altium Designer', 'Engllish', '', '', '<p>demo</p>', '<p>long description</p>', 'http://localhost/aartoon/control_panel/dist/img/course/images.jpg', 'https://www.orcad.com/resources/library/allegroorcadecad-mcad-integration-solidworksoverview', 1, '2022-04-27 05:06:21'),
(2, 2, 'iOS developer Beginner Course', '3999', 'mahadev', 'Engllish', '', '', 'ddd', '', 'http://localhost/aartoon/control_panel/dist/img/course/illu.jpg', 'https://www.youtube.com/watch?v=-ZphqP82CbU', 1, '2022-04-28 09:57:54'),
(3, 3, '3D Course', '4999', 'mahadev', 'Engllish', '48', '250', 'demo', 'demo', 'http://localhost/aartoon/control_panel/dist/img/course/3d.jpg', 'https://www.youtube.com/watch?v=-ZphqP82CbU', 1, '2022-05-11 11:11:58'),
(6, 1, 'PCB Design Advance Course  I', '5999', 'Altium Designer', 'Engllish', '72', '250', 'an event at which objects such as paintings are shown to the public, a situation in which someone shows a particular skill or quality to the public, or the act of showing ', '<div class=\"d9FyLd\" style=\"padding: 0px 0px 10px; color: rgb(32, 33, 36); font-family: arial, sans-serif;\">A Definition of Descriptive Detail</div><span class=\"hgKElc\" style=\"padding: 0px 8px 0px 0px; color: rgb(32, 33, 36); font-family: arial, sans-serif;\">Descriptive details <b>allow sensory recreations of experiences, objects, or imaginings</b>. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.<span style=\"font-size: 1rem;\">A Definition of Descriptive Detail</span><br></span><span class=\"hgKElc\" style=\"padding: 0px 8px 0px 0px; color: rgb(32, 33, 36); font-family: arial, sans-serif;\">Descriptive details <b>allow sensory recreations of experiences, objects, or imaginings</b>. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.</span>', 'http://localhost/aartoon/control_panel/dist/img/course/1_pcb-routing.jpg', 'https://www.youtube.com/watch?v=-ZphqP82CbU', 1, '2022-05-18 04:36:24');

-- --------------------------------------------------------

--
-- Table structure for table `course_features`
--

CREATE TABLE `course_features` (
  `course_feature_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `icons` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active	',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_features`
--

INSERT INTO `course_features` (`course_feature_id`, `course_id`, `icons`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'http://localhost/aartoon/dist/img/course/icons/log.jpg', 'pcb features', 1, '2022-04-27 05:07:47', '2022-04-27 10:38:03');

-- --------------------------------------------------------

--
-- Table structure for table `course_learn_more`
--

CREATE TABLE `course_learn_more` (
  `course_learn_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active	',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_learn_more`
--

INSERT INTO `course_learn_more` (`course_learn_id`, `course_id`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'PCB learn', 1, '2022-04-27 05:06:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_played_video`
--

CREATE TABLE `course_played_video` (
  `c_p_v_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `course_played_video`
--

INSERT INTO `course_played_video` (`c_p_v_id`, `uid`, `course_id`, `video_id`, `created_at`) VALUES
(1, 2, 1, 1, '2022-04-28 11:38:38'),
(20, 2, 3, 2, '2022-06-02 17:30:56'),
(21, 2, 3, 3, '2022-06-02 17:31:12'),
(22, 2, 3, 6, '2022-06-02 17:32:38'),
(24, 2, 1, 4, '2022-06-02 18:31:31'),
(27, 2, 1, 5, '2022-06-03 09:48:57'),
(28, 2, 3, 7, '2022-06-08 15:10:04'),
(36, 60, 3, 0, '2022-07-05 12:17:46'),
(37, 60, 3, 2, '2022-07-05 12:34:38'),
(38, 60, 3, 6, '2022-07-05 12:35:46'),
(39, 60, 3, 7, '2022-07-05 12:37:18'),
(41, 60, 3, 3, '2022-07-05 14:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `course_review`
--

CREATE TABLE `course_review` (
  `co_r_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rating` int(11) NOT NULL DEFAULT 0,
  `review_date` datetime NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0:-No, 1:-Yes',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_review`
--

INSERT INTO `course_review` (`co_r_id`, `course_id`, `uid`, `rating`, `review_date`, `remark`, `status`, `created_at`) VALUES
(1, 1, 2, 4, '2022-04-27 11:15:22', 'remark and rating is good', 0, '2022-04-27 05:45:22'),
(20, 1, 2, 4, '2022-06-03 11:11:08', 'course_review is better', 1, '2022-06-03 05:41:08'),
(21, 3, 2, 2, '2022-06-09 11:01:19', 'ok', 1, '2022-06-09 05:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `course_video_management`
--

CREATE TABLE `course_video_management` (
  `video_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `module_item_id` int(11) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `video_type` varchar(255) DEFAULT NULL COMMENT '0:-Free, 1:-Paid',
  `video_length` varchar(255) NOT NULL,
  `upload_file` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_video_management`
--

INSERT INTO `course_video_management` (`video_id`, `category_id`, `course_id`, `module_id`, `module_item_id`, `created_by`, `video_link`, `video_type`, `video_length`, `upload_file`, `description`, `created_at`) VALUES
(1, 1, 1, 1, 1, 'Altium Designer', 'https://www.youtube.com/watch?v=Z2LgmIGE2nI', 'Free', '10:26', '', '', '2022-04-27 05:26:16'),
(2, 3, 3, 2, 2, 'mahadev1', 'https://www.youtube.com/watch?v=-ZphqP82CbU', 'Free', '10:15', 'http://localhost/aartoon/dist/img/course_video_management/pr.png', '', '2022-05-11 11:59:44'),
(3, 3, 3, 2, 2, 'Mahadev 4 ', 'https://www.youtube.com/watch?v=eHQOruCH-lk', 'Free', '15:22', 'http://localhost/aartoon/dist/img/course_video_management/3296323.jpg', '', '2022-05-11 12:26:10'),
(4, 1, 1, 1, 1, 'mahadev', 'https://www.youtube.com/watch?v=-ZphqP82CbU', 'Free', '10:15', 'http://localhost/aartoon/dist/img/course_video_management/Offer_Letter_-_Mahadev_Ingawale.pdf', '', '2022-05-20 04:12:34'),
(5, 1, 1, 4, 4, 'mahadev', 'https://www.youtube.com/watch?v=-ZphqP82CbU', 'Free', '02:32', 'http://localhost/aartoon/dist/img/course_video_management/live_vide_class.mp4', '', '2022-05-25 06:34:49'),
(6, 3, 3, 3, 3, 'mahadev2', 'https://www.youtube.com/watch?v=StkDur_vXNI', 'Free', '1:07:53', 'http://localhost/aartoon/dist/img/course_video_management/Digital_Art.jpg', '', '2022-05-27 06:29:41'),
(7, 3, 3, 3, 5, 'mahadev3', 'https://www.youtube.com/watch?v=-ZphqP82CbU', 'Free', '10:26', 'http://localhost/aartoon/dist/img/course_video_management/2d.png', '', '2022-05-30 10:28:20');

-- --------------------------------------------------------

--
-- Table structure for table `course_video_manag_options`
--

CREATE TABLE `course_video_manag_options` (
  `option_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `opt_1` varchar(255) NOT NULL,
  `opt_2` varchar(255) NOT NULL,
  `opt_3` varchar(255) NOT NULL,
  `opt_4` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_video_manag_options`
--

INSERT INTO `course_video_manag_options` (`option_id`, `question_id`, `video_id`, `opt_1`, `opt_2`, `opt_3`, `opt_4`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Design Software', 'Design ', 'Software', 'Other', '2022-04-27 05:27:40', '0000-00-00 00:00:00'),
(2, 2, 1, 'A', 'B', 'C', 'D', '2022-04-28 07:00:46', '0000-00-00 00:00:00'),
(3, 3, 2, 'Design Software', 'Design ', 'Software', 'Other', '2022-05-11 12:00:18', '0000-00-00 00:00:00'),
(4, 4, 6, 'Design', 'prototype ', 'Printing', '3D Printing', '2022-05-27 06:34:08', '2022-05-27 12:05:10'),
(5, 5, 3, 'Design', 'prototype ', 'Software', '2D Printing', '2022-05-27 10:13:21', '2022-07-05 12:43:07'),
(6, 6, 3, 'A', 'B', 'C', 'D', '2022-06-02 05:49:51', '0000-00-00 00:00:00'),
(7, 7, 6, 'W', 'X', 'Y', 'Z', '2022-06-02 08:48:35', '0000-00-00 00:00:00'),
(8, 8, 7, 'A', 'B', 'C', 'D', '2022-06-09 05:17:31', '0000-00-00 00:00:00'),
(9, 9, 5, 'W', 'Z', 'X', 'Y', '2022-06-09 05:18:47', '0000-00-00 00:00:00'),
(10, 10, 4, 'A', 'B', 'C', 'D', '2022-06-09 05:19:01', '0000-00-00 00:00:00'),
(11, 11, 3, ' developement1', 'developement2', 'developement3', 'developement4', '2022-07-05 06:04:52', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_video_manag_questions`
--

CREATE TABLE `course_video_manag_questions` (
  `question_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `interval_time_slot` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_video_manag_questions`
--

INSERT INTO `course_video_manag_questions` (`question_id`, `video_id`, `question`, `answer`, `interval_time_slot`, `status`, `created_at`) VALUES
(1, 1, 'What is PHP?', 'Design Software', '5', 1, '2022-04-27 05:27:40'),
(2, 1, 'What is Quetion?', 'D', '9', 1, '2022-04-28 07:00:46'),
(3, 2, 'What is PHP?', 'Design Software', '5', 1, '2022-05-11 12:00:18'),
(4, 6, 'What is 3D printing ?', '3D Printing', '4', 1, '2022-05-27 06:34:08'),
(5, 3, 'What is 2D?', '2D Printing', '2', 1, '2022-05-27 10:13:21'),
(6, 3, 'What is HTML?', 'D', '5', 1, '2022-06-02 05:49:51'),
(7, 6, 'What is Quetion 2xx2?', 'X', '6', 1, '2022-06-02 08:48:35'),
(8, 7, 'what is 3D Course Module 2?', 'D', '5', 1, '2022-06-09 05:17:31'),
(9, 5, 'what is pcb 2xx?', 'Z', '9', 1, '2022-06-09 05:18:47'),
(10, 4, 'what is pcb 1xx?', 'C', '5', 1, '2022-06-09 05:19:01'),
(11, 3, 'What is developement?', 'developement4', '1', 1, '2022-07-05 06:04:52');

-- --------------------------------------------------------

--
-- Table structure for table `course_wishlist`
--

CREATE TABLE `course_wishlist` (
  `wishlist_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `wishlist` int(11) NOT NULL DEFAULT 0 COMMENT '1:-added wishlist, 0:-no wishlist',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_wishlist`
--

INSERT INTO `course_wishlist` (`wishlist_id`, `uid`, `course_id`, `wishlist`, `created_at`, `updated_at`) VALUES
(13, 2, 1, 1, '2022-06-03 16:10:10', '0000-00-00 00:00:00'),
(15, 2, 3, 1, '2022-06-03 16:25:14', '0000-00-00 00:00:00'),
(16, 2, 2, 1, '2022-06-16 14:53:13', '0000-00-00 00:00:00'),
(17, 60, 2, 1, '2022-06-28 11:31:59', '0000-00-00 00:00:00'),
(18, 6, 2, 0, '2022-10-03 11:12:01', '2022-10-03 05:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1:-student, 2:-school',
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `user_type`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 1, 'What is PHP?', '<p>scripting langauge</p>', '2022-05-25 11:54:00', '2022-05-25 17:24:13'),
(2, 2, 'What is aartoon?', '<p>Aartoon or our partners may offer gift and promotional codes to students. Certain codes may be redeemed for gift or promotional credits applied to your Aartoon account, which then may be used to purchase eligible content on our platform, subject to the', '2022-05-25 11:54:52', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `home_work_slider`
--

CREATE TABLE `home_work_slider` (
  `slider_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `documents_file` varchar(255) NOT NULL,
  `file_type` int(11) NOT NULL COMMENT '1:-image, 2:-video_link	',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active	',
  `check_type` int(11) NOT NULL COMMENT '0:-vdo, 1:-img',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_work_slider`
--

INSERT INTO `home_work_slider` (`slider_id`, `title`, `description`, `documents_file`, `file_type`, `status`, `check_type`, `created_at`, `updated_at`) VALUES
(14, 'A Definition of Descriptive Detail', 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', 'http://localhost/aartoon/control_panel/dist/img/home_slider_mng/1378793.jpg', 1, 1, 1, '2022-05-27 09:32:38', '2022-08-19 09:29:20'),
(15, 'Details and Descriptions | ', 'Purpose of Exercise: This exercise fosters creative thinking in descriptions, development of voice, and playing effectively with sarcasm. Description: Students', 'http://localhost/aartoon/control_panel/dist/img/home_slider_mng/3296323.jpg', 1, 1, 1, '2022-05-27 09:34:54', '2022-08-19 09:28:42'),
(16, '', '', 'http://localhost/aartoon/control_panel/dist/img/home_slider_mng/video.mp4', 2, 1, 0, '2022-05-29 00:37:57', '2022-08-19 10:11:28');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `title`, `image`, `description`, `type`, `created_at`) VALUES
(1, 'PCB Manufacture ', 'http://localhost/aartoon/control_panel/dist/img/uploaded_images/images.jpg', '<p>Manufacture&nbsp;PCB&nbsp;<span style=\"font-weight: 700; font-size: 1rem;\">Description</span></p>', 1, '2022-04-27 05:25:48'),
(2, 'PCB Assembly,Manufacture  demos', 'http://localhost/aartoon/control_panel/dist/img/uploaded_images/1_pcb-routing.jpg', '<p>Manufacture&nbsp;Manufacture&nbsp;Manufacture&nbsp;Manufacture&nbsp;Manufacture&nbsp;<br></p>', 1, '2022-08-19 09:17:26');

-- --------------------------------------------------------

--
-- Table structure for table `leader_board_score`
--

CREATE TABLE `leader_board_score` (
  `l_b_s_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `answer_flag` int(11) NOT NULL DEFAULT 0 COMMENT '0:-No, 1:-Yes',
  `score` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `leader_board_score`
--

INSERT INTO `leader_board_score` (`l_b_s_id`, `uid`, `video_id`, `question_id`, `answer`, `answer_flag`, `score`, `created_at`) VALUES
(2, 2, 1, 1, 'Design Software', 0, '', '2022-04-29 14:13:24'),
(7, 2, 1, 2, 'Design Software', 0, '', '2022-04-29 14:38:27'),
(19, 1, 3, 5, '', 1, '', '2022-06-02 14:09:48'),
(20, 1, 3, 5, 'Software', 1, '', '2022-06-02 14:09:52'),
(21, 1, 3, 5, 'A', 1, '', '2022-06-02 14:09:54'),
(28, 60, 2, 3, 'Software\n                                                                            \n                                                                            \n                                                                        ', 1, '', '2022-07-05 12:34:38'),
(29, 60, 6, 4, '3D Printing\n                                                                            \n                                                                            \n                                                                        ', 1, '', '2022-07-05 12:35:29'),
(30, 60, 6, 7, 'X\n                                                                            \n                                                                            \n                                                                        ', 1, '', '2022-07-05 12:35:46'),
(31, 60, 7, 8, 'A\n                                                                            \n                                                                            \n                                                                        ', 1, '', '2022-07-05 12:37:17'),
(32, 60, 3, 5, 'developement3\n                                                                            \n                                                                            \n                                                                        ', 1, '', '2022-07-05 14:24:57'),
(33, 60, 3, 11, ' developement1\n                                                                            \n                                                                            \n                                                                        ', 1, '', '2022-07-05 14:25:19'),
(34, 60, 3, 6, 'D\n                                                                            \n                                                                            \n                                                                        ', 1, '', '2022-07-05 14:54:50');

-- --------------------------------------------------------

--
-- Table structure for table `live_class_amount`
--

CREATE TABLE `live_class_amount` (
  `id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `live_class_amount`
--

INSERT INTO `live_class_amount` (`id`, `amount`, `updated_at`) VALUES
(1, '100', '2022-04-20 13:00:16');

-- --------------------------------------------------------

--
-- Table structure for table `live_class_details`
--

CREATE TABLE `live_class_details` (
  `page_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `videos` varchar(255) NOT NULL,
  `live_class_types` int(11) NOT NULL COMMENT '	1:-3d_modeling, 2:-3d_modeling_texturing, 3:-game_design, 4:-digital_art',
  `status` int(11) NOT NULL COMMENT '0:-Deactive, 1:-Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `live_class_details`
--

INSERT INTO `live_class_details` (`page_id`, `title`, `description`, `videos`, `live_class_types`, `status`) VALUES
(1, 'Welcome to 3D Animation', '<p> 3D Computer animation is the process used for digitally generating animated images. </p><p>The more general term computer-generated imagery encompasses both static scenes and dynamic images, while computer animation only refers to moving images</p><p>Image result for 3d animation</p><p><span style=\"font-size: 1rem;\">3D animation is the process of creating moving three-dimensional images in a digital context. These visuals are made using 3D software, allowing animators to create computerized objects that look 3D even though they\'re on a 2D surface.</span></p>', 'http://localhost/aartoon/control_panel/dist/img/live_class_subscription/Animation_sample_work.mp4', 1, 1),
(2, 'Welcome to 3D model texture', '3D Texturing is basically wrapping a 2D image around a 3D object and defining how light would affect it. Various software packages have different tools and techniques for adding texture to a 3D model. The texturing stage of the 3D animation pipeline includes unwrapping, texture painting & shading, and rendering.', 'http://localhost/aartoon/control_panel/dist/img/live_class_subscription/Animation_sample_work.mp4', 2, 1),
(3, 'Welcome to Game Design', 'Game design is the art of applying design and aesthetics to create a game for entertainment or for educational, exercise, or experimental purposes. Increasingly, elements and principles of game design are also applied to other interactions, in the form of gamification.\r\n\r\nGame design is a large field, drawing from the fields of computer science/programming, creative writing, and graphic design. Game designers take the creative lead in imagining and bringing to life video game worlds.', 'http://localhost/aartoon/dist/img/live_class_subscription/video.mp4', 3, 1),
(4, 'Welcome to Digital art', 'Digital art is an artistic work or practice that uses digital technology as part of the creative or presentation process. Since the 1960s, various names have been used to describe the process, including computer art and multimedia art. Digital art is itself placed under the larger umbrella term new media art.', 'http://localhost/aartoon/control_panel/dist/img/live_class_subscription/Animation_sample_work.mp4', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `live_class_subscription_plans`
--

CREATE TABLE `live_class_subscription_plans` (
  `subs_id` int(11) NOT NULL,
  `subs_type` int(11) NOT NULL COMMENT '1:-3d_modeling, 2:-3d_modeling_texturing, 3:-game_design, 4:-digital_art',
  `subs_title` varchar(255) NOT NULL,
  `total_classes` varchar(255) NOT NULL,
  `subs_description` varchar(10000) NOT NULL,
  `total_price` varchar(255) NOT NULL,
  `offer_price` varchar(255) NOT NULL,
  `per_class_fee` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `docs_file` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `live_class_subscription_plans`
--

INSERT INTO `live_class_subscription_plans` (`subs_id`, `subs_type`, `subs_title`, `total_classes`, `subs_description`, `total_price`, `offer_price`, `per_class_fee`, `images`, `docs_file`, `created_at`, `updated_at`) VALUES
(1, 1, '3D Animation', '10', 'Animating objects that appear in a three-dimensional space. They can be rotated and moved like real objects. 3D animation is at the heart of games and virtual reality, but it may also be used in presentation graphics to add flair to the visuals', '8999', '7999', '1000', 'http://localhost/aartoon/dist/img/live_class_subscription/proo.jpg', '', '2022-05-24 11:17:14', '2022-05-24 06:38:36'),
(2, 1, 'What is 3D in animation?', '5', 'A 3D Animator interprets concept art into its three-dimensional realization and creates moving images using digital models via CGI software. They may be asked to create animation and special effects for film, television, video games, advertisements, websites,', '7999', '7199', '900', 'http://localhost/aartoon/dist/img/live_class_subscription/about-us.jpg', 'http://localhost/aartoon/dist/img/live_class_subscription/Offer_Letter_-_Mahadev_Ingawale.pdf', '2022-05-24 11:19:08', '2022-05-31 11:26:40'),
(4, 2, '3D Modelling & Texturing', '25', '3D Texturing is basically wrapping a 2D image around a 3D object and defining how light would affect it. Various software packages have different tools and techniques for adding texture to a 3D model. The texturing stage of the 3D animation pipeline includes unwrapping, texture painting & shading, and rendering.', '9999', '8999', '2000', 'http://localhost/aartoon/dist/img/live_class_subscription/2d.png', 'http://localhost/aartoon/dist/img/live_class_subscription/sample.pdf', '2022-05-24 14:03:49', '2022-05-31 11:27:45'),
(5, 2, '3D Modelling & Texturing ', '45', 'escriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or', '9999', '8888', '999', 'http://localhost/aartoon/dist/img/live_class_subscription/about-us.jpg', 'http://localhost/aartoon/dist/img/live_class_subscription/Offer_Letter_-_Mahadev_Ingawale1.pdf', '2022-05-24 14:45:20', '2022-05-31 11:27:24'),
(6, 3, 'Game Design', '15', 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '8888', '7777', '666', 'http://localhost/aartoon/dist/img/live_class_subscription/2d.png', 'http://localhost/aartoon/dist/img/live_class_subscription/Offer_Letter_-_Mahadev_Ingawale2.pdf', '2022-05-24 15:21:23', '2022-05-31 11:34:30'),
(7, 4, 'Digital Art', '25', ' work or practice that uses digital technology as part of the creative or presentation process. Since the 1960s, various names have been used to describe the process, including computer art and multimedia art. Digital art is itself placed under the larger umbrella term new media art. artistic work or practice that uses digital technology as part of the creative or presentation process. Since the 1960s, various names have been used to describe the process, including computer art and multimedia art.', '5999', '5100', '500', 'http://localhost/aartoon/dist/img/live_class_subscription/2d.png', 'http://localhost/aartoon/dist/img/live_class_subscription/sample.pdf', '2022-05-24 15:41:21', '2022-05-31 11:38:40'),
(13, 1, '3D Animation2', '2', 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '5999', '999', '999', 'http://localhost/aartoon/dist/img/live_class_subscription/about-us.jpg', 'http://localhost/aartoon/dist/img/live_class_subscription/sample.pdf', '2022-05-31 14:25:23', '2022-05-31 10:42:41'),
(15, 2, '3D Modelling & Texturing', '5', 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '8999', '7999', '900', 'http://localhost/aartoon/dist/img/live_class_subscription/1111.jpg', 'http://localhost/aartoon/dist/img/live_class_subscription/ditrp_school_app_updated.pdf', '2022-05-31 15:11:27', '2022-05-31 09:41:27'),
(16, 3, 'Game Design 2', '4', 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '8999', '7999', '900', 'http://localhost/aartoon/dist/img/live_class_subscription/game.jpg', 'http://localhost/aartoon/dist/img/live_class_subscription/ResumeLaxmiShyju.pdf', '2022-05-31 15:16:06', '2022-05-31 09:46:06'),
(17, 4, 'Digital Art 2', '5', 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '10000', '8888', '900', 'http://localhost/aartoon/dist/img/live_class_subscription/Digital_Art2.jpg', 'http://localhost/aartoon/dist/img/live_class_subscription/ResumeLaxmiShyju.pdf', '2022-05-31 15:20:08', '2022-05-31 09:50:08');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `module_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`module_id`, `category_id`, `course_id`, `module_name`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'PCB Design Module  biginner 1x', '2022-04-27 05:09:15', '2022-06-09 11:03:19'),
(2, 3, 3, '3D begineer Module 1x', '2022-05-11 11:58:57', '2022-07-05 14:11:18'),
(3, 3, 3, '3D Advance Module 2x', '2022-05-18 12:58:31', '2022-06-09 11:04:29'),
(4, 1, 1, 'PCB Advance Module 2x', '2022-05-25 06:33:47', '2022-06-09 11:03:49');

-- --------------------------------------------------------

--
-- Table structure for table `module_item`
--

CREATE TABLE `module_item` (
  `module_item_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_item`
--

INSERT INTO `module_item` (`module_item_id`, `module_id`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Socket item ', '<p>demo   item 1xx <span style=\"font-weight: 700; font-size: 1rem;\">Description</span></p>', 1, '2022-04-27 05:09:42', '2022-06-09 11:11:35'),
(2, 2, 'development  Item ', '<p>3D Item1<br></p>', 1, '2022-05-11 11:59:15', '2022-06-09 11:27:53'),
(3, 3, 'designing  Item 2xx2', '<p>module 2 <span style=\"font-weight: 700; font-size: 1rem;\">Description</span><br></p>', 1, '2022-05-18 12:58:52', '2022-06-09 11:11:51'),
(4, 4, 'Layout item ', '<p><span style=\"text-align: center;\">Human Resources</span><span style=\"text-align: center; font-size: 1rem;\">Human Resources</span><span style=\"text-align: center; font-size: 1rem;\">Human Resources</span><span style=\"text-align: center; font-size: 1rem;\"', 1, '2022-05-25 06:33:58', '2022-06-09 11:07:09'),
(5, 3, 'printing  Item 2xx1', ' Item 2xx', 1, '2022-05-30 10:25:14', '2022-06-09 11:11:43');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_active` int(11) DEFAULT NULL COMMENT '0:-No, 1:-Yes',
  `refer_given_to` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `uid`, `description`, `is_active`, `refer_given_to`, `created_at`) VALUES
(1, 4, 'You got Rs. 30 as wallet point, as your refferal code used by new user.', 1, 5, '2022-04-27 14:19:27'),
(2, 3, 'You got Rs. 30 as wallet point, as your refferal code used by new user.', 1, 6, '2022-04-27 14:53:40'),
(3, 3, 'You got Rs. 30 as wallet point, as your refferal code used by new user.', 1, 7, '2022-05-05 10:43:04'),
(4, 4, 'You got Rs. 30 as wallet point, as your refferal code used by new user.', 1, 8, '2022-05-05 12:57:36'),
(5, 4, 'You got Rs. 30 as wallet point, as your refferal code used by new user.', 1, 11, '2022-05-05 14:24:43'),
(6, 4, 'You got Rs. 30 as wallet point, as your refferal code used by new user.', 1, 12, '2022-05-05 14:31:11'),
(7, 4, 'You got Rs. $wallet_amount_ref as wallet point, as your refferal code used by new user.', 1, 13, '2022-05-05 16:44:32'),
(8, 4, 'You got Rs. 390 as wallet point, as your refferal code used by new user.', 1, 14, '2022-05-05 16:45:58'),
(9, 4, 'You got Rs. 490 as wallet point, as your refferal code used by new user.', 1, 15, '2022-05-05 17:25:56'),
(10, 3, 'You got Rs. 160 as wallet point, as your refferal code used by new user.', 1, 22, '2022-05-13 16:44:58'),
(11, 1, 'You got Rs. 100 as wallet point, as your refferal code used by new user.', 1, 23, '2022-05-13 17:00:58'),
(12, 1, 'You got Rs. 200 as wallet point, as your refferal code used by new user.', 1, 24, '2022-05-13 17:10:43'),
(13, 1, 'You got Rs. 300 as wallet point, as your refferal code used by new user.', 1, 25, '2022-05-13 17:13:27'),
(14, 1, 'You got Rs. 400 as wallet point, as your refferal code used by new user.', 1, 26, '2022-05-13 17:14:08'),
(15, 3, 'You got Rs. 260 as wallet point, as your refferal code used by new user.', 1, 30, '2022-05-15 09:51:03'),
(16, 4, 'You got Rs. 590 as wallet point, as your refferal code used by new user.', 1, 34, '2022-05-15 10:05:16'),
(17, 4, 'You got Rs. 690 as wallet point, as your refferal code used by new user.', 1, 45, '2022-05-15 11:49:56'),
(18, 3, 'You got Rs. 360 as wallet point, as your refferal code used by new user.', 1, 61, '2022-05-21 17:06:18'),
(19, 3, 'You got Rs. 460 as wallet point, as your refferal code used by new user.', 1, 67, '2022-06-27 17:04:19'),
(20, 4, 'You got Rs. 790 as wallet point, as your refferal code used by new user.', 1, 68, '2022-06-27 17:49:38');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_post_review_amount`
--

CREATE TABLE `portfolio_post_review_amount` (
  `p_p_r_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `portfolio_post_review_amount`
--

INSERT INTO `portfolio_post_review_amount` (`p_p_r_id`, `amount`, `updated_at`) VALUES
(1, '149', '2022-06-07 15:06:44');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy`
--

CREATE TABLE `privacy_policy` (
  `id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1:-student, 2:-school',
  `privacy_policy` varchar(20000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacy_policy`
--

INSERT INTO `privacy_policy` (`id`, `user_type`, `privacy_policy`) VALUES
(1, 1, '<p>demo1</p>'),
(2, 2, '<p>demo2</p>'),
(3, 3, '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Privacy Policy</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">This Privacy Policy was last updated on May 5, 2022.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Thank you for joining Aartoon. We at Aartoon (“Aartoon”, “we”, “us”) respect your privacy and want you to understand how we collect, use, and share data about you. This Privacy Policy covers our data collection practices and describes your rights regarding your personal data.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Unless we link to a different policy or state otherwise, this Privacy Policy applies when you visit or use the Aartoon and CorpU websites, APIs, or related services. It also applies to prospective customers of our business and enterprise products.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">By using the Services, you agree to the terms of this Privacy Policy. You shouldn’t use the Services if you don’t agree with this Privacy Policy or any other agreement that governs your use of the Services.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Table of Contents</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">1. What Data We Get</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">2. How We Get Data About You</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">3. What We Use Your Data For</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">4. Who We Share Your Data With</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">5. Security</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">6. Your Rights</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">7. Jurisdiction-Specific Rules</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">8. Updates &amp; Contact Info</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">1. What Data We Get</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">We collect certain data from you directly, like information you enter yourself, data about your consumption of content, and data from third-party platforms you connect with Aartoon. We also collect some data automatically, like information about your device and what parts of our Services you interact with or spend time using.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">1.1 Data You Provide to Us</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">We may collect different data from or about you depending on how you use the Services. Below are some examples to help you better understand the data we collect.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">When you create an account and use the Services, including through a third-party platform, we collect any data you provide directly, including:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Account Data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>In order to use certain features (like accessing content), you need to create a user account which requires us to collect and store your email address, password, and account settings. To create an instructor account, we collect and store your name, email address, password, and account settings. As you use certain features on the site, you may be prompted to submit additional information including occupation, government ID information, verification photo, date of birth, race/ethnicity, skill interests, and phone number. Upon account creation, we assign you a unique identifying number.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Profile Data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>You can also choose to provide profile information like a photo, headline, biography, language, website link, social media profiles, country, or other data. Your Profile Data will be publicly viewable by others.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Shared Content data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>Parts of the Services let you interact with other users or share content publicly, including by posting reviews about content, asking or answering questions, sending messages to students or instructors, or posting photos or other work you upload. Such shared content may be publicly viewable by others depending on where it is posted.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Educational Content Data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>When you access content, we collect certain data including which courses, assignments, labs, work spaces, and quizzes you’ve started and completed; content purchases and credits; subscriptions; completion certificates; your exchanges with instructors, teaching assistants, and other students; and essays, answers to questions, and other items submitted to satisfy course and related content requirements. If you are an instructor, we store your educational content which may contain data about you.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Student Payment Data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>If you make purchases, we collect certain data about your purchase (such as your name, billing address, and ZIP code) as necessary to process your order and which may optionally be saved to process future orders. You must provide certain payment and billing data directly to our payment service providers, including your name, credit card information, billing address, and ZIP code. We may also receive limited information, like the fact that you have a new card and the last four digits of that card, from payment service providers to facilitate payments. For security, Aartoon does not collect or store sensitive cardholder data, such as full credit card numbers or card authentication data.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Instructor Payment Data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>If you are an instructor, you can link your PayPal, Payoneer, or other payment account to the Services to receive payments. When you link a payment account, we collect and use certain information, including your payment account email address, account ID, physical address, or other data necessary for us to send payments to your account. In some instances, we may collect ACH or wire information to send payments to your account. In order to comply with applicable laws, we also work with trusted third parties who collect tax information as legally required. This tax information may include residency information, tax identification numbers, biographical information, and other personal information necessary for taxation purposes. For security, Aartoon does not collect or store sensitive bank account information. The collection, use, and disclosure of your payment, billing, and taxation data is subject to the privacy policy and other terms of your payment account provider.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Data About Your Accounts on Other Services:<span style=\"white-space:pre\">	</span></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">We may obtain certain information through your social media or other online accounts if they are connected to your Aartoon account. If you login to Aartoon via Facebook or another third-party platform or service, we ask for your permission to access certain information about that other account. For example, depending on the platform or service we may collect your name, profile picture, account ID number, login email address, location, physical location of your access devices, gender, birthday, and list of friends or contacts.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Those platforms and services make information available to us through their APIs. The information we receive depends on what information you (via your privacy settings) or the platform or service decide to give us.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">If you access or use our Services through a third-party platform or service, or click on any third-party links, the collection, use, and sharing of your data will also be subject to the privacy policies and other agreements of that third party.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Sweepstakes, Promotions, and Surveys:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>We may invite you to complete a survey or participate in a promotion (like a contest, sweepstakes, or challenge), either through the Services or a third-party platform. If you participate, we will collect and store the data you provide as part of participating, such as your name, email address, postal address, date of birth, or phone number. That data is subject to this Privacy Policy unless otherwise stated in the official rules of the promotion or in another privacy policy. The data collected will be used to administer the promotion or survey, including for notifying winners and distributing rewards. To receive a reward, you may be required to allow us to post some of your information publicly (like on a winner’s page). Where we use a third-party platform to administer a survey or promotion, the third party’s privacy policy will apply.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Communications and Support:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>If you contact us for support or to report a problem or concern (regardless of whether you have created an account), we collect and store your contact information, messages, and other data about you like your name, email address, messages, location, Aartoon user ID, refund transaction IDs, and any other data you provide or that we collect through automated means (which we cover below). We use this data to respond to you and research your question or concern, in accordance with this Privacy Policy.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">The data listed above is stored by us and associated with your account.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">1.2 Data We Collect through Automated Means</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">When you access the Services (including browsing content), we collect certain data by automated means, including:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">System Data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>Technical data about your computer or device, like your IP address, device type, operating system type and version, unique device identifiers, browser, browser language, domain and other systems data, and platform types.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Usage Data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>Usage statistics about your interactions with the Services, including content accessed, time spent on pages or the Service, pages visited, features used, your search queries, click data, date and time, referrer, and other data regarding your use of the Services.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">Approximate Geographic Data:</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><span style=\"white-space:pre\">	</span>An approximate geographic location, including information like country, city, and geographic coordinates, calculated based on your IP address.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">The data listed above is collected through the use of server log files and tracking technologies, as detailed in the “Cookies and Data Collection Tools” section below. It is stored by us and associated with your account.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">1.3 Data from Third Parties</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">If you are a Aartoon Business enterprise or corporate prospect, in addition to information you submit to us, we may collect certain business contact information from third-party commercial sources.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\"><br></font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">2. How We Get Data About You</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">We use tools like cookies, web beacons, and similar tracking technologies to gather the data listed above. Some of these tools offer you the ability to opt out of data collection.</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">2.1 Cookies and Data Collection Tools</font></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 28px;\"><font color=\"#505050\" face=\"Roboto, sans-serif\">We use cookies, which are small text files stored by your browser, to collect, store, and share data about your activities across websites, including on Aartoon. They allow us to remember things about your visits to Aartoon, like your preferred language, and to make the site easier to use');

-- --------------------------------------------------------

--
-- Table structure for table `referal_code`
--

CREATE TABLE `referal_code` (
  `ref_code_id` int(11) NOT NULL,
  `ref_amount` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referal_code`
--

INSERT INTO `referal_code` (`ref_code_id`, `ref_amount`, `updated_at`) VALUES
(1, '200', '2022-04-20 12:20:11');

-- --------------------------------------------------------

--
-- Table structure for table `referred_code`
--

CREATE TABLE `referred_code` (
  `reff_code_id` int(11) NOT NULL,
  `referred_amount` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `referred_code`
--

INSERT INTO `referred_code` (`reff_code_id`, `referred_amount`, `updated_at`) VALUES
(1, '100', '2022-05-05 14:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `refund_policy`
--

CREATE TABLE `refund_policy` (
  `id` int(11) NOT NULL,
  `refund_policy` mediumtext NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `refund_policy`
--

INSERT INTO `refund_policy` (`id`, `refund_policy`, `updated_at`) VALUES
(1, '<div>Payments, Credits, and Refunds</div><div><br></div><div>When you make a payment, you agree to use a valid payment method.</div><div><br></div><div>1 Pricing</div><div><br></div><div>The prices of content on Aartoon are determined based on the terms of the Instructor Terms and our Promotions Policy. In some instances, the price of content offered on the Aartoon website may not be exactly the same as the price offered on our mobile or TV applications, due to mobile platform providers’ pricing systems and their policies around implementing sales and promotions.</div><div><br></div><div>We occasionally run promotions and sales for our content, during which certain content is available at discounted prices for a set period of time. The price applicable to the content will be the price at the time you complete your purchase of the content (at checkout). Any price offered for particular content may also be different when you are logged into your account from the price available to users who aren’t registered or logged in, because some of our promotions are available only to new users.</div><div><br></div><div>If you are logged into your account, the listed currency you see is based on your location when you created your account. If you are not logged into your account, the price currency is based on the country where you are located. We do not enable users to see pricing in other currencies.</div><div><br></div><div>If you are a student located in a country where use and sales tax, goods and services tax, or value added tax is applicable to consumer sales, we are responsible for collecting and remitting that tax to the proper tax authorities. Depending on your location, the price you see may include such taxes, or tax may be added at checkout.</div><div><br></div><div>2 Payments</div><div><br></div><div>You agree to pay the fees for content that you purchase, and you authorize us to charge your debit or credit card or process other means of payment (such as Boleto, SEPA, direct debit, or mobile wallet) for those fees. Aartoon works with payment service providers to offer you the most convenient payment methods in your country and to keep your payment information secure. We may update your payment methods using information provided by our payment service providers. Check out our Privacy Policy for more details.</div><div><br></div><div>When you make a purchase, you agree not to use an invalid or unauthorized payment method. If your payment method fails and you still get access to the content you are enrolling in, you agree to pay us the corresponding fees within thirty (30) days of notification from us. We reserve the right to disable access to any content for which we have not received adequate payment.</div><div><br></div><div>3 Refunds and Refund Credits</div><div><br></div><div>Aartoon Education Pvt.Ltd. Is not entitle for refunds to any individual institutions or companies that have parches the course.</div><div><br></div><div>4 Gift and Promotional Codes</div><div><br></div><div>Aartoon or our partners may offer gift and promotional codes to students. Certain codes may be redeemed for gift or promotional credits applied to your Aartoon account, which then may be used to purchase eligible content on our platform, subject to the terms included with your codes. Other codes may be directly redeemable for specific content. Gift and promotional credits can’t be used for purchases in our mobile or TV applications.</div><div><br></div><div>These codes and credits, as well as any promotional value linked to them, may expire if not used within the period specified in your Aartoon account. Gift and promotional codes offered by Aartoon may not be refunded for cash, unless otherwise specified in the terms included with your codes or as required by applicable law. Gift and promotional codes offered by a partner are subject to that partner’s refund policies. If you have multiple saved credit amounts, Aartoon may determine which of your credits to apply to your purchase. Check out our Support Page and any terms included with your codes for more details.</div>', '2022-05-25 17:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `revenue_management`
--

CREATE TABLE `revenue_management` (
  `revenue_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `school_uid` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `revenue_type` int(11) NOT NULL COMMENT '1:-course revenue, 2:-live class revenue,\r\n3:-portfolio_review',
  `user_p_re_id` int(11) NOT NULL,
  `course_amount` varchar(255) NOT NULL,
  `revenue` varchar(255) NOT NULL,
  `wallet_amount` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_type` int(11) DEFAULT 2 COMMENT '1:-wallet, 2:-online',
  `transaction_id` varchar(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL COMMENT '0:fail, 1:success',
  `payment_mode` varchar(255) NOT NULL,
  `buy_course` int(11) NOT NULL DEFAULT 0 COMMENT '0:-No, 1:-Yes',
  `completed_course` int(11) NOT NULL DEFAULT 0 COMMENT '0:-No, 1:-Yes',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `revenue_management`
--

INSERT INTO `revenue_management` (`revenue_id`, `uid`, `school_uid`, `course_id`, `revenue_type`, `user_p_re_id`, `course_amount`, `revenue`, `wallet_amount`, `payment_date`, `payment_type`, `transaction_id`, `order_id`, `payment_status`, `payment_mode`, `buy_course`, `completed_course`, `created_at`, `updated_at`) VALUES
(1, 6, 0, 0, 3, 28, '', '149', '', '2022-06-22', 2, 'pay_JkVdyIBOzUmKpY', 'order_JkVdsBwY9dM0xY', 1, '', 0, 0, '2022-06-22 10:35:32', '0000-00-00 00:00:00'),
(2, 6, 0, 3, 1, 0, '', '4999', '', '2022-06-23', 2, 'pay_JksYRzFYPELZhG', 'order_JksYLTuNmjurEQ', 1, 'upi', 1, 0, '2022-06-23 09:00:17', '0000-00-00 00:00:00'),
(3, 2, 0, 1, 1, 0, '', '2999', '', '2022-06-23', 2, 'pay_JksZAzWrx7iJPE', 'order_JksZ4NpoIzHpj4', 1, 'upi', 1, 1, '2022-06-23 09:00:57', '0000-00-00 00:00:00'),
(4, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl0sEykbkJ30mb', 'order_Jl0pWQUIJ8pZYX', 1, 'upi', 1, 1, '2022-06-23 17:08:35', '2022-09-28 09:54:00'),
(5, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl0uaZLkL7aPUA', 'order_Jl0uGhEKTBpC2U', 1, 'upi', 1, 1, '2022-06-23 17:10:48', '2022-09-28 09:54:00'),
(6, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl0veAqAZkwdUf', 'order_Jl0vPXlisw3zXv', 1, 'upi', 1, 1, '2022-06-23 17:11:48', '2022-09-28 09:54:00'),
(7, 2, 0, 0, 3, 29, '', '149', '', '2022-06-23', 2, 'pay_Jl14ytm2mrxMiQ', 'order_Jl12nXz9xacJuE', 1, '', 0, 0, '2022-06-23 17:20:37', '0000-00-00 00:00:00'),
(8, 2, 0, 3, 1, 0, '', '4999', '', '2022-06-23', 2, 'pay_Jl1BKHKwMB6tVi', 'order_Jl1AodYvTGBuUZ', 1, 'upi', 1, 1, '2022-06-23 17:26:39', '2022-09-28 09:54:00'),
(9, 2, 0, 3, 1, 0, '', '4999', '', '2022-06-23', 2, 'pay_Jl1Eyxzt7V5BKc', 'order_Jl1EnR2RJXPDCx', 1, 'upi', 1, 1, '2022-06-23 17:30:06', '2022-09-28 09:54:00'),
(10, 2, 0, 3, 1, 0, '', '4999', '', '2022-06-23', 2, 'pay_Jl1K5uGaYa3S24', 'order_Jl1JwbqmnVr3xn', 1, 'upi', 0, 1, '2022-06-23 17:34:56', '2022-09-28 09:54:00'),
(11, 2, 0, 0, 3, 30, '', '149', '', '2022-06-23', 2, 'pay_Jl1Kk6QUMoh3ci', 'order_Jl1KdtQLUers2r', 1, '', 0, 0, '2022-06-23 17:35:32', '0000-00-00 00:00:00'),
(12, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl1L9ghcUtccC2', 'order_Jl1KzZAPdNihrd', 1, 'upi', 1, 1, '2022-06-23 17:35:58', '2022-09-28 09:54:00'),
(13, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl1lJjjgHk1H8K', 'order_Jl1l05O75rKak8', 1, 'upi', 1, 1, '2022-06-23 18:00:43', '2022-09-28 09:54:00'),
(14, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl1qxpgAHpvyca', 'order_Jl1qoFTBsqsubV', 1, 'upi', 1, 1, '2022-06-23 18:06:04', '2022-09-28 09:54:00'),
(15, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl1ybLLA0EM5Tg', 'order_Jl1yO9238a0EAK', 1, 'upi', 1, 1, '2022-06-23 18:13:17', '2022-09-28 09:54:00'),
(16, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl20vS8JP3fAzr', 'order_Jl20nw2HEH7TPA', 1, 'upi', 1, 1, '2022-06-23 18:15:29', '2022-09-28 09:54:00'),
(17, 2, 0, 3, 2, 0, '0', '99', '', '2022-06-23', 2, 'pay_Jl276n7W0zQGqy', 'order_Jl24M4HZBQy6zm', 1, 'netbanking', 1, 1, '2022-06-23 18:21:20', '2022-09-28 09:54:00'),
(18, 2, 0, 0, 3, 31, '', '149', '', '2022-06-24', 2, 'pay_JlFFXBL3JScLak', 'order_JlFFPtsqKTTfx9', 1, '', 0, 0, '2022-06-24 07:12:19', '0000-00-00 00:00:00'),
(19, 6, 0, 6, 1, 0, '', '5999', '', '2022-06-24', 2, 'pay_JlJVIqMlXPLVS2', 'order_JlJVCeLryaLDPS', 1, 'upi', 1, 0, '2022-06-24 11:22:01', '0000-00-00 00:00:00'),
(20, 2, 0, 3, 2, 0, '0', '49', '', '2022-06-27', 2, 'pay_JmSUBbr7DzPwKg', 'order_JmSU2Jylc2ArTc', 1, 'upi', 1, 1, '2022-06-27 08:48:15', '2022-09-28 09:54:00'),
(21, 2, 0, 3, 2, 0, '0', '49', '', '2022-06-27', 2, 'pay_JmSZWQYQnKFR1d', 'order_JmSZPm87DHyKSB', 1, 'upi', 1, 1, '2022-06-27 08:53:18', '2022-09-28 09:54:00'),
(22, 2, 0, 3, 2, 0, '0', '49', '', '2022-06-27', 2, 'pay_JmSeQm9wWNeG3k', 'order_JmSeJLEzcJeGkp', 1, 'upi', 1, 1, '2022-06-27 08:57:57', '2022-09-28 09:54:00'),
(23, 2, 0, 3, 2, 0, '0', '49', '', '2022-06-27', 2, 'pay_JmSgUSWLRhQmxJ', 'order_JmSgOCLcFrAR46', 1, 'upi', 1, 1, '2022-06-27 08:59:54', '2022-09-28 09:54:00'),
(24, 2, 0, 3, 2, 0, '0', '49', '', '2022-06-27', 2, 'pay_JmSmb84wdvCsJo', 'order_JmSmUDsPHg222h', 1, 'upi', 1, 1, '2022-06-27 09:05:40', '2022-09-28 09:54:00'),
(25, 2, 0, 3, 2, 0, '0', '49', '', '2022-06-27', 2, 'pay_JmSt1H74OxExbD', 'order_JmSstfoUontEf2', 1, 'upi', 1, 1, '2022-06-27 09:11:45', '2022-09-28 09:54:00'),
(26, 2, 0, 3, 2, 0, '0', '49', '', '2022-06-27', 2, 'pay_JmSu6zQzDuvVlg', 'order_JmSu118Kx9dfte', 1, '', 1, 1, '2022-06-27 09:13:09', '2022-09-28 09:54:00'),
(27, 2, 0, 3, 2, 0, '0', '49', '', '2022-06-27', 2, 'pay_JmSyol28wX19ND', 'order_JmSycxI2cHTPgv', 1, 'card', 1, 1, '2022-06-27 09:17:13', '2022-09-28 09:54:00'),
(28, 2, 0, 0, 3, 32, '', '149', '', '2022-06-27', 2, 'pay_JmTZaRTpF3ZyrG', 'order_JmTZUPpyoDLqCN', 1, '', 0, 0, '2022-06-27 09:52:03', '0000-00-00 00:00:00'),
(29, 2, 0, 6, 1, 0, '', '5999', '', '2022-06-27', 2, 'pay_JmTnU9tBXcBHZf', 'order_JmTnNb6yZBwfOC', 1, 'upi', 1, 0, '2022-06-27 10:05:12', '0000-00-00 00:00:00'),
(30, 60, 0, 3, 2, 0, '0', '10', '', '2022-06-28', 2, 'pay_JmnARPkqWTVHES', 'order_JmnAIduqGRMKa2', 1, 'upi', 1, 1, '2022-06-28 05:02:07', '2022-07-05 15:51:54'),
(31, 60, 0, 2, 1, 0, '', '3999', '', '2022-04-28', 2, 'pay_JmoBzuth1OQ3bn', 'order_JmoBsLaHwnukZY', 1, 'upi', 1, 0, '2022-06-28 06:02:17', '0000-00-00 00:00:00'),
(32, 60, 0, 0, 3, 33, '', '149', '', '2022-06-28', 2, 'pay_JmoFFPG84U4Zvm', 'order_JmoF7GgfgEC3Qf', 1, '', 0, 0, '2022-06-28 06:05:20', '0000-00-00 00:00:00'),
(33, 60, 0, 0, 3, 34, '', '149', '', '2022-06-28', 2, 'pay_JmoKG3AqnMHXIj', 'order_JmoK03EHVTacI5', 1, '', 0, 0, '2022-06-28 06:10:05', '0000-00-00 00:00:00'),
(35, 60, 0, 1, 2, 0, '0', '0', '', '2022-06-29', 2, '-', '-', 1, 'wallet amount', 1, 0, '2022-06-29 05:44:00', '0000-00-00 00:00:00'),
(37, 60, 0, 2, 2, 0, '0', '155', '', '2022-05-29', 2, '-', '-', 1, 'wallet amount', 1, 0, '2022-06-29 06:28:00', '0000-00-00 00:00:00'),
(38, 61, 0, 2, 2, 0, '0', '170', '200', '2022-04-29', 1, '-', '', 1, 'wallet amount', 1, 0, '2022-06-29 07:32:47', '0000-00-00 00:00:00'),
(39, 60, 0, 2, 2, 0, '0', '49', '', '2022-06-30', 1, '-', '-', 1, 'wallet amount', 1, 0, '2022-06-30 12:27:29', '0000-00-00 00:00:00'),
(40, 60, 0, 2, 2, 0, '0', '10', '', '2022-06-30', 1, '-', '-', 1, 'wallet amount', 1, 0, '2022-06-30 12:37:32', '0000-00-00 00:00:00'),
(41, 60, 0, 2, 2, 0, '0', '51', '', '2022-06-30', 1, '-', '-', 1, 'wallet amount', 1, 0, '2022-06-30 12:40:48', '0000-00-00 00:00:00'),
(42, 60, 0, 2, 2, 0, '0', '49', '', '2022-06-30', 1, '-', '-', 1, 'wallet amount', 1, 0, '2022-06-30 12:40:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `revenue_management_backup`
--

CREATE TABLE `revenue_management_backup` (
  `revenue_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `school_uid` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `revenue_type` int(11) NOT NULL COMMENT '1:-course revenue, 2:-online course,\r\n3:-portfolio_review',
  `user_p_re_id` int(11) NOT NULL,
  `course_amount` varchar(255) NOT NULL,
  `revenue` varchar(255) NOT NULL,
  `wallet_amount` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_type` int(11) DEFAULT 2 COMMENT '1:-offline, 2:-online',
  `transaction_id` varchar(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL COMMENT '0:fail, 1:success',
  `payment_mode` varchar(255) NOT NULL,
  `buy_course` int(11) NOT NULL DEFAULT 0 COMMENT '0:-No, 1:-Yes',
  `completed_course` int(11) NOT NULL DEFAULT 0 COMMENT '0:-No, 1:-Yes',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `revenue_management_backup`
--

INSERT INTO `revenue_management_backup` (`revenue_id`, `uid`, `school_uid`, `course_id`, `revenue_type`, `user_p_re_id`, `course_amount`, `revenue`, `wallet_amount`, `payment_date`, `payment_type`, `transaction_id`, `order_id`, `payment_status`, `payment_mode`, `buy_course`, `completed_course`, `created_at`) VALUES
(6, 2, 0, 0, 3, 1, '', '149', '', '2022-05-02', 2, 'txt12345abc', '', 0, '', 0, 0, '2022-05-02 12:22:19'),
(7, 3, 0, 0, 3, 2, '', '149', '', '2022-05-02', 2, 'txt50345pqr', '', 0, '', 0, 0, '2022-05-02 12:31:58'),
(10, 11, 4, 2, 1, 0, '3999', '3899', '200', '2022-05-05', 2, '23csx1vst1237dsa', '', 0, '', 0, 0, '2022-05-05 09:52:34'),
(12, 14, 4, 2, 1, 0, '3999', '3999', '100', '2022-05-05', 2, 'e23csx1vst1237dsa', '', 0, '', 1, 0, '2022-05-05 11:25:02'),
(13, 14, 4, 1, 1, 0, '2999', '2950', '49', '2022-05-05', 2, 'e23csx1vst1237dsa', '', 0, '', 1, 0, '2022-05-05 11:33:24'),
(14, 14, 4, 1, 1, 0, '2999', '2950', '49', '2022-05-05', 2, 'e23csx1vst1237dsa', '', 0, '', 1, 0, '2022-05-05 11:43:15'),
(15, 14, 4, 1, 1, 0, '2999', '2950', '49', '2022-05-05', 2, 'e23csx1vst1237dsa', '', 0, '', 1, 0, '2022-05-05 11:43:18'),
(16, 14, 4, 1, 1, 0, '2999', '2999', '00', '2022-05-05', 2, 'e23csx1vst1237dsa', '', 0, '', 1, 0, '2022-05-05 11:44:32'),
(21, 2, 0, 1, 1, 0, '0', '200', '30', '2022-05-11', 2, '12345abcdef', '', 0, '', 1, 1, '2022-05-11 07:04:33'),
(22, 2, 0, 3, 1, 0, '4999', '4999', '00', '2022-05-11', 2, 'e23csx1vst1237dsa', '', 0, '', 1, 1, '2022-05-16 12:03:31'),
(23, 2, 0, 0, 3, 8, '', '149', '', '2022-06-06', 2, 'tx213000', '', 0, '', 0, 0, '2022-06-06 09:45:56'),
(28, 2, 0, 0, 3, 17, '', '149', '', '2022-06-15', 2, 'dummy_123', '', 0, '', 0, 0, '2022-06-15 06:25:07'),
(29, 2, 0, 0, 3, 18, '', '149', '', '2022-06-15', 2, 'pay_JhkBggXkzWqZ2P', '', 1, '', 0, 0, '2022-06-15 10:52:10'),
(30, 2, 0, 0, 3, 20, '', '149', '', '2022-06-15', 2, 'pay_JhkElKoiiQRvcI', 'order_JhkEfRj55YVJd8', 1, '', 0, 0, '2022-06-15 10:56:22'),
(39, 2, 0, 2, 1, 0, '', '3999', '', '2022-06-16', 2, 'pay_JiAi18itN7gfnf', 'order_JiAhtnDQL0Tbwg', 1, 'upi', 1, 0, '2022-06-16 12:48:48'),
(40, 6, 0, 2, 1, 0, '', '3599.1', '', '2022-06-16', 2, 'pay_JiAiyOzXQuLjKo', 'order_JiAisLNEgpigYK', 1, 'upi', 1, 0, '2022-06-16 12:49:43'),
(41, 2, 0, 1, 1, 0, '0', '280', '120', '2022-06-17', 2, 'txt123', '', 0, '', 1, 1, '2022-06-17 08:49:41'),
(43, 6, 0, 6, 2, 0, '0', '180', '', '2022-06-20', 2, 'pay_JjiGhyKGXLB0vd', 'order_JjiGXf8VpfIH3Y', 1, 'upi', 1, 0, '2022-06-20 10:17:33'),
(44, 6, 0, 2, 2, 0, '0', '500', '', '2022-06-20', 2, 'pay_Jjj1laScmWglol', 'order_Jjj1gmXwdALLgE', 1, 'upi', 1, 0, '2022-06-20 11:02:06');

-- --------------------------------------------------------

--
-- Table structure for table `school_management`
--

CREATE TABLE `school_management` (
  `school_id` int(11) NOT NULL,
  `school_college_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `referel_code` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `alt_mobile_no` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `upload_file` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:- Deactive, 1:-Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `school_management_course`
--

CREATE TABLE `school_management_course` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `nos_of_students` varchar(255) NOT NULL,
  `flat_off` varchar(255) NOT NULL,
  `final_course_price` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_management_course`
--

INSERT INTO `school_management_course` (`id`, `uid`, `category_id`, `course_id`, `nos_of_students`, `flat_off`, `final_course_price`, `status`, `created_at`) VALUES
(2, 4, 1, 1, '10', '5', '', 1, '2022-04-27 08:41:12'),
(6, 4, 2, 2, '20', '10', '3599.1', 1, '2022-05-05 04:45:32'),
(8, 3, 2, 2, '15', '10', '3599.1', 1, '2022-06-16 12:10:32'),
(9, 3, 1, 1, '10', '10', '2699.1', 1, '2022-06-16 12:10:57');

-- --------------------------------------------------------

--
-- Table structure for table `spending_time_on_app`
--

CREATE TABLE `spending_time_on_app` (
  `time_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `spend_time` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `spending_time_on_app`
--

INSERT INTO `spending_time_on_app` (`time_id`, `uid`, `spend_time`, `created_at`) VALUES
(2, 2, 6, '2022-05-23 10:52:31'),
(3, 6, 5, '2022-05-23 10:55:29'),
(33, 14, 2, '2022-04-30 16:57:09'),
(34, 14, 2, '2022-05-21 16:58:47'),
(35, 14, 2, '2022-05-23 16:58:56'),
(38, 15, 2, '2022-05-23 17:13:38'),
(39, 15, 1, '2022-05-31 10:05:05'),
(40, 14, 2, '2022-05-31 10:06:38'),
(41, 2, 1, '2022-05-31 10:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `student_course`
--

CREATE TABLE `student_course` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `school_college_name` varchar(255) NOT NULL,
  `referal_code` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:- Deactive, 1:-Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_work_slider`
--

CREATE TABLE `student_work_slider` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_work_slider`
--

INSERT INTO `student_work_slider` (`id`, `name`, `images`, `created_at`, `updated_at`) VALUES
(3, 'mahadev 2', 'http://localhost/aartoon/dist/img/student_work_slider/1.webp', '2022-05-26 11:20:17', '0000-00-00 00:00:00'),
(4, 'Aartoon School ', 'http://localhost/aartoon/dist/img/student_work_slider/2.webp', '2022-05-26 11:39:26', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `support_management`
--

CREATE TABLE `support_management` (
  `support_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `class_date` date NOT NULL,
  `class_time` time NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT 0 COMMENT '0:-pending,2:-confirm; 1:-paid',
  `payment_amount` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `send_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support_management`
--

INSERT INTO `support_management` (`support_id`, `uid`, `course_id`, `class_date`, `class_time`, `payment_status`, `payment_amount`, `transaction_id`, `payment_type`, `description`, `send_date`, `created_at`) VALUES
(8, 2, 1, '2022-05-11', '10:00:00', 1, '280', 'txt123', '', 'fancy new espresso machine to your weekend guests, so they\'ll know how to use it. Demo is short for demonstrate or demonstration. It can be a verb, as when a tech company demos its new tablet or laptop.', '2022-05-11', '2022-05-11 06:06:09'),
(9, 6, 2, '2022-06-17', '15:33:00', 1, '500', 'pay_Jjj1laScmWglol', '', 'ios class required', '2022-06-17', '2022-06-17 09:04:16'),
(10, 6, 6, '2022-06-18', '10:00:00', 1, '180', 'pay_JjiGhyKGXLB0vd', '', 'PCB class required', '2022-06-17', '2022-06-17 09:56:54'),
(11, 6, 3, '2022-06-21', '10:00:00', 1, '199', 'pay_Jk8hV5uyEaNjGW', '', 'new class for 3D', '2022-06-21', '2022-06-21 11:40:57'),
(12, 2, 3, '2022-06-24', '11:00:00', 1, '49', 'pay_JmSyol28wX19ND', '', 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '2022-06-23', '2022-06-23 11:09:51'),
(25, 61, 2, '2022-06-29', '05:30:00', 1, '170', '-', 'wallet amount', 'Descriptive details allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '2022-06-29', '2022-06-29 07:30:34'),
(27, 60, 2, '2022-07-01', '02:30:00', 2, '10', '-', 'wallet amount', 'allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '2022-06-30', '2022-06-30 11:16:41'),
(28, 60, 2, '2022-07-01', '02:30:00', 2, '51', '-', 'wallet amount', 'allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '2022-06-30', '2022-06-30 12:09:54'),
(29, 60, 2, '2022-08-16', '16:05:00', 2, '49', '-', 'wallet amount', 'allow sensory recreations of experiences, objects, or imaginings. In other words, description encourages a more concrete or sensory experience of a subject, one which allows the reader to transport himself or herself into a scene.', '2022-06-30', '2022-06-30 12:09:56');

-- --------------------------------------------------------

--
-- Table structure for table `terms_and_conditions`
--

CREATE TABLE `terms_and_conditions` (
  `id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1:-student, 2:-school, 3:-web',
  `terms_conditions` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms_and_conditions`
--

INSERT INTO `terms_and_conditions` (`id`, `user_type`, `terms_conditions`) VALUES
(1, 1, '<p>demo1</p>'),
(2, 2, '<p>demo2<br></p>'),
(3, 3, '<div>Terms of Use</div><div><br></div><div>Aartoon’s mission is to improve lives through learning. We enable anyone anywhere to create and share educational content (instructors) and to access that educational content to learn (students). We consider our marketplace model the best way to offer valuable educational content to our users. We need rules to keep our platform and services safe for you, us, and our student and instructor community. These Terms apply to all your activities on the Aartoon website, our APIs, and other related services (“Services”).</div><div>If you publish a course on the Aartoon platform, you must also agree to the Instructor Terms. We also provide details regarding our processing of personal data of our students and instructors in our Privacy Policy. If you are using Aartoon as part of your employer’s Aartoon Business learning and development program, you can consult our Aartoon Business Privacy Statement.</div><div>If you live in the United States or Canada, by agreeing to these Terms, you agree to resolve disputes with Aartoon through binding arbitration (with very limited exceptions, not in court), and you waive certain rights to participate in class actions, as detailed in the Dispute Resolution section.</div><div>Table of Contents</div><div>•<span style=\"white-space:pre\">	</span>1. Accounts</div><div>•<span style=\"white-space:pre\">	</span>2. Content Enrolment and Lifetime Access</div><div>•<span style=\"white-space:pre\">	</span>3. Content and Behaviour Rules</div><div>•<span style=\"white-space:pre\">	</span>4. Aartoon’s Rights to Content You Post</div><div>•<span style=\"white-space:pre\">	</span>5. Using Aartoon at Your Own Risk</div><div>•<span style=\"white-space:pre\">	</span>6. Aartoon’s Rights</div><div>•<span style=\"white-space:pre\">	</span>7. Vouchers</div><div>•<span style=\"white-space:pre\">	</span>8. Miscellaneous Legal Terms</div><div>•<span style=\"white-space:pre\">	</span>9. Dispute Resolution</div><div>•<span style=\"white-space:pre\">	</span>10. Updating These Terms</div><div>•<span style=\"white-space:pre\">	</span>11. How to Contact Us</div><div><br></div><div>1. Accounts</div><div>You need an account for most activities on our platform. Keep your password somewhere safe, because you’re responsible for all activity associated with your account. If you suspect someone else is using your account, let us know by contacting our Support Team. You must have reached the age of consent for online services in your country to use Aartoon.</div><div>You need an account for most activities on our platform, including to purchase and access content or to submit content for publication. When setting up and maintaining your account, you must provide and continue to provide accurate and complete information, including a valid email address. You have complete responsibility for your account and everything that happens on your account, including for any harm or damage (to us or anyone else) caused by someone using your account without your permission. This means you need to be careful with your password. You may not transfer your account to someone else or use someone else’s account. If you contact us to request access to an account, we will not grant you such access unless you can provide us with the information that we need to prove you are the owner of that account. In the event of the death of a user, the account of that user will be closed.</div><div>You may not share your account login credentials with anyone else. You are responsible for what happens with your account and Aartoon will not intervene in disputes between students or instructors who have shared account login credentials. You must notify us immediately upon learning that someone else may be using your account without your permission (or if you suspect any other breach of security) by contacting our Support Team. We may request some information from you to confirm that you are indeed the owner of your account.</div><div>Students and instructors must be at least 18 years of age to create an account on Aartoon and use the Services. If you are younger than 18 but above the required age for consent to use online services where you live (for example, 13 in the US or 16 in Ireland), you may not set up an account, but we encourage you to invite a parent or guardian to open an account and help you access content that is appropriate for you. If you are below this age of consent to use online services, you may not create a Aartoon account. If we discover that you have created an account that violates these rules, we will terminate your account. Under our Instructor Terms, you may be requested to verify your identity before you are authorized to submit content for publication on Aartoon.</div><div>You can terminate your account at any time by following the steps here. Check our Privacy Policy to see what happens when you terminate your account.</div><div>2. Content Enrolment and Lifetime Access</div><div>When you enrol in a course or other content, you get a license from us to view it via the Aartoon Services and no other use. Don’t try to transfer or resell content in any way. We generally grant you a lifetime access license, except when we must disable the content because of legal or policy reasons.</div><div>Under our Instructor Terms, when instructors publish content on Aartoon, they grant Aartoon a license to offer a license to the content to students. This means that we have the right to sublicense the content to enrolled students. As a student, when you enroll in a course or other content, whether it’s free or paid content, you are getting a license from Aartoon to view the content via the Aartoon platform and Services, and Aartoon is the licensor of record. Content is licensed, and not sold, to you. This license does not give you any right to resell the content in any manner (including by sharing account information with a purchaser or illegally downloading the content and sharing it on torrent sites).</div><div>In legal, more complete terms, Aartoon grants you (as a student) a limited, non-exclusive, non-transferable license to access and view the content for which you have paid all required fees, solely for your personal, non-commercial, educational purposes through the Services, in accordance with these Terms and any conditions or restrictions associated with the particular content or feature of our Services. All other uses are expressly prohibited. You may not reproduce, redistribute, transmit, assign, sell, broadcast, rent, share, lend, modify, adapt, edit, create derivative works of, sublicense, or otherwise transfer or use any content unless we give you explicit permission to do so in a written agreement signed by a Aartoon authorized representative. This also applies to content you can access via any of our APIs.</div><div>We generally give a lifetime access license to our students when they enroll in a course or other content. However, we reserve the right to revoke any license to access and use any content at any point in time in the event where we decide or are obligated to disable access to the content due to legal or policy reasons, for example, if the course or other content you enrolled in is the object of a copyright complaint, or if we determine it violates our Trust &amp; Safety Guidelines. This lifetime access license does not apply to enrollments via Subscription Plans or to add-on features and services associated with the course or other content you enroll in. For example, instructors may decide at any time to no longer provide teaching assistance or Q&amp;A services in association with the content. To be clear, the lifetime access is to the course content but not to the instructor.</div><div>Instructors may not grant licenses to their content to students directly, and any such direct license shall be null and void and a violation of these Terms.</div><div>3. Content and Behaviour Rules</div><div>You can only use Aartoon for lawful purposes. You’re responsible for all the content that you post on our platform. You should keep the reviews, questions, posts, courses and other content you upload in line with our Trust &amp; Safety Guidelines and the law, and respect the intellectual property rights of others. We can ban your account for repeated or major offenses. If you think someone is infringing your copyright on our platform, let us know.</div><div>You may not access or use the Services or create an account for unlawful purposes. Your use of the Services and behaviour on our platform must comply with applicable local or national laws or regulations of your country. You are solely responsible for the knowledge of and compliance with such laws and regulations that are applicable to you.</div><div>If you are a student, the Services enable you to ask questions to the instructors of courses or other content you are enrolled in, and to post reviews of content. For certain content, the instructor may invite you to submit content as “homework” or tests. Don’t post or submit anything that is not yours.</div><div>If you are an instructor, you can submit content for publication on the platform and you can also communicate with the students who have enrolled in your courses or other content. In both cases, you must abide by the law and respect the rights of others: you cannot post any course, question, answer, review or other content that violates applicable local or national laws or regulations of your country. You are solely responsible for any courses, content, and actions you post or take via the platform and Services and their consequences. Make sure you understand all the copyright restrictions set forth in the Instructor Terms before you submit any content for publication on Aartoon.</div><div>If we are put on notice that your course or content violates the law or the rights of others (for example, if it is established that it violates intellectual property or image rights of others, or is about an illegal act');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `qulification` varchar(255) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `images` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `name`, `qulification`, `description`, `images`, `created_at`, `updated_at`) VALUES
(2, 'mahadev School web', 'PhD', 'We’re here to help you start, run, and grow your business online. We’ve put together our best resources on how to create a website', 'http://localhost/aartoon/control_panel/dist/img/testimonial/maxresdefault.jpg', '2022-05-26 10:14:51', '2022-08-19 09:28:22'),
(3, 'web', 'ME 1', 'the very end of the url. This is optional, default is the image dimensions', 'http://localhost/aartoon/control_panel/dist/img/testimonial/lap.png', '2022-05-26 11:20:52', '2022-08-19 09:28:01'),
(5, 'mahadev', 'PhD', 'Custom text can be entered using a query string at the very end of the url. This is optional, default is the image dimensions', 'http://localhost/aartoon/control_panel/dist/img/testimonial/ezgif_com-gif-maker.gif', '2022-05-26 12:03:13', '2022-08-19 09:27:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `uid` int(11) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1:-admin, 2:-student 3:-School',
  `full_name` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_text` varchar(255) NOT NULL,
  `referel_code` varchar(255) NOT NULL,
  `wallet_amount` int(11) NOT NULL,
  `refer_by` int(11) NOT NULL,
  `leader_score` int(11) NOT NULL,
  `gcm_id` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `parent_mobile_no` varchar(255) NOT NULL,
  `mob_otp` int(11) NOT NULL,
  `mob_otp_verfied` int(11) NOT NULL,
  `email_otp` int(11) NOT NULL,
  `email_otp_verfied` int(11) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `student_school_name` varchar(255) NOT NULL,
  `flat_off` varchar(255) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `cover_photo` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `pincode` int(11) NOT NULL,
  `user_bio` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0:-Deactive, 1:-Active',
  `is_subscribe` int(11) NOT NULL DEFAULT 0 COMMENT '0;-No, 1:-Yes',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`uid`, `user_type`, `full_name`, `user_name`, `email`, `password`, `password_text`, `referel_code`, `wallet_amount`, `refer_by`, `leader_score`, `gcm_id`, `mobile_no`, `parent_mobile_no`, `mob_otp`, `mob_otp_verfied`, `email_otp`, `email_otp_verfied`, `dob`, `gender`, `student_school_name`, `flat_off`, `branch_id`, `photo`, `cover_photo`, `adress`, `area`, `city`, `pincode`, `user_bio`, `status`, `is_subscribe`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mahade Admin', 'admin', 'mahadev_admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'admin', '', 400, 0, 0, '', '7775915269', '', 0, 1, 0, 1, '1989-12-05', 'Male', '', '', 0, 'http://localhost/aartoon/control_panel/dist/img/profile/proo.jpg', '', 'Pune', '', '', 411014, '', 1, 0, '2022-04-27 05:00:10', '2022-04-27 06:56:52'),
(2, 2, 'Mahadev stu', '', 'mahadev21@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '1234', 'MAH6vx8fpyp', 100, 100, 4, 'wwedcfedw3231', '8877665542', '8889789999', 3323, 1, 0, 1, '0000-00-00', 'Male', 'S P Patil High School, Satara', '', 1, 'http://localhost/aartoon/assets/images/proo1.jpg', 'http://localhost/aartoon/assets/images/13787931.jpg', '', '', '', 0, 'think twice,code once', 1, 1, '2022-04-27 05:02:10', '0000-00-00 00:00:00'),
(3, 3, 'PCB School', '', 'pcbSchool@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'PCBiksjtrpp', 460, 0, 0, '', '8421111821', '', 0, 0, 0, 0, '0000-00-00', '', '', '00', 0, 'http://localhost/aartoon/control_panel/dist/img/profile/dummy.png', '', 'Pune', '', '', 123456, '', 1, 0, '2022-04-27 05:28:23', '0000-00-00 00:00:00'),
(4, 3, 'Bishop School', '', 'Bishopschool@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'BISo11ra37c', 790, 0, 0, 'jvjhcvdjdhdnnc1234', '9876543220', '', 0, 1, 0, 1, '0000-00-00', '', '', '10', 0, 'http://localhost/aartoon/control_panel/dist/img/profile/du.png', '', 'Pune', '', '', 123456, '', 1, 0, '2022-04-27 08:39:40', '0000-00-00 00:00:00'),
(6, 2, 'mahadev stu_pcb', '', 'mahadev_stu_pcb@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'MAHhoptc88m', 100, 3, 0, '', '8877665509', '8811189999', 0, 1, 0, 1, '0000-00-00', '', 'PCB School, Satara', '', 1, '', '', '', '', '', 0, '', 1, 1, '2022-04-27 09:23:40', '0000-00-00 00:00:00'),
(11, 2, '', '', 'mahadev_stu1@gmail.com', '19918b29fe1c625ce856529cf8c33735', '60085', 'MAHhdauxbem', 0, 4, 0, '413ewf44rgg', '8837665511', '', 0, 0, 0, 0, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 1, '2022-05-05 08:54:43', '0000-00-00 00:00:00'),
(13, 2, '', '', 'mahadev_stu3@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'MAHq9vdxb3i', 200, 4, 0, '413ewf44rgg', '8837665500', '', 0, 0, 0, 0, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-05-05 11:14:32', '0000-00-00 00:00:00'),
(14, 2, 'M R Ingawale', '', 'mahadev_stu4@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'MAHqngc8sho', 0, 4, 0, '413ewf44rgg', '8837665504', '9876567898', 0, 0, 0, 0, '0000-00-00', 'Male', '', '', 0, 'http://localhost/aartoon/control_panel/dist/img/profile/proo.jpg', '', 'A/P Pune', '', '', 0, '', 1, 1, '2022-05-05 11:15:58', '0000-00-00 00:00:00'),
(15, 2, '', '', 'mahadev_stu5@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'MAHa5uotjqx', 200, 4, 0, '413ewf44rgg', '8537665504', '', 0, 0, 0, 0, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-05-05 11:55:56', '0000-00-00 00:00:00'),
(39, 2, '', '', 'ingawale@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', '1', 'INGilyt4wl7', 0, 0, 0, '', '1234567890', '', 0, 0, 5401, 0, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-05-15 05:47:58', '0000-00-00 00:00:00'),
(46, 2, 'mahadevSchool_web', '', 'mahadevSchool_web@gmail.coma', '202cb962ac59075b964b07152d234b70', '123', 'MAHbmgsn6r1', 0, 0, 0, '', '8421111221', '9898989999', 0, 0, 5988, 0, '0000-00-00', '', 'School_web', '', 1, '', '', '', '', '', 0, '', 1, 0, '2022-05-15 07:58:35', '0000-00-00 00:00:00'),
(60, 2, 'mahadevTest', '', 'mahadevTest@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'MAH99kxseaa', 41, 3, 0, '', '9876540987', '9898989999', 1411, 1, 3195, 1, '0000-00-00', 'Male', 'mahadevTest@gmail.com', '', 0, 'http://localhost/aartoon/control_panel/dist/img/profile/proo.jpg', '', 'Satara', '', '', 0, '', 1, 1, '2022-05-21 11:33:58', '0000-00-00 00:00:00'),
(61, 2, 'mahadevTest1', '', 'mahadevTst1@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'MAHjbt6xued', 0, 3, 0, '', '9876549870', '9898989999', 1471, 1, 6378, 1, '0000-00-00', '', 'ABC School', '', 1, '', '', '', '', '', 0, '', 1, 0, '2022-05-21 11:36:18', '0000-00-00 00:00:00'),
(63, 2, '', '', 'mahadev11@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '1234', 'MAH8wx00jqd', 0, 0, 0, '', '8423456821', '', 5644, 0, 7426, 1, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-06-01 06:55:03', '0000-00-00 00:00:00'),
(64, 2, '', '', 'mahadev123@gmail.com', '698d51a19d8a121ce581499d7b701668', '111', 'MAHe1jkhpb3', 0, 0, 0, '', '8421111811', '', 8611, 1, 8251, 1, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-06-24 10:29:43', '0000-00-00 00:00:00'),
(66, 2, '', '', 'harshszende@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', '1', 'HARm48hur9h', 0, 0, 0, '', '8600137050', '', 1493, 0, 7341, 1, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-06-27 05:16:29', '0000-00-00 00:00:00'),
(68, 2, '', '', 'mahaev_stu@gmail.com', '202cb962ac59075b964b07152d234b70', '123', 'MAHox0xdaq0', 200, 4, 0, '', '9876500220', '', 1484, 0, 1407, 1, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-06-27 12:19:38', '0000-00-00 00:00:00'),
(69, 2, '', '', 'admin12@yahoo.com', '202cb962ac59075b964b07152d234b70', '123', 'ADMov9r65ki', 0, 0, 0, '', '9876543299', '', 1045, 0, 7822, 1, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-06-29 10:52:55', '0000-00-00 00:00:00'),
(73, 2, '', '', 'mahadevTest_otp@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', '1', 'MAHp8dczscm', 0, 0, 0, '', '9899887766', '', 1346, 0, 1799, 1, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-07-01 09:37:52', '0000-00-00 00:00:00'),
(74, 2, '', '', 'mahadevTest_email@gmail.com', '3e94953170a3f384526ef42fe8a07115', '60361', 'MAHgg464977', 0, 0, 0, '', '9898767898', '', 1614, 0, 8129, 0, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-07-01 10:24:12', '0000-00-00 00:00:00'),
(87, 2, '', '', 'ingawalem222@gmail.com', '698d51a19d8a121ce581499d7b701668', '111', 'ING1jasixcl', 0, 0, 0, '', '9970410123', '', 0, 0, 7146, 0, '0000-00-00', '', '', '', 0, '', '', '', '', '', 0, '', 1, 0, '2022-09-27 09:46:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_portfolio`
--

CREATE TABLE `user_portfolio` (
  `user_p_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `school_uid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `file_document` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_portfolio`
--

INSERT INTO `user_portfolio` (`user_p_id`, `uid`, `school_uid`, `title`, `description`, `file_document`, `created_at`) VALUES
(1, 6, 0, 'Batman Game 2', 'Buildings and\ncreadted characters in Zbrush and Unity for\ndevelopment', 'http://localhost/aartoon/dist/img/uploaded_images/2353b90f6ae179ce3b70ac7226f3a2ad.jpg', '2022-04-28 11:10:57'),
(3, 6, 3, 'Batman Game 2', 'and\r\ncreadted characters in Zbrush and Unity for\r\ndevelopment', 'http://localhost/aartoon_website/assets/images/uploaded_images/3d.jpg', '2022-04-28 11:14:44'),
(5, 6, 0, 'demos', 'xharacters in Zbrush and Unity for\ndevelopment', 'http://localhost/aartoon/dist/img/uploaded_images/9a6b2f0643c381fa22b9ec9c7e51aaf2.jpg', '2022-05-04 04:52:06'),
(9, 2, 0, 'demo1', 'demo1', 'http://localhost/aartoon_website/assets/images/uploaded_images/1378793.jpg', '2022-06-07 07:07:36'),
(12, 2, 0, 'new animation course1', 'new animation course development1', 'http://localhost/aartoon_website/assets/images/uploaded_images/c1.jpg', '2022-06-22 06:06:18'),
(16, 2, 0, 'new animation course', 'new animation course development', 'http://localhost/aartoon/dist/img/uploaded_images/c2ba234963c919686fbf0bb5a29eb3d2.jpg', '2022-06-23 10:19:49'),
(17, 2, 0, 'new animation course', 'new animation course development', 'http://localhost/aartoon_website/assets/images/uploaded_images/Career-Pathing-Learning-Feature.jpg', '2022-06-23 10:20:06'),
(18, 2, 0, 'new animation course', 'new animation course development', 'http://localhost/aartoon/dist/img/uploaded_images/c2f5c2bd0453cc8ff12f02cda0c7d2fe.jpg', '2022-06-23 10:20:17'),
(19, 2, 0, '1', '1', 'http://localhost/aartoon_website/assets/images/uploaded_images/12.jpg', '2022-06-24 07:09:47'),
(20, 2, 0, '1', '1', 'http://localhost/aartoon_website/assets/images/uploaded_images/download.jpg', '2022-06-27 10:31:05'),
(21, 60, 0, 'testing transaction', 'demo', 'http://localhost/aartoon/assets/images/uploaded_images/du.png', '2022-06-28 06:05:02'),
(22, 60, 0, 'slider image portfolio', 'demo desc', 'http://localhost/aartoon/assets/images/uploaded_images/ezgif_com-gif-maker.gif', '2022-06-28 06:09:36');

-- --------------------------------------------------------

--
-- Table structure for table `user_portfolio_review`
--

CREATE TABLE `user_portfolio_review` (
  `user_p_re_id` int(11) NOT NULL,
  `user_p_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '1:-approve,2:-reject',
  `payment_status` int(11) NOT NULL COMMENT '0:-Fail, 1:-Success',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_portfolio_review`
--

INSERT INTO `user_portfolio_review` (`user_p_re_id`, `user_p_id`, `uid`, `status`, `payment_status`, `created_at`) VALUES
(3, 3, 6, 2, 0, '2022-05-04 04:28:05'),
(4, 5, 6, 1, 0, '2022-05-04 04:52:34'),
(17, 9, 2, 0, 0, '2022-06-15 06:25:07'),
(18, 9, 2, 0, 1, '2022-06-15 10:52:10'),
(19, 11, 2, 0, 1, '2022-06-15 10:55:04'),
(20, 11, 2, 0, 1, '2022-06-15 10:56:22'),
(21, 5, 6, 0, 1, '2022-06-21 12:38:21'),
(22, 1, 6, 0, 1, '2022-06-21 12:47:12'),
(23, 6, 6, 0, 1, '2022-06-21 12:50:55'),
(24, 5, 6, 0, 1, '2022-06-21 12:57:18'),
(25, 5, 6, 0, 1, '2022-06-21 12:57:35'),
(26, 11, 2, 0, 1, '2022-06-22 06:02:52'),
(27, 1, 6, 0, 1, '2022-06-22 06:30:55'),
(28, 3, 6, 0, 1, '2022-06-22 10:35:32'),
(29, 17, 2, 0, 1, '2022-06-23 17:20:37'),
(30, 18, 2, 0, 1, '2022-06-23 17:35:32'),
(31, 19, 2, 0, 1, '2022-06-24 07:12:19'),
(32, 16, 2, 0, 1, '2022-06-27 09:52:03'),
(33, 21, 60, 1, 1, '2022-06-28 06:05:19'),
(34, 22, 60, 1, 1, '2022-06-28 06:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT 2,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `video_link`, `description`, `type`, `created_at`) VALUES
(1, 'Manufacture and PCB Assembly', 'https://www.youtube.com/watch?v=o8NOK1JJbgw', '<p><ytd-thumbnail use-hovered-property=\"\" class=\"style-scope ytd-video-renderer\" style=\"display: block; position: relative; flex: 1 1 1e-09px; margin-right: 16px; max-width: 360px; min-width: 240px; text-decoration-thickness: initial; text-decoration-styl', 2, '2022-04-27 05:25:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about_us_for_web`
--
ALTER TABLE `about_us_for_web`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_banner_screen_images`
--
ALTER TABLE `app_banner_screen_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_home_screen_images`
--
ALTER TABLE `app_home_screen_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_login_screen_images`
--
ALTER TABLE `app_login_screen_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_review`
--
ALTER TABLE `app_review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `career_growth`
--
ALTER TABLE `career_growth`
  ADD PRIMARY KEY (`c_g_id`);

--
-- Indexes for table `career_growth_files`
--
ALTER TABLE `career_growth_files`
  ADD PRIMARY KEY (`c_g_f_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `communication_management`
--
ALTER TABLE `communication_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `course_features`
--
ALTER TABLE `course_features`
  ADD PRIMARY KEY (`course_feature_id`);

--
-- Indexes for table `course_learn_more`
--
ALTER TABLE `course_learn_more`
  ADD PRIMARY KEY (`course_learn_id`);

--
-- Indexes for table `course_played_video`
--
ALTER TABLE `course_played_video`
  ADD PRIMARY KEY (`c_p_v_id`);

--
-- Indexes for table `course_review`
--
ALTER TABLE `course_review`
  ADD PRIMARY KEY (`co_r_id`);

--
-- Indexes for table `course_video_management`
--
ALTER TABLE `course_video_management`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `course_video_manag_options`
--
ALTER TABLE `course_video_manag_options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `course_video_manag_questions`
--
ALTER TABLE `course_video_manag_questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `course_wishlist`
--
ALTER TABLE `course_wishlist`
  ADD PRIMARY KEY (`wishlist_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_work_slider`
--
ALTER TABLE `home_work_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leader_board_score`
--
ALTER TABLE `leader_board_score`
  ADD PRIMARY KEY (`l_b_s_id`);

--
-- Indexes for table `live_class_amount`
--
ALTER TABLE `live_class_amount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_class_details`
--
ALTER TABLE `live_class_details`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `live_class_subscription_plans`
--
ALTER TABLE `live_class_subscription_plans`
  ADD PRIMARY KEY (`subs_id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `module_item`
--
ALTER TABLE `module_item`
  ADD PRIMARY KEY (`module_item_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_post_review_amount`
--
ALTER TABLE `portfolio_post_review_amount`
  ADD PRIMARY KEY (`p_p_r_id`);

--
-- Indexes for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referal_code`
--
ALTER TABLE `referal_code`
  ADD PRIMARY KEY (`ref_code_id`);

--
-- Indexes for table `referred_code`
--
ALTER TABLE `referred_code`
  ADD PRIMARY KEY (`reff_code_id`);

--
-- Indexes for table `refund_policy`
--
ALTER TABLE `refund_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revenue_management`
--
ALTER TABLE `revenue_management`
  ADD PRIMARY KEY (`revenue_id`);

--
-- Indexes for table `revenue_management_backup`
--
ALTER TABLE `revenue_management_backup`
  ADD PRIMARY KEY (`revenue_id`);

--
-- Indexes for table `school_management_course`
--
ALTER TABLE `school_management_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spending_time_on_app`
--
ALTER TABLE `spending_time_on_app`
  ADD PRIMARY KEY (`time_id`);

--
-- Indexes for table `student_course`
--
ALTER TABLE `student_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_work_slider`
--
ALTER TABLE `student_work_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_management`
--
ALTER TABLE `support_management`
  ADD PRIMARY KEY (`support_id`);

--
-- Indexes for table `terms_and_conditions`
--
ALTER TABLE `terms_and_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `user_portfolio`
--
ALTER TABLE `user_portfolio`
  ADD PRIMARY KEY (`user_p_id`);

--
-- Indexes for table `user_portfolio_review`
--
ALTER TABLE `user_portfolio_review`
  ADD PRIMARY KEY (`user_p_re_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `about_us_for_web`
--
ALTER TABLE `about_us_for_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `app_banner_screen_images`
--
ALTER TABLE `app_banner_screen_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `app_home_screen_images`
--
ALTER TABLE `app_home_screen_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `app_login_screen_images`
--
ALTER TABLE `app_login_screen_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `app_review`
--
ALTER TABLE `app_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `career_growth`
--
ALTER TABLE `career_growth`
  MODIFY `c_g_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `career_growth_files`
--
ALTER TABLE `career_growth_files`
  MODIFY `c_g_f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `communication_management`
--
ALTER TABLE `communication_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `course_features`
--
ALTER TABLE `course_features`
  MODIFY `course_feature_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course_learn_more`
--
ALTER TABLE `course_learn_more`
  MODIFY `course_learn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `course_played_video`
--
ALTER TABLE `course_played_video`
  MODIFY `c_p_v_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `course_review`
--
ALTER TABLE `course_review`
  MODIFY `co_r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `course_video_management`
--
ALTER TABLE `course_video_management`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `course_video_manag_options`
--
ALTER TABLE `course_video_manag_options`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `course_video_manag_questions`
--
ALTER TABLE `course_video_manag_questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `course_wishlist`
--
ALTER TABLE `course_wishlist`
  MODIFY `wishlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `home_work_slider`
--
ALTER TABLE `home_work_slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `leader_board_score`
--
ALTER TABLE `leader_board_score`
  MODIFY `l_b_s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `live_class_amount`
--
ALTER TABLE `live_class_amount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `live_class_details`
--
ALTER TABLE `live_class_details`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `live_class_subscription_plans`
--
ALTER TABLE `live_class_subscription_plans`
  MODIFY `subs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `module_item`
--
ALTER TABLE `module_item`
  MODIFY `module_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `portfolio_post_review_amount`
--
ALTER TABLE `portfolio_post_review_amount`
  MODIFY `p_p_r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `referal_code`
--
ALTER TABLE `referal_code`
  MODIFY `ref_code_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referred_code`
--
ALTER TABLE `referred_code`
  MODIFY `reff_code_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `refund_policy`
--
ALTER TABLE `refund_policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `revenue_management`
--
ALTER TABLE `revenue_management`
  MODIFY `revenue_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `revenue_management_backup`
--
ALTER TABLE `revenue_management_backup`
  MODIFY `revenue_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `school_management_course`
--
ALTER TABLE `school_management_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `spending_time_on_app`
--
ALTER TABLE `spending_time_on_app`
  MODIFY `time_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `student_course`
--
ALTER TABLE `student_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_work_slider`
--
ALTER TABLE `student_work_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `support_management`
--
ALTER TABLE `support_management`
  MODIFY `support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `terms_and_conditions`
--
ALTER TABLE `terms_and_conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `user_portfolio`
--
ALTER TABLE `user_portfolio`
  MODIFY `user_p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_portfolio_review`
--
ALTER TABLE `user_portfolio_review`
  MODIFY `user_p_re_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
